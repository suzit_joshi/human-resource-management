<!--MODAL to choose curriculum vitae for users-->
<div class="modal fade" id="cv-sample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Select CV Sample</h4> </div>
            <form class="form-horizontal" role="form" action="<?=base_url()?>candidate/viewPdf" method="post" target="_blank">
                <div class="modal-body">
                    <input type="hidden" id="candidate-id-for-cv" name="random">
                    <div class="form-group">
                        <label class="col-sm-4">Select Sample</label>
                        <div class="col-sm-8">
                            <select name="cv_sample" class="form-control">
                                <option value="1">Sample 1</option>
                                <option value="2">Sample 2</option>
                                <option value="3">Sample 3</option>
                                <option value="4">Sample 4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            <!--END FORM-->

        </div>
    </div>
</div>
<!--END MODAL TO choose curriculum vitae for users-->