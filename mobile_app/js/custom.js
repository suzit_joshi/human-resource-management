$('.panel .btn').on('click',function(e){
	e.preventDefault();

	var transactionNo = $(this).attr('transaction_no');
	$('#transaction_no').val(transactionNo);
	$('#remarksModal .btn').show();
	if($(this).hasClass('approve')){
		$('#remarksModal').find('.modal-title').text('Confirm Approval?');
		$('#remarksModal').find('.decline').hide();
	}else{
		$('#remarksModal').find('.modal-title').text('Confirm Decline?');
		$('#remarksModal').find('.approve').hide();
	}
	
	$('#remarksModal').modal();
});