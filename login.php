<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

    //@author suzit
    //@params none
    //@returns none

    public function index() {
        $email = mysql_real_escape_string($this->input->post('email'));
        $password = mysql_real_escape_string($this->input->post('password'));

        $salt = "wdvhyf@%$@fgfrtyqq";
        $salt .= $password;
        $password = $salt;
        $password = md5($password);
        //defining the conditions
        $where_array = array('email_id' => $email, 'password' => $password);
        //method chaining
        $query_user = $this->db->select('*')->from('tbl_user')->where($where_array)->join('tbl_role','tbl_user.role_id = tbl_role.role_id')->get();

        if($query_user->num_rows() > 0) {
            foreach($query_user->result() as $rowUser) {
                if ($password == $rowUser->password) {
                    $result = $query_user->row();

                    $this->session->set_userdata('user_id', $result->user_id);
                    $this->session->set_userdata('role', $result->role_name);
                   	$this->session->set_userdata('email',$email);
					$redirect = base_url()."admin";
                    redirect($redirect);
                }
            }
        } else {
            $this->session->set_userdata('Errormsg', 'Incorrect username and password');
            $redirect = base_url()."_cpanel";
            redirect($redirect);
        }
    }

}