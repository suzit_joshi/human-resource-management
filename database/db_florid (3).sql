-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2015 at 11:02 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_florid`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advertisement`
--

CREATE TABLE IF NOT EXISTS `tbl_advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `released_date` date NOT NULL,
  `paper` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_advertisement`
--

INSERT INTO `tbl_advertisement` (`id`, `job_id`, `released_date`, `paper`, `size`, `page_no`, `updated_on`, `image`) VALUES
(1, 1, '2015-03-01', 'Himalayan Times', '10', '10', '2015-03-09 11:48:48', 'ojxA7AkJ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent`
--

CREATE TABLE IF NOT EXISTS `tbl_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` int(20) NOT NULL,
  `phone` int(20) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perm_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `temp_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regd_date` date NOT NULL,
  `dob` date NOT NULL,
  `status` int(1) NOT NULL,
  `approved_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved_date` date NOT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_agent`
--

INSERT INTO `tbl_agent` (`id`, `agent_name`, `email`, `mobile`, `phone`, `image`, `perm_address`, `temp_address`, `regd_date`, `dob`, `status`, `approved_by`, `approved_date`, `notification_id`) VALUES
(1, 'Ram Bahadur', 'ram@gmail.com', 21819289, 89898989, '6.jpg', 'Syangja', 'Kathmandu', '2015-02-18', '1970-01-01', 1, '', '0000-00-00', 1),
(2, 'shyam bahadur', 'shyam@gmail.com', 123123, 98798798, 'prXf12YN.jpg', 'sadlfk', 'lkjlk', '2015-02-20', '2015-02-10', 1, '', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_agent_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `sent_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_agent_notification`
--

INSERT INTO `tbl_agent_notification` (`id`, `agent_id`, `notification_date`, `status`, `user_id`, `remarks`, `sent_user`) VALUES
(1, 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', '2015-03-12', 0, 0, 'abcdef', 'admin_user');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bank`
--

CREATE TABLE IF NOT EXISTS `tbl_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_bank`
--

INSERT INTO `tbl_bank` (`id`, `bank_name`, `balance`, `updated_date`) VALUES
(1, 'Nepal Investment Bank', 19000, '2015-02-20 07:20:51'),
(2, 'Nabil Bank', 18000, '2015-02-20 07:57:25'),
(3, 'Everest Bank', 15000, '2015-02-20 07:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate`
--

CREATE TABLE IF NOT EXISTS `tbl_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mother_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_id` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_address` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perm_address` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_no` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `issued_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `issued_from` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualification` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `training` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_candidate`
--

INSERT INTO `tbl_candidate` (`id`, `candidate_name`, `dob`, `gender`, `height`, `weight`, `father_name`, `mother_name`, `email_id`, `phone`, `mobile`, `temp_address`, `perm_address`, `birth_place`, `religion`, `marital_status`, `nationality`, `passport_no`, `issued_date`, `expire_date`, `issued_from`, `qualification`, `language`, `experience`, `training`, `user_id`, `agent_id`, `updated_on`) VALUES
(1, 'John Wick', '2015-02-25', 'Male', '23', '57', 'qwe', 'Shree Krishna Shrestha Mother', '9849276774', '54665487', '9776497898', 'Tinkune', 'New baneshwor', 'adsf', '0', 'single', 'sa', '8987987', '2015-02-03', '2015-02-25', 'asdasd', 'BSC qualified', 'a:1:{i:0;a:1:{s:7:"english";s:6:"Select";}}', 'asdf', '0', 3, 1, '2015-02-25 05:11:33'),
(2, 'Rajaram Phuyal', '2000-03-01', 'Male', '5.6', '70', 'Ram Phuyal', 'Mother Phuyal', 'rajaram@gmail.com', '26565565', '89898498', 'wxyz', 'ABCD', 'Lumbini', '0', 'single', 'Nepalese', '8987987', '2015-03-01', '2025-03-12', 'asdasd', 'BSC second year cleared', 'a:1:{i:0;a:1:{s:7:"english";s:4:"Good";}}', 'oiqueoiqueoi', '0', 3, 2, '2015-03-02 08:08:39'),
(3, 'Deepa pathak', '1996-03-12', 'Male', '5', '50', 'Deepak  Pathak', 'Deepti pathak', 'deepa1@hotmail.com', '1223', '456', 'Gaushala', 'Old baneshwor', '0', 'Hinduism', 'single', 'Nepalese', '8987987', '2015-03-05', '2015-03-02', 'asdasd', 'BBS cleared', 'a:4:{s:7:"english";s:1:"1";s:5:"hindi";s:1:"1";s:6:"nepali";s:1:"3";s:5:"other";b:0;}', ' 1 year', ' abcd', 3, 1, '2015-03-02 08:10:59'),
(4, 'Fayaz aahmed', '1972-02-09', 'Male', '6', '80', 'Shree Krishna Shrestha father', 'Shree Krishna Shrestha Mother', 'fayaz@gfmail.com', '54665487', '9776497898', 'Tinkune', 'New baneshwor', 'Nepal', '0', 'single', 'sa', '8987987', '2015-03-11', '2015-03-24', 'asdasd', 'BSC qualified', 'a:1:{i:0;a:1:{s:7:"english";s:4:"Good";}}', 'uoi', '0', 3, 2, '2015-03-02 08:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_feedback`
--

CREATE TABLE IF NOT EXISTS `tbl_candidate_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `feedback` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate_image`
--

CREATE TABLE IF NOT EXISTS `tbl_candidate_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `profile` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `office_form` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `academics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `training` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_candidate_image`
--

INSERT INTO `tbl_candidate_image` (`id`, `candidate_id`, `profile`, `passport`, `office_form`, `cv`, `academics`, `experience`, `training`, `other`) VALUES
(1, 1, 'NHMejXrM.jpg', 'DpqXc2Fa.jpg', 'f8vv4I5U.jpg', 'KzD76DJe.jpg', 'a:2:{i:0;s:12:"wEtNa8oS.jpg";i:1;s:12:"JXhpt1u3.jpg";}', '', 'a:2:{i:0;s:12:"zWKyxms2.png";i:1;s:12:"YX2XpIt8.png";}', ''),
(2, 2, 'lAi9IsMN.jpg', 'O3atmX54.jpg', '', '', '', '', 'a:2:{i:0;s:12:"sUJsUyPx.png";i:1;s:12:"KT95Elix.png";}', ''),
(3, 3, 'kJmaGxpu.jpg', 'YHJ2m7rk.png', 'lAiJWfEI.jpg', '', '', '', 'a:2:{i:0;s:12:"eQNG56Si.png";i:1;s:12:"tU00k9jc.png";}', ''),
(4, 4, 'imHlTGUc.jpg', 'WhJEePLW.png', '', '', '', '', 'a:2:{i:0;s:12:"clE9XKar.png";i:1;s:12:"ZKNYd8mB.png";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cash`
--

CREATE TABLE IF NOT EXISTS `tbl_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_cash`
--

INSERT INTO `tbl_cash` (`id`, `amount`, `updated_on`, `status`, `year`, `month`) VALUES
(1, 19044, '2015-03-03 07:56:54', 0, 2015, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_category_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_category_transaction`
--

INSERT INTO `tbl_category_transaction` (`id`, `category_title`, `type`, `subtype`, `updated_on`, `status`) VALUES
(1, 'Medical Income', 'income', 'Candidate', '2015-03-02 11:45:16', 0),
(2, 'Job Related Income', 'income', 'Candidate', '2015-03-02 11:59:38', 0),
(5, 'Expense 1', 'expense', 'Other', '2015-03-03 05:12:54', 0),
(6, 'Ticketing', 'expense', 'Candidate', '2015-03-03 05:38:48', 0),
(7, 'Stationary Expense', 'expense', 'Other', '2015-03-03 07:08:19', 0),
(8, 'Visa Expense', 'expense', 'Other', '2015-03-03 07:08:58', 0),
(9, 'Income 2', 'income', 'Other', '2015-03-03 07:16:11', 0),
(10, 'Income 3', 'income', 'Other', '2015-03-03 07:16:20', 0),
(11, 'Advance', 'income', 'Candidate', '2015-03-09 12:39:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `email_id` varchar(250) NOT NULL,
  `website` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`id`, `company_name`, `address`, `email_id`, `website`, `description`, `country_id`, `status`, `updated_on`) VALUES
(1, 'Drydocks Worlds', 'Dubai', 'drydockss@gmail.com', 'http://www.drydocks.com.uae', 'Information w<br>', 1, 1, '2015-02-16 05:52:25'),
(2, 'Test Company 1', 'Test Address', 'test@gmail.com', 'http://www.test.com', 'General Information<br>', 3, 1, '2015-02-16 05:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_branch`
--

CREATE TABLE IF NOT EXISTS `tbl_company_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_company_branch`
--

INSERT INTO `tbl_company_branch` (`id`, `branch_name`, `address`, `phone`, `updated_on`, `status`) VALUES
(1, 'Baneshwor Main Branch', 'Baneshwor', '23423424', '2015-02-16 11:21:51', 0),
(2, 'Dang Branch', 'Dang', '65489798', '2015-03-11 09:45:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE IF NOT EXISTS `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(250) NOT NULL,
  `status` int(5) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `country_name`, `status`, `updated_on`) VALUES
(1, 'Malaysia', 1, '2015-02-16 04:46:25'),
(2, 'Dubai', 1, '2015-02-16 04:44:39'),
(3, 'Singapore', 1, '2015-02-16 04:44:47'),
(4, 'China', 1, '2015-02-16 07:23:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courier`
--

CREATE TABLE IF NOT EXISTS `tbl_courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courier_via` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job_candidate_id` int(11) NOT NULL,
  `ref_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_general_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `receipent_role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `candidate_no` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `sender_role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen_by` int(11) NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_general_notification`
--

INSERT INTO `tbl_general_notification` (`id`, `sender`, `receipent_role`, `candidate_no`, `remarks`, `status`, `sender_role`, `seen_by`, `heading`) VALUES
(2, 'user_documentation', 'finance', 1, 'Medical passed list', 1, 'Documentation', 9, 'medical passed'),
(3, 'user_documentation', 'finance', 1, 'Medical passed list', 0, 'Documentation', 9, 'medical passed');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_interview`
--

CREATE TABLE IF NOT EXISTS `tbl_interview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `interview_date` date NOT NULL,
  `taken_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `result` int(1) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_interview`
--

INSERT INTO `tbl_interview` (`id`, `candidate_id`, `interview_date`, `taken_by`, `result`, `type`, `remarks`, `updated_on`) VALUES
(8, 1, '2015-02-04', 'Mr. Lanister', 1, 'Pre-interview', 'abcd', '2015-03-11 08:01:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

CREATE TABLE IF NOT EXISTS `tbl_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(250) NOT NULL,
  `vacancy_no` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `salary` varchar(250) NOT NULL,
  `period` varchar(250) NOT NULL,
  `job_type` varchar(250) NOT NULL,
  `demand_letter` varchar(250) NOT NULL,
  `pre_interview_date` date NOT NULL,
  `final_interview_date` date NOT NULL,
  `final_interviewer` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requirement` text NOT NULL,
  `facility` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`id`, `job_title`, `vacancy_no`, `company_id`, `salary`, `period`, `job_type`, `demand_letter`, `pre_interview_date`, `final_interview_date`, `final_interviewer`, `updated_on`, `requirement`, `facility`, `status`) VALUES
(1, 'HR Manager', 2, 1, '456 Real', '4 years', 'full_time', '', '2015-02-04', '2015-02-28', 'Mr. Rushell Peter', '2015-03-11 04:45:51', 'asfdasf', 'asdfasfds', 0),
(2, 'Car Washer', 20, 2, '500 Reals', '3 years', 'full_time', '', '2015-02-11', '2015-02-17', 'Mr. Pattrick Jones', '2015-03-11 04:45:18', 'asdff', 'qwer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_candidate`
--

CREATE TABLE IF NOT EXISTS `tbl_job_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `current_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_job_candidate`
--

INSERT INTO `tbl_job_candidate` (`id`, `job_id`, `candidate_id`, `date`, `current_status`, `remarks`, `type`) VALUES
(1, 1, 1, '2015-02-25', 'medical_passed', '', ''),
(2, 2, 2, '2015-03-02', 'New', '', ''),
(3, 1, 3, '2015-03-02', 'medical_passed', '', ''),
(4, 2, 4, '2015-03-02', 'New', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_rate`
--

CREATE TABLE IF NOT EXISTS `tbl_job_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office_rate` varchar(250) NOT NULL,
  `mol_rate` varchar(250) NOT NULL,
  `pre_approval_date` date NOT NULL,
  `final_approval_date` date NOT NULL,
  `pre_approval_file` varchar(250) NOT NULL,
  `final_approval_file` varchar(250) NOT NULL,
  `quota_filled` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approval_flag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `tbl_job_rate`
--

INSERT INTO `tbl_job_rate` (`id`, `office_rate`, `mol_rate`, `pre_approval_date`, `final_approval_date`, `pre_approval_file`, `final_approval_file`, `quota_filled`, `job_id`, `updated_on`, `approval_flag`) VALUES
(57, '3500', '25000', '2015-03-02', '2015-03-02', 'a:3:{i:0;s:12:"Gg5jzs2I.png";i:1;s:12:"tYRI6LfT.jpg";i:2;s:12:"7ELfHNNh.png";}', 'a:3:{i:0;s:12:"hcsGNbhV.png";i:1;s:12:"bDnvCAqo.png";i:2;s:12:"xBI5PFJj.png";}', 35, 24, '2015-03-02 11:20:04', 3),
(58, '25000', '20000', '0000-00-00', '0000-00-00', '', '', 0, 1, '2015-03-09 11:45:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_medical`
--

CREATE TABLE IF NOT EXISTS `tbl_medical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `medical_date` date NOT NULL,
  `medical_center` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `result` int(1) NOT NULL,
  `remarks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_medical`
--

INSERT INTO `tbl_medical` (`id`, `candidate_id`, `medical_date`, `medical_center`, `result`, `remarks`, `type`, `updated_on`) VALUES
(1, 2, '2014-09-01', 'Civil Hospital', 1, 'eeeee', '1st-Medical', '2015-03-05 04:44:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `notification_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `receipent_role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `received_user_id` int(11) NOT NULL,
  `sent_date` date NOT NULL,
  `sent_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reply_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orientation`
--

CREATE TABLE IF NOT EXISTS `tbl_orientation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `center` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_orientation`
--

INSERT INTO `tbl_orientation` (`id`, `candidate_id`, `date`, `center`, `remarks`, `updated_on`) VALUES
(1, 'a:2:{i:0;s:1:"3";i:1;s:1:"4";}', '2015-03-06', 'asdf', '', '2015-03-06 08:57:02'),
(2, 'a:2:{i:0;s:1:"3";i:1;s:1:"4";}', '2015-03-09', 'Florid Headquarter', 'owiejfoasoifjas', '2015-03-06 09:01:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `role_name`, `status`, `last_updated`) VALUES
(1, 'super admin', 1, '0000-00-00 00:00:00'),
(2, 'System Admin', 1, '0000-00-00 00:00:00'),
(3, 'General Admin', 1, '0000-00-00 00:00:00'),
(4, 'Documentation', 1, '0000-00-00 00:00:00'),
(5, 'Finance', 1, '0000-00-00 00:00:00'),
(6, 'MOL Admin', 1, '0000-00-00 00:00:00'),
(7, 'General user', 1, '0000-00-00 00:00:00'),
(8, 'Recruitment', 1, '0000-00-00 00:00:00'),
(9, 'admin', 1, '2015-03-10 06:14:18'),
(10, 'ticketing', 1, '2015-03-11 05:07:18'),
(11, 'visa processing', 1, '2015-03-11 05:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket`
--

CREATE TABLE IF NOT EXISTS `tbl_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `airlines` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flight_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baggage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure` datetime NOT NULL,
  `arrival` datetime NOT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure_flag` int(1) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_ticket`
--

INSERT INTO `tbl_ticket` (`id`, `ticket_no`, `airlines`, `flight_no`, `baggage`, `departure`, `arrival`, `origin`, `destination`, `departure_flag`, `candidate_id`) VALUES
(1, 'tkt33939', 'Turkish Airlines', 'FZ 574', '20 Kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Kathmandu', 'Dubai', 0, 1),
(2, 'tkt 333', 'Malaysian Airlines', 'F3930', '40 KG', '2015-03-06 00:00:00', '1970-01-01 00:00:00', 'Kathmandu', 'Malaysia', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `transaction_date` date NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cheque_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voucher_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `income_cheque_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`id`, `transaction_no`, `category_id`, `amount`, `transaction_date`, `type`, `subtype`, `cheque_no`, `voucher_no`, `income_cheque_no`, `bank_id`, `candidate_id`, `remarks`, `updated_on`) VALUES
(1, 'inc_1', 1, 2000, '2015-03-04', 'income', 'Cash', '', '', '', 0, 2, 'Medical Payment done by Mr.Rajaram Phuyal', '2015-03-04 05:13:46'),
(2, 'exp_1', 6, 6000, '2015-03-04', 'expense', 'Cash', '', '', '', 1, 2, 'Pre Ticket Expense', '2015-03-04 06:49:06'),
(7, 'inc_2', 2, 2000, '2015-03-04', 'income', 'Cash', '', '', '', 0, 4, 'abcd', '2015-03-04 09:10:56'),
(8, 'inc_3', 10, 5000, '2015-03-04', 'income', 'Cheque', '', '', '12345', 0, 0, 'qeqeq', '2015-03-04 09:11:23'),
(9, 'inc_4', 10, 8000, '2015-03-04', 'income', 'Deposit', '', '8792', '', 2, 0, '232eqadas', '2015-03-04 09:11:45'),
(10, 'inc_5', 1, 10000, '2015-03-04', 'income', 'Deposit', '', '8952121', '', 3, 2, 'abcdewqeq', '2015-03-04 09:12:19'),
(11, 'inc_6', 9, 44, '2015-03-09', 'income', 'Cash', '', '', '', 0, 0, '2323er', '2015-03-09 10:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `post` varchar(250) NOT NULL,
  `email_id` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `name`, `post`, `email_id`, `password`, `role_id`, `status`, `branch_id`, `updated_on`) VALUES
(1, 'sunbi', 'sunbi', 'sunbi', 'sunbi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 1, 1, 0, '2015-02-13 06:03:09'),
(5, 'admin_user', 'admin_user', 'Managing Director', 'admin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 2, 1, 1, '2015-03-10 06:24:29'),
(6, 'general_admin', 'general_admin', 'Director', 'general_admin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 3, 1, 1, '2015-03-12 10:29:49'),
(7, 'user_documentation', 'Gyanendra Malla', 'Manager', 'gyanendra@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 4, 1, 1, '2015-03-12 10:29:52'),
(8, 'admin_user2', 'sujan khadgi', 'sweeper', 'kahdgi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 9, 1, 1, '2015-03-11 09:55:59'),
(9, 'finance_user', 'Sanjan Piya', 'ios developer', 'piya@sanjan.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 5, 1, 1, '2015-03-11 09:54:47'),
(10, 'mol_admin', 'Sudeep tuladhar', 'android developer', 'tuladhar@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 6, 1, 1, '2015-03-11 09:55:01'),
(11, 'general_user', 'sujit pathak', 'manager', 'pathak@sujit.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 7, 1, 1, '2015-03-11 09:55:06'),
(12, 'recruitment_user', 'bejoy barakoti', 'manager', 'barakoti@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 8, 1, 1, '2015-03-11 09:56:07'),
(13, 'ticketing_user', 'nabin thapa', 'manager', 'khadgi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 10, 1, 1, '2015-03-11 09:56:22'),
(14, 'visa_user', 'sabin thapa', 'manager', 'sabin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 11, 1, 1, '2015-03-11 09:56:25'),
(15, 'suzit', 'sujit', 'manager', 'suzit@gmail.cmo', '3fc2e17f35c212cd7c312be6f2ca8d14', 10, 1, 2, '2015-03-11 09:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa`
--

CREATE TABLE IF NOT EXISTS `tbl_visa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `received_date` date NOT NULL,
  `valid_date` date NOT NULL,
  `cancel_flag` int(1) NOT NULL,
  `remarks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_visa`
--

INSERT INTO `tbl_visa` (`id`, `candidate_id`, `position`, `received_date`, `valid_date`, `cancel_flag`, `remarks`, `updated_on`) VALUES
(1, 1, 'Worker', '2015-03-02', '2015-03-31', 0, 'ABCD', '2015-03-06 07:18:45');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
