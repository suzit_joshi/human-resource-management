-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 11, 2015 at 12:05 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_florid`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `role_name`, `status`, `last_updated`) VALUES
(1, 'super admin', 1, '0000-00-00 00:00:00'),
(2, 'System Admin', 1, '0000-00-00 00:00:00'),
(3, 'General Admin', 1, '0000-00-00 00:00:00'),
(4, 'Documentation', 1, '0000-00-00 00:00:00'),
(5, 'Finance', 1, '0000-00-00 00:00:00'),
(6, 'MOL Admin', 1, '0000-00-00 00:00:00'),
(7, 'General user', 1, '0000-00-00 00:00:00'),
(8, 'Recruitment', 1, '0000-00-00 00:00:00'),
(9, 'admin', 1, '2015-03-10 06:14:18'),
(10, 'ticketing', 1, '2015-03-11 05:07:18'),
(11, 'visa processing', 1, '2015-03-11 05:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `post` varchar(250) NOT NULL,
  `email_id` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `name`, `post`, `email_id`, `password`, `role_id`, `status`, `branch_id`, `updated_on`) VALUES
(1, 'sunbi', 'sunbi', 'sunbi', 'sunbi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 1, 1, 0, '2015-02-13 06:03:09'),
(5, 'admin_user', 'admin_user', 'Managing Director', 'admin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 2, 1, 1, '2015-03-10 06:24:29'),
(6, 'general_admin', 'general_admin', 'Director', 'general_admin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 3, 1, 0, '2015-02-13 06:03:09'),
(7, 'user_documentation', 'Gyanendra Malla', 'Manager', 'gyanendra@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 4, 1, 0, '2015-02-13 06:03:09'),
(8, 'sujan_khadgi', 'sujan khadgi', 'sweeper', 'kahdgi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 9, 1, 1, '2015-03-10 06:26:23'),
(9, 'sanjan piya', 'Sanjan Piya', 'ios developer', 'piya@sanjan.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 5, 1, 1, '2015-03-10 06:19:30'),
(10, 'sudeep_tuladhar', 'Sudeep tuladhar', 'android developer', 'tuladhar@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 6, 1, 1, '2015-03-10 06:20:18'),
(11, 'pathak_sujit', 'sujit pathak', 'manager', 'pathak@sujit.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 7, 1, 1, '2015-03-10 06:21:13'),
(12, 'barakoti_bejoy', 'bejoy barakoti', 'manager', 'barakoti@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 8, 1, 1, '2015-03-10 06:23:40'),
(13, 'nabin_thapa', 'nabin thapa', 'manager', 'khadgi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 10, 1, 1, '2015-03-11 06:19:02'),
(14, 'sabin_thapa', 'sabin thapa', 'manager', 'sabin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 11, 1, 1, '2015-03-11 06:19:33');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
