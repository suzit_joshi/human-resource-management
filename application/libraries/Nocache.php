<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nocache{

	public function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
}

/* End of file Someclass.php */