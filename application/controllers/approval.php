<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Approval extends Finance_management_controller {
	
	public $_table="tbl_candidate";
	public function __construct(){
		parent::__construct();
		$this->load->model('Job_model');
		$this->load->model('Candidate_model');
	}

	public function approvalForVisa(){
		  $data['candidate'] = $this->db->query("SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                    job_title, company_name, t1.current_status, $this->_table.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM $this->_table
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = $this->_table.id
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = $this->_table.agent_id
                    WHERE t1.current_status = 'medical_passed' and t1.advanced=0 
                    		AND $this->_table.id NOT IN(SELECT candidate_id FROM tbl_visa_ticketing_approval WHERE type = 'visa')
                    ")->result();
                  
		// $data['candidate']=$this->db->where(array('current_status'=>'medical_passed','advanced'=>0))->get('tbl_job_candidate')->result();
		$this->template->set_layout('site_layout')->build('admin/approval/visaApproval',$data);
	}

	public function sentVisaApproval(){
		$candidateId 	=	$this->input->post('candidateId');
		$remarks 		=	$this->input->post('msgForApproval');
		$approval_type	=	$this->input->post('approval_type');
		$today			= 	date('Y-m-d');

		foreach($candidateId as $candidateData){
			$data=array('candidate_id'=> $candidateData,
						'type' => $approval_type,
						'sender_remarks' => $remarks,
						'sent_date' => $today);
			$this->db->insert('tbl_visa_ticketing_approval',$data);
		}
	}

/*	public function visaApprovalListForAdmin(){
		// $data['candidate'] = $this->db->query("select ta.* from ")->result();
		$data['candidate']=$this->db->query("SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                    job_title, company_name,t5.id as approval_id,t5.remarks as approval_remarks, t1.current_status, $this->_table.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM $this->_table
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = $this->_table.id
                    	INNER JOIN tbl_approval t5
                    		ON  t1.candidate_id = t5.candidate_id	
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = $this->_table.agent_id
                    WHERE t5.approved=0")->result();
		$this->template->set_layout('site_layout')->build('admin/approval/visa_approval_list_admin',$data);
	}*/

	public function makeApprovalForVisa(){
		$candidateId=$this->input->post('candidateId');
		$status=$this->input->post('status');
		$username=$this->session->userdata('username');

		if($status=="approve"){

			$data=array('approved'=>1,'approved_by'=>$username);
			$update=$this->db->where('candidate_id',$candidateId)->update('tbl_approval',$data);
			$this->db->where('candidate_id',$candidateId)->update('tbl_job_candidate',array('approval'=>1));
			if($update){
				echo "Approved";
			}
		}else{

			
			 $delete=$this->db->delete('tbl_approval', array('id' => $approvalId)); 
			 if($delete){
			  echo "Declined";

			}
		}

	}


	public function approvalForTicket(){
		  $data['candidate'] = $this->db->query("SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                    job_title, company_name, t1.current_status, $this->_table.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM $this->_table
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = $this->_table.id
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = $this->_table.agent_id
                    WHERE t1.current_status = 'final_approval_received' and t1.advanced=0
                    	AND $this->_table.id NOT IN(SELECT candidate_id FROM tbl_visa_ticketing_approval WHERE type = 'ticket')
                    ")->result();
                   
		// $data['candidate']=$this->db->where(array('current_status'=>'medical_passed','advanced'=>0))->get('tbl_job_candidate')->result();
		$this->template->set_layout('site_layout')->build('admin/approval/ticketApproval',$data);

	}
	
	public function sendTicketApproval(){
		$candidateId 	=	$this->input->post('candidateId');
		$remarks 		=	$this->input->post('msgForApproval');
		$approval_type	=	$this->input->post('approval_type');
		$today			= 	date('Y-m-d');

		foreach($candidateId as $candidateData){
			$data=array('candidate_id'=> $candidateData,
						'type' => $approval_type,
						'sender_remarks' => $remarks,
						'sent_date' => $today);
			$this->db->insert('tbl_visa_ticketing_approval',$data);
		}
	}



	




}