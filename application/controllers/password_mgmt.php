<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Password_mgmt extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->no_cache();
	}

    function index()
	{
		$this->template->set_layout('site_layout')->build('admin/password/index');
	}
	
	public function change(){
		$user_id = $this->session->userdata('user_id');

		$entered_pwd = $this->input->post('old_pwd');
		$salt = "wdvhyf@%$@fgfrtyqq";
		$pwd = $salt.$entered_pwd;
		$encrypted_pwd = md5($pwd);

		$new_pwd = $this->input->post('new_pwd');
		$re_new_pwd = $this->input->post('re_new_pwd');
		
		/*Extracting Old Password for comparison*/

		if($this->input->post('frm-pwd-submit')){
			if($re_new_pwd != $new_pwd){
				$this->session->set_flashdata('noticeMessage','Error! Re-entered password Mismatch! Please try again');				
			}
			else{
				$query = $this->db->get_where('tbl_user',array('id'=>$user_id , 'password' => $encrypted_pwd));
				if($query->num_rows() > 0)
				{
					$salt = "wdvhyf@%$@fgfrtyqq";
					$password = $salt.$new_pwd;
					$set_pwd = md5($password);
					
					$data = array('password' => $set_pwd );
					$this->db->where('id',$user_id);
					if($this->db->update('tbl_user', $data)){
						$this->session->set_flashdata('sessionMessage','Success! password changed successfully!!');
					}
				}
				else
				{
					$this->session->set_flashdata('noticeMessage','Error! Old password Mismatch! Please try again');
				}
			}
		}
		else{
			$this->session->set_flashdata('noticeMessage','Error! Form submission error! Please try again');
		}
		$redirect=base_url()."password_mgmt/index";
		redirect($redirect);

				

	}
	
		/** Clear the old cache (usage optional) **/ 
		protected function no_cache(){
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0',false);
			header('Pragma: no-cache'); 
		}
	
}