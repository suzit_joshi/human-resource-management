<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MobileController extends CI_Controller {

	public $user_id ;
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Mobile_model');
		$this->load->model('Bank_model');
		$this->load->model('Cash_model');
		$this->user_id = $this->session->userdata('user_id');
	}

	public function mobileAgentNotification($notification_id=NULL){
		if(is_null($notification_id)){
			$query = $this->db->query("SELECT t2.id AS agent_id, t2.agent_name, t2.phone, t2.email
				FROM tbl_agent t2
				WHERE t2.status=1
				");
		}
		else{
			$query = $this->db->query("SELECT t1.id as notification_id, t2.id AS agent_id, t2.agent_name, t2.phone, t2.email
				FROM tbl_agent_notification t1 
				INNER JOIN tbl_agent t2 
				ON t1.id = t2.notification_id 
				WHERE (t1.id = '$notification_id' AND t2.status=1) OR t2.status = 1
				");
		}
		$data['agents'] = $query->result();
		// change agent notification status for making seen in the notification
		/*$updateStatus=array('status'=>1);
		$update=$this->db->where('id',$notification_id)->update('tbl_agent_notification',$updateStatus);*/
		$this->load->view('admin/notification/mobileAgentNotification',$data);
	}

	public function login(){
		$data['type'] = "app";
		$this->load->view('login_frm',$data);
	}


	public function index(){
		// if($sessionRole=="general admin" || $sessionRole=="system admin") {
		
		$query=$this->db->order_by("id", "desc")->limit(10)->get('tbl_agent_notification')->result();
		$data['agentNotification']=count($query);
		$finance_result = $this->Mobile_model->getTransactions($this->user_id);
		$data['financeNotification'] = count($finance_result);
		$data['username'] = $this->session->userdata('name');
		$this->load->view('admin/mobile/index',$data);
	}

	public function getFinance(){
		$data['type'] = "finance";
		$transactions = $this->Mobile_model->getTransactions($this->user_id);

		$num = count($transactions);
		if($num!=0){
			foreach ($transactions as $row) {
				$approved_by=array();
				$id = $row->id;
				$query = $this->db->query("SELECT t1.name, t2.decision, t2.remarks, t2.date
											FROM tbl_user t1
										   LEFT JOIN tbl_finance_approval t2
												ON t2.user_id = t1.id
											WHERE (t1.role_id = 2 OR t1.role_id = 3)
													AND t2.transaction_id = $id")->result();
				foreach ($query as $record) {
					$approved_by[] = array('name' => $record->name,
											'decision' => $record->decision,
											'remarks' => $record->remarks);
				}
				$arr=(array)$row;
				$arr['approved_by']=$approved_by;
				$mainData[]=$arr;
			}
		}else{
			$mainData = 0;
		}
		$data['transactions'] = $mainData;
		$this->load->view('admin/mobile/inside',$data);
	}

	public function setDecision(){
		$transaction_id = $this->input->post('transaction_no');
		$decision = $this->input->post('btnvalue');
		$remarks = $this->input->post('remarks');

		$user_role = $sessionRole=strtolower($this->session->userdata('role'));
		$user_id = $this->session->userdata('user_id');

		$cash_result = (array)$this->Cash_model->getCash();
		$cash_id = $cash_result['id'];
		$current_cash = $cash_result['amount'];

		$data = array('transaction_id' => $transaction_id,
							'user_id' => $user_id,
							'decision' => $decision,
							'remarks' => $remarks,
							'date' => date('Y-m-d')
						 );
		$this->db->insert('tbl_finance_approval',$data);

		if($user_role == "system admin"){
			if($decision == "Approved"){
				$transaction = $this->db->get_where('tbl_transaction',array('id' => $transaction_id))->row();
				$payment_type = $transaction->subtype;
				$amount = $transaction->amount;
							
				if($payment_type == "Cash"){
					$cheque_no = '';
					$new_cash = $current_cash - $amount;
					$data = array(
							'amount' => $new_cash
							);
					$this->Cash_model->update($cash_id,$data);
					$status = 1;
				}
				elseif($payment_type == "Cheque"){
					$bank_id = $transaction->bank_id;
					$cheque_no = $transaction->cheque_no;
					$field = "id,balance";
					$bank_result = $this->Bank_model->get_data($field,$bank_id);
					$bank_balance = $bank_result->balance;
					$new_balance = $bank_balance - $amount;
					$data = array('balance' => $new_balance);
					$this->Bank_model->update($bank_id,$data);
				}
				$new_data = array('sent_flag' => 2);
				$this->db->where('id',$transaction_id)->update('tbl_transaction',$new_data);
			}			
		}
		$this->session->set_flashdata('msg',"Transaction decision saved successfully!");
		$redirect = base_url()."mobileController/getFinance/";
		redirect($redirect);		
	}

}