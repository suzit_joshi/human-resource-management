<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertise extends Office_management_controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Advertise_model');
		$this->load->model('Job_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$advertisement= $this->Advertise_model->listAdvertisement();
		$data['advertisement']=$advertisement;
		$this->template->set_layout('site_layout')->build('admin/advertisement/index',$data);
	}

	//@author Sudeep
	//@params None
	//@returns None
	//@Used to save new advertisement
	public function newAd(){
		$data['jobs'] = $this->Job_model->multiple_where(array('status'=>0),'id,job_title');
		$this->template->set_layout('site_layout')->build('admin/advertisement/newadvertisement',$data);
	}

	public function save(){
		$job=$this->input->post('job');
		$releaseDate=date('Y-m-d',strtotime($this->input->post('releaseDate')));

		$paper=$this->input->post('paper');
		$size=$this->input->post('size');
		$page_no=$this->input->post('page_no');
		$tmp_img_name = $_FILES['image']['name'];
		$path = "uploads/advertisement";
		$img_ret = $this->image_upload($tmp_img_name,$path,'image');
		if($img_ret['status']){
			$data = array('released_date' => $releaseDate,
							'job_id'=>$job,
						  'paper' => $paper,
						  'size' => $size,
						  'page_no' => $page_no,
						  'image' => $img_ret['msg']						 
						);
			$insert=$this->Advertise_model->insert($data);
			if($insert){
				$this->session->set_flashdata('sessionMessage','Advertisement added successfully');
				$redirect = base_url()."advertise/index";

			}
		}
		else{
			$this->session->set_flashdata('sessionMessage',$img_ret['msg']);
			$redirect = base_url()."advertise/newAd";

		}
		redirect($redirect);
	}


	public function delete($id){
		$advertiseId = base64_decode($this->uri->segment(3));
		
			 $check =  $this->Advertise_model->get($advertiseId);        
			  $old_image = $check->image;  
			  if($old_image!=""){  
			    if(file_exists("uploads/advertisement/".$old_image))  {      
			     $this->image_remove('uploads/advertisement',$old_image);
			   }
			 }

			 $delete=$this->Advertise_model->delete($advertiseId);
			 if($delete){
			 $this->session->set_flashdata('sessionMessage',"Record deleted successfully!");
			 $redirect = base_url()."advertise/index";
			 redirect($redirect);

			}
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit an agent
	public function editBank(){
		$bank_id = $this->input->post('bank_id');
		$bank_name = $this->input->post('bank');

		$data = array('bank_name' => $bank_name);
		$this->Bank_model->update($bank_id,$data);
		$data['result'] = $this->Bank_model->get_all();
		$this->load->view('ajax/ajax_bank',$data);
	}

}