<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends Office_management_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
	}

	
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$query = $this->db->query("SELECT tbl_user.*,tbl_role.role_name 
								   FROM tbl_user
								   LEFT JOIN tbl_role ON tbl_user.role_id = tbl_role.id
								   WHERE tbl_role.id != '1'
								   ");
		$data['result_user'] = $query->result();
		$this->load->view('admin/user/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to redirect into new form to create job
	public function newUser(){
		$this->load->model('branch_model');
		$query = $this->db->get_where('tbl_role',array('status' => 1));
		$data['result_role'] = $query->result();
		$data['branch']=$this->branch_model->get_all();
		
		$this->load->view('admin/user/newuser',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add user
	public function save(){

		$this->form_validation->set_rules('name','name','required');
		$this->form_validation->set_rules('designation','designation','required');
		$this->form_validation->set_rules('email_id','email_id','valid_email|is_unique[tbl_user.email_id]');
		$this->form_validation->set_rules('username','username','required|is_unique[tbl_user.username]|max_length[30]');
		$this->form_validation->set_rules('password', 'password', 'required|matches[re_pwd]|max_length[20]');
		$this->form_validation->set_rules('re_pwd', 're-password', 'required');


		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$name = $this->input->post('name');
			$designation = $this->input->post('designation');
			$password = $this->input->post('password');
			$email_id = $this->input->post('email_id');
			
			$branch_id=$this->input->post('branch');


			if($branch_id != 1){
				$role_id = 4;
			}else{
				$role_id = $this->input->post('role');
			}

			$salt = "wdvhyf@%$@fgfrtyqq";
	        $salt .= $password;
	        $password = $salt;
	        $password = md5($password);

	        $data = array(
	        	'username' => $username,
	        	'name' => $name,
	        	'post' => $designation,
	        	'email_id' => $email_id,
	        	'role_id' => $role_id,
	        	'password' => $password,
	        	'branch_id'=>$branch_id,
	        	'updated_by' => $this->session->userdata('name'),
	        	'status' => 1
	        	);

	        if($this->db->insert('tbl_user',$data)){
	        	$this->session->set_flashdata('sessionMessage','New user created successfully');
	        }else{
	        	$this->session->set_flashdata('noticeMessage','Error! creating user. Please try again!');	        	
	        }
	        $redirect = base_url()."user/index";
			redirect($redirect);
		}
		else{
			$this->load->model('branch_model');
			$query = $this->db->get_where('tbl_role',array('status' => 1));
			$data['result_role'] = $query->result();
			$data['branch']=$this->branch_model->get_all();
			
			$this->load->view('admin/user/newuser',$data);
		}

	}

	public function editUser(){
		$this->load->model('branch_model');
		$data['branch']=$this->branch_model->get_all();
		$query_role = $this->db->get_where('tbl_role',array('status' => 1));
		$data['result_role'] = $query_role->result();

		$user_id = base64_decode($this->uri->segment(3));
		$query = $this->db->get_where('tbl_user',array('id' => $user_id));
		
		$data['user_record'] = $query->row();
		$this->load->view('admin/user/newuser',$data);
	}


	public function editsave(){
		$password = $this->input->post('password');
		$old_pwd = $this->input->post('random_value');
		$user_id = base64_decode($this->input->post('random_2'));
		$email_id = $this->input->post('email_id');
		$role_id = $this->input->post('role');
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$designation = $this->input->post('designation');
		$branch_id=$this->input->post('branch');

		$validation_return = $this->User_model->validateUserEmail($username,$email_id);

        $result_no = $validation_return['number'];
        $result_set = $validation_return['result'];
		if($result_no <= 1){
			foreach ($result_set as $validated_value) {
				$returned_user_id = $validated_value->id;
			}
			if($returned_user_id == $user_id){
				if($password == ""){
					$password = $old_pwd;
				}else{
					$salt = "wdvhyf@%$@fgfrtyqq";
					$salt .= $password;
					$password = $salt;
					$password = md5($password);	
				}

				$data = array(
					'username' => $username,
					'name' => $name,
					'post' => $designation,
					'email_id' => $email_id,
					'role_id' => $role_id,
					'password' => $password,
					'branch_id'=>$branch_id,
					'updated_by' => $this->session->userdata('name'),
					'status' => 1
					);
				$this->db->where('id',$user_id);
				if($this->db->update('tbl_user',$data)){
					$this->session->set_flashdata('sessionMessage','The record has been updated successfully');
				}else{
					$this->session->set_flashdata('noticeMessage','Error! The record could not be updated. Please try again');
				}	
				$redirect = base_url()."user/index/";
			}
			else{
				$redirect = base_url()."user/editUser/".base64_encode($user_id)."/ErrRec";
			}
		}
		else{
				$redirect = base_url()."user/editUser/".base64_encode($user_id)."/ErrRec";
		}		
		redirect($redirect);
	}


	public function changeStatus(){
		$user_id = base64_decode($this->uri->segment(3));
		$type = $this->uri->segment(4);
		if($type == "deactivate"){
			$data = array('status' => 0);			
			$redirect = base_url()."user/index/deactivated";
		}
		elseif($type == "activate"){
			$data = array('status' => 1);
			$redirect = base_url()."user/index/activated";
		}
		else{
			$redirect = base_url()."user/index/error";
			redirect($redirect);
		}
		$this->db->where('id',$user_id);
		$this->db->update('tbl_user',$data);
		redirect($redirect);
	}

	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0',false);
	header('Pragma: no-cache'); 
	}
	
}