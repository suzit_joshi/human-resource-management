<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Visa extends Visa_management_controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('Visa_model');
		$this->load->model('Candidate_model');
        $this->load->model('General_notification_model');
        $this->load->model('Transaction_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['visa'] = $this->Visa_model->getVisaDetails();
		$this->template->set_layout('site_layout')->build('admin/visa/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new Visa Details
	public function newVisa(){
		$data['candidate'] = $this->Candidate_model->getCandidateList('visa_processing');
		$this->template->set_layout('site_layout')->build('admin/visa/newvisa',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO SAVE DATA FOR NEW visa
	public function save(){
		$candidate = $this->input->post('candidate');
		$position = $this->input->post('position');
		$received_date = date('Y-m-d', strtotime($this->input->post('received_date')));
		$valid_date = date('Y-m-d', strtotime($this->input->post('valid_date')));
		$remarks = $this->input->post('remarks');

        $tmp_img_name = $_FILES['image']['name'];

        $path = "uploads/visa_copy";
        $img_ret = $this->image_upload($tmp_img_name,$path,'image');
            
        if($img_ret['status']){
            $visa_copy = $img_ret['msg'];
            if($this->input->post('visa-submit')){
                $data = array(
                            'position' => $position,
                            'received_date' => $received_date,
                            'valid_date' => $valid_date,
                            'remarks' => $remarks,
                            'candidate_id' => $candidate,
                            'visa_copy' => $visa_copy,
                            'updated_by' => $this->session->userdata('name')
                        );
                if($this->Visa_model->insert($data)){
                    $job_candidate_data = array('current_status' => "visa_received", 'data_entry' => 1 , 'advanced' => 0,'approval' => 0);
                    $this->db->where('candidate_id',$candidate)->update('tbl_job_candidate',$job_candidate_data);

                    $notify_mol     =   array(
                                            'sender'            =>  $this->session->userdata('name'),
                                            'receipent_role'    =>  'MOL Admin',
                                            'candidate_no'      =>  1,
                                            'remarks'           =>  'Visa received candidate',
                                            'sender_role'       =>  $this->session->userdata('role'),
                                            'heading'           =>  'visa_received'  
                                        );
                    $this->General_notification_model->insert($notify_mol);
                    $this->session->set_flashdata('sessionMessage','Visa details inserted successfully!');
                    $redirect = base_url()."visa/index";
                }
                else{
                    $this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
                    $redirect = base_url()."visa/newVisa";
                }
            }
            else{
                $this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
                $redirect = base_url()."visa/newVisa";
            }
        }
        else{
            $error_msg = $img_ret['msg'];
            $this->session->set_flashdata('noticeMessage',$error_msg);
            $redirect = base_url()."visa/newVisa";
        }
		redirect($redirect);
	}

	//@author Sujit
    //@params None
    //@returns None
    //@accessed by finance department to Approve visa processing
    public function getTransferList($type){
        if($type == "medical_passed"){
            $status = "medical_passed";
        }else{
            $this->session->set_flashdata('noticeMessage',"Wrong type inserted please try again!");
            $redirect = base_url()."visa/index";
            redirect($redirect);
        }

        $query = "SELECT tbl_candidate.id AS candidate_id, candidate_name, passport_no, tbl_candidate.phone, tbl_candidate.mobile, 
                    job_title, company_name, t1.current_status, tbl_candidate.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM tbl_candidate
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = tbl_candidate.id
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = tbl_candidate.agent_id
                    WHERE t1.current_status = '$status' AND (t1.advanced = 1 OR t1.approval = 1)"; 

        $data['candidates'] = $this->db->query($query)->result();

        $this->template->set_layout('site_layout')->build('admin/visa/visaProcessingList',$data);
    }


    //@author Sujit
    //@params None
    //@returns None
    public function sendNotification(){
        //on-medical status
        $candidates = $this->input->post('candidate');
        $no_candidate = count($candidates);
        $remarks = "Visa processing candidate list";
        $new_status = "visa_processing";

        if($this->input->post('frm_submit')){
            foreach ($candidates as $row => $value) {
                $update_data = array('current_status' => $new_status , 'data_entry' => 0);
                $this->db->where('candidate_id', $value);
                $this->db->update('tbl_job_candidate',$update_data);
            }
            $data = array(    
                array(
                    'sender' => $this->session->userdata('username'),
                    'receipent_role' => 'visa processing',
                    'candidate_no' => $no_candidate,
                    'remarks' => $remarks,
                    'sender_role' => $this->session->userdata('role'),
                    'heading' => $new_status
                    ),
                array(
                    'sender' => $this->session->userdata('username'),
                    'receipent_role' => 'general admin',
                    'candidate_no' => $no_candidate,
                    'remarks' => $remarks,
                    'sender_role' => $this->session->userdata('role'),
                    'heading' => $new_status
                    )
                );
            $this->General_notification_model->insert_many($data);  
            $this->session->set_flashdata('sessionMessage',"Notification sent!");            
        }
        else{
            $this->session->set_flashdata('noticeMessage',"Notification could not be sent. Please try again!");
        }
        $redirect = base_url()."visa/index";
        redirect($redirect);
    }


    //@author Sujit
    //@params none
    //@returns none
    public function cancelVisa($id){
        $visa_id = base64_decode($id);
        $record = $this->db->get_where('tbl_visa',array('id' => $visa_id))->row();
        $candidate = $record->candidate_id;
        
        $data = array('cancel_flag' => 1,'updated_by' => $this->session->userdata('name'));
        $this->db->where('id',$visa_id);
        $this->db->update('tbl_visa',$data);
        $this->session->set_flashdata('noticeMessage',"The visa has been cancelled successfully!");
        
        $job_candidate_data = array('current_status' => "visa_cancelled");
        $this->db->where('candidate_id',$candidate)->update('tbl_job_candidate',$job_candidate_data);
        $redirect = base_url()."visa/index";
        redirect($redirect);
    }

    //@author Sujit
    //@params none
    //@returns none
    //@GENERAL ADMIN NOTIFICATION FOR VISA APPROVAL BY FINANCE DEPARTMENT
    public function getVisaApproval($id=NULL){
        $user_id = $this->session->userdata('user_id');
        if($id==NULL){
            $data['candidate_result'] = $this->Candidate_model->approvalList('visa');

            $data['approved_candidate'] = $this->Candidate_model->approvedList('visa');
            $this->template->set_layout('site_layout')->build('admin/visa/visaApprovalList',$data);
        }
        else{
            $query = $this->db->get_where('tbl_visa_ticketing_approval',array('id'=>$id))->row();
            $candidate_id = $query->candidate_id;
            $select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, tc.perm_address, tc.temp_address,
                    tc.father_name, tc.mother_name, tc.email_id, tc.height, tc.weight, tc.dob, tc.birth_place, tc.language,
                    tc.marital_status,tc.religion,tc.nationality,tc.qualification,tc.experience, tc.training, tc.issued_from,tc.issued_date,
                    tc.expire_date,
                    ta.agent_name, ta.mobile AS agent_mob,
                    tj.job_title, tc1.company_name, tjb.current_status";
            $data['candidate'] = (array)$this->Candidate_model->getGeneralCandidateInfo($candidate_id,$select);
            /*      echo "<pre>";
            var_dump($data['candidate']);
            echo "</pre>";
            die();*/
            $data['candidate_doc'] = (array)$this->Candidate_model->getDocuments($candidate_id);
            $data['preInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Pre-interview');
            $data['finalInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Final-interview');
            $data['medicalResult']=(array)$this->Candidate_model->medicalResult($candidate_id);
            $data['ticketing']=(array)$this->Candidate_model->ticketingResult($candidate_id);
            $data['visa']   =   (array) $this->Candidate_model->visaResult($candidate_id);

            $data['finacial_info'] = $this->Transaction_model->getFinancialInfo($candidate_id);

            $data['decision']   =   (array)$this->db->query("SELECT * FROM tbl_visa_ticketing_approval WHERE candidate_id = '$candidate_id' AND type ='visa'")->row();
            $this->template->set_layout('site_layout')->build('admin/visa/ApprovalCandidateVisa',$data);
        }
    }


    //@author Sujit
    //@params none
    //@returns none
    //@APPROVAL DONE BY GENERAL ADMIN
    public function approveVisa(){
        $candidate_id   =   $this->input->post('candidate_id');
        $result         =   $this->input->post('set_result');
        $remarks        =   $this->input->post('remarks');
        if($result == 1){
            $decision = "Approved";
            $data       =   array('approval' => 1);
            $this->db->where('candidate_id', $candidate_id);
            $this->db->update('tbl_job_candidate',$data);

            $this->session->set_flashdata('sessionMessage','Visa has been approved successfully!');
        }else{
            $decision = "Declined";
            $this->session->set_flashdata('noticeMessage','Visa has been declined successfully!');
        }
        $data           =   array(
                                'decision'       =>  $decision,
                                'decided_by'    =>  $this->session->userdata('name'),
                                'decided_date'  =>  date('Y-m-d'),
                                'remarks'       =>  $remarks,
                                'sent_flag'     =>  1
                            );
        $where  =   array('candidate_id' => $candidate_id , 'type' => 'visa');
        $this->db->where($where);
        $this->db->update('tbl_visa_ticketing_approval',$data);
        $redirect = base_url()."visa/getVisaApproval";
        redirect($redirect);
    }

    //@author Sujit
    //@params none
    //@returns none
    //@Transfer to MOL visa received candidates
    public function transferCandidateToMol(){

    }


}