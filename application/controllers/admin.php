<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

			$this->no_cache();
			$loggedInStatus=$this->session->userdata('hrm_login_status');
			if($loggedInStatus!="1"){
				$redirect=base_url()."_cpanel";
				redirect($redirect);
			}
			$this->load->model('Cash_model');
			$this->load->model('Bank_model');
		$this->setCashInHand();
	}




	public function index(){

		$cash_result = (array)$this->Cash_model->getCash();
		if($cash_result){
			$data['cash'] = $cash_result['amount'];
		}
		else{
			$data['cash'] = 0;
		}
		$data['bank_result'] = $this->Bank_model->get_all();
		$user_id = $this->session->userdata('user_id');
		$current_date = date("Y-m-d");

    	/*$reminder_query = $this->db->query("SELECT * FROM `tbl_reminder` 
											WHERE notify_on <= \"$current_date\" 
											AND status=1
											AND user_id = \"$user_id\"
											");
    	$result_no = $reminder_query->num_rows();*/
    	$result_no = 0;
    	
    	if($result_no > 0){
    		$data['notifications'] = $reminder_query->result();
    		$data['no_of_notification'] = $result_no;
    	}else{
    		$data['notifications'] = 0;
    		$data['no_of_notification'] = 0;
    	}
  		// $data['viewData']='';
    	$this->template->set_layout('site_layout')->build("admin/index",$data);
	}

	public function logout(){
            $this->session->sess_destroy();
            $redirectLog = base_url()."_cpanel";
            redirect($redirectLog);
           
    }

    public function setCashInHand(){
    	$current_year = date("Y");
    	$current_month = date("m");
		$cash_result = (array)$this->Cash_model->getCash();

		if($cash_result){
			$cash_year = $set_year = $cash_result['year'];
			$cash_month = $cash_result['month'];
			$current_amount = $cash_result['amount'];
			$flag = 0;

			if($cash_year != $current_year){
				$set_year = $current_year;
				$flag = 1;
			}

			if($current_month != $cash_month){
				$set_month = $current_month;
				$flag = 1;
			}

			if($flag == 1){
				$data = array(
					'amount' => $current_amount,
					'year' => $set_year,
					'month' => $set_month
					);
				$this->Cash_model->insert($data);
			}
		}		
    }

	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0',false);
	header('Pragma: no-cache'); 
	}	
}