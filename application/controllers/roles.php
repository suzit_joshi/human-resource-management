<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roles extends Base_controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('nocache');
		$this->nocache->no_cache();
		$loggedInStatus=$this->session->userdata('hrm_login_status');
		if($loggedInStatus!="1"){
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
		$usertype = strtolower($this->session->userdata('role'));
		if($usertype != "super admin"){
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$query = $this->db->get_where('tbl_role',array('status'=>'1'));
		$data['result_role'] = $query->result();
		$this->load->view('admin/role/index',$data);
	}

	// @author sujit
	// @params None
	// @returns None
	// @ADD THROUGH AJAX
	public function newRole(){
		$role_name = $this->input->post('role_name');
		$role_id = $this->Idgenerator->genId('tbl_role','role_id');
		$data = array(
				'role_id' => $role_id,
				'role_name' => $role_name,
				'status' => '1'
			);
		$this->db->insert('tbl_role',$data);
			$query = $this->db->get_where('tbl_role',array('status'=>'1'));
		$data['result_role'] = $query->result();
		$data['type'] = "new";
		$this->load->view('ajax/ajax_role',$data);

	}

	// @author sujit
	// @params None
	// @returns None
	// @EDIT THROUGH AJAX
	public function editRole(){
		$role_id = base64_decode($this->input->post('id'));
		$role_name = $this->input->post('role_name');
		$data = array('role_name' => $role_name );
		$query = $this->db->where('role_id',$role_id)->update('tbl_role',$data);
		$data['type'] = "edit";
		$query = $this->db->get_where('tbl_role', array('status' => '1'));
		$data['result_role'] = $query->result();
		$this->load->view('ajax/ajax_role',$data);
	}

	// @author sujit
	// @params None
	// @returns None
	// @Deactivate the status of role for future records
	public function deactivate(){
		$id = base64_decode($this->uri->segment(3));
		$data = array('status' => '0');
		$query = $this->db->where('role_id',$id)->update('tbl_role',$data);

		$redirect=base_url()."roles/index/"."deactivated";
		redirect($redirect);
	}

}