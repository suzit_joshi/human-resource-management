<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Job extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Job_model');
		$this->load->model('Company_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['result_job'] = $this->Job_model->getJob();
	 	$this->template->set_layout('site_layout')->build('admin/job/index',$data);
	}

	public function pdf(){
		$this->load->helper('pdf_helper');

		$job_id = base64_decode($this->uri->segment(3));

		$data['result'] = (array)$this->Job_model->getJob($job_id);
		$this->load->view('admin/pdf/demand_letter',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to redirect into new form to create job
	public function newJob(){
		$data['result_company'] = $this->Company_model->getCompany();
		$this->template->set_layout('site_layout')->build('admin/job/newjob',$data);
		//$this->load->view('admin/job/newjob',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit a job
	public function save(){

		$id = $this->input->post('random');
		$job_title = $this->input->post('job_title');
		$vacancy = $this->input->post('vacancy');
		$salary = $this->input->post('salary');
		$company_id = $this->input->post('company');
		$requirement = $this->input->post('req_desc');
		$facility = $this->input->post('facility_desc');
		$period = $this->input->post('period');
		$pre_to_date = date("Y-m-d", strtotime($this->input->post('pre-to')));
		$pre_for_date = date("Y-m-d", strtotime($this->input->post('pre-for')));
		$final_to_date = date("Y-m-d", strtotime($this->input->post('final-to')));
		$final_for_date = date("Y-m-d", strtotime($this->input->post('final-for')));
		$demand_expiry_date = date("Y-m-d", strtotime($this->input->post('demand_expiry_date')));
		$final_interviewer = $this->input->post('final_interviewer');

		$pic=$_FILES['userfile']['name'];
		$count = count($pic);

		if($pic[0]!=''){
			$path="uploads/demand_letter";

			$images=$this->multiple($path);
			if($images['success']){
				$db_image=serialize($images['success']);
			} 
			else{
				$this->session->set_flashdata('sessionMessage',implode($images['unsuccess'])."file could not be inserted");
				$redirect = base_url()."job/newJob";
				redirect($redirect);
			}		 
		}
		else{
			$db_image="";
		}
		$input_post = array('job_title' => $job_title,
					  'vacancy_no' => $vacancy,
					  'company_id' => $company_id,
					  'requirement' => $requirement,
					  'facility' => $facility,
					  'period' => $period,
					  'demand_letter_expiry' => $demand_expiry_date,
					  'demand_letter'=>$db_image,
					  'salary' => $salary,
					  'pre_interview_to_date' => $pre_to_date,
					  'pre_interview_for_date' => $pre_for_date,
					  'final_interview_to_date' => $final_to_date,
					  'final_interview_for_date' => $final_for_date,
					  'final_interviewer' => $final_interviewer,
					  'updated_by' => $this->session->userdata('name')
					);

		if($id == ""){
			$input_post = array_merge($input_post,array('demand_letter' => $db_image));
			$insert=$this->db->insert('tbl_job',$input_post);
			if($insert){
				$this->session->set_flashdata('sessionMessage','Inserted Successfully');
				$redirect = base_url()."job/index";
				redirect($redirect);
			}
		}
		else{			
			$update=$this->Job_model->update($id,$input_post);
			if($update){
				$this->session->set_flashdata('sessionMessage','Updated Successfully');
				$redirect = base_url()."job/index";
				redirect($redirect);
			}
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific job according to the id
	public function getInformation(){
		$job_id = base64_decode($this->uri->segment(3));
		$data['job'] = (array)$this->Job_model->getJob($job_id);
		$data['result_company'] = $this->Company_model->getCompany();
		$this->template->set_layout('site_layout')->build('admin/job/newjob',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific job according to the id
/*	public function deactivate(){
		$job_id = base64_decode($this->uri->segment(3));
		$data['status'] = '0';
		$this->db->where('id',$job_id);
		$this->db->update('tbl_job',$data);
		$redirect = base_url()."job/index"."/deactivated";
		redirect($redirect);
	}*/

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific job according to the id
	public function getPrintPreview(){
		$job_id = $this->input->post('job_id');
		$data['job'] = (array)$this->Job_model->getJob($job_id);
		$this->load->view('ajax/ajax_job_preview',$data);
	}






	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0',false);
	header('Pragma: no-cache'); 
	}
	
}