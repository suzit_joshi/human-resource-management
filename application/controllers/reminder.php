<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reminder extends Base_controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('Nocache');
		$this->nocache->no_cache();
		$loggedInUser=$this->session->userdata('email');
		if($loggedInUser==""){
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('*')->from('tbl_reminder')->where('user_id',$user_id)->order_by('reminder_date','asc');
		$query = $this->db->get();
		$data['result_reminder'] = $query->result();
		$this->load->view('admin/reminder/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to load view of new Reminder
	public function newReminder(){
		$this->load->view('admin/reminder/newReminder');		
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to edit and add reminders
	public function save(){

		$value_id = $this->input->post('id');
		$reminder_date = $this->input->post('reminder_date');
		$days = "-". $this->input->post('notification'). "day";

		$description = $this->input->post('reminder_desc');
		$status = 1;

		$newdate = strtotime ( $days , strtotime ($reminder_date));
		$newdate = date ( 'Y-m-d' , $newdate );
		$data = array(
				'notify_on' => $newdate,
				'reminder_date' => $reminder_date,
				'description' => $description,
				'status' => $status
				);

		if($value_id == ""){
			$user_id = $this->session->userdata('user_id');
 			$reminder_id = $this->Idgenerator->genId('tbl_reminder','reminder_id');
 			$data['reminder_id'] = $reminder_id;
 			$data['user_id'] = $user_id;
			$this->db->insert('tbl_reminder',$data);
			$redirect = base_url().'reminder/index/success';
			redirect($redirect);
			
		}else{		
			$reminder_id = base64_decode($id);
			$this->db->where('reminder_id',$reminder_id);
			$this->db->update('tbl_reminder',$data);
			$redirect = base_url().'reminder/index/editsuccess';
			redirect($redirect);
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to get a specific reminder according to Id
	public function getReminder(){
		$id = $this->uri->segment(3);
		$reminder_id = base64_decode($id);
		$query = $this->db->get_where('tbl_reminder', array('reminder_id' => $reminder_id));
		if($query->num_rows() > 0){
			$data['result'] = $query->row();
			$this->load->view('admin/reminder/newReminder',$data);
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to delete the reminder
	public function deleteReminder(){
		$id = $this->uri->segment(3);
		$reminder_id = base64_decode($id);
		$this->db->where('reminder_id',$reminder_id);
		$this->db->delete('tbl_reminder');
		$redirect = base_url().'reminder/index/delsuccess';
		redirect($redirect);

	}

}