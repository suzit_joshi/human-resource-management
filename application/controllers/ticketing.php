<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ticketing extends Base_controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('Ticketing_model');
		$this->load->model('Candidate_model');
		$this->load->model('Transaction_model');		
        $this->load->model('General_notification_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['ticketing'] = $this->Ticketing_model->getTicketDetails();
		$this->template->set_layout('site_layout')->build('admin/ticketing/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new Transaction
	public function newTicketing(){
		$data['candidate'] = $this->Candidate_model->getCandidateList('ticket_processing');
		$this->template->set_layout('site_layout')->build('admin/ticketing/newticketing',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO GET DATA FOR NEW ticketing
	public function save(){
		$candidate = $this->input->post('candidate');
		$ticket_no = $this->input->post('ticket_no');
		$flight_no = $this->input->post('flight_no');
		$airlines = $this->input->post('airlines');
		$baggage = $this->input->post('baggage');
		$departure = date('Y-m-d H:i:s', strtotime($this->input->post('departure_date')));
		$arrival = date('Y-m-d H:i:s', strtotime($this->input->post('arrival_date')));
		$origin = $this->input->post('origin');
		$destination = $this->input->post('destination');

		$job_candidate_data = array('current_status' => "ticket_received", 'data_entry' => 1);
		$this->db->where('candidate_id',$candidate)->update('tbl_job_candidate',$job_candidate_data);

		if($this->input->post('ticketing-submit')){
			$data = array(
						'ticket_no' => $ticket_no,
						'airlines' => $airlines,
						'flight_no' => $flight_no,
						'baggage' => $baggage,
						'departure' => $departure,
						'arrival' => $arrival,
						'origin' => $origin,
						'destination' => $destination,
						'candidate_id' => $candidate
					);
			if($this->Ticketing_model->insert($data)){
				$this->session->set_flashdata('sessionMessage','Ticketing details inserted successfully!');
				$redirect = base_url()."ticketing/index";
			}
			else{
				$this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
				$redirect = base_url()."ticketing/newTicketing";
			}
		}
		else{
			$this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
			$redirect = base_url()."ticketing/newTicketing";
		}
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO EDIT DATA FOR ticketing
	public function editTicketing(){
		$id = base64_decode($this->uri->segment(3));
		

	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO GET DATA FOR NEW ticketing
	public function getSource(){
		$fetch = $this->input->post('fetchData');
		$type = $this->input->post('type');
		if ($fetch == "income" || $fetch == "expense"){
			$data['result'] = $this->Category_model->getFilteredCategory($fetch,$type);
			$this->load->view('ajax/ajax_source',$data);
		}
		else{
			echo "a";
		}
	}

	//@author Sujit
    //@params None
    //@returns None
    public function getTransferList($type){
        if($type == "final_approval_received"){
            $status = "final_approval_received";
        }else{
            $this->session->set_flashdata('noticeMessage',"Wrong type inserted please try again!");
            $redirect = base_url()."ticketing/index";
            redirect($redirect);
        }

        $query = "SELECT tbl_candidate.id AS candidate_id, candidate_name, passport_no, tbl_candidate.phone, tbl_candidate.mobile, 
                    job_title, company_name, t1.current_status, tbl_candidate.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM tbl_candidate
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = tbl_candidate.id
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = tbl_candidate.agent_id
                    WHERE t1.current_status = '$status' AND (t1.advanced = 1 OR t1.approval = 1)"; 

        $data['candidates'] = $this->db->query($query)->result();

        $this->template->set_layout('site_layout')->build('admin/ticketing/ticketProcessingList',$data);
    }


    //@author Sujit
    //@params None
    //@returns None
    public function sendNotification(){
        //on-medical status
        $candidates = $this->input->post('candidate');
        $no_candidate = count($candidates);
        $remarks = "Ticket processing candidate list";
        $new_status = "ticket_processing";

        if($this->input->post('frm_submit')){
            foreach ($candidates as $row => $value) {
                $update_data = array('current_status' => $new_status , 'data_entry' => 0);
                $this->db->where('candidate_id', $value);
                $this->db->update('tbl_job_candidate',$update_data);
            }

             $data = array(    
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'ticketing',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        ),
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'general admin',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        )
                    );

            $this->General_notification_model->insert_many($data);  
            $this->session->set_flashdata('sessionMessage',"Notification sent!");            
        }
        else{
            $this->session->set_flashdata('noticeMessage',"Notification could not be sent. Please try again!");
        }
        $redirect = base_url()."visa/index";
        redirect($redirect);
    }


    //@author Sujit
    //@params none
    //@returns none
    //@GENERAL ADMIN NOTIFICATION FOR VISA APPROVAL BY FINANCE DEPARTMENT
    public function getticketApproval($id=NULL){
        $user_id = $this->session->userdata('user_id');
        if($id==NULL){
            $data['candidate_result'] = $this->Candidate_model->approvalList('ticket');

            $data['approved_candidate'] = $this->Candidate_model->approvedList('ticket');
            $this->template->set_layout('site_layout')->build('admin/ticketing/ticketingApprovalList',$data);
        }
        else{
            $query = $this->db->get_where('tbl_visa_ticketing_approval',array('id'=>$id))->row();
            $candidate_id = $query->candidate_id;
            $select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, tc.perm_address, tc.temp_address,
                    tc.father_name, tc.mother_name, tc.email_id, tc.height, tc.weight, tc.dob, tc.birth_place, tc.language,
                    tc.marital_status,tc.religion,tc.nationality,tc.qualification,tc.experience, tc.training, tc.issued_from,tc.issued_date,
                    tc.expire_date,
                    ta.agent_name, ta.mobile AS agent_mob,
                    tj.job_title, tc1.company_name, tjb.current_status";
            $data['candidate'] = (array)$this->Candidate_model->getGeneralCandidateInfo($candidate_id,$select);
            /*      echo "<pre>";
            var_dump($data['candidate']);
            echo "</pre>";
            die();*/
            $data['candidate_doc'] = (array)$this->Candidate_model->getDocuments($candidate_id);
            $data['preInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Pre-interview');
            $data['finalInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Final-interview');
            $data['medicalResult']=(array)$this->Candidate_model->medicalResult($candidate_id);
            $data['ticketing']=(array)$this->Candidate_model->ticketingResult($candidate_id);
            $data['visa']	=	(array) $this->Candidate_model->visaResult($candidate_id);
            $data['decision']   =   (array)$this->db->query("SELECT * FROM tbl_visa_ticketing_approval WHERE candidate_id = '$candidate_id' AND type='ticket'")->row();
            $this->template->set_layout('site_layout')->build('admin/ticketing/approvalCandidateTicketing',$data);
        }
    }

        //@author Sujit
    //@params none
    //@returns none
    //@APPROVAL DONE BY GENERAL ADMIN
    public function approveTicketing(){
        $candidate_id   =   $this->input->post('candidate_id');
        $result         =   $this->input->post('set_result');
        $remarks        =   $this->input->post('remarks');
        if($result == 1){
            $decision = "Approved";
            $data       =   array('approval' => 1);
            $this->db->where('candidate_id', $candidate_id);
            $this->db->update('tbl_job_candidate',$data);

            $this->session->set_flashdata('sessionMessage','Ticketing has been approved successfully!');
        }else{
            $decision = "Declined";
            $this->session->set_flashdata('noticeMessage','Ticketing has been declined successfully!');
        }
        $data           =   array(
                                'decision'       =>  $decision,
                                'decided_by'    =>  $this->session->userdata('name'),
                                'decided_date'  =>  date('Y-m-d'),
                                'remarks'       =>  $remarks,
                                'sent_flag'     =>  1
                            );
        $where  =   array('candidate_id' => $candidate_id , 'type' => 'ticket');
        $this->db->where($where);
        $this->db->update('tbl_visa_ticketing_approval',$data);
        $redirect = base_url()."ticketing/getticketApproval";
        redirect($redirect);
    }

}