<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mol extends Mol_management_controller {
	
	public function __construct(){
		parent::__construct();

		$this->load->model('mol_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$advertisement= $this->Advertise_model->listAdvertisement();
		if($advertisement){
			$data['advertisement']=$advertisement;
		}
		
		$this->template->set_layout('site_layout')->build('admin/advertisement/index',$data);
	}

	//@author Sudeep
	//@params None
	//@returns None
	//@Used to save new advertisement
	public function molRate(){
		// error_reporting(0);
		$this->load->model('Job_model');
		$jobs=$this->mol_model->selectedJob();
		if($jobs){
			$data['jobs']=$jobs;
		}
		$data['mol'] = $this->db->query('select t1.job_title,t2.id as mol_id,t2.mol_rate,t2.office_rate from tbl_job t1 inner join tbl_job_rate t2 on t1.id=t2.job_id')->result();
		
		$this->template->set_layout('site_layout')->build('admin/mol/addMol',$data);
	}

	public function save(){
		$job=$this->input->post('job');
		$officeRate=$this->input->post('officeRate');
		$mollRate=$this->input->post('mollRate');
		$data=array('job_id'=>$job,
			'office_rate'=>$officeRate,
			'mol_rate'=>$mollRate,
			'approval_flag'=>1
			);
		
		$insert=$this->mol_model->insert($data);
		if($insert){
			echo json_encode(array('success'=>'true','officeRate'=>$officeRate,'mollRate'=>$mollRate));

		}
		
		else{
			echo json_encode(array('success'=>'false'));

		}
		
	}
	
	public function approval(){
		// error_reporting(0);
		$data['approvals'] = $this->db->query('select t1.job_title,t2.id as approval_id,t2.pre_approval_date,t2.final_approval_date,t2.quota_filled from tbl_job t1 inner join tbl_job_rate t2 on t1.id=t2.job_id')->result();
		$this->template->set_layout('site_layout')->build('admin/mol/approval',$data);
	}

	public function approvalForm(){
		// error_reporting(0);
		// $this->load->model('Job_model');
		$jobs=$this->mol_model->preApprovalJobList();
		if($jobs){
			$data['jobs']=$jobs;
		}
		

		// $data['jobs'] = $this->Job_model->multiple_where(array('status'=>0),'id,job_title');
		$this->template->set_layout('site_layout')->build('admin/mol/approvalForm',$data);

	}

	public function getFinalApprovalJob(){
		
		$option=$this->input->post('option');
		if($option==1){
			$jobs=$this->mol_model->preApprovalJobList();
			if($jobs){
				foreach($jobs as $jobData){
					echo "<option value='$jobData->id'>$jobData->job_title</option>";
				}
			}
			else{
				return false;
			}
		}
		else{
			$jobs=$this->mol_model->finalApprovalJobList();
			if($jobs){

				foreach($jobs as $jobData){
					echo "<option value='$jobData->id'>$jobData->job_title</option>";
				}
			}
			else{
				return false;
			}
		}
	}

	public function saveApproval(){
		$approval=$this->input->post('selectApprovalType');
		echo 

		$approvalDate=date("Y-m-d", strtotime($this->input->post('approvalDate')));
		$jobId=$this->input->post('job');
		$pic=$_FILES['userfile']['name'];

		if($pic){
			$path="uploads/approval_file";

			$file=$this->multiple($path);
			if($file['success']){
				$db_file=serialize($file['success']);

			} 
			else{

				$this->session->set_flashdata('sessionMessage',implode($file['unsuccess'])."file could not be inserted");
				$redirect = base_url()."mol/preApprovalForm";
				redirect($redirect);
			}		 
			// $db_unsucess_image = $images['unsucess'];
		}
		else{
			$db_file="";
		}
		if($approval=="1"){
			$data=array('pre_approval_date'=>$approvalDate,
				'pre_approval_file'=>$db_file,
				'job_id'=>$jobId,
				'approval_flag'=>2
				);
			$update=$this->mol_model->update_by('job_id',$jobId,$data);
			if($update){
				$this->session->set_flashdata('sessionMessage','Inserted Successfully');
				$redirect = base_url()."mol/preApprovalForm";
				redirect($redirect);
			}
		}
		else{
			$this->load->model('final_approval_model');
			$candidate_id	=	$this->input->post('candidate');
			$data 	=	array('approval_date'	=>	$approvalDate,
								'file'			=>	$db_file,
								'candidate_id'	=>	$candidate_id
							);
			$insert=$this->final_approval_model->insert($data);
			if($insert){
				$job_candidate_data = array('current_status' => 'final_approval_received');
				$this->db->where('candidate_id',$candidate_id)->update('tbl_job_candidate',$job_candidate_data);
				
				$this->session->set_flashdata('sessionMessage','Final Approval record inserted Successfully');
				$redirect = base_url()."mol/finalApprovalForm";
				redirect($redirect);
			}
		}
	}

	//@author Sudeep
	//@params None
	//@returns None
	//@Used to get pre-approval list
	public function preApproval(){
		$this->load->model('Job_model');
		$data['jobs']=$this->Job_model->get_all_data('id,job_title');//get job list to view the pdf of the selected job in pre-approval modal
		$data['approvalList']=$this->mol_model->getPreApprovalList();//get the pre approval list
		$this->template->set_layout('site_layout')->build('admin/mol/pre-approval',$data);

	}

	//@author Sudeep
	//@params None
	//@returns None
	//@Used to get pre-approval Form
	public function preApprovalForm(){
		$data['jobs']=$this->mol_model->preApprovalJobList();
		$this->template->set_layout('site_layout')->build('admin/mol/pre_approval_form',$data);

	}


	public function getJobPdf(){
		$this->load->helper('pdf_helper');
		// $this->load->model('Job_model');
		if($this->input->post()){
			$job_id = $this->input->post('selectJob');
			$query = $this->db->get_where('tbl_job',array('id' => $job_id));
			$data['result'] = (array)$query->row();
			$this->load->view('job_pdf',$data);
		}

	}


	//@author Sudeep
	//@params None
	//@returns None
	//@Used to get final-approval list
	public function finalApproval(){
		$this->load->model('candidate_model');
		$this->load->model('final_approval_model');
		$data['candidate'] = $this->candidate_model->get_all_data('id,candidate_name');
		$data['finalApprovalList'] = $this->final_approval_model->finalApprovalList();
		// $data['jobs']=$this->Job_model->get_all_data('id,job_title');//get job list to view the pdf of the selected job in pre-approval modal
		$this->template->set_layout('site_layout')->build('admin/mol/final-approval',$data);

	}

	public function getCandidatePdf()
	{
		$this->load->helper('pdf_helper');
		$this->load->model('Candidate_model');
		if($this->input->post()){
			$candidate_id = $this->input->post('candidate');
			$data['candidateDetail']=(array)$this->Candidate_model->get($candidate_id);
			$this->load->view('admin/pdf/candidate',$data);
		}	
		
	}


	//@author Sudeep
	//@params None
	//@returns None
	//@Used to get final-approval Form
	public function finalApprovalForm(){
		$this->load->model('final_approval_model');
		$data['candidate']=$this->final_approval_model->generateFinalApprovalCandidateList();
		$this->template->set_layout('site_layout')->build('admin/mol/final_approval_form',$data);
	}


}