<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Transaction extends Base_controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Transaction_model');
		$this->load->model('Category_model');
		$this->load->model('Bank_model');
		$this->load->model('Candidate_model');
		$this->load->model('Cash_model');
		$this->load->model('Idgenerator');
		$this->load->library('form_validation');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['transaction'] = $this->Transaction_model->listTransaction();
		$this->template->set_layout('site_layout')->build('admin/transaction/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new Transaction
	public function newTransaction(){
		$data['income_category'] = $this->Category_model->getCategory('income');
		$data['expense_category'] = $this->Category_model->getCategory('expense');
		$data['bank'] = $this->Bank_model->get_all();
		$data['candidate'] = $this->Candidate_model->getCandidate();
		$cash_result = (array)$this->Cash_model->getCash();
		$data['cash_id'] = $cash_result['id'];
		$data['current_cash'] = $cash_result['amount'];
		$this->template->set_layout('site_layout')->build('admin/transaction/newTransaction',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO GET DATA FOR NEW TRANSACTION
	public function getData(){
		$fetch = $this->input->post('fetchData');
		$type = $this->input->post('type');
		if($type == "candidate"){
			$data['candidate_result'] = $this->Candidate_model->getCandidate();
			$this->load->view('ajax/ajax_candidate_list',$data);
		}
		else
		{
			echo "a";
		}
	}


	//@author Sujit
	//@params None
	//@returns None
	//@USED TO GET DATA FOR NEW TRANSACTION
	public function getSource(){
		$fetch = $this->input->post('fetchData');
		$type = $this->input->post('type');
		if ($fetch == "income" || $fetch == "expense" || $fetch == "advance"){
			$data['result'] = $this->Category_model->getFilteredCategory($fetch,$type);
			$this->load->view('ajax/ajax_source',$data);
		}
		else{
			echo "a";
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save the transactions
	public function save(){
		$transaction_type = $this->input->post('transaction_type');
		$amount = $this->input->post('amount');
		$transaction_date = date("Y-m-d", strtotime($this->input->post('transaction_date')));
		$remarks = $this->input->post('remarks');
		$voucher_no = '';
		$income_cheque_no = '';
		$cheque_no = '';
		$sent_flag = 0;
		$transaction_no = $this->Transaction_model->genId($transaction_type);
		$updated_by = $this->session->userdata('username');
		//die();

		//Getting information of current cash in hand
		$cash_result = (array)$this->Cash_model->getCash();
		$cash_id = $cash_result['id'];
		$current_cash = $cash_result['amount'];

		$config = array(
               array(
                     'field'   => 'transaction_date',
                     'label'   => 'transaction_date',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'amount',
                     'label'   => 'amount',
                     'rules'   => 'required|decimal|min_length[1]'
                  )
            );

		$this->form_validation->set_rules($config); 

		if($this->form_validation->run() == TRUE){
		
			if($transaction_type == "income"){

				$payment_type = $this->input->post('payment-type');
				
				$bank_id = $this->input->post('bank_id');

				$income_category_type = $this->input->post('income_category_type');
				$category_id = $this->input->post('source');

				$candidate_id = $this->input->post('income_candidate');

				//Identifying whether the income is cash or bank
				if($payment_type == "Cash" || $payment_type == "Cheque"){
					$income_cheque_no = $this->input->post('voucher_no');
					$new_cash = $current_cash + $amount;
					$data = array(
							'amount' => $new_cash
							);
					$this->Cash_model->update($cash_id,$data);
					$status = 1;
				}
				elseif($payment_type == "Deposit"){
					$voucher_no = $this->input->post('voucher_no');
					$cheque_no = '';
					$field = "id,balance";
					$bank_result = $this->Bank_model->get_data($field,$bank_id);
					$current_balance = $bank_result->balance;
					$new_balance = $current_balance + $amount;
					$data = array('balance' => $new_balance);
					$this->Bank_model->update($bank_id,$data);
					$status = 1;
				}
			}elseif($transaction_type == "expense"){
				$payment_type = $this->input->post('expense_payment_type');
				/*$expense_category_type = $this->input->post('expense_category_type');
				$expense_category_id = $this->input->post('expense_category');*/
				$category_id = $this->input->post('expense_for');
				$candidate_id = $this->input->post('expense_candidate');
				$bank_id = '';
				$sent_flag = 1;

				if($payment_type == "Cash"){
					//$cheque_no = '';
					//$new_cash = $current_cash - $amount;
					/*$data = array(
							'amount' => $new_cash
							);
					*///$this->Cash_model->update($cash_id,$data);
					//$status = 1;
				}
				elseif($payment_type == "Cheque"){
					$bank_id = $this->input->post('expense_bank_id');
					$cheque_no = $this->input->post('cheque_no');
					//$field = "id,balance";
					//$bank_result = $this->Bank_model->get_data($field,$bank_id);
					//$bank_balance = $bank_result->balance;
					//$new_balance = $bank_balance - $amount;
					//$data = array('balance' => $new_balance);
					//$this->Bank_model->update($bank_id,$data);
				}
				$expense_category_id = $this->input->post('expense_category');
			}
			else{
				$payment_type = $this->input->post('adv-payment-type');			
				$bank_id = $this->input->post('adv_bank_id');
				$category_id = $this->input->post('advance_source');
				$candidate_id = $this->input->post('advance_candidate');
				if($payment_type == "Cash" || $payment_type == "Cheque"){
					$income_cheque_no = $this->input->post('adv_voucher_no');
					$new_cash = $current_cash + $amount;
					$data = array(
							'amount' => $new_cash
							);
					$this->Cash_model->update($cash_id,$data);
					$status = 1;
				}else{
					$voucher_no = $this->input->post('adv_voucher_no');
					$cheque_no = '';
					$field = "id,balance";
					$bank_result = $this->Bank_model->get_data($field,$bank_id);
					$current_balance = $bank_result->balance;
					$new_balance = $current_balance + $amount;
					$data = array('balance' => $new_balance);
					$this->Bank_model->update($bank_id,$data);
					$status = 1;
				}
				if($candidate_id != ""){
					$job_data = array('advanced' => 1);
					$this->db->where('candidate_id',$candidate_id);
					$this->db->update('tbl_job_candidate',$job_data);
				}
			}
			$data = array(
				'transaction_no' => $transaction_no,
				'category_id' => $category_id,
				'amount' => $amount,					
				'transaction_date' => $transaction_date,
				'type' => $transaction_type,
				'subtype' => $payment_type,
				'bank_id' => $bank_id,
				'cheque_no' => $cheque_no,
				'voucher_no' => $voucher_no,
				'income_cheque_no' => $income_cheque_no,
				'bank_id' => $bank_id,
				'remarks' => $remarks,
				'candidate_id' => $candidate_id,
				'sent_flag' => $sent_flag,
				'updated_by' => $updated_by
				);
			$this->Transaction_model->insert($data);
			$this->session->set_flashdata('sessionMessage','Transaction recorded successfully!');
			$redirect = base_url()."transaction/index/";
			redirect($redirect);
		}
		else{
			$data['income_category'] = $this->Category_model->getCategory('income');
			$data['expense_category'] = $this->Category_model->getCategory('expense');
			$data['bank'] = $this->Bank_model->get_all();
			$data['candidate'] = $this->Candidate_model->getCandidate();
			$cash_result = (array)$this->Cash_model->getCash();
			$data['cash_id'] = $cash_result['id'];
			$data['current_cash'] = $cash_result['amount'];
			$this->template->set_layout('site_layout')->build('admin/transaction/newTransaction',$data);
		}
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to entry cash in hand for the first time
	public function setCash(){
		$year = date('Y');
		$month = date('m');
		$amount = $this->input->post('balance');
		$data = array(
				'amount' => $amount,
				'year' => $year,
				'month' => $month
				);
		$this->db->insert('tbl_cash',$data);
		$this->session->set_flashdata('sessionMessage','1');
		$redirect = base_url().'admin/index';
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@used to delete the transaction
	public function delTrans(){
		$transaction_id = base64_decode($this->uri->segment(3));
		$transaction = (array)$this->Transaction_model->listTransaction($transaction_id);
		$type = $transaction['type'];
		$amount = $transaction['amount'];
		$subtype = strtolower($transaction['subtype']);


		$cash_result = (array)$this->Cash_model->getCash();
		$cash_id = $cash_result['id'];
		$current_cash = $cash_result['amount'];
		
		if($type == "income"){			
			if($subtype == "cash" || $subtype == "cheque"){
				$new_cash = $current_cash - $amount;
				$data = array(
						'amount' => $new_cash
						);
				$this->Cash_model->update($cash_id,$data);
			}elseif ($subtype == "deposit"){
				$current_bank_balance = $transaction['balance'];
				$new_balance = $current_bank_balance - $amount;
				$data = array('balance' => $new_balance);
				$bank_id = $transaction['bank_id'];
				$this->Bank_model->update($bank_id,$data);
			}
			$this->Transaction_model->delete($transaction_id);
			$this->session->set_flashdata('sessionMessage','Transaction deleted successfully!');
		}
		elseif($type == "expense") {
			if($subtype == "cash"){
				$new_cash = $current_cash + $amount;
				$data = array(
						'amount' => $new_cash
						);
				$this->Cash_model->update($cash_id,$data);
				
			}elseif ($subtype == "cheque"){
				$current_bank_balance = $transaction['balance'];
				$new_balance = $current_bank_balance + $amount;
				$data = array('balance' => $new_balance);
				$bank_id = $transaction['bank_id'];
				$this->Bank_model->update($bank_id,$data);
			}
			$this->Transaction_model->delete($transaction_id);
			$this->session->set_flashdata('sessionMessage','Transaction deleted successfully!');
		}else{
			$this->session->set_flashdata('noticeMessage','Transaction could not be deleted. Please try again!');
		}
		$redirect = base_url()."transaction/index";
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@used to get specific transaction
	public function loadTransaction(){
		$id = $this->input->post('id');
		$data['transaction'] = (array)$this->Transaction_model->listTransaction($id);
		$data['contentType'] = "transaction";
		$this->load->view('ajax/ajax_content',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@used to get specific transaction
	public function getTransaction($id=NULL){
		$user_id = $this->session->userdata('user_id');
		if($id==NULL){
			$data['transactions'] = $this->Transaction_model->listApprovalTransactions();
			$query = $this->db->query("SELECT tbl_transaction.*, tb.id as 'bank_id', tb.bank_name, tb.balance, tc.candidate_name, 
							tcl.category_title, tcl.subtype as 'category_sub',
							ta.user_id, ta.decision
						FROM tbl_transaction
							INNER JOIN tbl_category_transaction AS tcl
								ON tcl.id = tbl_transaction.category_id
							LEFT JOIN tbl_bank AS tb
								ON tb.id = tbl_transaction.bank_id
							LEFT JOIN tbl_candidate AS tc
								ON tc.id = tbl_transaction.candidate_id
							LEFT JOIN tbl_finance_approval AS ta
								ON ta.transaction_id = tbl_transaction.id	
								AND ta.user_id = (SELECT id FROM tbl_user WHERE role_id = 2)
							WHERE tbl_transaction.type = 'expense' ORDER BY transaction_date DESC");
			$data['finance_transactions'] = $query->result();
			$this->template->set_layout('site_layout')->build('admin/transaction/transactionApprovalList',$data);
		}
		else{
			$data['row'] = (array)$this->Transaction_model->listApprovalTransactions($id);
			$data['approval_status'] = (array)$this->db->query("SELECT * FROM tbl_finance_approval 
																WHERE transaction_id = $id AND user_id = $user_id")->row();

			$query = $this->db->query("SELECT t1.name, t2.decision, t2.remarks, t2.date
											FROM tbl_user t1
											LEFT JOIN tbl_finance_approval t2
												ON t2.user_id = t1.id
											WHERE (t1.role_id = 2 OR t1.role_id = 3)
												AND t2.transaction_id = $id
										");

			$data['others_decision'] = $query->result();
			$this->template->set_layout('site_layout')->build('admin/transaction/transactionDetail',$data);
		}		
	}

	//@author Sujit
	//@params None
	//@returns None
	//@used to set approval and decline for transaction
	public function saveApproval(){
		$transaction_id = $this->input->post('transaction_no');

		$set_value = $this->input->post('set_result');
		$remarks = $this->input->post('remarks');

		$user_role = $sessionRole=strtolower($this->session->userdata('role'));
		$user_id = $this->session->userdata('user_id');


		$cash_result = (array)$this->Cash_model->getCash();
		$cash_id = $cash_result['id'];
		$current_cash = $cash_result['amount'];


		//echo $set_value;
		//For Approved Condition
		if($set_value == 1){
			$decision = "Approved";
		}else{
			$decision = "Declined";
		}

		$data = array('transaction_id' => $transaction_id,
							'user_id' => $user_id,
							'decision' => $decision,
							'remarks' => $remarks,
							'date' => date('Y-m-d')
						 );
		$this->db->insert('tbl_finance_approval',$data);

		if($user_role == "system admin"){

			if($set_value == 1){

				$transaction = $this->db->get_where('tbl_transaction',array('id' => $transaction_id))->row();
				$payment_type = $transaction->subtype;
				$amount = $transaction->amount;
							
				if($payment_type == "Cash"){
					$cheque_no = '';
					$new_cash = $current_cash - $amount;
					$data = array(
							'amount' => $new_cash
							);
					$this->Cash_model->update($cash_id,$data);
					$status = 1;
				}
				elseif($payment_type == "Cheque"){
					$bank_id = $transaction->bank_id;
					$cheque_no = $transaction->cheque_no;
					$field = "id,balance";
					$bank_result = $this->Bank_model->get_data($field,$bank_id);
					$bank_balance = $bank_result->balance;
					$new_balance = $bank_balance - $amount;
					$data = array('balance' => $new_balance);
					$this->Bank_model->update($bank_id,$data);
				}
				$new_data = array('sent_flag' => 2);
				$this->db->where('id',$transaction_id)->update('tbl_transaction',$new_data);
			}			
		}
		$this->session->set_flashdata('msg',"Transaction decision saved successfully!");
		$redirect = base_url()."transaction/getTransaction/".$transaction_id;
		redirect($redirect);
	}

	// @author sudeep and sujit
	// @params id of the transaction
	public function viewPdf(){
		$this->load->helper('pdf_helper');
		$date=date("Y-m-d", strtotime($this->input->post('transactionDate')));
		$data['income']=$this->Transaction_model->selectByDate($date,'income');
		$data['expence']=$this->Transaction_model->selectByDate($date,'expense');
		$data['advance']=$this->Transaction_model->selectByDate($date,'advance');
		
		$this->load->view('admin/pdf/transaction',$data);
	}
}
