<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends Base_controller {
	
	//@author Sujit
	//@params None
	//@returns None
	public function getTransactionReport(){
		$current_year = date("Y");
		$cash_query = $this->db->get_where('tbl_cash',array('year' => $current_year));
		$data['cash_result'] = $cash_query->result();

		$year_query = $this->db->query("SELECT DISTINCT(year) AS `year` FROM tbl_cash");
		$data['year'] = $year_query->result();
		$this->load->view('admin/report/index',$data);
	}

	public function getReport(){
		$year = $this->input->post('year');
		$cash_query = $this->db->get_where('tbl_cash',array('year' => $year));
		$data['cash_result'] = $cash_query->result();
		$this->load->view('ajax/ajax_report',$data);
	}

	public function getCandidateReport(){
		$candidate_query = $this->db->get('tbl_candidate');
		$data['candidates'] = $candidate_query->result();
		$this->load->view('admin/report/candidateReport',$data);
	}

	public function getCandidateReportById(){
		$candidate_id = $this->input->post('candidate_id');
		$income_query = $this->db->query("SELECT ti.*,tb.bank_name
											FROM tbl_income_transaction AS `ti`
											LEFT JOIN tbl_bank AS `tb` ON ti.bank_id = tb.bank_id
											WHERE ti.candidate_id = \"$candidate_id\"
										");

		$expense_query = $this->db->query("SELECT ti.*,tb.bank_name
											FROM tbl_expense_transaction AS `ti`
											LEFT JOIN tbl_bank AS `tb` ON ti.bank_id = tb.bank_id
											WHERE ti.candidate_id = \"$candidate_id\"
										");
		$data['income_no'] = $income_query->num_rows();
		$data['expense_no'] = $expense_query->num_rows();
		$data['incomes'] = $income_query->result();
		$data['expenses'] = $expense_query->result();
		$data['candidate_id'] = base64_encode($candidate_id);

		//$candidate_query = $this->db->get_where('tbl_candidate',array('candidate_id' => $candidate_id));
		$this->db->select('tbl_candidate.*, tbl_job.job_title, tbl_agent.agent_name')->from('tbl_candidate')->where('candidate_id',$candidate_id);
		$this->db->join('tbl_job', 'tbl_job.job_id = tbl_candidate.job_id','left');		
		$this->db->join('tbl_agent', 'tbl_agent.agent_id = tbl_candidate.agent_id','left');
		$candidate_query = $this->db->get();
		$data['candidate'] = $candidate_query->row();

		$this->load->view('ajax/ajax_candidate_report',$data);		
	}

}