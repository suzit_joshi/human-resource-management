<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company extends Office_management_controller{
	
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$this->db->select('tbl_company.*,tbl_country.country_name')->from('tbl_company')->where('tbl_company.status','1')->join('tbl_country', 'tbl_country.id = tbl_company.country_id');
		$query = $this->db->get();

		/*$query = $this->db->query("SELECT tbl_company.*, tbl_country.country_name, (SELECT count(*) FROM tbl_job_candidate
																					WHERE tbl_job_candidate.job_id = tbl_job.id) AS candidate_count
										FROM tbl_company
											INNER JOIN tbl_country
												ON tbl_country.id = tbl_company.country_id
											LEFT JOIN tbl_job
												ON tbl_job.company_id = tbl_company.id
										GROUP BY tbl_company.company_name


										");*/


$data['result_company'] = $query->result();

/*		echo "<pre>";

		var_dump($data['result_company']);
		echo "</pre>";
		die();*/
		/*$this->load->view('admin/company/index',$data);*/
		$this->template->set_layout('site_layout')->build('admin/company/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to redirect into new form to create company
	public function createCompany(){
		$query = $this->db->get_where('tbl_country', array('status' => '1'));
		$data['result_country'] = $query->result();
		$this->template->set_layout('site_layout')->build('admin/company/newcompany',$data);
		//$this->load->view('admin/company/newcompany',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit a company
	public function save(){
		$this->load->model('company_model');

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('address','Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('country','Country', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description','Description', 'trim|xss_clean');

		$company_name = $this->input->post('company_name');
		$id = $this->input->post('company_id');
		$email = $this->input->post('email');

		if($email!=""){
			$this->form_validation->set_rules('email','Email', 'trim|required|xss_clean');
		}
		if($id!=""){
			
			$comp=$this->company_model->get(base64_decode($id));
			
			if($company_name!=$comp->company_name){
				$this->form_validation->set_rules('company_name','Company Name', 'trim|required|xss_clean|is_unique[tbl_company.company_name]');
			}

		}else{
			$this->form_validation->set_rules('company_name','Company Name', 'trim|required|xss_clean|is_unique[tbl_company.company_name]');
		}

		if ($this->form_validation->run() == FALSE)
		{

			$this->createCompany();
		}
		else{	
			
			
			$address = $this->input->post('address');
			$website = $this->input->post('website');
			$country = $this->input->post('country');
			$description = $this->input->post('description');


			$old_img = $this->input->post('old_img');

			$tmp_img_name = $_FILES['image']['name'];
			$path = "uploads/company";
			$img_ret = $this->image_upload($tmp_img_name,$path,'image');
			if($img_ret['status']){
				$company_logo = $img_ret['msg'];
			}
			else{
				$company_logo = '';
				$error_msg = $img_ret['msg'];
			}
			
			
			$data = array('company_name' => $company_name,
				'address' => $address,
				'email_id' => $email,
				'website' => $website,
				'country_id' => $country,
				'description' => $description,
				'company_logo' => $company_logo,
				'updated_by' => $this->session->userdata('name')
				);

			
			if(isset($error_msg)){
				$this->session->set_flashdata('noticeMessage',$error_msg);
			}

			if($id == ""){
				$data['status'] = '1';
				$this->db->insert('tbl_company',$data);
				$this->session->set_flashdata('sessionMessage','New company created successfully!!');
				
			}
			else{
				if(!isset($error_msg)){
					$remove_path = "uploads/company/".$old_img;
					unlink($remove_path);
				}
				
				$company_id = base64_decode($id);
				$this->db->where('id',$company_id);
				$this->db->update('tbl_company',$data);
				$this->session->set_flashdata('sessionMessage','Company details updated successfully!!');
			}
			$redirect = base_url()."company/index";
			redirect($redirect);

			
		}			

	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific company according to the id
	public function getInformation(){
		$company_id = base64_decode($this->uri->segment(3));
		
		$query = $this->db->get_where('tbl_company',array('id' => $company_id));
		$data['result'] = $query->result();
/*		echo $this->db->last_query();

		var_dump($data['result']);
		die();*/

		$country_query = $this->db->get_where('tbl_country', array('status' => '1'));
		$data['result_country'] = $country_query->result();
		//$this->load->view('admin/company/newcompany',$data);
		$this->template->set_layout('site_layout')->build('admin/company/newcompany',$data);

	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific company according to the id
	public function deactivate(){
		$company_id = base64_decode($this->uri->segment(3));
		$data['status'] = '0';
		$this->db->where('id',$company_id);
		$this->db->update('tbl_company',$data);
		$redirect = base_url()."company/index"."/delsuccess";
		redirect($redirect);

	}

}