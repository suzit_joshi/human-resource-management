<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends Finance_management_controller{
	public function __construct(){
        parent::__construct();
        $this->load->model('Category_model');
    }
    
    
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		//die();
		$data['result_income'] = $this->Category_model->getCategory('income');
		$data['result_expense'] = $this->Category_model->getCategory('expense');
		$this->template->set_layout('site_layout')->build('admin/category/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new Category
	public function newCategory(){
		$type = $this->input->post('category_type');
		$title = $this->input->post('category_title');
		$heading = $this->input->post('category_heading');
		$data = array(
					'category_title' => $title,
					'type' => $type,
					'subtype' => $heading
					);
		$this->Category_model->insert($data);
		if($type == "income"){
			$data['flag'] = 1;
		}else{
			$data['flag'] = 2;
		}
		$data['result_income'] = $this->Category_model->getCategory('income');
		$data['result_expense'] = $this->Category_model->getCategory('expense');
		$this->load->view('ajax/ajax_category',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit category
	public function editCategory(){
		$category_id = $this->input->post('category_id');
		$type = $this->input->post('category_type');
		$title = $this->input->post('category_title');
		$heading = $this->input->post('category_heading');

		$data = array(
					'category_title' => $title,
					'subtype' => $heading
					);
		$this->Category_model->update($category_id,$data);
		
		if($type == "income"){
			$data['flag'] = 1;
		}else{
			$data['flag'] = 2;
		}
		$data['result_income'] = $this->Category_model->getCategory('income');
		$data['result_expense'] = $this->Category_model->getCategory('expense');;
		$this->load->view('ajax/ajax_category',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and delete category
	public function delCategory(){
		$id = base64_decode($this->uri->segment(3));
		$data = array('status' => '1');
		if($this->Category_model->update($id,$data)){
			$this->session->set_flashdata('sessionMessage','Category deleted successfully!');
		}else{
			$this->session->set_flashdata('noticeMessage','Category could not be updated. Please try again!');
		}
		$redirect = base_url().'category/index';
		redirect($redirect);
	}

}