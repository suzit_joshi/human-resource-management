<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

    //@author suzit
    //@params none
    //@returns none

    public function index() {
        $username = mysql_real_escape_string($this->input->post('username'));
        $password = mysql_real_escape_string($this->input->post('password'));
        $type = $this->input->post('type');

        if($type == "desktop"){
            $flag = 1;
        }elseif($type == "app"){
            $flag = 2;
        }else{
            $flag = 3;
        }

        if($flag == 1 || $flag == 2){
            $salt = "wdvhyf@%$@fgfrtyqq";
            $salt .= $password;
            $password = $salt;
            $password = md5($password);
            //defining the conditions
            $where_array = array('username' => $username, 'password' => $password, 'tbl_user.status' => '1' );
            //method chaining
            $select="tbl_user.id as user_id,tbl_role.role_name,tbl_user.username,tbl_user.password, tbl_user.name,tbl_user.branch_id";
            $query_user = $this->db->select($select)->from('tbl_user')->where($where_array)->join('tbl_role','tbl_user.role_id = tbl_role.id')->get();

            if($query_user->num_rows() > 0) {
                foreach($query_user->result() as $rowUser) {
                    if ($password == $rowUser->password) {
                        $result = $query_user->row();
                        $this->session->set_userdata('user_id', $result->user_id);
                        $this->session->set_userdata('role', $result->role_name);
                        // $this->session->set_userdata('role_id', $result->role_id);
                        $this->session->set_userdata('username', $result->username);
                        $this->session->set_userdata('name',$result->name);
                        $this->session->set_userdata('branch',$result->branch_id);
                       	$this->session->set_userdata('hrm_login_status','1');
                        if($flag == 1){
    					   $redirect = base_url()."admin";
                        }else{
                            $redirect = base_url()."mobileController/index";
                        }
                        redirect($redirect);
                    }
                }
            } else {
                $this->session->set_userdata('Errormsg', 'Incorrect username and password');
                $redirect = base_url()."_cpanel";
                redirect($redirect);
            }
        }else{
            $this->session->set_userdata('Errormsg', 'Error during form submission! Try again.');
            $redirect = base_url()."_cpanel";
            redirect($redirect);
        }
    }
}