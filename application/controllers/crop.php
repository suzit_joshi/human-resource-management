<?php

class Crop extends CI_Controller{
    // Index function would be called automatically
    public function index(){
            $this->load->view('crop');
        
    }
	
	public function imgcrop(){
		$image_name = $this->input->post('image_name');
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
		
			$targ_w = $targ_h = 150;
			$jpeg_quality = 90;
		
			$src = base_url()."tmp/".$image_name;
			$img_r = imagecreatefromjpeg($src);
			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
		
			imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			$targ_w,$targ_h,$_POST['w'],$_POST['h']);
		
			header('Content-type: image/jpeg');
			$location = './uploads/agent/'.$image_name;

			imagejpeg($dst_r,$location,$jpeg_quality);
			echo "<img id='img-cropped' title='".$image_name."'src='".base_url()."uploads/agent/".$image_name."'>";
		}
	}
	
	public function imgupload(){
		$config['upload_path'] = './tmp/';
		$config['allowed_types'] = 'jpg|jpeg|JPEG|JPG';
		$config['max_size']	= '20000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('ajax/imgview', $data);
		}
	}
}
?>