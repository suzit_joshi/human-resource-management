<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Interview extends Interview_management_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Interview_model');
        $this->load->model('Candidate_model');
        $this->load->model('Job_model');
        $this->load->model('General_notification_model');
        

	}
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['pre_interviews'] = $this->Interview_model->getInterview('Pre-interview');
        $data['final_interviews'] = $this->Interview_model->getInterview('Final-interview');
        $this->template->set_layout('site_layout.php')->build('admin/interview/index',$data);
	}
    
    //@author Sujit
	//@params None
	//@returns None
    //@View to load new interview form
	public function newInterview(){
        $select = "tbl_job.id AS job_id, tbl_job.job_title, tbl_job.pre_interview_to_date, tbl_job.pre_interview_for_date,
                    tbl_job.final_interview_to_date, tbl_job.final_interview_for_date, tbl_job.final_interviewer";
        $data['jobs'] = $this->Job_model->getJobInfo($select);
        $this->template->set_layout('site_layout.php')->build('admin/interview/newinterview',$data);
	}
    
    //@author Sujit
	//@params None
	//@returns None
    //@Save the interview
	public function save(){
        if($this->input->post('interview-submit')){
            $id = base64_decode($this->input->post('random'));
            $type = $this->input->post('type');
            $candidate = $this->input->post('candidate');
            $interviewee = $this->input->post('interviewee');
            $date = date('Y-m-d',strtotime($this->input->post('date')));
            $result = $this->input->post('result');

            if($result == "1"){
                if($type == "Pre-interview"){
                    $current_status = "preinterview_passed";
                }
                else{
                    $current_status = "finalinterview_passed";
                }
                $job_candidate_data = array('current_status' => $current_status , 'data_entry' => 1);
            }
            elseif($result == 2){
                 $current_status = "finalinterview_withheld";
                 $job_candidate_data = array('current_status' => $current_status , 'data_entry' => 1);
            }
            else{
                $job_candidate_data = array('current_status' => "failed", 'remarks' => 'Interview Failed');
            }
            $this->db->where('candidate_id',$candidate)->update('tbl_job_candidate',$job_candidate_data);

            $remarks = $this->input->post('description');
            $data = array(
                            'candidate_id' => $candidate,
                            'interview_date' => $date,
                            'taken_by' => $interviewee,
                            'result' => $result,
                            'type' => $type,
                            'remarks' => $remarks,
                            'updated_by' => $this->session->userdata('name')
                            );
            if($id == ""){
                if($this->Interview_model->insert($data)){
                    $this->session->set_flashdata('sessionMessage','Interview record has been added successfully!');
                    $redirect = base_url()."interview/index";
                }
                else{
                     $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
                    $redirect = base_url()."interview/newInterview";
                }
            }
            else{
                if($this->Interview_model->update($id,$data)){
                    $this->session->set_flashdata('sessionMessage','Interview record has been edited successfully!');
                    $redirect = base_url()."interview/index";
                }
                else{
                     $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
                    $redirect = base_url()."interview/newInterview";
                }
                
            }
        }
        else{
            $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
            $redirect = base_url()."interview/newInterview";
        }
        redirect($redirect);
	}
    
    //@author Sujit
	//@params None
	//@returns None
	public function getInterview(){
        $id = $this->input->post('id');
        $type = $this->input->post('type');
        $data['interview'] = (array)$this->Interview_model->getInterview($type,$id);
        $data['contentType'] = "interview";
        $this->load->view('ajax/ajax_content',$data);        
	}
    
    //@author Sujit
	//@params None
	//@returns None
	public function editInterview(){
        $id = base64_decode($this->uri->segment(3));
        $data['interview'] = (array)$this->Interview_model->getInterview(NULL,$id);
        $data['candidates'] = $this->Candidate_model->getCandidate();
        $this->template->set_layout('site_layout.php')->build('admin/interview/newinterview',$data);
	}
    
    //@author Sujit
	//@params None
	//@returns None
    public function getCandidateList(){
        $type = $this->input->post('type');
        $job_id = $this->input->post('job');
        if($type == "Pre-interview"){
            $data['candidate_result'] = $this->Interview_model->getPreinterviewList($job_id);
        }
        else{
            $data['candidate_result'] = $this->Interview_model->getFinalInterviewList($job_id);
        }
        $data['type'] = "interview";
        $this->load->view('ajax/ajax_candidate_list',$data);
    }


    //@author Sujit
    //@params None
    //@returns None
    public function getTransferList($type){
        //applied status
        if($type == "pre"){
            $status = "applied";
            $data['type'] = "pre";
        }elseif($type == "final"){
            $status = "preinterview_passed";
            $data['type'] = "final";
        }else{
            $this->session->set_flashdata('noticeMessage',"Wrong type inserted please try again!");
            $redirect = base_url()."interview/index";
            redirect($redirect);
        }
        $data['candidates'] = $this->Candidate_model->getCandidateList($status);

        $this->template->set_layout('site_layout')->build('admin/interview/interviewApprovalList',$data);
    }

    //@author Sujit
    //@params None
    //@returns None
    public function sendNotification(){
        //on-medical status
        $candidates = $this->input->post('candidate');
        $no_candidate = count($candidates);
        $type = $this->input->post('type');
       
        if($this->input->post('frm_submit')){
            if($type == "pre"){
                $remarks = "Pre Interview passed candidate list";
                $new_status = "preinterview_passed"; 

                $this->session->set_flashdata('sessionMessage',"Pre-interview passed candidates shortlisted!");  
            }
            else{
                $remarks = "Final Interview passed candidate list";
                $new_status = "finalinterview_passed"; 

                $data = array(    
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'documentation',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        ),
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'general admin',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        )
                    );

            $this->General_notification_model->insert_many($data);            
            $this->session->set_flashdata('sessionMessage',"Notification sent!");  
            }
            foreach ($candidates as $row => $value) {
                $update_data = array('current_status' => $new_status , 'data_entry' => 0);
                $this->db->where('candidate_id', $value);
                $this->db->update('tbl_job_candidate',$update_data);
            }                      
        }
        else{
            $this->session->set_flashdata('noticeMessage',"Notification could not be sent. Please try again!");
        }
        $redirect = base_url()."interview/index";
        redirect($redirect);
    }


    public function getInterviewDate(){
        $job_id = $this->input->post('jobId');
        $data['type'] =  $this->input->post('type');
        $data['jobs'] = (array)$this->Job_model->getJob($job_id);
        $this->load->view('ajax/ajax_job_dates',$data);

    }

}
/*END OF FILE INTERVIEW.PHP*/