<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank extends Finance_management_controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Bank_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['result_bank'] = $this->Bank_model->get_all();
		$this->template->set_layout('site_layout')->build('admin/bank/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new bank
	public function newBank(){
		$bank = $this->input->post('bank');
		$balance = $this->input->post('balance');
		$data = array(
					'bank_name' => $bank,
					'balance' => $balance
			);
		$this->db->insert('tbl_bank',$data);
		$data['result'] = $this->Bank_model->get_all();
		$this->load->view('ajax/ajax_bank',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit an agent
	public function editBank(){
		$bank_id = $this->input->post('bank_id');
		$bank_name = $this->input->post('bank');

		$data = array('bank_name' => $bank_name);
		$this->Bank_model->update($bank_id,$data);
		$data['result'] = $this->Bank_model->get_all();
		$this->load->view('ajax/ajax_bank',$data);
	}

}