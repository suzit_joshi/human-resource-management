<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Branch extends Office_management_controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('branch_model');
		$this->load->library('form_validation');
			
	}
		


	public function index(){
		$data['branch']=$this->branch_model->get_many_by('status',0);
		$this->template->set_layout('site_layout')->build('admin/company_branch/index',$data);
	}

	public function pdf(){
	$this->load->helper('pdf_helper');
	$this->load->view('test');
	}
	public function createBranch(){
		$this->checkUserRole();
		$this->template->set_layout('site_layout')->build('admin/company_branch/newBranch');
	}





	public function save(){
		$this->checkUserRole();
		$company_id=$this->input->post('company_id');


		$this->form_validation->set_rules('branchName','branch_name','required');
		$this->form_validation->set_rules('managerName','manager_name','');
		$this->form_validation->set_rules('address','address','');
		$this->form_validation->set_rules('phone','phone','integer|max_length[10]');

		if($company_id == ""){
			$this->form_validation->set_rules('email','email_id','valid_email|is_unique[tbl_company_branch.email]');
		}else{
			$this->form_validation->set_rules('email','email_id','valid_email|callback_edit_unique[tbl_company_branch.email.'.$company_id .']');
		}

		if($this->form_validation->run() == TRUE){
			$branch_name=$this->input->post('branchName');
			$address=$this->input->post('address');
			$phone=$this->input->post('phone');
			$managerName=$this->input->post('managerName');
			$email=$this->input->post('email');
			$data=array('branch_name' => $branch_name,
				'address'=>$address,
				'phone'=>$phone,
				'manager_name' => $managerName,
				'email' => $email,
				'updated_by' => $this->session->userdata('name')
				);
			if($company_id!=""){

				$query=$this->branch_model->update($company_id,$data);
				$this->session->set_flashdata('sessionMessage','Updated Successfully');
			}
			else{

				$query=$this->branch_model->insert($data);
				$this->session->set_flashdata('sessionMessage','Inserted Successfully');
			}
			if($query){
				$redirect=base_url()."branch";
				redirect($redirect);
			}
		}
		else{
			if($company_id!=""){
				$data['branchData']=(array)$this->branch_model->get($company_id);
				$this->template->set_layout('site_layout')->build('admin/company_branch/newBranch',$data);
			}
			else{
				$this->template->set_layout('site_layout')->build('admin/company_branch/newBranch');
			}		
		}
	}

	public function editBranch($id){
		$this->checkUserRole();
		$data['branchData']=(array)$this->branch_model->get($id);
		$this->template->set_layout('site_layout')->build('admin/company_branch/newBranch',$data);


	}

	public function deactivate($id){
		$this->checkUserRole();
		$deactivate=$this->branch_model->deactivate($id);
		if($deactivate){
			$this->session->set_flashdata('sessionMessage','Deactivated Successfully');
			$redirect=base_url()."branch";
			redirect($redirect);
		}
	}

	protected function checkUserRole(){
		$role=strtolower($this->session->userdata('role'));
		if($role=="documentation" || $role=="general user" || $role=="ticketing" || $role=="finance" 
			|| $role=="mol admin" || $role=="recruitment" || $role=="visa processing"){
			$this->session->set_userdata('Errormsg', 'Error! you are not authorized to access the panel. Login with your credentials');
			$redirect = base_url()."_cpanel";
			redirect($redirect);
			exit();
		}
	}

	public function redirect(){
		$ref = $this->input->server('HTTP_REFERER', TRUE);
		redirect($ref, 'location');
	}


	public function currencyConverter(){
		$amount='1';
		$amount = urlencode($amount);
	  $from_Currency = urlencode('EUR');
	  $to_Currency = urlencode('NPR');
	  $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
	  $get = explode("<span class=bld>",$get);
	  $get = explode("</span>",$get[1]);  

	  $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
	  echo round($converted_amount,2);
	  die();
	// 	$amount = $_POST['amount'];
	// $from = $_POST['from'];
	// $to = $_POST['to'];
	 
	// //make string to be put in API
	// $string = "1".$from."=?".$to;
	 
	// //Call Google API
	// $google_url = "http://www.google.com/ig/calculator?hl=en&q=".$string;

	// echo $google_url;
	// die();
	 
	// //Get and Store API results into a variable
	// $result = file_get_contents($google_url);

	 
	// //Explode result to convert into an array
	// $result = explode('"', $result);
	 
	// ################################
	// # Right Hand Side
	// ################################
	// $converted_amount = explode(' ', $result[3]);
	// $conversion = $converted_amount[0];
	// $conversion = preg_replace('/[x00-x08x0B-x1F]/', '', $conversion);
	// $conversion = $conversion * $amount;
	// $conversion = round($conversion, 2);
	 
	// //Get text for converted currency
	// $rhs_text = ucwords(str_replace($converted_amount[0],"",$result[3]));
	 
	// //Make right hand side string
	// $rhs = $conversion.$rhs_text;
	 
	// ################################
	// # Left Hand Side
	// ################################
	// $google_lhs = explode(' ', $result[1]);
	// $from_amount = $google_lhs[0];
	 
	// //Get text for converted from currency
	// $from_text = ucwords(str_replace($from_amount,"",$result[1]));
	 
	// //Make left hand side string
	// $lhs = $amount." ".$from_text;
	 
	// ################################
	// # Make the result
	// ################################
	 
	// echo $lhs." = ".$rhs;
	}



	protected function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
}