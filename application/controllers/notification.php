<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification extends Base_controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Advertise_model');
		$this->load->model('Job_model');
		$this->load->model('Candidate_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function getAgentNotification($notification_id){
		$query = $this->db->query("SELECT t1.id as notification_id, t2.id AS agent_id, t2.agent_name, t2.phone, t2.email
									FROM tbl_agent_notification t1 
										INNER JOIN tbl_agent t2 
											ON t1.id = t2.notification_id 
										WHERE t1.id = '$notification_id' AND t2.status=1
									");
		$data['agents'] = $query->result();

		// change agent notification status for making seen in the notification
		$updateStatus=array('status'=>1);
		$update=$this->db->where('id',$notification_id)->update('tbl_agent_notification',$updateStatus);
		$this->template->set_layout('site_layout')->build('admin/notification/agentnotification',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	public function changeStatus(){
		$id = $this->input->post('notification_id');
		$type = $this->input->post('type');
		if($type == "agent"){
			$data = array('status' => 1);
			$this->db->where('id',$id);
			if($this->db->update('tbl_agent_notification',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}
		elseif ($type == "finance") {
			$data = array('sent_flag' => 3);
			$this->db->where('id',$id);
			if($this->db->update('tbl_transaction',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}
		elseif ($type == "visa") {
			$data = array('sent_flag' => 3);
			$this->db->where('id',$id);
			if($this->db->update('tbl_visa_ticketing_approval',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}
		elseif ($type == "ticket") {
			$data = array('sent_flag' => 3);
			$this->db->where('id',$id);
			if($this->db->update('tbl_visa_ticketing_approval',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}
		elseif ($type == "mol") {
			$data = array('status' => 1,'seen_by' => $this->session->userdata('user_id'));
			$this->db->where('id',$id);
			if($this->db->update('tbl_general_notification',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}
		else{
			$data  = array('status' => 1,
				'seen_by'=> $this->session->userdata('user_id')
				);
			$this->db->where('id',$id);
			if($this->db->update('tbl_general_notification',$data)){
				echo 1;
			}
			else{
				echo 2;
			}
		}

	}


	public function approveAgent(){
		$this->load->model('agent_model');
		$agentid=$this->input->post('agentid');
		$status=$this->input->post('status');
		$username=$this->session->userdata('username');
		if($status=="approve"){
			$data=array('status'=>2,'approved_by'=>$username);
			$update=$this->agent_model->update($agentid,$data);
			if($update){
				echo '<i class="fa fa-check"></i>';
			}
		}else{

			 $check =  $this->agent_model->get($agentid);        
			  $old_image = $check->image;  
			  if($old_image!=""){  
			    if(file_exists("uploads/agent/".$old_image))  {      
			     $this->image_remove('uploads/agent',$old_image);
			   }
			 }

			 $delete=$this->agent_model->delete($agentid);
			 if($delete){
			  echo "Declined";

			}
		}

	}


	//@author Sujit
	//@params None
	//@returns None
	public function getCandidateList(){
		$notification = $this->uri->segment(3);
		$data['candidate_result'] = $this->Candidate_model->getCandidateList($notification);
		$data['current_status'] = $notification;
		$this->template->set_layout('site_layout')->build('admin/candidate/index',$data);		
	}


}