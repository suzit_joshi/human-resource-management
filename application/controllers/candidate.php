<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Candidate extends Base_Controller{
	
	public $branch_id;

	public function __construct(){
		parent::__construct();
		// error_reporting(0);
		$this->load->model('Candidate_model');
		$this->load->model('Job_model');
		$this->load->model('Agent_model');
		$this->load->model('General_notification_model');
		$this->load->model('Transaction_model');
		$this->branch_id  = $this->session->userdata('branch');
	}

	//@author Sujit
	//@params None
	//@returns None
	//Index Function - Directs towards list of candidates
	public function index(){
		//$data['query'] = $this->db->query("SELECT * FROM tbl_candidate");
		
		$select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, 
		ta.agent_name, ta.mobile AS agent_mob, tc.agent_id, tc.branch_id,
		tj.job_title, tc1.company_name, tjb.current_status
		";
		/*$select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, 
		ta.agent_name, ta.mobile AS agent_mob, tc.agent_id
		";*/
		$data['candidate_result'] = $this->Candidate_model->getGeneralCandidateInfo($id=NULL,$select,$this->branch_id);

		$this->template->set_layout('site_layout')->build('admin/candidate/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	/*Triggering View for new Candidate*/
	public function newCandidate(){
		$this->load->model("branch_model");
		$selections = "tbl_job.id, tbl_job.job_title, tbl_company.company_name, tbl_country.country_name";
		$data['job'] = $this->Job_model->getJobInfo($selections);

		$agent_select = "tbl_agent.agent_name, tbl_agent.id";
		$data['agent'] = $this->Agent_model->getAgent($agent_select);
		$data['branch'] = $this->branch_model->get_all();
		$this->template->set_layout('site_layout')->build('admin/candidate/newcandidate',$data);
		//$this->load->view('admin/candidate/newcandidate',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//INSERTING NEW CANDIDATE
	public function save(){
		/*PERSONAL INFORMATION*/
		$name = $this->input->post('full_name');
		$dob = date("Y-m-d",strtotime($this->input->post('dob')));
		$gender = $this->input->post('gender');
		$father_name = $this->input->post('father_name');
		$mother_name = $this->input->post('mother_name');
		/*NEW FIELDS*/
		$birth_place = $this->input->post('birth_place');
		$height = $this->input->post('height');
		$weight = $this->input->post('weight');

		/*ASSIGNING language with proficiency*/
		$english = $this->input->post('eng_language');
		$hindi = $this->input->post('h_language');
		$arabic = $this->input->post('n_language');
		$other = $this->input->post('o_language');
		$otherLanguage=$this->input->post('otherLanguage');
		
		$language = serialize(array('english' => $english, 'hindi' => $hindi, 'arabic'=> $arabic, $otherLanguage=>$other));
		
		/*END ASSIGNING language with proficiency*/
		$marital_status = $this->input->post('marital_status');
		$religion = $this->input->post('religion');
		$nationality = $this->input->post('nationality');
		$qualification = $this->input->post('qualification');
		$other_info = $this->input->post('other_info');

		/*CONTACT INFORMATION*/
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$phone = $this->input->post('phone');
		$temp_address = $this->input->post('temp_address');
		$perm_address = $this->input->post('perm_address');
		/*PASSPORT INFORMATION*/
		$original_pp = $this->input->post('original_pp');
		$passport_no = $this->input->post('passport_no');
		$issued_date = date("Y-m-d",strtotime($this->input->post('issued_date')));
		$expiration_date = date("Y-m-d",strtotime($this->input->post('expiration_date')));
		$issued_from = $this->input->post('issued_from');
		/*JOB INFORMATION*/
		$job_id = $this->input->post('job');
		$branch_id = $this->input->post('branch_id');
		$training = $this->input->post('training');
		$experience = $this->input->post('experience');
		//echo $_POST['branch_id'];
		
		if($branch_id != 1){
				$agent_id = '4';
		}else{
			$agent_id = $this->input->post('agent');
		}
		
		$profile_image = $_FILES['profile_image']['name'];
		$passport_image = $_FILES['passport_image']['name'];

		$profile_ret = $this->image_upload($profile_image,"uploads/candidate/profile_image",'profile_image');
	
		$passport_ret = $this->image_upload($passport_image,"uploads/candidate/passport",'passport_image');

		$certificate=$_FILES['userfile']['name'];

		if($certificate[0] != ""){
			$images=$this->multiple("uploads/candidate/training_certificate");
			$training_image=serialize($images['success']);  
			$db_unsucess_image = $images['unsuccess'];
		}

		$user_id = $this->session->userdata('user_id');

		if($profile_ret['status']){
			$profile_documents = $profile_ret['msg'];
		}else{
			$profile_documents = '';
			$profile_error = $profile_ret['msg'];
		}

		if($passport_ret['status']){
			$passport_documents = $passport_ret['msg'];
		}
		else{
			$passport_documents = '';
			$passport_error = $passport_ret['msg'];
		}

		if($training_image){
			$training_documents = $training_image;
			$training_error = implode(",",$db_unsucess_image);
		}else{
			$training_documents = serialize(array());
			$training_error = implode(",",$db_unsucess_image);
		}

		$data = array(
			'candidate_name' => $name,
			'dob' => $dob,
			'gender' => $gender,
			'father_name' => $father_name,
			'mother_name' => $mother_name,
			'email_id' => $email,
			'mobile' => $mobile,
			'phone' => $phone,
			'temp_address' => $temp_address,
			'perm_address' => $perm_address,
			'passport_no' => $passport_no,
			'issued_from' => $issued_from,
			'issued_date' => $issued_date,
			'expire_date' => $expiration_date,
			'original_pp' => $original_pp,
			'agent_id' => $agent_id,
			'language' => $language,
			'birth_place' => $birth_place,
			'height' => $height,
			'weight' => $weight,
			'marital_status' => $marital_status,
			'religion' => $religion,
			'nationality' => $nationality,
			'qualification' => $qualification,
			'training' => $training,
			'experience' => $experience,
			'branch_id' => $branch_id,
			'user_id' => $user_id,
			'updated_by' => $this->session->userdata('name')
			);

		$candidate_id = $this->Candidate_model->insert($data);
		if($candidate_id){
			$new_data = array('job_id' => $job_id,
								'candidate_id' => $candidate_id,
								'date' => date('Y-m-d'),
								'current_status' => "New"
						);
			$this->db->insert('tbl_job_candidate',$new_data);


			$image_data = array(
							'candidate_id' => $candidate_id,
							'profile' => $profile_documents,
							'passport' => $passport_documents,
							'training' => $training_documents
						);
			$this->db->insert('tbl_candidate_image',$image_data);
			
			$error_msg = '';
			if(isset($profile_error)){
				$error_msg .= "Profile- $profile_error"."<br>"; 
			}
			if(isset($passport_error)){
				$error_msg .= "Passport- $passport_error"."<br>"; 
			}
			if(isset($training_error)){
				$error_msg .= "Training certificate- $training_error files could not be uploaded";
			}
			if($error_msg != ''){
				$this->session->set_flashdata('candidate_data',$error_msg);
			}
			$this->session->set_flashdata('sessionMessage','New candidate has been inserted successfully!');
			$redirect = base_url()."candidate/index";
		}
		else{
		$this->session->set_flashdata('sessionMessage','Candidate could not be saved. Please try again!');
			$redirect = base_url()."candidate/index";
		}
		redirect($redirect);
	}


	//@author Sujit
	//@params None
	//@returns None
	//@Getting Candidate Details
	public function getCandidateDetails($candidate_id){
		$candidate_id = base64_decode($candidate_id);
		$select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, tc.perm_address, tc.temp_address,
					tc.father_name, tc.mother_name, tc.email_id, tc.height, tc.weight, tc.dob, tc.birth_place, tc.language,
					tc.marital_status,tc.religion,tc.nationality,tc.qualification,tc.experience, tc.training, tc.issued_from,tc.issued_date,
					tc.expire_date,
					ta.agent_name, ta.mobile AS agent_mob,
					tj.job_title, tc1.company_name, tjb.current_status";
		$data['candidate'] = (array)$this->Candidate_model->getGeneralCandidateInfo($candidate_id,$select,$this->branch_id);
		/*		echo "<pre>";
		var_dump($data['candidate']);
		echo "</pre>";
		die();*/
		$data['candidate_doc'] = (array)$this->Candidate_model->getDocuments($candidate_id);
		$data['preInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Pre-interview');
		$data['finalInterview']=(array)$this->Candidate_model->interviewResult($candidate_id,'Final-interview');
		$data['medicalResult']=(array)$this->Candidate_model->medicalResult($candidate_id);
        $data['ticketing']=(array)$this->Candidate_model->ticketingResult($candidate_id);
        $data['visa']	=	(array) $this->Candidate_model->visaResult($candidate_id);
        $data['finacial_info'] = $this->Transaction_model->getFinancialInfo($candidate_id);
		$this->template->set_layout('site_layout')->build('admin/candidate/viewInfo',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting Candidate Details
	public function editCandidateDetails($candidate_id){
		$candidate_id = base64_decode($candidate_id);
		$select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, tc.perm_address, tc.temp_address,
					tc.gender, tc.father_name, tc.mother_name, tc.email_id, tc.height, tc.weight, tc.dob, tc.birth_place, tc.language,
					tc.marital_status,tc.religion,tc.nationality,tc.qualification,tc.experience, tc.training, tc.issued_from,tc.issued_date,
					tc.expire_date,
					ta.agent_name, ta.mobile AS agent_mob,
					tj.job_title, tc1.company_name, tjb.current_status";
		$data['candidate'] = (array)$this->Candidate_model->getGeneralCandidateInfo($candidate_id,$select,$this->branch_id);
		/*		echo "<pre>";
		var_dump($data['candidate']);
		echo "</pre>";
		die();*/
		$data['candidate_doc'] = (array)$this->Candidate_model->getDocuments($candidate_id);
		$this->template->set_layout('site_layout')->build('admin/candidate/info',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//UPLOADING MORE FILES OF CANDIDATE
	public function moreFiles(){
		$id = $this->input->post('random_no');
		$candidate_id = base64_decode($id);
		$type = $this->input->post('upload_type');

		if($type == "passport" || $type == "office_form" || $type == "cv"){
			$file = $_FILES['image']['name'];
			$path = "uploads/candidate/".$type;
			$img_returns = $this->image_upload($file,$path,'image');
			if($img_returns['status']){
				$data = array($type => $img_returns['msg']);
				$this->Candidate_model->updateDocuments($candidate_id,$data);
				$this->session->set_flashdata('sessionMessage',"File uploaded successfully!");
				$redirect = base_url()."candidate/getCandidateDetails/".$id;
			}
			else{
				$this->session->set_flashdata('noticeMessage',$img_returns['msg']);
				$redirect = base_url()."candidate/getCandidateDetails/".$id;
			}
		}
		else{
			$pic=$_FILES['userfile']['name'];
			$path = "uploads/candidate/".$type;
			$images=$this->multiple($path);
			if($images['success']){
				$db_image=serialize($images['success']);
				$data = array($type => $db_image);
				$this->Candidate_model->updateDocuments($candidate_id,$data);
				$this->session->set_flashdata('sessionMessage',"File uploaded successfully!");
				$redirect = base_url()."candidate/getCandidateDetails/".$id;
			} 
			else{
				$this->session->set_flashdata('noticeMessage',implode($images['unsuccess'])."file could not be inserted");
				$redirect = base_url()."candidate/getCandidateDetails/".$id;
			}
		}
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//Get Candidate List according to status
	public function getCandidateList(){
		$status = $this->input->post('status');
		if($status == "all"){
			$select = "tc.id AS candidate_id, tc.candidate_name, tc.passport_no, tc.phone, tc.mobile, 
						ta.agent_name, ta.mobile AS agent_mob, tc.agent_id,
						tj.job_title, tc1.company_name, tjb.current_status";
			$data['candidate_result'] = $this->Candidate_model->getGeneralCandidateInfo($id=NULL,$select,$this->branch_id);
		}
		else{
			$data['candidate_result'] = $this->Candidate_model->getCandidateList($status);
		}
		$data['type'] ="filtered_list";
		/*var_dump($candidates);*/
		$this->load->view('ajax/ajax_candidate_list',$data);
	}


	//@author Sujit
	//@params None
	//@returns None
	//Get Candidate List transfers
	public function transfer(){
		$candidates = $this->input->post('candidate');
		$transfer_to = $this->input->post('transfer_to');
	}

	//@author Sujit
	//@params None
	//@returns None
	//Update Candidates personal information
	public function updateCandidate(){
		$posted_candidate_id = $this->input->post('candidate_id');
		$candidate_id = base64_decode($posted_candidate_id);
		$name = $this->input->post('full_name');
		$dob = date("Y-m-d",strtotime($this->input->post('dob')));
		$gender = $this->input->post('gender');
		$father_name = $this->input->post('father_name');
		$mother_name = $this->input->post('mother_name');
		/*CONTACT INFORMATION*/
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$phone = $this->input->post('phone');
		$temp_address = $this->input->post('temp_address');
		$perm_address = $this->input->post('perm_address');

		$birth_place = $this->input->post('birth_place');
		$height = $this->input->post('height');
		$weight = $this->input->post('weight');

		$english = $this->input->post('eng_language');
		$hindi = $this->input->post('h_language');
		$arabic = $this->input->post('n_language');
		$other = $this->input->post('o_language');
		$otherLanguage=$this->input->post('otherLanguage');

		$language = serialize(array('english' => $english, 'hindi' => $hindi, 'arabic'=> $arabic, $otherLanguage=>$other));
		

		$marital_status = $this->input->post('marital_status');
		$religion = $this->input->post('religion');
		$nationality = $this->input->post('nationality');
		$qualification = $this->input->post('qualification');
		$training = $this->input->post('training');
		$experience = $this->input->post('experience');
		$data = array(
					'candidate_name' => $name,
					'dob' => $dob,
					'gender' => $gender,
					'father_name' => $father_name,
					'mother_name' => $mother_name,
					'email_id' => $email,
					'mobile' => $mobile,
					'phone' => $phone,
					'temp_address' => $temp_address,
					'perm_address' => $perm_address,
					'language' => $language,
					'birth_place' => $birth_place,
					'height' => $height,
					'weight' => $weight,
					'marital_status' => $marital_status,
					'religion' => $religion,
					'nationality' => $nationality,
					'qualification' => $qualification,
					'training' => $training,
					'experience' => $experience
					);

		if($this->Candidate_model->update($candidate_id,$data)){
			$this->session->set_flashdata('sessionMessage',"Candidate's personal information updated successfully!");
			
		}else{
			$this->session->set_flashdata('noticeMessage',"Error! Candidate's personal information could not be updated!");
		}
		$redirect = base_url()."candidate/editCandidateDetails/".$posted_candidate_id;
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//Update Candidates passport Information
	public function updatePassportInfo(){
		$posted_candidate_id = $this->input->post('candidate_id');
		$candidate_id = base64_decode($posted_candidate_id);

		$passport_no = $this->input->post('passport_no');
		$issued_date = date("Y-m-d",strtotime($this->input->post('issued_date')));
		$expiration_date = date("Y-m-d",strtotime($this->input->post('expired_date')));
		$issued_from = $this->input->post('issued_from');		

		$data = array(
					'passport_no' => $passport_no,
					'issued_from' => $issued_from,
					'issued_date' => $issued_date,
					'expire_date' => $expiration_date
					);

		if($this->Candidate_model->update($candidate_id,$data)){
			$this->session->set_flashdata('sessionMessage',"Candidate's passport information updated successfully!");
			
		}else{
			$this->session->set_flashdata('noticeMessage',"Error! Candidate's passport information could not be updated!");
		}
		$redirect = base_url()."candidate/editCandidateDetails/".$posted_candidate_id;
		redirect($redirect);
	}


	//@author Sujit
	//@params None
	//@returns None
	//Update Candidates personal information
	public function getTransferList(){
		$data['candidates'] = $this->Candidate_model->getCandidateList("New");
        $this->template->set_layout('site_layout')->build('admin/candidate/candidateApprovalList',$data);
	}

	//@author Sujit
    //@params None
    //@returns None
    public function sendNotification(){
        //on-medical status
        $candidates = $this->input->post('candidate');
        $no_candidate = count($candidates);
    
        if($this->input->post('frm_submit')){
        	$new_status = "applied";
        	$remarks = "New list of candidates";
            foreach ($candidates as $row => $value) {
                $update_data = array('current_status' => $new_status);
                $this->db->where('candidate_id', $value);
                $this->db->update('tbl_job_candidate',$update_data);
            }

            $data = array(    
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'recruitment',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        ),
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'general admin',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        )
                    );

            $this->General_notification_model->insert_many($data);            
            $this->session->set_flashdata('sessionMessage',"Notification sent!");            
        }
        else{
            $this->session->set_flashdata('noticeMessage',"Notification could not be sent. Please try again!");
        }
        $redirect = base_url()."candidate/index";
        redirect($redirect);
    }


	public function viewPdf()
	{
		$decode=$this->input->post('random');
		$sample = $this->input->post('cv_sample');
		if(is_numeric($decode)){
			$this->load->helper('pdf_helper');
			$data['candidateDetail']=(array)$this->Candidate_model->get($decode);
		
			$this->load->view('admin/pdf/candidate',$data);
		}else{
			$this->template->set_layout('site_layout')->build('admin/error_page');
		}
	}


	public function editCandidate(){
		$type = $this->input->post('type');
		$candidate_id = base64_decode($this->input->post('candidate_id'));
		if($type == "passport"){
			$passport_no = $this->input->post('passport_no');
			$issued_date = date("Y-m-d",strtotime($this->input->post('issued_date')));
			$issued_from = $this->input->post('issued_from');
			/*JOB INFORMATION*/
			$job_id = $this->input->post('job_id');
			$agent_id = $this->input->post('agent_id');

			$data = array(
						'passport_no' => $passport_no,
						'issued_date' => $issued_date,
						'issued_from' => $issued_from,
						'job_id' => $job_id,
						'agent_id' => $agent_id
					);

			$this->db->where('candidate_id', $candidate_id);
			if($this->db->update('tbl_candidate', $data)){
				$job_query = $this->db->get_where('tbl_job',array('job_id' => $job_id));
				$job_result = $job_query->row();
				$data['job'] = $job_result->job_title;

				$agent_query = $this->db->get_where('tbl_agent',array('agent_id' => $agent_id));
				$agent_result = $agent_query->row();
				$data['agent'] = $agent_result->agent_name;
				$this->load->view('ajax/ajax_candidate_passport',$data);
			}else{
				echo "error";
			}
		}elseif($type == "personal"){

		}
	}
	
	/*DELETE IMAGE*/
	public function delImage(){
		$image_id = $this->input->post('image_id');
		$image_name = $this->input->post('image_name');
		$folder_name = $this->input->post('folder');
		$name = explode(".",$image_name);
		$img_thumb = $name[0]."_thumb".".".$name[1];
		$img_name_path = "./album/".$folder_name."/".$image_name;
		$img_thumb_path = "./album/".$folder_name."/thumb/".$img_thumb;
		/*Deleting Image Files*/
		unlink($img_name_path);
		unlink($img_thumb_path);
		$image_del = $this->db->query("DELETE FROM tbl_image_gallery WHERE image_id = '$image_id'");
	}

	public function getCompanyJob(){
		$company_id = $this->input->post('company_id');
		$job_query = $this->db->get_where('tbl_job',array('company_id' => $company_id, 'status' => 1));
		$data['jobs'] = $job_query->result();
		$this->load->view('ajax/ajax_job_listing',$data);
	}

	public function updateCandidateStatus(){
		$status = $this->input->post('status');
		$candidate_id = base64_decode($this->input->post('candidate_id'));

		$this->db->where('candidate_id',$candidate_id);
		$data['candidate_status'] = $status;
		if($this->db->update('tbl_candidate',$data)){
			return true;
		}else{
			return false;
		}
	}

	public function deleteCandidate(){
		$candidate_id = base64_decode($this->uri->segment(3));
		$data['status'] = 0;
		$this->db->where('candidate_id',$candidate_id);
		if($this->db->update('tbl_candidate',$data)){
			$redirect = base_url()."candidate/index/delsuccess";
		}else{
			$redirect = base_url()."candidate/index/error";
		}
		redirect($redirect);
	}

	public function generateForm(){
		$this->load->helper('pdf_helper');
		
		$select = "tc.*,
		ta.agent_name, ta.mobile AS agent_mob, tc.agent_id,
		tj.job_title, tc1.company_name, tjb.current_status,
		tc2.country_name";
		$candidate_id = base64_decode($this->uri->segment(3));
		$data['candidate'] = (array)$this->Candidate_model->getGeneralCandidateInfo($candidate_id,$select,$this->branch_id);
		$data['file'] = (array)$this->Candidate_model->getDocuments($candidate_id); 
		
		$this->load->view('admin/pdf/office_form',$data);
	}
    
	        
	//    view cover letter 
    public function generateCoverLetter()
	{
		$this->load->helper('pdf_helper');
        $candidate_id = base64_decode($this->uri->segment(3));
       	$data['candidate']= $this->db->query("select t1.candidate_name,t1.passport_no,t3.job_title,t4.company_name,t5.country_name from tbl_candidate t1
                                            INNER JOIN tbl_job_candidate t2 ON t1.id=t2.candidate_id
                                            INNER JOIN tbl_job t3 ON t2.job_id=t3.id
                                            INNER JOIN tbl_company t4 ON t3.company_id=t4.id
                                            INNER JOIN tbl_country t5 ON t4.country_id=t5.id
                                            WHERE t2.candidate_id=$candidate_id")->row();
        $this->load->view('admin/pdf/coverLetter',$data);
	}

	public function serverProcessing(){
	    echo "<pre>";
	    print_r($_REQUEST);
	    echo "</pre>";
		die;
	}

	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
}