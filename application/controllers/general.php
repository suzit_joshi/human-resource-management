<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General extends Base_controller{

	public function __construct(){
		parent::__construct();
	}
	
	//@author Sujit
	//@params None
	//@returns None
	public function getContent(){
		$tbl_name = $this->input->post('table');
		$id = $this->input->post('id');
		$temp = explode('_', $tbl_name);
		$data['title'] = $temp[1];
		$query_statement = "SELECT * FROM $tbl_name WHERE id=$id";
		$data['record'] = $this->db->query($query_statement)->row();
		$this->load->view('ajax/ajax_content_load',$data);
		/*$this->load->view('admin/company/index',$data);*/
	}

}