<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Medical extends Interview_management_controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('Medical_model');
		$this->load->model('Candidate_model');
        $this->load->model('General_notification_model');
	}

	//@author Sujit
	//@params None
	//@returns None
    //@View to load medical list
    public function index(){
    	$data['medicals'] = $this->Medical_model->listMedical();
    	$this->template->set_layout('site_layout')->build('admin/medical/index',$data);
    }
	
	//@author Sujit
	//@params None
	//@returns None
    //@View to load new medical form
	public function newMedical(){
        $select = "id,candidate_name,passport_no";
		$data['candidates'] = $this->Medical_model->getMedicalList();
        $this->template->set_layout('site_layout.php')->build('admin/medical/newmedical',$data);
	}
    
    //@author Sujit
	//@params None
	//@returns None
    //@Save the medical
	public function save(){
        if($this->input->post('medical-submit')){
            $id = base64_decode($this->input->post('random'));
            $type = $this->input->post('type');
            $candidate = $this->input->post('candidate');
            $medical_center = $this->input->post('medical_center');
            $medical_date = date('Y-m-d',strtotime($this->input->post('medical_date')));
            $result = $this->input->post('result');
            if($result == "1"){
                $job_candidate_data = array('current_status' => "medical_passed", 'data_entry' => 1);
            }else{
                $job_candidate_data = array('current_status' => "failed", 'remarks' => 'Medical Failed');
            }
            $this->db->where('candidate_id',$candidate)->update('tbl_job_candidate',$job_candidate_data);

            $remarks = $this->input->post('description');
            $data = array(
                            'candidate_id' => $candidate,
                            'medical_date' => $medical_date,
                            'medical_center' => $medical_center,
                            'result' => $result,
                            'type' => $type,
                            'remarks' => $remarks,
                            'updated_by' => $this->session->userdata('name')
                            );
            if($id == ""){
                if($this->Medical_model->insert($data)){
                    $this->session->set_flashdata('sessionMessage','Medical record has been added successfully!');
                    $redirect = base_url()."medical/index";
                }
                else{
                     $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
                    $redirect = base_url()."medical/newMedical";
                }
            }
            else{
                if($this->Medical_model->update($id,$data)){
                    $this->session->set_flashdata('sessionMessage','Medical record has been edited successfully!');
                    $redirect = base_url()."medical/index";
                }
                else{
                     $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
                    $redirect = base_url()."medical/newMedical";
                }
                
            }
        }
        else{
            $this->session->set_flashdata('noticeMessage','Form not submitted properly.Please try again!');
            $redirect = base_url()."medical/newMedical";
        }
        redirect($redirect);
	}
    
    //@author Sujit
	//@params None
	//@returns None
	public function getMedical(){
        $id = base64_decode($this->input->post('id'));

        $data['medical'] = (array)$this->Medical_model->listMedical($id);
        $data['contentType'] = "medical";
        $this->load->view('ajax/ajax_content',$data);        
	}

    //@author Sujit
	//@params None
	//@returns None
	public function editMedical(){
        $id = base64_decode($this->uri->segment(3));
        $data['medical'] = (array)$this->Medical_model->listMedical($id);
        $this->template->set_layout('site_layout')->build('admin/medical/newmedical',$data);
	}
    
    //@author Sujit
	//@params None
	//@returns None
    public function delete(){
    	$id = base64_decode($this->uri->segment(3));
        $this->Medical_model->delete($id);
        $this->session->set_flashdata('noticeMessage','Medical information deleted successfully!');
        $redirect = base_url()."medical/index";
        redirect($redirect);
    }


    //@author Sujit
    //@params None
    //@returns None
    public function getTransferList(){
        $data['candidates'] = $this->Candidate_model->getCandidateList('finalinterview_passed');
        $this->template->set_layout('site_layout')->build('admin/medical/medicalApprovalList',$data);
    }



    //@author Sujit
    //@params None
    //@returns None
    public function sendNotification(){
        //on-medical status
        $candidates = $this->input->post('candidate');
        $no_candidate = count($candidates);
        $remarks = "Medical passed candidate list";
        $new_status = "medical_passed";

        if($this->input->post('frm_submit')){
            foreach ($candidates as $row => $value) {
                $update_data = array('current_status' => $new_status , 'data_entry' => 0);
                $this->db->where('candidate_id', $value);
                $this->db->update('tbl_job_candidate',$update_data);
            }

             $data = array(    
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'finance',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        ),
                        array(
                        'sender' => $this->session->userdata('username'),
                        'receipent_role' => 'general admin',
                        'candidate_no' => $no_candidate,
                        'remarks' => $remarks,
                        'sender_role' => $this->session->userdata('role'),
                        'heading' => $new_status
                        )
                    );

            $this->General_notification_model->insert_many($data);  
            $this->session->set_flashdata('sessionMessage',"Notification sent!");            
        }
        else{
            $this->session->set_flashdata('noticeMessage',"Notification could not be sent. Please try again!");
        }
        $redirect = base_url()."medical/index";
        redirect($redirect);
    }


}