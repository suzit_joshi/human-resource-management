<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Agent extends Office_management_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('agent_model');
		$this->load->library('form_validation');

	}
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		// $query = $this->agent_model->get_all();
		$data['result_agent'] = $this->agent_model->get_all();
		/*$this->load->view('admin/agent/index',$data);*/
		$this->template->set_layout('site_layout')->build('admin/agent/index',$data);
	}


    	
    




	//@author Sujit
	//@params None
	//@returns None
	//@Used to redirect into new form to create agent
	public function newAgent(){
		$this->template->set_layout('site_layout')->build('admin/agent/newagent');
	}

	public function agentApproval(){
		$checked=$this->input->post('agentCheck');
		$message=$this->input->post('msgForApproval');
		// $random=
		
		$userName=$this->session->userdata('username');
		$date=date('Y-m-d');
		$serializeId=serialize($checked);
		$value=array('agent_id'=>$serializeId,'remarks'=>$message,'sent_user'=>$userName,'notification_date'=>$date);
		$insert=$this->db->insert('tbl_agent_notification',$value);	
		if($insert){
			$returnId=$this->db->insert_id();
			// return "Successfuly sent for approval";
		}


		if($checked){
			foreach($checked as $agentId){
				$data=array('status'=>1,'notification_id'=>$returnId);
				$this->agent_model->update($agentId,$data);
			}
		}

	}

// load the agent list from ajax for approval
	public function showAgentList(){
		$data['agentList']=$this->agent_model->multiple_where(array('status'=>0),'id,agent_name,mobile');
		$this->load->view('admin/agent/agentList',$data);
	}
	
	//@author Sujit
	//@params None
	//@returns None
	//@Used to add and edit an agent
	public function save(){
		
		 $config = array(
               array(
                     'field'   => 'agent_name',
                     'label'   => 'agent_name',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'email_id',
                     'label'   => 'email_id',
                     'rules'   => 'required|valid_email|is_unique[tbl_agent.email]'
                  ),
               array(
                     'field'   => 'mobile_no',
                     'label'   => 'mobile',
                     'rules'   => 'required|integer|is_unique[tbl_agent.mobile]'
                  ),
               array(
                     'field'   => 'phone',
                     'label'   => 'phone',
                     'rules'   => 'integer'
                  )
            );

		$this->form_validation->set_rules($config); 

		if($this->form_validation->run() == TRUE){
			$user = $this->session->userdata('name');
			$agent = $this->input->post('agent_name');
			$email_id = $this->input->post('email_id');
			$mobile = $this->input->post('mobile_no');
			$phone = $this->input->post('phone');
			$address = $this->input->post('address');
			$remarks = $this->input->post('remarks');

			$regd_date = date('Y-m-d');
			$tmp_img_name = $_FILES['image']['name'];
			$path = "uploads/agent";
			$img_ret = $this->image_upload($tmp_img_name,$path,'image');
			if($img_ret['status']){
				$data = array('agent_name' => $agent,
							  'email' => $email_id,
							  'mobile' => $mobile,
							  'phone' => $phone,
							  'address' => $address,
							  'remarks' => $remarks,
							  'image' => $img_ret['msg'],
							  'regd_date' => $regd_date,
							  'updated_by' => $user
							);
				$this->db->insert('tbl_agent',$data);
				$this->session->set_flashdata('sessionMessage','New Agent created successfully');
				$redirect = base_url()."agent/index";
			}
			else{
				$this->session->set_flashdata('sessionMessage',$img_ret['msg']);
				$redirect = base_url()."agent/newAgent";
			}
			redirect($redirect);
		}
		else{
			$this->template->set_layout('site_layout')->build('admin/agent/newagent');
		}
		
	}

	public function editSave(){
		//echo "here";
		$user = $this->session->userdata('name');
		$agent_id = base64_decode($this->input->post('id'));
		$old_img = $this->input->post('old_img');
		$agent = $this->input->post('agent_name');
		$email_id = $this->input->post('email_id');
		$mobile = $this->input->post('mobile_no');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$remarks = $this->input->post('remarks');

		$data = array('agent_name' => $agent,
							  'email' => $email_id,
							  'mobile' => $mobile,
							  'phone' => $phone,
							  'address' => $address,
							  'remarks' => $remarks,
							  'regd_date' => $regd_date,
							  'updated_by' => $user
							);

		$tmp_img_name = $_FILES['image']['name'];
		//echo $tmp_img_name;

		if($tmp_img_name != ""){
			$path = "uploads/agent";
			$img_ret = $this->image_upload($tmp_img_name,$path);	
			if($img_ret['status']){
				$data = array('image' => $img_ret['msg']);
				$remove_path = "uploads/agent/".$old_img;
				unlink($remove_path);
				$flag = 1;
			}
			else{
				$redirect = base_url()."agent/getInformation/".base64_encode($agent_id);
				$this->session->set_flashdata('sessionMessage',$img_ret['msg']);
			}
		}
		else{
			$flag = 1;
		}


		if($flag == 1){
			$this->db->where('id',$agent_id);

			if($this->db->update('tbl_agent',$data)){
				$this->session->set_flashdata('sessionMessage','Agent edited successfully');
				$redirect = base_url()."agent/index";
			}else{
				$this->session->set_flashdata('sessionMessage','Error! something went wrong. Please try again later');
				$redirect = base_url()."agent/getInformation/".base64_encode($agent_id);
				
			}
		}
		redirect($redirect);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific job according to the id
	public function getInformation(){
		$agent_id = base64_decode($this->uri->segment(3));
		
		$query = $this->db->get_where('tbl_agent',array('id' => $agent_id));
		$data['result'] = $query->result();
		$this->template->set_layout('site_layout')->build('admin/agent/newagent',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Getting information of a specific job according to the id
	// public function deactivate(){
	// 	$agent_id = base64_decode($this->uri->segment(3));
	// 	$deactivate_return = $this->Agent_model->deactivate($agent_id);
	// 	if($deactivate_return){
	// 		$this->session->set_flashdata('sessionMessage',"Record deactivated successfully!");
	// 	}
	// 	else{
	// 		$this->session->set_flashdata('sessionMessage',"Something went wrong.Please try again!");
	// 	}
	// 	$redirect = base_url()."agent/index";
	// 	redirect($redirect);
	// }

	public function deactivate(){
		$agentid = base64_decode($this->uri->segment(3));
		
			 $check =  $this->agent_model->get($agentid);        
			  $old_image = $check->image;  
			  if($old_image!=""){  
			    if(file_exists("uploads/agent/".$old_image))  {      
			     $this->image_remove('uploads/agent',$old_image);
			   }
			 }

			 $delete=$this->agent_model->delete($agentid);
			 if($delete){
			 $this->session->set_flashdata('sessionMessage',"Record deactivated successfully!");
			 $redirect = base_url()."agent/index";
			 redirect($redirect);

			}
	}

}