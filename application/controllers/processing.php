<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Processing extends Base_controller {
	
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$query = $this->db->get('tbl_processing_work');
		$data['result'] = $query->result();
		$this->load->view('admin/processing_jobs/index',$data);
	}

	// @author sujit
	// @params None
	// @returns None
	// @ADD THROUGH AJAX
	public function newProcessing(){
		$processing_title = $this->input->post('processing_title');
		$processing_id = $this->Idgenerator->genId('tbl_processing_work','processing_work_id');
		$data = array(
				'processing_work_id' => $processing_id,
				'processing_work_title' => $processing_title
			);
		$this->db->insert('tbl_processing_work',$data);
		$query = $this->db->get('tbl_processing_work');
		$data['result'] = $query->result();
		$data['type'] = "new";
		$this->load->view('ajax/ajax_processing',$data);

	}

	// @author sujit
	// @params None
	// @returns None
	// @EDIT THROUGH AJAX
	public function editProcessing(){
		$processing_id = base64_decode($this->input->post('id'));
		$processing_title = $this->input->post('processing_title');
		$data = array('processing_work_title' => $processing_title );
		$query = $this->db->where('processing_work_id',$processing_id)->update('tbl_processing_work',$data);
		$data['type'] = "edit";
		$query = $this->db->get('tbl_processing_work');

		$data['result'] = $query->result();
		$this->load->view('ajax/ajax_processing',$data);
	}


	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
}