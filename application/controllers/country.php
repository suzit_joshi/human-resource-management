<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Country extends Office_management_controller {
	
	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		//$query = $this->db->get_where('tbl_country', array('status' => '1'));


		$query = $this->db->query("SELECT tbl_country.*, 
									(SELECT COUNT(*) FROM tbl_company 
										WHERE tbl_company.country_id = tbl_country.id) 
									AS company_count FROM tbl_country");

		$data['result_country'] = $query->result();
/*		echo "<pre>";
		var_dump($data['result_country']);
		echo "</pre>";
		die();*/
		//$this->load->view('admin/country/index',$data);
		$this->template->set_layout('site_layout')->build('admin/country/index',$data);
	}

	// @author sujit
	// @params None
	// @returns None
	// @ADD THROUGH AJAX
	public function newCountry(){
		$country_name = $this->input->post('country_name');
		$data = array(
				'country_name' => $country_name,
				'status' => '1'
			);
		$this->db->insert('tbl_country',$data);
		//$query = $this->db->get_where('tbl_country', array('status' => '1'));
		$query = $this->db->query("SELECT tbl_country.id, tbl_country.country_name, COUNT(tbl_company.id) AS company_count
								FROM tbl_country 
									INNER JOIN tbl_company
										ON tbl_country.id = tbl_company.country_id
								GROUP BY tbl_company.id;");
		$data['result_country'] = $query->result();
		$data['type'] = "new";
		$this->load->view('ajax/ajax_country',$data);

	}

	// @author sujit
	// @params None
	// @returns None
	// @EDIT THROUGH AJAX
	public function editCountry(){
		$country_id = base64_decode($this->input->post('id'));
		$country_name = $this->input->post('country_name');
		$data = array('country_name' => $country_name );
		$query = $this->db->where('id',$country_id)->update('tbl_country',$data);
		$data['type'] = "edit";
		//$query = $this->db->get_where('tbl_country', array('status' => '1'));
		$query = $this->db->query("SELECT tbl_country.id, tbl_country.country_name, COUNT(tbl_company.id) AS company_count
								FROM tbl_country 
									INNER JOIN tbl_company
										ON tbl_country.id = tbl_company.country_id
								GROUP BY tbl_company.id;");
		$data['result_country'] = $query->result();
		$this->load->view('ajax/ajax_country',$data);
	}

	// @author sujit
	// @params None
	// @returns None
	// @Deactivate the status of country for future records
/*	public function deactivate(){
		$id = base64_decode($this->uri->segment(3));
		$data = array('status' => '0');
		$query = $this->db->where('id',$id)->update('tbl_country',$data);
		$redirect=base_url()."country/index/"."deactivated";
		redirect($redirect);
	}*/

	/** Clear the old cache (usage optional) **/ 
	protected function no_cache(){
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
}