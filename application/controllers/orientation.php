<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Orientation extends Orientation_management_controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('Orientation_model');
		$this->load->model('Candidate_model');
	}

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$data['orientation'] = $this->Orientation_model->listOrientation();
		$this->template->set_layout('site_layout')->build('admin/orientation/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@Used to save new Visa Details
	public function newOrientation(){
		$status = "medical_passed";
		$data['candidate'] = $this->Candidate_model->getCandidateList($status);
		$this->template->set_layout('site_layout')->build('admin/orientation/neworientation',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	//@USED TO GET DATA FOR NEW visa
	public function save(){
		$candidate = $this->input->post('candidate');
		$updated_by = $this->session->userdata('name');

		$center = $this->input->post('center');
		$orientation_date = date('Y-m-d', strtotime($this->input->post('orientation_date')));
		$remarks = $this->input->post('remarks');

		if($this->input->post('orientation-submit')){
			foreach ($candidate as $record => $candidate_id) {
				$status = "orientation_appeared";
				$candidate_data = array('current_status' => $status);
				$this->db->where('candidate_id',$candidate_id);
				$this->db->update('tbl_job_candidate',$candidate_data);

				$data = array(
						'date' => $orientation_date,
						'center' => $center,
						'remarks' => $remarks,
						'candidate_id' => $candidate_id,
						'updated_by' => $updated_by
					);

				if($this->Orientation_model->insert($data)){
					$flag = 1;
				}else{
					$flag = 2;
				}
			}
			if($flag == 1){
				$this->session->set_flashdata('sessionMessage','Orientation details inserted successfully!');
				$redirect = base_url()."orientation/index";
			}
			else{
				$this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
				$redirect = base_url()."orientation/newOrientation";
			}
		}
		else{
			$this->session->set_flashdata('noticeMessage','Form not submitted properly. Please try again!');
			$redirect = base_url()."orientation/newOrientation";
		}
		redirect($redirect);
	}


}