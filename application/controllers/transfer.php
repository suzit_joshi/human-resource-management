<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transfer extends Base_controller {

	//@author Sujit
	//@params None
	//@returns None
	public function index(){
		$query = $this->db->query("SELECT tr.*,tb.bank_name FROM tbl_transfer_amount AS `tr`
									LEFT JOIN tbl_bank AS `tb` ON tr.bank_id = tb.bank_id
								");
		$data['result_transfer'] = $query->result();
		$this->load->view('admin/transfer/index',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	public function newTransfer(){
		$bank_query = $this->db->get('tbl_bank');
		$data['bank_result'] = $bank_query->result();

		$condition = array('type'=>'Cheque', 'status'=>0);
		$this->db->where($condition);
		$undeposited_cheques_query = $this->db->get('tbl_income_transaction');
		$data['undeposited_result'] = $undeposited_cheques_query->result();

		$cash_query = $this->db->query("SELECT cash_id,amount FROM tbl_cash 
										WHERE updated_on = (SELECT MAX(updated_on) from tbl_cash)");
		$cash_result = $cash_query->row();
		$data['cash_id'] = $cash_result->cash_id;
		$data['current_cash'] = $cash_result->amount;

		$this->load->view('admin/transfer/newtransfer',$data);
	}

	//@author Sujit
	//@params None
	//@returns None
	public function save(){
		$transfer_type = $this->input->post('transfer_type');
		$transfer_date = date("Y-m-d", strtotime($this->input->post('transfer_date')));
		$amount = $this->input->post('amount');
		$bank_id = $this->input->post('bank');
		$voucher_no = $this->input->post('voucher_no');
		$remarks = $this->input->post('remarks');
		$cash_id = $this->input->post('cash_id');
		$current_cash = $this->input->post('current_cash');
		$undeposited_income_id = $this->input->post('undeposited_cheque');

		$bank_query = $this->db->get_where('tbl_bank',array('bank_id' => $bank_id));
		$bank_result = $bank_query->row();
		$bank_balance = $bank_result->balance;
		$flag = 0;
		//CASH TO BANK
		if($transfer_type == 1){
			$transfer_type_desc = "Cash to Bank A/C";
			$new_cash = $current_cash - $amount;
			$new_bank_balance = $bank_balance + $amount;
			
			$cash_data = array('amount' => $new_cash);
			$this->db->where('cash_id',$cash_id);
			$this->db->update('tbl_cash',$cash_data);
			$flag = 1;
		}
		//BANK TO CASH
		elseif($transfer_type == 2){
			$transfer_type_desc = "Bank A/C to Cash";
			$new_cash = $current_cash + $amount;
			$new_bank_balance = $bank_balance - $amount;

			$cash_data = array('amount' => $new_cash);
			$this->db->where('cash_id',$cash_id);
			$this->db->update('tbl_cash',$cash_data);
			$flag = 1;
		}
		//UNDEPOSITED CHEQUES TO BANK
		else{
			$transfer_type_desc = "Undeposited Cheques to Bank A/C";
			$new_bank_balance = $bank_balance + $amount;
			$data = array('status' => '1');
			$this->db->where('income_id',$undeposited_income_id);
			$this->db->update('tbl_income_transaction',$data);
			$flag = 1;
		}
		
		$bank_data = array('balance' => $new_bank_balance);
		$this->db->where('bank_id',$bank_id);
		$this->db->update('tbl_bank',$bank_data);

		if($flag == 1){
			$transfer_id = $this->Idgenerator->genId('tbl_transfer_amount','transfer_id');
			$data = array(
					'transfer_id' => $transfer_id,
					'bank_id' => $bank_id,
					'amount' => $amount,
					'description' => $remarks,
					'transfer_date' => $transfer_date,
					'transfer_type' => $transfer_type_desc,
					'income_id' => $undeposited_income_id
					);
			$this->db->insert('tbl_transfer_amount',$data);
			$redirect = base_url()."transfer/index/success";
		}else{
			$redirect = base_url()."transfer/index/error";
		}
		redirect($redirect);		
	}


}