<?php

/**
 * The base controller which is used by the Front and the Admin controllers
 */
class Base_Controller extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		//error_reporting(0);
		$this->load->library('Nocache');
		$this->nocache->no_cache();
		$loggedInStatus=$this->session->userdata('hrm_login_status');
		if($loggedInStatus!="1"){
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

	//CHECK UNIQUE VALIDATION FOR EDIT
	public function edit_unique($value, $params)  {
		$CI =& get_instance();
		$CI->load->database();

		$CI->form_validation->set_message('edit_unique', "Sorry, that %s is already being used.");

		list($table, $field, $current_id) = explode(".", $params);

		$query = $CI->db->select()->from($table)->where($field, $value)->limit(1)->get();

		if ($query->row() && $query->row()->id != $current_id)
		{
			return FALSE;
		} else {
			return TRUE;
		}
	}

	protected function image_upload($pic,$path,$imgName){
		$this->load->helper('my_helper');
		$r=random_string();
		$ext=findexts($pic);
		$image_name=$r.".".$ext;
		$image_name=str_replace(" ","_",$image_name);
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|PNG|jpeg|JPG|JPEG|PDF|pdf|doc|DOC|DOCX|docx';
		$config['max_size'] = '10000000';
		$config['file_name']=$image_name;
		$this->upload->initialize($config);

		if ($this->upload->do_upload($imgName))
		{	
			$data = $this->upload->data();
			$temp_return = array('status' => TRUE,
								 'msg' => $image_name,
								 'other_info' => $data);			
		}
		else
		{
			$error = $this->upload->display_errors();
			$temp_return = array('status' => FALSE,
								  'msg' => $error
								);
		}
		return $temp_return;
	}


	protected function multiple($path){
		$this->load->helper('my_helper');

		// $files = $imgFile;
		
  //   	$cpt = count($imgFile['userfile']['name']);
		// for($i=0; $i<$cpt; $i++)
  //   	{
		// 		$_FILES['userfile']['name']= $files['userfile']['name'][$i];
		//  		$_FILES['userfile']['type']= $files['userfile']['type'][$i];
		//  		$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
		//  		$_FILES['userfile']['error']= $files['userfile']['error'][$i];
		//  		$_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

  //       		$config['upload_path'] = 'uploads/demand_letter';
		//  		$config['allowed_types'] = 'jpg|png|PNG|jpeg|pdf|PDF|docs|DOCS|docx|DOCX';
		// 	    $config['max_size'] = '10000000'; // 0 = no file size limit
		// 	    $config['overwrite'] = TRUE;

		// 		$r=random_string();
		// 		$pic=$image_name=$r.$_FILES['userfile']['name'];
		// 		$ext=findexts($pic);
		// 		$image_name=$r.".".$ext;
		// 		$image_name=str_replace(" ","_",$image_name);

	  			
		//  		$config['file_name']=$image_name;

		//  		$this->upload->initialize($config);
	 //        	if($this->upload->do_upload()){
		// 			$data = $this->upload->data();
		// 			$name = $data['file_name'];
		// 			$db_image[]=$image_name;
		// 		}
		// 		else{
		// 			// $db_image=false;
		// 			$unsucess_image[] = $_FILES['userfile']['name'];
		// 		}
		// } 
		
		// if(!isset($unsucess_image)){
		// 	$unsucess_image=array();
		// }
		// if(!isset($db_image)){
		// 	$db_image=array();
		// }
		

		$files = $_FILES;

    	$cpt = count($_FILES['userfile']['name']);


    	$config['upload_path'] = $path;
    	$config['allowed_types'] = 'jpg|png|PNG|jpeg|pdf|PDF|docs|DOCS|docx|DOCX';
		$config['max_size'] = '10000000'; // 0 = no file size limit
		$config['overwrite'] = TRUE;
		
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i]; 	

			$r=random_string();
			$pic=$image_name=$r.$_FILES['userfile']['name'];
			$ext=findexts($pic);
			$image_name=$r.".".$ext;
			$image_name=str_replace(" ","_",$image_name);
			$config['file_name']=$image_name;

			$this->upload->initialize($config);
			if($this->upload->do_upload()){
				$data = $this->upload->data();
				$name = $data['file_name'];
				$db_image[]=$image_name;
			}
			else{
				$unsucess_image[]=$_FILES['userfile']['name'];
			}
		} 

		if(empty($unsucess_image)){
			$unsucess_image = 0;
		}

		if(empty($db_image)){
			$db_image = 0;
		}


		$returns = array('success' => $db_image,
						'unsuccess' => $unsucess_image
						);

		return $returns;
	}

	protected function image_remove($path ,$name)
	{

		if(file_exists($path."/".$name)){
			unlink($path."/".$name);
		}
		
	}

	protected function checkPrevilege($task,$controller,$role){
		if($controller == "finance"){
			if($role != "finance" || $role != "system admin" || $role != "general admin"){
				$this->session->set_flashdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
				$this->session->sess_destroy();
				$redirect = base_url()."_cpanel";
			}
		}


	}

	protected function checkUsers(){

	}

}

class Office_management_controller extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		//$this->load->library('session');
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		
		if ($usertype == "admin" || $usertype == "general admin" || $usertype == "general user" || $usertype == "system admin" || $usertype == "documentation") {
			return true;
		}else{
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

}






class Mol_management_controller extends Base_Controller {

	
	public function __construct(){
		parent::__construct();
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		if ($usertype == "mol admin" || $usertype == "general admin" || $usertype == "system admin" || $usertype == "finance") {
			return true;
		}else{
			$this->session->set_userdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

}


class Interview_management_controller extends Base_Controller{
	public function __construct(){
		parent::__construct();
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		if ($usertype != "finance" || $usertype!= "mol admin") {
			return true;
		}else{
			$this->session->set_userdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}
}


class Visa_management_controller extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		if ($usertype != "documentation" || $usertype!= "mol admin" || $usertype!="finance") {
			return true;
		}else{
			$this->session->set_userdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

}

// orientation and medical and courier
class Orientation_management_controller extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		if ($usertype != "finance") {
			return true;
		}else{
			$this->session->set_userdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

}

class Finance_management_controller extends Base_Controller {
	
	public function __construct(){
		parent::__construct();
		//Condition to check whether the usertype is valid
		$usertype = strtolower($this->session->userdata('role'));
		if ($usertype == "finance" || $usertype == "general admin" || $usertype == "system admin") {
			return true;
		}else{
			$this->session->set_userdata('Errormsg',"Error! you are not authorized to access the panel. Login with your credentials");
			$this->session->sess_destroy();
			$redirect=base_url()."_cpanel";
			redirect($redirect);
		}
	}

}
