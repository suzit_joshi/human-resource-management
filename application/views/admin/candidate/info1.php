<style>
    .custom-textarea {
        height: 8% !important;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                        Candidate
                        <small>Manage the candidate information</small>
                    </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Candidate</li>
        </ol>
    </section>
    <!-- Main content -->
    <?php $this->load->view('admin/include/notification'); ?>
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <!-- Personal Information -->

                <div class='box box-info'>
                    <div class='box-header'>
                        <h3 class='box-title'>Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="col-sm-8" id="personal-information">
                            <p class="heading-candidate">
                                <?=$candidate[ 'candidate_name']?>
                            </p>
                            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                <?php echo $candidate[ 'perm_address']. "(per.)  ".$candidate[ 'temp_address']. "(tem.)"?>
                            </p>
                            <p><i class="fa fa-phone"></i>&nbsp;&nbsp;
                                <?php echo $candidate[ 'phone']. "(Res.)   ".$candidate[ 'mobile']. "(Mob.)"?>
                            </p>
                            <p><i class="fa fa-info-circle"></i> &nbsp;&nbsp;
                                <?php echo $candidate[ 'email_id']?>
                            </p>
                            <p><i class="fa fa-users"></i> &nbsp;&nbsp;
                                <?=$candidate[ 'father_name']?>(father),
                                    <?=$candidate[ 'mother_name']?>(mother)</p>
                            <p title="Current Processing Status"><i class="fa fa-tags"></i>&nbsp;&nbsp;
                                <?=$candidate[ 'current_status']?>
                            </p>
                        </div>
                        <div class="col-sm-4" style="text-align:right;">
                            <img src="<?=base_url()?>uploads/candidate/profile_image/<?=$candidate_doc['profile']?>" style="height:200px; width:auto;" />
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <!-- /.Personal Information -->

                <!--Other Personal Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Other Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="other-info-table">
                            <tr>
                                <td width="15%"><b>Date Of Birth:</b>
                                </td>
                                <td>
                                    <?php 
                                        $dob=date( "d-M-Y",strtotime($candidate[ 'dob'])); 
                                        $dob_year=date( "Y",strtotime($dob)); 
                                        $today=date( "Y"); 
                                        $years=$today - $dob_year; 
                                        echo $dob . " , ". $years . " years"; ?> ,
                                    <?=$candidate[ 'birth_place']?> (birth place)

                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Languages Known</b>
                                </td>
                                <td>
                                    <?php 
                                        $languages=unserialize($candidate[ 'language']); 
                                        foreach ($languages as $language) { 
                                            foreach ($language as $key=> $value) { 
                                                echo $key." (".$value.")"." , "; 
                                            } 
                                        } 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Body Description</b>
                                </td>
                                <td><span class="height"><?=$candidate['height']?>ft , <?=round($candidate['height']*30.48)?> cm </span>
                                    <span class="weight"><?=$candidate['weight']?> Kg, <?=round($candidate['weight']*2.20462)?></span>
                            </tr>
                            <tr>
                                <td width="15%"><b>Marital Status</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'marital_status']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Religion</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'religion']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Nationality</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'nationality']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Qualification</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'qualification']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Work Experience</b>
                                </td>
                                <td>
                                    <?=$candidate['experience']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Training</b>
                                </td>
                                <td>
                                    <?=$candidate['training']?>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <!--END Other personal INFORMATION-->

                <!--Passport and JOB Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Passport and Job Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="passport-table">
                            <tr>
                                <td><b>Applied Job</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'job_title']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Agent</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'agent_name']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Passport No</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'passport_no']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued From</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_from']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_date']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Expiration Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'expire_date']?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--END Passport and job INFORMATION-->

                <!--Work Processing Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Processing Work</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <p>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#processing-modal">New Processing Work</button>
                        </p>
                        <table class="table" id="processing-table">
                            <thead>
                                <tr>
                                    <th>S.NO.</th>
                                    <th>Processing Work</th>
                                    <th>Date</th>
                                    <th>Descriptions</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody id="processing-content">

                                <?php $counter=1 ; if($count_result_set==0 ){ ?>
                                <tr>
                                    <td colspan="5"> No processing work carried out till now. </td>
                                </tr>
                                <?php } else{ foreach($result_set as $row){ ?>
                                <tr>
                                    <td>
                                        <p style="display:none;" class="work-id">
                                            <?=$row->work_id?></p>
                                        <?=$counter?>
                                    </td>
                                    <td class="work-title">
                                        <?=$row->processing_work_title?></td>
                                    <td class="work-date">
                                        <?=$row->date?></td>
                                    <td class="work-desc">
                                        <?=$row->description?></td>
                                    <td><a href="#" class="edit-processing-info" data-toggle="modal" data-target="#edit-processing-modal"><i class="fa fa-edit" title="Edit"></i></a>
                                    </td>
                                </tr>

                                <?php $counter++; } } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!--Working Processing INFORMATION-->

                <!--Uploaded Documents Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Client Documents</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        
                        <table class="table" id="document-table">
                            <thead>
                                <tr>
                                    <th>S.NO.</th>
                                    <th>Document Title</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody id="processing-content">
                                <tr>
                                    <td>1</td>
                                    <td>Passport</td>
                                    <td><?php if($candidate_doc['passport']){?>
                                        <a href="<?=base_url()?>uploads/candidate/passport/<?=$candidate_doc['passport']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="passport">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Office Form</td>
                                    <td><?php if($candidate_doc['office_form']){?>
                                        <a href="<?=base_url()?>uploads/candidate/office_form/<?=$candidate_doc['office_form']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="office_form">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Curriculum Vitae</td>
                                    <td><?php if($candidate_doc['cv']){?>
                                        <a href="<?=base_url()?>uploads/candidate/cv/<?=$candidate_doc['cv']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="cv">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Academics</td>
                                    <td><?php 
                                            if($candidate_doc['academics']){
                                                $values = unserialize($candidate_doc['academics']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="academics">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Experience</td>
                                    <td><?php if($candidate_doc['experience']){
                                         $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="experience">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Training Documents</td>
                                    <td><?php if($candidate_doc['training']){
                                            $values = unserialize($candidate_doc['training']);
                                            foreach ($values as $row) {
                                            ?>
                                        <a href="<?=base_url()?>uploads/candidate/training/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                            }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="training">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Other Documents</td>
                                    <td><?php if($candidate_doc['other']){
                                       $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="other">Upload Now</button>
                                    <?    
                                    }
                                    ?></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!--END UPLOADED DOCUMENTS INFORMATION-->
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>

<!-- Modal for uploading more documents-->
<div class="modal fade documents-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Documents</h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>candidate/moreFiles" enctype="multipart/form-data">
            <input type="hidden" id="type" name="upload_type">
            <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="random_no" >

            <div>
                <label for="image_upload">Select Files</label>
                <input type="file" id="image_upload" required="required" name="image" >
                <p class="help-block">Please upload documents less than 5 MB</p>
            </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal for uploading more documents-->
<div class="modal fade multiple-documents-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Documents</h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>candidate/moreFiles" enctype="multipart/form-data">
            <input type="hidden" id="multi-type" name="upload_type">
            <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="random_no"  >

            <div>
                <label for="image_upload">Select Files</label>
                <input type="file" id="image_upload" name="userfile[]" 
                required="required" multiple="multiple">
                <p class="help-block">Please upload documents less than 5 MB</p>
            </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>



<!-- Modal for Personal Information-->
<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Personal Information</h4>
      </div>
      <form class="form-horizontal" role="form" action="<?=base_url()?>candidate/updateCandidate" method="post">
      <div class="modal-body">
 				
 					<input type="hidden" value="<?=base64_encode($candidate->candidate_id)?>" name="candidate_id">
 					<div class="form-group">
 						<label for="full_name" class="col-sm-4 control-label">Full Name*</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="full_name" 
 							value="<?=$candidate->candidate_name?>" name="full_name" 
 							required="required">
 						</div>
 					</div>
          
 					<div class="form-group">
 						<label for="dob" class="col-sm-4 control-label">Date of Birth*</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="datepicker" 
 							value="<?=date("d-M-Y",strtotime($candidate->date_of_birth))?>" name="dob" 
 							required="required">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="gender" class="col-sm-4 control-label">Gender*</label>
 						<div class="col-sm-8">
 							<div class="radio">
 								<label>
 									<input type="radio" name="gender" value="1" class="gender"  <?php if($candidate->gender == 1){echo "checked";}?>>
 									Male
 								</label>
 								<label>
 									<input type="radio" name="gender" value="0" class="gender" <?php if($candidate->gender == 0){echo "checked";}?>>
 									Female
 								</label>
 							</div>
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="father_name" class="col-sm-4 control-label">Father Name</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="father_name" 
 							value="<?=$candidate->father_name?>" name="father_name" 
 							required="required">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="mother_name" class="col-sm-4 control-label">Mother Name</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="mother_name" 
 							value="<?=$candidate->mother_name?>" name="mother_name" 
 							required="required">
 						</div>
 					</div> 

 					<div class="form-group">
 						<label for="perm_address" class="col-sm-4 control-label">Permanent Address</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="perm_address" 
 							value="<?=$candidate->perm_address?>" name="perm_address" 
 							required="required">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="temp_address" class="col-sm-4 control-label">Temporary Address</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="temp_address" 
 							value="<?=$candidate->temp_address?>" name="temp_address" 
 							required="required">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="phone" class="col-sm-4 control-label">Phone No</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="phone" 
 							value="<?=$candidate->phone?>" name="phone">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="mobile" class="col-sm-4 control-label">Mobile No</label>
 						<div class="col-sm-8">
 							<input type="text" class="form-control" id="mobile" 
 							value="<?=$candidate->mobile?>" name="mobile">
 						</div>
 					</div>

 					<div class="form-group">
 						<label for="email" class="col-sm-4 control-label">Email Id</label>
 						<div class="col-sm-8">
 							<input type="email" class="form-control" id="email" 
 							value="<?=$candidate->email_id?>" name="email">
 						</div>
 					</div>	

          <div class="form-group">
            <label for="height" class="col-sm-4 control-label">Height</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="height" 
              value="<?=$candidate->height?>" name="height" 
              required="required">
            </div>
          </div>

          <div class="form-group">
            <label for="weight" class="col-sm-4 control-label">Weight</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="weight" 
              value="<?=$candidate->weight?>" name="weight" 
              required="required">
            </div>
          </div>  

          <div class="form-group">

            <label for="languages" class="col-sm-4 control-label">Languages Known*</label>
              <div class="col-sm-8">    
              <?php
                $language = array("english","hindi","arabic","others");
                $language_candidate = unserialize($candidate->language);
                if($language_candidate){
                  for($i=0;$i<4;$i++){
                ?>
                  <label class="checkbox-inline">
                    <input type="checkbox" id="language" name="language[]" value="<?=$language[$i]?>" 
                    <?php if(in_array($language[$i],$language_candidate)){ echo "checked";}?>> <?=ucfirst($language[$i])?>
                  </label>
                <?    
                  }
                }else{
                   for($i=0;$i<4;$i++){
                ?>
                   <label class="checkbox-inline">
                    <input type="checkbox" id="language" name="language[]" value="<?=$language[$i]?>"> <?=ucfirst($language[$i])?>
                  </label>
                <?php
                    }
                  }
                ?>
              </div>
          </div>

          <div class="form-group">
            <?php
              $sts = $candidate->marital_status;
            ?>
            <label for="marital_status" class="col-sm-4 control-label">Marital Status*</label>
            <div class="col-sm-8">
              <div class="radio">
                <label><input type="radio" name="marital_status" value="single" <?php if($sts == "single"){ echo "checked"; }?>>Single</label>
                <label><input type="radio" name="marital_status" value="married" <?php if($sts == "married"){ echo "checked"; }?>>Married</label>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="religion" class="col-sm-4 control-label">Religion</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="religion" 
              value="<?=$candidate->religion?>" name="religion" 
              required="required">
            </div>
          </div>

          <div class="form-group">
            <label for="nationality" class="col-sm-4 control-label">Nationality</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="nationality" 
              value="<?=$candidate->nationality?>" name="nationality" 
              required="required">
            </div>
          </div>

          <div class="form-group">
            <label for="qualification" class="col-sm-4 control-label">Latest Qualification</label>
            <div class="col-md-8">                       
              <input type="text" class="form-control" name="qualification" value="<?=$candidate->qualification?>">
            </div>
          </div>

          <div class="form-group">
            <label for="other" class="col-sm-4 control-label">Other Information(if any)</label>
            <div class="col-md-8">                      
            <textarea class="custom-textarea text-area-style" name="other_info" cols="5"><?=$candidate->other_info?></textarea>
            </div>
          </div>
 	  </div>

 	  <div class="modal-footer">
 	  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	  	<button type="submit" class="btn btn-primary" id="edit-personal-save">Save changes</button>
 	  </div> 
  </form>
 	
 </div>
  </div>
</div>

<!-- Modal for Passport and JOb Information-->
<div class="modal fade" id="job-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Passport and Job Details</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form">
      		<input type="hidden" value="<?=base64_encode($candidate->candidate_id)?>" name="candidate_id" id="candidate_id">
      		<div class="form-group">
      			<label for="passport_no" class="col-sm-4 control-label">Passport No</label>
      			<div class="col-sm-8">
      				<input type="text" class="form-control" id="passport_no" 
      				value="<?=$candidate->passport_no?>" name="passport_no" 
      				required="required">
      			</div>
      		</div>

      		<div class="form-group">
      			<label for="issued_from" class="col-sm-4 control-label">Issued From</label>
      			<div class="col-sm-8">
      				<input type="text" class="form-control" id="issued_from" 
      				value="<?=$candidate->issued_from?>" name="issued_from" 
      				required="required">
      			</div>
      		</div>

      		<div class="form-group">
      			<label for="issued_date" class="col-sm-4 control-label">Issued Date</label>
      			<div class="col-sm-8">
      				<input type="text" class="form-control" id="issued_date" 
      				value="<?=date("d-M-Y",strtotime($candidate->issued_date))?>" name="issued_date" 
      				required="required">
      			</div>
      		</div>

          <div class="form-group">
            <label for="company" class="col-sm-4 control-label">Company Name</label>
            <div class="col-sm-8">
              <select name="company" class="form-control" id="company_id">
                <?php
                  $job_query = $this->db->get_where('tbl_job',array('job_id' => $candidate->job_id));
                  $job_result = $job_query->row();

                  $company_query = $this->db->get_where('tbl_company',array('company_id' => $job_result->company_id));
                  $company_result = $company_query->row();
                  $company_selected_id = $company_result->company_id;
                foreach($company as $company_row){
                  ?>
                  <option value="<?=$company_row->company_id?>" <?php if($company_row->company_id == $company_selected_id){
                    echo "selected"; }?>><?=$company_row->company_name?></option>
                  <?php   
                }
                ?>
              </select>
            </div>
          </div>

      		<div class="form-group">
      			<label for="issued_date" class="col-sm-4 control-label">Application For</label>
      			<div class="col-sm-8">
      				<select name="job" class="form-control" id="job_id">
      					<?php
      					foreach($job as $row){
      						?>
      						<option value="<?=$row->job_id?>" <?php if($candidate->job_id == $row->job_id){
      							echo "selected"; }?>><?=$row->job_title?></option>
      						<?php		
      					}
      					?>
      				</select>
      			</div>
      		</div>

      		<div class="form-group">
      			<label for="issued_date" class="col-sm-4 control-label">Reported By:</label>
      			<div class="col-sm-8">
      				<select name="agent" class="form-control" id="agent_id">
      					<option value="0">Select Agent</option>

      					<?php
      					foreach($agent as $record){
      						?>
      						<option value="<?=$record->agent_id?>" <?php if($candidate->agent_id == $record->agent_id){
      							echo "selected"; }?>><?=$record->agent_name?>
      						</option>
      						<?php		
      					}
      					?>
      				</select>
      			</div>
      		</div>
 	  </div>
 	  <div class="modal-footer">
 	  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 	  	<button type="submit" class="btn btn-primary" id="passport-edit">Save changes</button>
 	  </form>
 	</div>
 </div>
  </div>
</div>

<!-- Modal for Processing Works-->
<div class="modal fade" id="processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Processing Work Details</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post">
          <input type="hidden" value="<?=base64_encode($candidate->candidate_id)?>" name="candidate_id" id="candidate_id">

          <div class="form-group">
            <label for="issued_date" class="col-sm-4 control-label">Select Processing Work</label>
            <div class="col-sm-8">
              <select name="processing_work" class="form-control" id="processing_work">
                <?php
                foreach($processing as $row){
                  ?>
                  <option value="<?=$row->processing_work_id?>"><?=$row->processing_work_title?></option>
                  <?php   
                }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="processed_date" class="col-sm-4 control-label">Processed Date</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="processed_date" 
              value="<?=date('d-M-Y')?>" name="processed_date" 
              required="required">
            </div>
          </div>

          <div class="form-group">
            <label for="process_desc" class="col-sm-4 control-label">General Description</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3" id="process_description"></textarea>
            </div>
          </div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="process-save">Save changes</button>
    </form>
  </div>
 </div>
  </div>
</div>

<!-- Modal for Edit Processing Works-->
<div class="modal fade" id="edit-processing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Processing Work Details</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post">
          <input type="hidden" id="work_id">

          <div class="form-group">
            <label for="issued_date" class="col-sm-4 control-label">Select Processing Work</label>
            <div class="col-sm-8">
              <select name="processing_work" class="form-control" id="edit_processing_work">
                <?php
                foreach($processing as $row){
                  ?>
                  <option value="<?=$row->processing_work_id?>"><?=$row->processing_work_title?></option>
                  <?php   
                }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="processed_date" class="col-sm-4 control-label">Processed Date</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="edit_processed_date" 
              value="<?=date('d-M-Y')?>" name="processed_date" 
              required="required">
            </div>
          </div>

          <div class="form-group">
            <label for="process_desc" class="col-sm-4 control-label">General Description</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3" id="edit_process_description"></textarea>
            </div>
          </div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="edit-process-save">Save changes</button>
    </form>
  </div>
 </div>
  </div>
</div>

<!-- Modal for uploading more documents-->
<div class="modal fade" id="documents-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload More Documents</h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>candidate/moreFiles" enctype="multipart/form-data">
          <input type="hidden" value="<?=base64_encode($candidate->candidate_id)?>" name="random_no" >

          <div>
            <label for="image_upload">Select Files</label>
            <input type="file" id="image_upload" name="userfile[]" 
            required="required" multiple="multiple">
            <p class="help-block">Please upload documents less than 5 MB</p>
          </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal for Update Status-->
<div class="modal fade" id="status-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Status</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post">
          <input type="hidden" value="<?=base64_encode($candidate->candidate_id)?>" name="candidate_id" id="candidate_id">

          <div class="form-group">
            <label for="issued_date" class="col-sm-4 control-label">Select Processing Status</label>
            <div class="col-sm-8">
              <select name="status" class="form-control" id="candidate-status">
                <option value="Hold" <?php if($candidate->candidate_status == "Hold"){ echo "selected"; }?>>Hold</option>
                <option value="Processing"  <?php if($candidate->candidate_status == "Processing"){ echo "selected"; }?>>Processing</option>
                <option value="Sent"  <?php if($candidate->candidate_status == "Sent"){ echo "selected"; }?>>Sent</option>
                <option value="Returned"  <?php if($candidate->candidate_status == "Returned"){ echo "selected"; }?>>Returned</option>
              </select>
            </div>
          </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="status-save">Save changes</button>
    </form>
  </div>
 </div>
  </div>
</div>
        



<script>
$(document).on('click','.upload-docs',function(e){
    var typeVal = $(this).attr('type');
    $('#type').val(typeVal);
});

$(document).on('click','.multi-upload-docs',function(e){
    var typeVal = $(this).attr('type');
    $('#multi-type').val(typeVal);
});

</script>

<script>
  $(function(){
    $("#datepicker").datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });

$(function(){
    $("#issued_date").datepicker({
    	yearRange: "-50:+0",
    dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });

$(function(){
    $("#processed_date").datepicker({
      yearRange: "-50:+0",
    dateFormat: "dd-M-yy",
    showAnim : "blind",
    changeMonth: true,
    changeYear: true
  });
  });

$(function(){
    $("#edit_processed_date").datepicker({
      yearRange: "-50:+0",
    dateFormat: "dd-M-yy",
    showAnim : "blind",
    changeMonth: true,
    changeYear: true
  });
  });
</script>

<script>
var workId,workTitle,workDate,workDesc;
  /*Generating the value of processing jobs and passing to modal*/
  $('.edit-processing-info').on('click',function(e){
    e.preventDefault();
    workId = $(this).parents('tr').find('.work-id').text();
    workTitle = $(this).parents('tr').find('.work-title').text();
    workDate = $(this).parents('tr').find('.work-date').text();
    workDesc = $(this).parents('tr').find('.work-desc').text();

    $('#edit_processing_work').val(workTitle);
    $('#edit_processed_date').val(workDate);
    $('#edit_process_description').val(workDesc);
    $('#work_id').val(workId);
    $('#edit_processing_work option').each(function(){
      if($(this).text() == workTitle){
        $(this).prop('selected',true);
      }
    });
  })

/*EDIT THE PASSPORT DETAILS THROUGH AJAX*/
    $('#passport-edit').click(function(e){
        e.preventDefault();
        var pp_no = $('#passport_no').val();
        var issued_date = $('#issued_date').val();
        var issued_from = $('#issued_from').val();
        var agent_id = $('#agent_id').val();
        var job_id = $('#job_id').val();
        var candidate_id = $('#candidate_id').val();

        $.ajax({
        	type: "POST",
        	url: "<?=base_url()?>candidate/editCandidate",
        	data: { 
        		candidate_id:candidate_id,
        		passport_no : pp_no,
        		issued_date : issued_date,
        		issued_from : issued_from,
        		agent_id : agent_id,
        		job_id : job_id,
        		type : "passport"
        	},
        	success: function(data) {
        		$('#passport-table').html(data);
        		$('#job-modal').modal('hide');
        	}
        });
    });   


/*SAVING PROCESSING WORKS THROUGH AJAX*/
    $('#process-save').click(function(e){
        e.preventDefault();
        var processed_date = $('#processed_date').val();
        var general_desc = $('#process_description').val();
        var processing_work_id = $('#processing_work').val();
        var type;

        var candidate_id = $('#candidate_id').val();

        $.ajax({
          type: "POST",
          url: "<?=base_url()?>candidate/saveProcessing",
          data: { 
            candidate_id:candidate_id,
            processed_date : processed_date,
            general_desc : general_desc,
            processing_work_id : processing_work_id,
            type : 'new'
          },
          success: function(data) {
            $('#processing-content').html(data);
            $('#processing-modal').modal('hide');
          }
        });
    });  

    /*SAVING EDIT PROCESSING WORKS THROUGH AJAX*/
    $('#edit-process-save').click(function(e){
        e.preventDefault();
        var processed_date = $('#edit_processed_date').val();
        var general_desc = $('#edit_process_description').val();
        var processing_work_id = $('#edit_processing_work').val();
        var type;
        var work_id = $('#work_id').val();

        $.ajax({
          type: "POST",
          url: "<?=base_url()?>candidate/saveProcessing",
          data: { 
            work_id:work_id,
            processed_date : processed_date,
            general_desc : general_desc,
            processing_work_id : processing_work_id,
            type: 'edit'
          },
          success: function(data) {
            $('#processing-content').html(data);
            $('#processing-modal').modal('hide');
          }
        });
    });

    /*GETTING INFORMATION FOR THE COMPANY*/
    $('#company_id').on('change',function(e){
      var companyId = $('#company_id option:selected').val();
      $.ajax({
        type: "POST",
        url: "<?=base_url()?>candidate/getCompanyJob",
        data: { 
          company_id : companyId
        },
        success: function(data) {
          $('#job_id').html(data);
        }
      });
    });

    /*CHANGING THE STATUS*/
    $(document).on('click','#status-save',function(e){
      e.preventDefault();
      var statusValue = $('#candidate-status option:selected').val();
       var candidate_id = $('#candidate_id').val();
      $.ajax({
        type: "POST",
        url: "<?=base_url()?>candidate/updateCandidateStatus",
        data: { 
          status : statusValue,
          candidate_id : candidate_id
        },
        success: function(data) {
          alert("Status Updated successfully");
          $('#status-modal').modal('hide');
        }
      });
    });


    /*CHANGING THE Height*/
    $(document).on('change','.height-change',function(e){
      var unit = $('.height-change option:selected').val();
      var heightVal = $('.height').attr('data');

      if(unit != "0"){
        $.ajax({
          type: "POST",
          url: "<?=base_url()?>candidate/changeUnit",
          data: { 
            convert : unit,
            type : "height",
            value : heightVal
          },
          success: function(data) {
            $('.height').html(data);
          }
        });
      }
    });


    /*changing the weight*/
    $(document).on('change','.weight-change',function(e){
      var unit = $('.weight-change option:selected').val();
      var weightVal = $('.weight').attr('data');

      if(unit != "0"){
        $.ajax({
          type: "POST",
          url: "<?=base_url()?>candidate/changeUnit",
          data: { 
            convert : unit,
            type : "weight",
            value : weightVal
          },
          success: function(data) {
            $('.weight').html(data);
          }
        });
      }
    });

    /*EDITING THE PERSONAL INFORMATION
    $(document).on('click','#edit-personal-save',function(e){
      e.preventDefault();
      var candidate_id = $('#candidate_id').val();
      var name = $('#full_name').val();
      var dob = $('#datepicker').val();
      var gender = $('.gender:checked').val();
      var father_name = $('#father_name').val();
      var mother_name = $('#mother_name').val();
      var perm_address = $('#perm_address').val();
      var temp_address = $('#temp_address').val();
      var mobile = $('#mobile').val();
      var phone = $('#phone').val();
      var email = $('#email').val();
      $.ajax({
        type: "POST",
        url: "<?=base_url()?>candidate/updateCandidate",
        data: { 
          candidate_id : candidate_id,
          name : name,
          dob : dob,
          gender :gender,
          father_name : father_name,
          mother_name : mother_name,
          perm_address : perm_address,
          temp_address : temp_address,
          mobile : mobile,
          phone : phone,
          email : email
        },
        success: function(data) {
          alert("Changes saved successfully");
          $('#personal-information').html(data);
          $('#info-modal').modal('hide');
        }
      });
    });*/

</script>


