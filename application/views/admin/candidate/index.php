<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
      Candidate
      <small>Manage your candidates</small>
  </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Candidates</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); $role = strtolower($this->session->userdata('role')); ?>
    <?php $msg=$this->session->userdata('candidate_data'); if($msg!=""){ ?>
    <div class="alert alert-info alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=$msg?>
    </div>

    <?php } ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class="box">
                    <?php 
                        $role   =   strtolower($this->session->userdata('role')); 
                        if( $role == "documentation" || $role == "admin" || $role == "general admin" || $role == "system admin"){ 
                    ?>
                    <div class="box-footer">
                        <button class="btn btn-primary" onClick="javascript:location.replace('<?=base_url()?>candidate/newCandidate')">
                            Add New</button>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Other Option <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <?php 
                                    if($role=="documentation" || $role=="general admin" || $role=="system admin" ){ 
                                ?>
                                <li> <a href="<?=base_url()?>candidate/getTransferList/" id="transfer-rec">Transfer to Recruitment Department</a>
                                </li>
                                <li class="divider"></li>
                                <li> <a href="#">Edit</a>
                                </li>
                                <?php }else{ echo "<li><a href='#'>No options available for your role</a></li>"; } ?>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>
                    <?php 
                        if(isset($current_status)){ 
                            $val=$current_status; 
                        }
                        else{ 
                            $val="" ; 
                        } 
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-footer">
                                <p>Filter Option</p>
                                <form>
                                    <select class="form-control" id="selection_option">
                                        <option value="all" <?=($val=="all" )? "selected": "" ?>>All</option>
                                        <option value="new" <?=($val=="new" )? "selected": "" ?>>New</option>
                                        <option value="applied" <?=($val=="applied" )? "selected": "" ?>>Applied</option>
                                        <option value="preinterview_passed" <?=($val=="pre_interview_passed" )? "selected": "" ?>>Pre Interview Passed</option>
                                        <option value="finalinterview_passed" <?=($val=="pre_interview_passed" )? "selected": "" ?>>Final Interview Passed</option>
                                        <option value="medical_passed" <?=($val=="medical_passed" )? "selected": "" ?>>Medical Passed</option>
                                        <option value="orientation_appeared" <?=($val=="orientation_appeared" )? "selected": "" ?>>Orientation Appeared</option>
                                        <option value="visa_processing" <?=($val=="visa_processing" )? "selected": "" ?>>Visa Processing</option>
                                        <option value="visa_received" <?=($val=="visa_received" )? "selected": "" ?>>Visa Received</option>
                                        <option value="ticket_processing" <?=($val=="ticket_processing" )? "selected": "" ?>>Ticket Processing</option>
                                        <option value="ticket_received" <?=($val=="ticket_received" )? "selected": "" ?>>Ticket Received</option>
                                        <option value="final_approval_received" <?=($val=="final_approval_received" )? "selected": "" ?>>Final Approval Received</option>
                                    </select>
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="box-body table-responsive">
                        <table id="candidate" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">S.No</th>
                                    <th width="20%">Full Name</th>
                                    <th>Job & Company</th>
                                    <th>Passport No</th>
                                    <th>Contact No</th>
                                    <th width="15%">Referred By</th>
                                    <th width="5%">Status</th>
                                    <th width="5%">Option</th>
                                </tr>
                            </thead>
                            <tbody id="candidate-detail">
                                <?php 
                                    $counter=1 ; 
                                    foreach($candidate_result as $candidate){ 
                                        $id=$candidate->candidate_id; 
                                        $encrypted_id = base64_encode($id); 
                                ?>
                                <tr>
                                    <td><?=$counter?></td>
                                    <td><?=$candidate->candidate_name?></td>
                                    <td><?=$candidate->job_title?><br><?=$candidate->company_name?></td>
                                    <td><?=$candidate->passport_no?></td>
                                    <td><?=$candidate->mobile. ", ".$candidate->phone?></td>
                                    <td><?php 
                                        if($candidate->agent_id == "0"){ 
                                            echo "Self"; 
                                        } 
                                        elseif (is_null($candidate->agent_id)) { 
                                            echo "NO Name"; 
                                        }
                                        else{ 
                                            echo $candidate->agent_name." , ". $candidate->agent_mob; 
                                        }
                                        ?>
                                    </td>
                                    <td><?php 
                                        $b=$candidate->current_status; 
                                        $pos = strpos($b, "_"); 
                                        if ($pos === false) { 
                                            echo ucfirst($b); 
                                        }
                                        else{ 
                                            $b = explode('_',$b); 
                                            echo ucfirst($b[0]).' '.$b[1]; 
                                            } 
                                        ?>
                                    </td>
                                    <td job-id="<?=$id?>">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li> <a href="<?=base_url()?>candidate/getCandidateDetails/<?=$encrypted_id?>" onclick="javascript:getCandidate('<?=$encrypted_id?>')">View</a>
                                                </li>
                                                <?php if($role=="recruitment" || $role=="admin" || $role=="system admin" || $role=="general admin" ){ ?>
                                                <li class="divider"></li>
                                                <li> <a href="<?=base_url()?>candidate/editCandidateDetails/<?=$encrypted_id?>">Edit</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li><a href="#" onclick="return confirmDeactivate();">Deactivate</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li> <a href="#" class="cv-select-option" candidate-id="<?=$id?>">Generate CV</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li> <a href="<?=base_url()?>candidate/generateForm/<?=$encrypted_id?>" target="_blank">Generate Office Form</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li> <a href="<?=base_url()?>candidate/generateCoverLetter/<?=$encrypted_id?>" target="_blank">Generate Cover Letter</a>
                                                </li>
                                                <?php } ?>

                                                <?php if($role=="documentation" ){ ?>
                                                <li class="divider"></li>
                                                <li> <a href="#" class="cv-select-option" candidate-id="<?=$id?>">Generate CV</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li> <a href="<?=base_url()?>candidate/generateForm/<?=$encrypted_id?>" target="_blank">Generate Office Form</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li> <a href="<?=base_url()?>candidate/generateCoverLetter/<?=$encrypted_id?>" target="_blank">Generate Cover Letter</a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                                <?php $counter+=1; } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
</aside>

<!--MODAL to choose curriculum vitae for users-->
<div class="modal fade" id="cv-sample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Select CV Sample</h4> </div>
            <form class="form-horizontal" role="form" action="<?=base_url()?>candidate/viewPdf" method="post" target="_blank">
                <div class="modal-body">
                    <input type="hidden" id="candidate-id-for-cv" name="random">
                    <div class="form-group">
                        <label class="col-sm-4">Select Sample</label>
                        <div class="col-sm-8">
                            <select name="cv_sample" class="form-control">
                                <option value="1">Sample 1</option>
                                <option value="2">Sample 2</option>
                                <option value="3">Sample 3</option>
                                <option value="4">Sample 4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            <!--END FORM-->
        </div>
    </div>
</div>
<!--END MODAL TO choose curriculum vitae for users-->


<script type="text/javascript">

    function confirmCandidateDel(candidateId) {
        if (confirm("Are you sure you want to delete?")) {
            var path = "<?=base_url()?>candidate/deleteCandidate/" + candidateId;
            window.location.href = path;
            return true;

        } else {
            return false;
        }
    }


    $('.cv-select-option').on('click', function (e) {
        e.preventDefault();
        var candidateId = $(this).attr('candidate-id');
        $('#candidate-id-for-cv').val(candidateId);
        $('#cv-sample').modal();
    });

    $(document).on('change', '#selection_option', function (e) {
        var status = $('#selection_option option:selected').val();
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>candidate/getCandidateList",
            data: {
                status: status,
            },
            success: function (data) {
                $('#candidate-detail').html(data);
            }
        });
    });
</script>