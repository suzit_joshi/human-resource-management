<style>
    .custom-textarea {
        height: 8% !important;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                        Candidate
                        <small>Manage the candidate information</small>
                    </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Candidate</li>
        </ol>
    </section>
    <!-- Main content -->
    <?php $this->load->view('admin/include/notification'); ?>
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <!-- Personal Information -->

                <div class='box box-info'>
                    <div class='box-header'>
                        <h3 class='box-title'>Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="col-sm-8" id="personal-information">
                            <p class="heading-candidate">
                                <?=$candidate[ 'candidate_name']?>
                            </p>
                            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                <?php echo $candidate[ 'perm_address']. "(per.)  ".$candidate[ 'temp_address']. "(tem.)"?>
                            </p>
                            <p><i class="fa fa-phone"></i>&nbsp;&nbsp;
                                <?php echo $candidate[ 'phone']. "(Res.)   ".$candidate[ 'mobile']. "(Mob.)"?>
                            </p>
                            <p><i class="fa fa-info-circle"></i> &nbsp;&nbsp;
                                <?php echo $candidate[ 'email_id']?>
                            </p>
                            <p><i class="fa fa-users"></i> &nbsp;&nbsp;
                                <?=$candidate[ 'father_name']?>(father),
                                    <?=$candidate[ 'mother_name']?>(mother)</p>
                            <p title="Current Processing Status"><i class="fa fa-tags"></i>&nbsp;&nbsp;
                                <?=$candidate[ 'current_status']?>
                            </p>

                             <p><i class="fa fa-pencil"></i> &nbsp;&nbsp;
                                <a href="#" data-toggle="modal" data-target="#info-modal" style="font-size:10px;">Edit personal information</a>
                            </p>
                        </div>
                        <div class="col-sm-4" style="text-align:right;">
                            <img src="<?=base_url()?>uploads/candidate/profile_image/<?=$candidate_doc['profile']?>" style="height:200px; width:auto;" />
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <!-- /.Personal Information -->

                <!--Other Personal Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Other Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="other-info-table">
                            <tr>
                                <td width="15%"><b>Date Of Birth:</b>
                                </td>
                                <td>
                                    <?php 
                                        $dob=date( "d-M-Y",strtotime($candidate[ 'dob'])); 
                                        $dob_year=date( "Y",strtotime($dob)); 
                                        $today=date( "Y"); 
                                        $years=$today - $dob_year; 
                                        echo $dob . " , ". $years . " years"; ?> ,
                                    <?=$candidate[ 'birth_place']?> (birth place)

                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Languages Known</b>
                                </td>
                                <td>
                                    <?php 

                                        $languages=unserialize($candidate[ 'language']); 


                                            foreach ($languages as $key=> $value) { 
                                                if($value==1){
                                                    $a="Good";
                                                }elseif($value==2){
                                                    $a="better";
                                                }elseif($value==3){
                                                    $a="excellent";
                                                }else{
                                                    $a="unknown";
                                                }
                                                echo $key." (".$a.")"." , "; 

                                            } 
                                        
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Body Description</b>
                                </td>
                                <td><span class="height"><?=$candidate['height']?>ft , <?=round($candidate['height']*30.48)?> cm </span>
                                    <span class="weight"><?=$candidate['weight']?> Kg, <?=round($candidate['weight']*2.20462)?></span>
                            </tr>
                            <tr>
                                <td width="15%"><b>Marital Status</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'marital_status']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Religion</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'religion']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Nationality</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'nationality']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Qualification</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'qualification']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Work Experience</b>
                                </td>
                                <td>
                                    <?=$candidate['experience']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Training</b>
                                </td>
                                <td>
                                    <?=$candidate['training']?>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <!--END Other personal INFORMATION-->

                <!--Passport and JOB Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Passport and Job Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="passport-table">
                            <tr>
                                <td><b>Applied Job</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'job_title']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Agent</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'agent_name']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Passport No</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'passport_no']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued From</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_from']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_date']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Expiration Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'expire_date']?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><i class="fa fa-pencil"></i> &nbsp;&nbsp;
                                <a href="#"  data-toggle="modal" data-target="#job-modal" style="font-size:10px;"> Edit passport & job information</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--END Passport and job INFORMATION-->



                <!--Uploaded Documents Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Client Documents</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        
                        <table class="table" id="document-table">
                            <thead>
                                <tr>
                                    <th>S.NO.</th>
                                    <th>Document Title</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody id="processing-content">
                                <tr>
                                    <td>1</td>
                                    <td>Passport</td>
                                    <td><?php if($candidate_doc['passport']){?>
                                        <a href="<?=base_url()?>uploads/candidate/passport/<?=$candidate_doc['passport']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="passport">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Office Form</td>
                                    <td><?php if($candidate_doc['office_form']){?>
                                        <a href="<?=base_url()?>uploads/candidate/office_form/<?=$candidate_doc['office_form']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="office_form">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Curriculum Vitae</td>
                                    <td><?php if($candidate_doc['cv']){?>
                                        <a href="<?=base_url()?>uploads/candidate/cv/<?=$candidate_doc['cv']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="cv">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Academics</td>
                                    <td><?php 
                                            if($candidate_doc['academics']){
                                                $values = unserialize($candidate_doc['academics']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="academics">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                   <td>Experience</td>
                                    <td><?php if($candidate_doc['experience']){
                                         $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="experience">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Training Documents</td>
                                    <td><?php 
                                        if($candidate_doc['training']){                                            
                                            $values = unserialize($candidate_doc['training']);
                                            if($values){
                                                echo "here";
                                            foreach ($values as $row) {
                                            ?>
                                        <a href="<?=base_url()?>uploads/candidate/training/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                        <?php
                                            }
                                        }
                                        else{?>
                                            <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="training">Upload Now</button>
                                        <?php
                                        }    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Other Documents</td>
                                    <td><?php if($candidate_doc['other']){
                                       $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="other">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!--END UPLOADED DOCUMENTS INFORMATION-->
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>

<!-- Modal for uploading more documents-->
<div class="modal fade documents-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Documents</h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>candidate/moreFiles" enctype="multipart/form-data">
            <input type="hidden" id="type" name="upload_type">
            <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="random_no" >

            <div>
                <label for="image_upload">Select Files</label>
                <input type="file" id="image_upload" required="required" name="image" >
                <p class="help-block">Please upload documents less than 5 MB</p>
            </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal for uploading more documents-->
<div class="modal fade multiple-documents-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Documents</h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>candidate/moreFiles" enctype="multipart/form-data">
            <input type="hidden" id="multi-type" name="upload_type">
            <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="random_no"  >

            <div>
                <label for="image_upload">Select Files</label>
                <input type="file" id="image_upload" name="userfile[]" 
                required="required" multiple="multiple">
                <p class="help-block">Please upload documents less than 5 MB</p>
            </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!--Modal for editing personal information-->
<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Personal Information</h4>
            </div>
            <form class="form-horizontal" role="form" action="<?=base_url()?>candidate/updateCandidate" method="post">
                <div class="modal-body">

                    <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="candidate_id">
                    <div class="form-group">
                        <label for="full_name" class="col-sm-4 control-label">Full Name*</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="full_name" value="<?=$candidate['candidate_name']?>" name="full_name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dob" class="col-sm-4 control-label">Date of Birth*</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="datepicker" value="<?=date("d-M-Y ",strtotime($candidate['dob']))?>" name="dob" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="gender" class="col-sm-4 control-label">Gender*</label>
                        <div class="col-sm-8">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="Male" class="gender" <?php if($candidate['gender'] == "Male"){echo "checked";}?>> Male
                                </label>
                                <label>
                                    <input type="radio" name="gender" value="Female" class="gender" <?php if($candidate['gender'] == "Female"){echo "checked";}?>> Female
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="father_name" class="col-sm-4 control-label">Father Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="father_name" value="<?=$candidate['father_name']?>" name="father_name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mother_name" class="col-sm-4 control-label">Mother Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mother_name" value="<?=$candidate['mother_name']?>" name="mother_name" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="perm_address" class="col-sm-4 control-label">Permanent Address</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="perm_address" value="<?=$candidate['perm_address']?>" name="perm_address" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="temp_address" class="col-sm-4 control-label">Temporary Address</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="temp_address" value="<?=$candidate['temp_address']?>" name="temp_address" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-sm-4 control-label">Phone No</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="phone" value="<?=$candidate['phone']?>" name="phone">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mobile" class="col-sm-4 control-label">Mobile No</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mobile" value="<?=$candidate['mobile']?>" name="mobile">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email Id</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" value="<?=$candidate['email_id']?>" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="height" class="col-sm-4 control-label">Height</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="height" value="<?=$candidate['height']?>" name="height" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="weight" class="col-sm-4 control-label">Weight</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="weight" value="<?=$candidate['weight']?>" name="weight" required="required">
                        </div>
                    </div>

                    <?php
                        $language = unserialize($candidate['language']);
                    ?>

                    <div class="form-group">
                        <label for="languages" class="col-sm-4 control-label">Languages Known*</label>
                        <div class="col-sm-8">
                            <table class="table table-bordered">
                            <?php
                            
                             $other=array_keys($language)[3];

                             ?>
                                <tr>
                                    <th>Lanugages</th>
                                    <th>Good</th>
                                    <th>Better</th>
                                    <th>Excellent</th>
                                </tr>
                                <tr>
                                    <td>English</td>
                                    <td><input type="radio" value="1" name="eng_language" <?=$language['english'] == 1 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="2" name="eng_language" <?=$language['english'] == 2 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="3" name="eng_language" <?=$language['english'] == 3 ? "checked" : ""?>></td>
                                </tr>
                                <tr>
                                    <td>Arabic</td>
                                    <td><input type="radio" value="1" name="n_language" <?=$language['arabic'] == 1 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="2" name="n_language" <?=$language['arabic'] == 2 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="3" name="n_language" <?=$language['arabic'] == 3 ? "checked" : ""?>></td>
                                </tr>  
                                <tr>
                                    <td>Hindi</td>
                                    <td><input type="radio" value="1" name="h_language" <?=$language['hindi'] == 1 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="2" name="h_language" <?=$language['hindi'] == 2 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="3" name="h_language" <?=$language['hindi'] == 3 ? "checked" : ""?>></td>
                                </tr>
                                <tr>
                                  
                                    <td><input type="text" name="otherLanguage" value="<?=$other?>" placeholder="Enter language"></td>
                                    <td><input type="radio" value="1" name="o_language" <?=$language[$other] == 1 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="2" name="o_language" <?=$language[$other] == 2 ? "checked" : ""?>></td>
                                    <td><input type="radio" value="3" name="o_language" <?=$language[$other] == 3 ? "checked" : ""?>></td>
                                </tr>

                            </table>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php $sts=$candidate['marital_status']; ?>
                        <label for="marital_status" class="col-sm-4 control-label">Marital Status*</label>
                        <div class="col-sm-8">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="marital_status" value="single" <?php if($sts=="single" ){ echo "checked"; }?>>Single</label>
                                <label>
                                    <input type="radio" name="marital_status" value="married" <?php if($sts=="married" ){ echo "checked"; }?>>Married</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="religion" class="col-sm-4 control-label">Religion</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="religion" value="<?=$candidate['religion']?>" name="religion" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nationality" class="col-sm-4 control-label">Nationality</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nationality" value="<?=$candidate['nationality']?>" name="nationality" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="qualification" class="col-sm-4 control-label">Latest Qualification</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="qualification" value="<?=$candidate['qualification']?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="other" class="col-sm-4 control-label">Training(if any)</label>
                        <div class="col-md-8">
                            <textarea class="custom-textarea text-area-style" name="training" cols="5">
                                <?=$candidate['training']?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="other" class="col-sm-4 control-label">Experience(if any)</label>
                        <div class="col-md-8">
                            <textarea class="custom-textarea text-area-style" name="experience" cols="5">
                                <?=$candidate['experience']?></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="edit-personal-save">Save changes</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal for Passport and JOb Information-->
<div class="modal fade" id="job-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Passport and Job Details</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" action="<?=base_url()?>candidate/updatePassportInfo" method="post">
                    <input type="hidden" value="<?=base64_encode($candidate['candidate_id'])?>" name="candidate_id" id="candidate_id">
                    <div class="form-group">
                        <label for="passport_no" class="col-sm-3 control-label">Passport No</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="passport_no" value="<?=$candidate['passport_no']?>" name="passport_no" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="issued_from" class="col-sm-3 control-label">Issued From</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="issued_from" value="<?=$candidate['issued_from']?>" name="issued_from" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="issued_date" class="col-sm-3 control-label">Issued Date</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="issued_date" value="<?=date(" d-M-Y ",strtotime($candidate['issued_date']))?>" name="issued_date" required="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="expired_date" class="col-sm-3 control-label">Expired Date</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="expired_date" value="<?=date(" d-M-Y ",strtotime($candidate['expire_date']))?>" name="expired_date" required="required">
                        </div>
                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="edit-personal-save">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<script>
$(document).on('click','.upload-docs',function(e){
    var typeVal = $(this).attr('type');
    $('#type').val(typeVal);
});

$(document).on('click','.multi-upload-docs',function(e){
    var typeVal = $(this).attr('type');
    $('#multi-type').val(typeVal);
});

$(function(){
    $("#issued_date" ).datepicker({
        dateFormat: "dd-M-yy",
        showAnim : "blind",
        changeMonth: true,
        changeYear: true
    });
  });

$(function(){
    $("#expired_date" ).datepicker({
        dateFormat: "dd-M-yy",
        showAnim : "blind",
        changeMonth: true,
        changeYear: true
    });
  });

</script>