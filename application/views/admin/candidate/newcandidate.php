<style>
.wizard > .steps > ul > li {
    float: left;
    width: 20% !important;
}

.custom-textarea{
	height: 8% !important;
}

.content .clearfix{
	height :auto !important;
}

.wizard > .content > .body{
	overflow-y:auto; 
}

.chzn-container, .chzn-search input , .chzn-results, .chzn-drop {
	width:100% !important;
	}
.content{
	height: auto !important;
	overflow: hidden !important;
}

.form-horizontal h4{
	text-transform: uppercase;
	font-weight: bold;
	margin-top: 50px;
	color: #3C8DBC;
}

.form-horizontal h4:first-child{
	margin-top: 0px;
}
</style>
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Candidate
                        <small>Manage the candidate information</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Candidate</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content" style="height:450px !important;">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title" style="display:block;">New Candidate</h3><br />
                                	<h7 class="pull-right"><i>Note: Field marked with * are compulsory.</i></h7>
                                </div>
                                

                                <div class='box-body pad'>
                                	<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="<?=base_url()?>candidate/save">
                                		<h4>Personal Information</h4>
									    <div>
								    		<div class="form-group">
												<label for="full_name" class="col-sm-2 control-label">Full Name*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="full_name" 
													placeholder="Enter Full Name" name="full_name" 
													required="required">
												</div>
											</div>

											<div class="form-group">
												<label for="dob" class="col-sm-2 control-label">Gender*</label>
												<div class="col-sm-10">
													<div class="radio">
													  <label>
													    <input type="radio" name="gender" value="Male" checked="checked">
													    Male
													  </label>
													  <label>
													    <input type="radio" name="gender" value="Female">
													    Female
													  </label>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="dob" class="col-sm-2 control-label">Date of Birth*</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="datepicker" 
													value="<?=date('d-M-Y')?>" name="dob" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="birth_place" class="col-sm-2 control-label">Birth Place</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="birth_place" 
													placeholder="Enter birth place" name="birth_place" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="height" class="col-sm-2 control-label">Height</label>
												<div class="col-sm-5">
													<div class="input-group">
													<input type="text" class="form-control height" id="height-ft" 
													placeholder="Enter height in ft." name="height" 
													><div class="input-group-addon">.ft</div>
													</div>
												</div>

												<div class="col-sm-5">
													<div class="input-group">
													<input type="text" class="form-control height" id="height-cm" 
													placeholder="Enter height in cm."  
													><div class="input-group-addon">.cm</div>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="height" class="col-sm-2 control-label">Weight</label>
												<div class="col-sm-5">
													<div class="input-group">
													<input type="text" class="form-control weight" id="weight-kg" 
													placeholder="Enter weight in Kg." name="weight" 
													><div class="input-group-addon">.Kg</div>
													</div>
												</div>

												<div class="col-sm-5">
													<div class="input-group">
													<input type="text" class="form-control weight" id="weight-pound" 
													placeholder="Enter weight in pound."  
													><div class="input-group-addon">.pound</div>
													</div>
												</div>
											</div>

														
										
											<div class="form-group">
												<label for="profile_image" class="col-sm-2 control-label">Upload Profile Image</label>
												<div class="col-sm-10">
													<input type="file" name="profile_image" id="profile_image"/>
												</div>
											</div>
	                               		</div>

	                               		<h4>Other Information</h4>
	                               		<div>
	                               			<div class="form-group">
												<label for="languages" class="col-sm-2 control-label">Languages Known*</label>
												<div class="col-sm-10">
													<table class="table table-bordered">

						                                <tr>
						                                    <th>Lanugages <button id="clearRadioBtn">Reset</button> </th>
						                                    <th>Good</th>
						                                    <th>Better</th>
						                                    <th>Excellent</th>
						                                </tr>
						                                <tr>
						                                    <td>English</td>
						                                    <td><input type="radio" value="1" name="eng_language" class="langRadio"></td>
						                                    <td><input type="radio" value="2" name="eng_language"  class="langRadio"></td>
						                                    <td><input type="radio" value="3" name="eng_language" class="langRadio"></td>
						                                </tr>
						                                <tr>
						                                    <td>Arabic</td>
						                                    <td><input type="radio" value="1" name="n_language" class="langRadio"></td>
						                                    <td><input type="radio" value="2" name="n_language" class="langRadio"></td>
						                                    <td><input type="radio" value="3" name="n_language" class="langRadio"></td>
						                                </tr>  
						                                <tr>
						                                    <td>Hindi</td>
						                                    <td><input type="radio" value="1" name="h_language" class="langRadio"></td>
						                                    <td><input type="radio" value="2" name="h_language" class="langRadio"></td>
						                                    <td><input type="radio" value="3" name="h_language" class="langRadio"></td>
						                                </tr>
						                                <tr>
						                                    <td><input type="text" name="otherLanguage" placeholder="Enter language"></td>
						                                    <td><input type="radio" value="1" name="o_language" class="langRadio"></td>
						                                    <td><input type="radio" value="2" name="o_language" class="langRadio"></td>
						                                    <td><input type="radio" value="3" name="o_language" class="langRadio"></td>
						                                </tr>

						                            </table>
												</div>
											</div>

	                               			<div class="form-group">
												<label for="marital_status" class="col-sm-2 control-label">Marital Status*</label>
												<div class="col-sm-10">
													<div class="radio">
													  <label>
													    <input type="radio" name="marital_status" value="single" checked="checked">
													    Single
													  </label>
													  <label>
													    <input type="radio" name="marital_status" value="married">
													    Married
													  </label>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="religion" class="col-sm-2 control-label">Religion</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="religion" 
													placeholder="Enter religion" name="religion" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="nationality" class="col-sm-2 control-label">Nationality</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="nationality" 
													value="Nepalese" name="nationality" 
													>
												</div>
											</div>

	                               			<div class="form-group">
												<label for="father_name" class="col-sm-2 control-label">Father Name</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="father_name" 
													placeholder="Enter Father Name" name="father_name" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="mother_name" class="col-sm-2 control-label">Mother Name</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="mother_name" 
													placeholder="Enter Mother Name" name="mother_name" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="qualification" class="col-sm-2 control-label">Latest Qualification</label>
												<div class="col-md-10">												
													<input type="text" name="qualification"  class="form-control"
													placeholder="Enter latest qualification">
												</div>
											</div>
	                               		</div>

                                		<h4>Contact Information</h4>
									    <div>
								    		<div class="form-group">
												<label for="perm_address" class="col-sm-2 control-label">Permanent Address</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="perm_address" 
													placeholder="Enter permanent address" name="perm_address" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="temp_address" class="col-sm-2 control-label">Temporary Address</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="temp_address" 
													placeholder="Enter temporary address" name="temp_address" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="phone" class="col-sm-2 control-label">Phone No</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="phone" 
													placeholder="Enter phone number" name="phone">
												</div>
											</div>

											<div class="form-group">
												<label for="mobile" class="col-sm-2 control-label">Mobile No</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="mobile" 
													placeholder="Enter mobile number" name="mobile">
												</div>
											</div>

											<div class="form-group">
												<label for="email" class="col-sm-2 control-label">Email Id</label>
												<div class="col-sm-10">
													<input type="email" class="form-control" id="email" 
													placeholder="Enter email address" name="email">
												</div>
											</div>
	                               		</div>
									    									 
									    <h4>Passport Information</h4>
									    <div>
									    	<div class="form-group">
												<label for="issued_from" class="col-sm-2 control-label">Original Passport Submitted</label>
												<div class="col-sm-10">
													<input type="checkbox" class="form-control" name="original_pp" value="1">
												</div>
											</div>

								    		<div class="form-group">
												<label for="passport_no" class="col-sm-2 control-label">Passport No</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="passport_no" 
													placeholder="Enter passport number" name="passport_no" 
													required="required">
												</div>
											</div>

											<div class="form-group">
												<label for="issued_from" class="col-sm-2 control-label">Issued From</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="issued_from" 
													placeholder="Enter Issued Place" name="issued_from" 
													>
												</div>
											</div>


											<div class="form-group">
												<label for="issued_date" class="col-sm-2 control-label">Issued Date</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="issued_date" 
													value="<?=date('d-M-Y')?>" name="issued_date" 
													>
												</div>
											</div>

											<div class="form-group">
												<label for="expiration_date" class="col-sm-2 control-label">Expiration Date</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="expiration_date" 
													value="<?=date('d-M-Y')?>" name="expiration_date" 
													 disabled="disabled">
												</div>
											</div>

											<div class="form-group">
												<label for="passport_img" class="col-sm-2 control-label">Upload Passport Image</label>
												<div class="col-sm-10">
													<input type="file" name="passport_image" id="image-change"/>
												</div>
											</div>
                                		</div>

                                		<h4>Job Description</h4>
											<div>
												<div class="form-group">
													<label for="issued_date" class="col-sm-2 control-label">Application For</label>
													<div class="col-sm-10">
														<select name="job" class="chzn-select form-control" 
														tabindex="4" required>
														<option value="0">Select Job</option>
														<?php
														foreach($job as $row){
															?>
															<option value="<?=$row->id?>"><?=$row->job_title?> , <?=$row->company_name?></option>
															<?php		
														}
														?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label for="branch_name" class="col-sm-2 control-label">Select Branch Name</label>
												<div class="col-sm-10">
												<?php
													$branch_id = $this->session->userdata('branch');
												?>
												<select name="branch_id" id="selectBranch" class="form-control" <?=($branch_id!=1)?"disabled='disabled'":""?>>
														<option value="">Select Branch</option>
														<?php
														if(isset($branch)){
															if($branch){
																foreach($branch as $branchData){
																	?>
																	<option value="<?=$branchData->id?>" 
																		<?php if($branch_id!=1)
																			{
																				if($branch_id == $branchData->id){
																					echo "selected = 'selected'";
																				}
																			}
																		?>><?=$branchData->branch_name?></option>
																	<?php
																}
															}
														}
														?>
													</select>

												</div>
											</div>

											<div class="form-group">
												<label for="issued_date" class="col-sm-2 control-label">Reported By:</label>
												<div class="col-sm-10">
													<select name="agent" id="selectAgent" class="<?=($branch_id==1)?"chzn-select":""?> form-control" 
													tabindex="4" required <?=($branch_id!=1)?"disabled":""?>>
													<option value="">Select Agent</option>
														<option value="self" <?=($branch_id!=1)?"selected":""?>>Self</option>
														<option value="">No Name</option>
														<?php
															foreach($agent as $record){
														?>
															<option value="<?=$record->id?>"><?=$record->agent_name?>
															</option>
														<?php		
															}
														?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label for="certificate" class="col-sm-2 control-label">Training Certificates (if any)</label>
												<div class="col-sm-10">
													<input type="file" name="userfile[]" multiple="multiple" id="certificate"/>
												</div>
											</div>

											<div class="form-group">
												<label for="other" class="col-sm-2 control-label">Training(if any)</label>
												<div class="col-md-10">												
													<textarea name="training" rows="3" cols="20"  style="width:100%"></textarea>
												</div>
											</div>

											<div class="form-group">
												<label for="other" class="col-sm-2 control-label">Experience(if any)</label>
												<div class="col-md-10">												
													<textarea name="experience" rows="3" style="width:100%"></textarea>
												</div>
											</div>

											<div class="clearfix">
												<button type="submit" value="Save Changes" class="btn btn-large btn-primary pull-right">Save Changes</button>
											</div>

                                		</div>
                                	</form>
							    </div>
                            </div>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
<?php /*$this->load->view('admin/include/footer'); */?>


<script type="text/javascript" src="<?=base_url()?>/plugin/jquery-steps-master/build/jquery.steps.js"></script>

<script>
 $(document).ready(function(){ 
 	$("#clearRadioBtn").click(function(e){
 		e.preventDefault();

 		$('.langRadio').iCheck('uncheck'); 
 });

/*var wizard = $("#wizard").steps({
	transitionEffect: "slideLeft",
	onFinished: function (event, currentIndex)
	{
		$('#wizard').submit();
	}
*/
});
</script> 
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });

$(function(){
    $( "#issued_date" ).datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });

/*$(function(){
    $( "#expiration_date" ).datepicker({
    	maxDate: "+10y +1m",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });*/

	
	$(document).on('blur','#issued_date',function(e){
		/*issuedDate =  $("#issued_date").datepicker('getDate');
		year = issuedDate.getFullYear();
		month = issuedDate.getMonth();
		day = issuedDate.getDate();*/
		//alert(year);



		$('#expiration_date').prop('disabled',false);
		$("#expiration_date" ).datepicker({
			/*minDate: new Date(year, month - 1, day),*/
			maxDate: "+10y +1m",
			dateFormat: "dd-M-yy",
			showAnim : "blind",
			changeMonth: true,
			changeYear: true
		});


	})
	

	function changeDimension(type,value){
		inputVal = parseFloat(value);

		if(type == "ft"){
			returnValue = inputVal * 30.48;
		}
		else if(type == "cm"){
			returnValue = inputVal * 0.0328084;
		}
		else if(type == "kg"){
			returnValue = inputVal * 2.20462;
		}
		else if(type == "pound"){
			returnValue = inputVal * 0.453592;
		}
		return Math.round(returnValue*100)/100;
	}
	
	$(document).on('blur','.height , .weight',function(e){
		var Id = $(this).attr('id');

		var value = $(this).val();
		if(Id == "height-cm"){
			changeVal = changeDimension('cm',value);
			$('#height-ft').val(changeVal);
		}
		else if(Id == "height-ft"){
			changeVal = changeDimension('ft',value);
			$('#height-cm').val(changeVal);
		}
		else if(Id == "weight-kg"){
			changeVal = changeDimension('kg',value);
			$('#weight-pound').val(changeVal);
		}
		else if(Id == "weight-pound"){
			changeVal = changeDimension('pound',value);
			$('#weight-kg').val(changeVal);
		}
	});

	$(function(){
		$("#selectBranch").change(function(){
			
			abc = this.value;
			if(abc!=""){
				if(abc==1){
						$("#selectAgent option").eq(1).attr('selected','selected');
						$('#selectAgent').prop('disabled', false).trigger("liszt:updated");
				}
				else{
					// $("#selectAgent option").eq(1).attr('selected','selected');
					$('#selectAgent').prop('disabled', true).trigger("liszt:updated");
				}
			}else{
				$("#selectAgent option").eq(0).attr('selected','selected');
				$('#selectAgent').prop('disabled', false).trigger("liszt:updated");
			}

		});
	});
</script>

