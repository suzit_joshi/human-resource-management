<?php $this->load->view('admin/include/header'); ?>

<div class="wrapper row-offcanvas row-offcanvas-left"> 
	<!---left_panel-->
	<?php $this->load->view('admin/include/left'); ?>
	<!--END LEFT PANEL-->
	
	<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> Candidate Report <small>Your Business transactions</small> </h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">

				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal" role="form"> 

							<div class="form-group">
								<label class="col-sm-4 control-label" style="padding-left:0px; margin-left: -40px;">Select Candidate</label>
								<div class="col-sm-8">	

									 <select data-placeholder="Type Candidate Name" class="chzn-select form-control" 
									 style="width:100%;" tabindex="4" id="candidate">
										 <option value=""></option>
										 <?php 
										 foreach($candidates as $candidate){
										 	?>
										 	<option value="<?=$candidate->candidate_id?>"><?=$candidate->candidate_name?></option>
											<?php
										 	}
										 	?>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="row" id="report-candidate">
					<div class="col-md-12">
						<table id="example1" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>S.No</th>
                                <th>Date</th>
                                <th>Payment Type</th>
                                <th>Amount</th>
                                <th>Bank</th>                                                      
                                <th>Remarks</th>
                              </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
				</div>
			</div>

		</div>
	</section>
	<!-- /.content -->
</aside>
<!--right_panel-->
</div>
<div class="clear"></div>


<!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
<!--footer-->

<script type="text/javascript">
$(".chzn-select").chosen();

	$(document).on('change','#candidate',function(e){
		var candidate = $('#candidate option:selected').val();
		if(candidate == 0){
			alert('Please select candidate');
		}else{
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>report/getCandidateReportById",
				data: { 
					candidate_id:candidate
				},
				success: function(data) {
					$('#report-candidate').html(data);
				}
			});
		}
	})

</script>
</body>
</html>