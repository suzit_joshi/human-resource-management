<style>
.clear{
	clear:both;
}

.demo-container {
	box-sizing: border-box;
	width: 800px;
	height: 400px;
	padding: 20px 15px 15px 15px;
	margin: 15px auto 30px auto;
	border: 1px solid #ddd;
	background: #fff;
	background: linear-gradient(#f6f6f6 0, #fff 50px);
	background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
	box-shadow: 0 3px 10px rgba(0,0,0,0.15);
	-o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
}

.demo-placeholder {
	width: 100%;
	height: 100%;
	font-size: 14px;
	line-height: 1.2em;
}

.legend table {
	border-spacing: 5px;
}
</style>
<?php $this->load->view('admin/include/header'); ?>

<div class="wrapper row-offcanvas row-offcanvas-left"> 
	<!---left_panel-->
	<?php $this->load->view('admin/include/left'); ?>
	<!--END LEFT PANEL-->
	
	<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> Report <small>Your Business transactions</small> </h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">

				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" role="form"> 

							<div class="form-group">
								<label class="col-sm-4 control-label" style="padding-left:0px;">Select year to view report</label>
								<div class="col-sm-8">	
									<select id="year" class="form-control">
										<?php
										foreach($year as $rec){
											?>
											<option value="<?=$rec->year?>"><?=$rec->year?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>


				<div class="demo-container">
					<div id="placeholder" class="demo-placeholder"></div>
				</div>
				<p class="text-center">
				<strong>Total Cash In Hand(NRs.)</strong>
				</p>
			</div>

		</div>
	</section>
	<!-- /.content -->
</aside>
<!--right_panel-->
</div>
<div class="clear"></div>


<!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
<!--footer-->


<?php
 	//$amount = new array();
 	foreach($cash_result as $cash){
 		$month = $cash->month;
 		$amount[$month] = $cash->amount; 
 	}

 	for($i = 1; $i<=12; $i++)
 	{
 		if(!array_key_exists($i, $amount))
 		{
 			$amount[$i] = 0;
 		}
 	}

 	$month = array("","January","February","March","April","May","June","July",
 					"August","September","October","November","December");

 	for($i=1;$i<=12;$i++){
 		$report_value[] = "[\"".$month[$i]."\",".$amount[$i]."]";
 	}

 	$final_report_string = implode(",",$report_value);
?>

<script type="text/javascript">

	$(function() {

		var data = [<?=$final_report_string?>];

		$.plot("#placeholder", [ data ], {
			series: {
				bars: {
					show: true,
					barWidth: 0.6,
					align: "center"
				}
			},
			xaxis: {
				mode: "categories",
				tickLength: 0
			}
		});
	});

	$(document).on('change','#year',function(e){
		var year = $('#year option:selected').val();
		//alert(year);
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>report/getReport",
			data: { 
				year:year
			},
			success: function(data) {
				$('.demo-container').html(data);
			}
		});
	})

</script>
</body>
</html>