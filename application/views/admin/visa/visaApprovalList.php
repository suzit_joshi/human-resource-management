<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Visa Approval List
            <small>Manage your visa for candidates</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Visa Approval List</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); $role = strtolower($this->session->userdata('role')); ?>
    <?php $msg=$this->session->userdata('candidate_data'); if($msg!=""){ ?>
    <div class="alert alert-info alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=$msg?>
    </div>
    <?php } ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12 category-tab">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Visa Approval List</a>
                        </li>
                        <li><a href="#tab_2" data-toggle="tab">Visa Decision List</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="20%">Full Name</th>
                                            <th>Job & Company</th>
                                            <th>Passport No</th>
                                            <th>Contact No</th>
                                            <th width="15%">Referred By</th>
                                            <th width="5%">Status</th>
                                            <th width="5%">Option</th>
                                        </tr>
                                    </thead>
                                    <tbody id="candidate-detail">
                                        <?php $counter=1 ; foreach($candidate_result as $candidate){ $id=$candidate->candidate_id; $encrypted_id = base64_encode($id); ?>
                                        <tr>
                                            <td>
                                                <?=$counter?>
                                            </td>
                                            <td>
                                                <?=$candidate->candidate_name?></td>
                                            <td>
                                                <?=$candidate->job_title?>
                                                    <br>
                                                    <?=$candidate->company_name?></td>
                                            <td>
                                                <?=$candidate->passport_no?></td>
                                            <td>
                                                <?=$candidate->mobile. ", ".$candidate->phone?></td>
                                            <td>
                                                <?php if($candidate->agent_id == "0"){ echo "Self"; } elseif(is_null($candidate->agent_id)) { echo "NO Name"; } else{ echo $candidate->agent_name." , ". $candidate->agent_mob; } ?>
                                            </td>
                                            <td>
                                                <?php $b=$candidate->current_status; $pos = strpos($b, "_"); if ($pos === false) { echo ucfirst($b); } else{ $b = explode('_',$b); echo ucfirst($b[0]).' '.$b[1]; } ?>
                                            </td>
                                            <td job-id="<?=$id?>">
                                                <?php if($candidate->decision == "Approved" ){ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <button class="btn btn-xs btn-success disabled">Approved</button>
                                                <?php } elseif($candidate->decision == "Declined" ){ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <button class="btn btn-xs btn-danger disabled">Declined</button>

                                                <?php }else{ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <?php } ?>
                                            </td>

                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="20%">Full Name</th>
                                            <th>Job & Company</th>
                                            <th>Passport No</th>
                                            <th>Contact No</th>
                                            <th width="15%">Referred By</th>
                                            <th width="5%">Status</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody id="candidate-detail">
                                        <?php $counter=1 ; foreach($approved_candidate as $candidate){ $id=$candidate->candidate_id; $encrypted_id = base64_encode($id); ?>
                                        <tr>
                                            <td>
                                                <?=$counter?>
                                            </td>
                                            <td>
                                                <?=$candidate->candidate_name?></td>
                                            <td>
                                                <?=$candidate->job_title?>
                                                    <br>
                                                    <?=$candidate->company_name?></td>
                                            <td>
                                                <?=$candidate->passport_no?></td>
                                            <td>
                                                <?=$candidate->mobile. ", ".$candidate->phone?></td>
                                            <td>
                                                <?php if($candidate->agent_id == "0"){ echo "Self"; } elseif(is_null($candidate->agent_id)) { echo "NO Name"; } else{ echo $candidate->agent_name." , ". $candidate->agent_mob; } ?>
                                            </td>
                                            <td>
                                                <?php $b=$candidate->current_status; $pos = strpos($b, "_"); if ($pos === false) { echo ucfirst($b); } else{ $b = explode('_',$b); echo ucfirst($b[0]).' '.$b[1]; } ?>
                                            </td>
                                            <td job-id="<?=$id?>">
                                                <?php if($candidate->decision == "Approved" ){ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <button class="btn btn-xs btn-success disabled">Approved</button>
                                                <?php } elseif($candidate->decision == "Declined" ){ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <button class="btn btn-xs btn-danger disabled">Declined</button>

                                                <?php }else{ ?>
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$candidate->approval_id?>">
                                                    <button class="btn btn-xs btn-primary">View</button>
                                                </a>
                                                <?php } ?>
                                            </td>

                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</aside>