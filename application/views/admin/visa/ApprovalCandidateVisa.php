<style>
    .custom-textarea {
        height: 8% !important;
    }
</style>
 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Visa Approval 
            <small>Manage your visa for candidates</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Visa Approval </li>
        </ol>
    </section>

    <!-- Main content -->
    <?php $this->load->view('admin/include/notification'); ?>
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <!-- Personal Information -->
                <div class='box box-info'>
                    <div class='box-header'>
                        <h3 class='box-title'>Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad'>
                        <div class="row">
                            <div class="col-sm-8" id="personal-information">
                                <p class="heading-candidate">
                                    <?=$candidate[ 'candidate_name']?>
                                </p>
                                <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                    <?php echo $candidate[ 'perm_address']. "(per.)  ".$candidate[ 'temp_address']. "(tem.)"?>
                                </p>
                                <p><i class="fa fa-phone"></i>&nbsp;&nbsp;
                                    <?php echo $candidate[ 'phone']. "(Res.)   ".$candidate[ 'mobile']. "(Mob.)"?>
                                </p>
                                <p><i class="fa fa-info-circle"></i> &nbsp;&nbsp;
                                    <?php echo $candidate[ 'email_id']?>
                                </p>
                                <p><i class="fa fa-users"></i> &nbsp;&nbsp;
                                    <?=$candidate[ 'father_name']?>(father),
                                        <?=$candidate[ 'mother_name']?>(mother)</p>
                                <p title="Current Processing Status"><i class="fa fa-tags"></i>&nbsp;&nbsp;
                                    <?=$candidate[ 'current_status']?>
                                </p>

                            </div>
                            <div class="col-sm-4" style="text-align:right;">
                                <img src="<?=base_url()?>uploads/candidate/profile_image/<?=$candidate_doc['profile']?>" style="height:200px; width:auto;" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php
                                    if(is_null($decision['decision'])){
                                        $role   =   strtolower($this->session->userdata('role'));
                                        if($role == "general admin" || $role == "system admin"){
                                ?>
                                <form method="post" action="<?=base_url()?>visa/approveVisa" id="frm-approval">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Approval/Decline Remarks</label>
                                        <div class="col-sm-8">
                                            <textarea name="remarks" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success res-btn" value="1" type="submit">Approve</button>
                                            <button class="btn btn-danger res-btn" value="2" type="submit">Decline</button>
                                        </div>
                                    </div> 

                                    <input type="hidden" value="<?=$candidate['candidate_id']?>" name="candidate_id">
                                    <input type="hidden" id="set-result" name="set_result">
                                </form>
                                <?php
                                        }
                                    }
                                    else{
                                ?>
                                    <form method="post" action="#" id="frm-approval">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Approval/Decline Remarks</label>
                                            <div class="col-sm-8">
                                                <textarea name="remarks" class="form-control" rows="5"><?=$decision['remarks']?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <?php 
                                                    if($decision['decision'] == "Approved"){
                                                ?>
                                                    <button class="btn btn-success res-btn" value="1" disabled="disabled">Approved</button>
                                                <?php   
                                                    }else{
                                                ?>
                                                    <button class="btn btn-danger res-btn" value="2" disabled="disabled">Declined</button>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div> 

                                        <input type="hidden" value="<?=$candidate['candidate_id']?>" name="candidate_id">
                                        <input type="hidden" id="set-result" name="set_result">
                                    </form>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>


                        <div style="clear:both;"></div>
                    </div>
                </div>
                <!-- /.Personal Information -->

                <!--Other Personal Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Other Personal Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="other-info-table">
                            <tr>
                                <td width="15%"><b>Date Of Birth:</b>
                                </td>
                                <td>
                                    <?php 
                                        $dob=date( "d-M-Y",strtotime($candidate[ 'dob'])); 
                                        $dob_year=date( "Y",strtotime($dob)); 
                                        $today=date( "Y"); 
                                        $years=$today - $dob_year; 
                                        echo $dob . " , ". $years . " years"; ?> ,
                                    <?=$candidate[ 'birth_place']?> (birth place)

                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Languages Known</b>
                                </td>
                                <td>
                                    <?php 

                                        $languages=unserialize($candidate[ 'language']); 

                                            foreach ($languages as $key=> $value) { 
                                                if($value==1){
                                                    $a="Good";
                                                }elseif($value==2){
                                                    $a="better";
                                                }elseif($value==3){
                                                    $a="excellent";
                                                }else{
                                                    $a="unknown";
                                                }
                                                echo $key." (".$a.")"." , "; 
                                            } 
                                        
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Body Description</b>
                                </td>
                                <td><span class="height"><?=$candidate['height']?>ft , <?=round($candidate['height']*30.48)?> cm </span>
                                    <span class="weight"><?=$candidate['weight']?> Kg, <?=round($candidate['weight']*2.20462)?></span>
                            </tr>
                            <tr>
                                <td width="15%"><b>Marital Status</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'marital_status']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Religion</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'religion']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Nationality</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'nationality']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Qualification</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'qualification']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Work Experience</b>
                                </td>
                                <td>
                                    <?=$candidate['experience']?>
                                </td>
                            </tr>
                            <tr>
                                <td width="15%"><b>Training</b>
                                </td>
                                <td>
                                    <?=$candidate['training']?>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <!--END Other personal INFORMATION-->

                <!--Passport and JOB Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Passport and Job Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        <table class="table" id="passport-table">
                            <tr>
                                <td><b>Applied Job</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'job_title']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Agent</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'agent_name']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Passport No</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'passport_no']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued From</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_from']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Issued Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'issued_date']?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Expiration Date</b>
                                </td>
                                <td>
                                    <?=$candidate[ 'expire_date']?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--END Passport and job INFORMATION-->

                <!--Work Processing Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Processing Work</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                       
                        <table class="table" id="processing-table">
                            <thead>
                                <tr>
                                    <th>S.NO.</th>
                                    <th>Processing Work</th>
                                    <th>Date</th>
                                    <th>Result</th>
                                   <!--  <th>Descriptions</th>
                                    <th>Option</th> -->
                                </tr>
                            </thead>
                            <tbody id="processing-content">
                            <tr>
                                    <td>1</td>
                                    <td>Pre-interview</td>
                                    <td><?php if($preInterview){ echo $preInterview['interview_date']; } else { echo "Not Appeared"; }?></td>
                                    <td><?php if($preInterview){ if($preInterview['result']==1){ echo "Passed"; } else{ echo "fail"; } } else { echo "Not Appeared"; }?></td>

                                    
                             </tr> 
                             <tr>
                                    <td>2</td>
                                    <td>Final-interview</td>
                                    <td><?php if($finalInterview){ echo $finalInterview['interview_date']; } else { echo "Not Appeared"; }?></td>
                                    <td><?php if($finalInterview){ if($finalInterview['result']==1){ echo "Passed"; } else{ echo "fail"; } } else { echo "Not Appeared"; }?></td>
                                    
                             </tr>
                             <tr>
                                    <td>3</td>
                                    <td>Medical</td>
                                    <td><?php if($medicalResult){ if($medicalResult['medical_date']){ echo $medicalResult['medical_date']; } }else{ echo "Not Appeared"; }?></td>
                                    <td><?php 
                                            if($medicalResult){ 
                                                if($medicalResult['medical_result'] == 1){ 
                                                    echo "Medical Pass"; 
                                                }else{
                                                    echo "Medical Fail";
                                                } 
                                            }
                                            else{ 
                                                echo "Not Appeared"; 
                                            }?>
                                    </td>
                             </tr>
                              <tr>
                                    <td>4</td>
                                    <td>Visa</td>
                                    <td><?php if($visa){ if($visa['received_date']){ echo $visa['received_date']; } }else{ echo "Not Appeared"; }?></td>
                                    <td><?php if($visa){ if($visa['received_date']){ echo "Visa Received"; } }else{ echo "Not Received"; }?></td>
                                    
                             </tr>
                             <tr>
                                    <td>5</td>
                                    <td>Ticket</td>
                                    <td><?php if($ticketing){ if($ticketing['ticket_no']){ echo date('d-M-Y',strtotime($ticketing['departure']))." (Departure Date)"; } }else{ echo "Not Appeared"; }?></td>
                                    <td><?php if($ticketing){ if($ticketing['ticket_no']){ echo $ticketing['ticket_no']."-".$ticketing['flight_no']; } }else{ echo "Not Received"; }?></td>
                             </tr>        

                            </tbody>

                        </table>
                    </div>
                </div>
                <!--Working Processing INFORMATION-->

                <!--Uploaded Documents Information-->
                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Client Documents</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        
                        <table class="table" id="document-table">
                            <thead>
                                <tr>
                                    <th>S.NO.</th>
                                    <th>Document Title</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody id="processing-content">
                                <tr>
                                    <td>1</td>
                                    <td>Passport</td>
                                    <td><?php if($candidate_doc['passport']){?>
                                        <a href="<?=base_url()?>uploads/candidate/passport/<?=$candidate_doc['passport']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    } else
                                    { ?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="passport">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Office Form</td>
                                    <td><?php if($candidate_doc['office_form']){?>
                                        <a href="<?=base_url()?>uploads/candidate/office_form/<?=$candidate_doc['office_form']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="office_form">Upload Now</button>
                                    <?php    
                                    } 
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Curriculum Vitae</td>
                                    <td><?php if($candidate_doc['cv']){?>
                                        <a href="<?=base_url()?>uploads/candidate/cv/<?=$candidate_doc['cv']?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                    }else{?>
                                        <button class="btn btn-primary btn-xs upload-docs" data-toggle="modal" data-target=".documents-modal" type="cv">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Academics</td>
                                    <td><?php 
                                            if($candidate_doc['academics']){
                                                $values = unserialize($candidate_doc['academics']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="academics">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Experience</td>
                                    <td><?php if($candidate_doc['experience']){
                                         $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="experience">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Training Documents</td>
                                    <td><?php 

                                            $values = unserialize($candidate_doc['training']);
                                            if(is_array($values)){
                                            foreach ($values as $row) {
                                            ?>
                                        <a href="<?=base_url()?>uploads/candidate/training/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                            }
                                            }
                                    else{
                                    ?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="training">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Other Documents</td>
                                    <td><?php if($candidate_doc['other']){
                                       $values = unserialize($candidate_doc['experience']);
                                                foreach ($values as $row) {
                                        ?>
                                        <a href="<?=base_url()?>uploads/candidate/academics/<?=$row?>" target="_blank">
                                        <button class="btn btn-primary btn-xs">View</button></a>
                                    <?php
                                        }
                                    }else{?>
                                        <button class="btn btn-primary btn-xs multi-upload-docs" data-toggle="modal" data-target=".multiple-documents-modal" type="other">Upload Now</button>
                                    <?php    
                                    }
                                    ?></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!--END UPLOADED DOCUMENTS INFORMATION-->
                                <!--Finance Information-->
                                <div class='box collapsed-box'>
                    <div class='box-header'>
                        <h3 class='box-title'>Financial Information</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i>
                            </button>

                            <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class='box-body pad' style="display:none;">
                        
                        <table class="table" id="document-table">
                            <tbody id="processing-content">
                                <?php
                                    if($finacial_info){
                                ?>
                                <tr>
                                    <td colspan="2" width="50%">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Income Title</th>
                                                    <th>Amount</th>
                                                </tr>  
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $counter = 1;
                                                    $income_total = 0;
                                                    foreach ($finacial_info as $record) {
                                                        if($record->type == "income" || $record->type == "advance"){
                                                ?>
                                                    <tr>
                                                        <td><?=$counter?></td>
                                                        <td><?=$record->category_title?> - <?=$record->type?></td>
                                                        <td><?=$record->amount?> On <?=$record->subtype?></td>
                                                    </tr>
                                                <?php
                                                        $counter++;
                                                        $income_total = $income_total+$record->amount;
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <!--EXPENSE -->
                                    <td colspan="2" width="50%">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Expense Title</th>
                                                    <th>Amount</th>
                                                </tr>  
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $counter = 1;
                                                    $expense_total = 0;
                                                    foreach ($finacial_info as $record) {
                                                        if($record->type == "expense"){
                                                ?>
                                                    <tr>
                                                        <td><?=$counter?></td>
                                                        <td><?=$record->category_title?></td>
                                                        <td><?=$record->amount?> On <?=$record->subtype?></td>
                                                    </tr>
                                                <?php
                                                        $counter++;
                                                        $expense_total = $expense_total+$record->amount;
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <!--END EXPENSE -->
                                </tr>
                                <tr>
                                    <td><Strong>Total income/advance</Strong></td>
                                    <td>Rs. <?=number_format($income_total,2)?></td>

                                    <td><Strong>Total expense</Strong></td>
                                    <td>Rs. <?=number_format($expense_total,2)?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td><Strong>Total Balance</Strong></td>
                                    <td><?php $total = $income_total - $expense_total; echo "Rs.".number_format($total,2);?></td>
                                </tr>
                                <?php
                                    }else{
                                ?>
                                    <tr><td>No Financial Data available</td></tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--END Finance INFORMATION-->
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>



<script type="text/javascript">
    $(document).on('click','.res-btn',function(e){
        e.preventDefault();
        var a = $(this).val();
        $('#set-result').val(a);
        /*form submit*/
        $('#frm-approval').submit();
    });
</script>