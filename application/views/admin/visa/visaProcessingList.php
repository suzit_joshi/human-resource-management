<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Visa
            <small>Manage your visa details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Visa</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <form action="<?=base_url()?>visa/sendNotification" method="post">
                   
                    <div class="box">

                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%">Select All</th>
                                        <th width="5%">S.No</th>
                                        <th>Candidate</th>
                                        <th> Passport No</th>
                                        <th>Contact No</th>
                                        <th>Applied Job</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                        $counter=1; 
                                        foreach($candidates as $record){ 
                                    ?>
                                    <tr>
                                        <td align="center">
                                            <input type="checkbox" value="<?=$record->candidate_id?>" name="candidate[]">
                                        </td>
                                        <td><?=$counter?></td>
                                        <td><?=$record->candidate_name?></td>
                                        <td><?=$record->passport_no?></td>
                                        <td><?=$record->phone?> , <?=$record->mobile?></td>
                                        <td><?=$record->job_title?><br><?=$record->company_name?></td>
                                    </tr>
                                    <?php 
                                        $counter+=1; 
                                        } 
                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit" value="notify" name="frm_submit">Send Notification</button>
                        </div>
                </form>

                </div>

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
</aside>
<!--right_panel-->