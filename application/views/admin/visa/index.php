<style>
    .actions {
        display: block;
        font-size: 10px;
        opacity: 0;
    }
    
    tr:hover .actions {
        opacity: 1;
    }
    
    #success-alert,
    #warning-alert {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Visa
            <small>Manage your visa details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Visa</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">
                            <?php
                            $role = strtolower($this->session->userdata('role'));
                            if( $role == "visa processing" || $role == "general admin" || $role == "system admin"){
                              ?>
                            <button class="btn btn-primary" onclick="window.location='<?=base_url()?>visa/newVisa'">
                                Add New
                            </button>
                            <?php
                                }
                            ?>

                            <?php
                            $role = strtolower($this->session->userdata('role'));
                            if( $role == "finance"){
                              ?>
                                <button class="btn btn-primary" onclick="window.location='<?=base_url()?>visa/getTransferList/medical_passed'">
                                    Transfer for visa processing    
                                </button>
                            <?php
                                }
                            ?>
                        </div>                        
                    </div>

                    <div class="box-body table-responsive">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>Candidate</th>
                                            <th>Position</th>
                                            <th>Received Date</th>
                                            <th>Valid Date</th>
                                            <th>Status</th>
                                            <th width="5%">Option</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php 
                                            $counter=1 ; 
                                            foreach($visa as $record){ 
                                                $id=$record->id; 
                                                $encrypt_id = base64_encode($id); ?>
                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$record->candidate_name?><br><?=$record->passport_no?></td>
                                            <td><?=$record->position?></td>
                                            <td><?=$record->received_date?></td>
                                            <td><?=$record->valid_date?></td>
                                            <td><?=(($record->cancel_flag == 0 )? 'Active' : 'Cancelled')?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li> <a href="#" content-id="<?=$id?>" id="view-ticket">View </a> 
                                                        </li>
                                                        <?php
                                                        $role = strtolower($this->session->userdata('role'));
                                                        if( $role == "visa processing" || $role == "general admin" || $role == "system admin"){
                                                          ?>
                                                        <li class="divider"></li>
                                                        <li> <a href="<?=base_url()?>uploads/visa_copy/<?=$record->visa_copy?>" target="_blank">Download Visa Copy</a> 
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="<?=base_url()?>visa/cancelVisa/<?=$encrypt_id?>" 
                                                             onclick="return confirmVisaCancel();">Cancel Visa</a>
                                                         </li>
                                                          <?php
                                                            }
                                                        ?>
                                                     </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
        </div>
        </div>
        <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>

</aside>
<!--right_panel-->
<script>
    $(document).on('click','#view-transaction',function(e){
        e.preventDefault();
        var Id = $(this).attr('content-id');
        $.post('<?=base_url()?>transaction/loadTransaction',{id:Id}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })
</script>