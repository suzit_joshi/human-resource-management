 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
 	<?php $this->load->view('admin/include/left'); ?>
<!--END LEFT PANEL-->
            
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                     <h1>
                        User
                        <small>Manage your users</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">User</li>
                    </ol>
                </section>
                <?php
                	$this->load->view('admin/include/notification');
                	@$error_flag = $this->uri->segment(3);
                	@$error_flag_2 = $this->uri->segment(4);
                	if(@$error_flag == "ErrRec" || @$error_flag_2 == "ErrRec"){
                ?>
                <div class="alert alert-info alert-dismissable">
                	<i class="fa fa-info"></i>
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                	<b>Error!!</b> The username/email already exists. Please try again with different username/email.
                </div>
                <?php
                	}
                ?>
                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<?php
								if(isset($user_record)){
							?>
							<div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">Edit User</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>user/editsave" id="user-edit-form" method="post">


									<div class="form-group">
										<label for="name">Full Name</label>
										<input type="text" class="form-control" id="name" 
										value="<?=$user_record->name?>" name="name" 
										required="required">
									</div>

									<div class="form-group">
										<label for="designation">Designation</label>
										<input type="text" class="form-control" id="designation" 
										value="<?=$user_record->post?>" name="designation" 
										required="required">
									</div>

									<div class="form-group">
										<label for="email_id">Email Id</label>
										<input type="email" class="form-control" id="email_id" 
										value="<?=$user_record->email_id?>" name="email_id" 
										required="required">
									</div>

									<div class="form-group">
										<label for="email_id">Select Branch</label>
										<select class="form-control" name="branch">
										
										<?php
										if($branch){
											foreach ($branch as $branchData) {
												if($user_record->branch_id!=$branchData->id){
											?>
										<option value="<?=$branchData->id?>"><?=$branchData->branch_name?> </option>>
										<?php }
											else{
												?>
												<option value="<?=$branchData->id?>" selected><?=$branchData->branch_name?> </option>
											<?php
										}}}?>
										 
										</select>
									</div>


									<div class="form-group">
										<label for="username">Username</label>
										<input type="text" class="form-control" id="username" 
										value="<?=$user_record->username?>" name="username" 
										required="required">
									</div>

									<input type="hidden" value="<?=base64_encode($user_record->id)?>" name="random_2">

									<div class="form-group">
										<label for="role">Role</label>
										<select name="role" class="form-control" id="role-select">
											<option value="0">Select One</option>
											<?php
												foreach ($result_role as $record) {
													if($record->id == "1"){
														continue;
													}
											?>
												<option value="<?=$record->id?>" 
													<?php if($record->id == $user_record->role_id){
														echo "selected";
													}?>><?=$record->role_name?></option>
											<?php	
												}
											?>
										</select>
									</div>

									<input type="hidden" name="random_value" value="<?=$user_record->password?>">
									
									<div class="form-group">
										<label for="password">Default Password</label>
										<input type="password" class="form-control" id="edit-password" 
										placeholder="Set Password" name="password" 
										required="required">
									</div>

									<div class="form-group">
										<label for="re-password">Re-Enter Default Password</label>
										<input type="password" class="form-control" id="re-edit-password" 
										placeholder="Re enter Password" name="re_pwd" 
										required="required">
									</div>

									

									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="edit-form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
								}else{
							?>
                            <div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">New User</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>user/save" id="user-form" method="post">

									<div class="form-group">
										<label for="name">Full Name</label>
										<input type="text" class="form-control" id="name" 
										placeholder="Full Name" name="name" value="<?=set_value('name')?>" 
										required="required">
									</div>

									<div class="form-group">
										<label for="designation">Designation</label>
										<input type="text" class="form-control" id="designation" 
										placeholder="Designation" name="designation" value="<?=set_value('designation')?>" 
										required="required">
									</div>

									<div class="form-group">
										<label for="email_id">Email Id</label>
										<input type="email" class="form-control" id="email_id" 
										placeholder="Email Id" name="email_id" value="<?=set_value('email_id')?>" 
										required="required">
									</div>

									<div class="form-group">
										<label for="branch">Select Branch</label>
										<select class="form-control" name="branch" id="branch">
										<?php
										if($branch){
											foreach ($branch as $branchData) {
											?>
										<option value="<?=$branchData->id?>" type="<?=$branchData->branch_name?>" >
											<?=$branchData->branch_name?> </option>>
										<?php } }?>
										</select>

									</div>

									<div class="form-group">
										<label for="username">Username</label>
										<input type="text" class="form-control" id="username" 
										placeholder="Username" name="username" value="<?=set_value('username')?>" 
										required="required">
									</div>

									<div class="form-group">
										<label for="role">Role</label>
										<select name="role" class="form-control" id="role">
											<option value="0">Select One</option>
											<?php
												foreach ($result_role as $record) {
													if($record->id == "1"){
														continue;
													}
											?>
												<option value="<?=$record->id?>"><?=$record->role_name?></option>
											<?php	
												}
											?>
										</select>
									</div>
									
									<div class="form-group">
										<label for="password">Default Password <i> - Max 20 char long</i></label>
										<input type="password" class="form-control" id="password" 
										placeholder="Set Password" name="password" 
										required="required">
									</div>

									<div class="form-group">
										<label for="re-password">Re-Enter Default Password</label>
										<input type="password" class="form-control" id="re-password" 
										placeholder="Re enter Password" name="re_pwd" 
										required="required">
									</div>						

									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
								}
							?>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
         <!--footer-->  
<script>
$(document).on('click','#form-submit',function(e){
	e.preventDefault();
	var password = $('#password').val();
	var rePwd = $('#re-password').val();
	if(password == " "){
		alert("Password Field cannot be blank");
		$('#password').focus();
	}else{
		if(password == rePwd){
			$('#user-form').submit();
		}else{
			alert('Re-Entered Password Mismatch. Please type again!');
			$('#password').focus();
		}
	}
});

$(document).on('click','#edit-form-submit',function(e){
	e.preventDefault();

	var password = $('#edit-password').val();
	var rePwd = $('#re-edit-password').val();
	if(password != " " && rePwd != " "){
		if(password == rePwd){
			$('#user-edit-form').submit();
		}else{
			alert('Re-Entered Password Mismatch. Please type again!');
			$('#password').focus();
		}
	}else{
		$('#user-edit-form').submit();
	}
});

$(document).on('change','#branch',function(e){
	var branch = $('#branch option:selected').attr('type').toLowerCase();
	if(branch != "head office"){
		$('#role').prop('disabled',true);
		$('#role option[value="4"]').attr('selected', true);
	}
	else{
		$('#role option[value="0"]').prop('selected', true);
		$('#role').prop('disabled',false);
	}
});



</script>


</body>
</html>
