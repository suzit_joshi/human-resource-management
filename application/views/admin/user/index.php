 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
    <?php $this->load->view('admin/include/left');?>
<!--END LEFT PANEL-->

<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        User
                        <small>Manage your users</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">User</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                <div class="box-footer">
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>user/newUser')">
                                    Add New</button>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Username</th>
                                                <th>Full Name</th>                                                
                                                <th>Email Id</th>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        echo "<th>Last Updated</th>";
                                                    }else{
                                                        echo "<th>Designation</th>";
                                                    }
                                                ?>
                                                <th>Role</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($result_user as $record){
                                            $id = $record->id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->username?></td>
                                                <td><?=$record->name?></td>
                                                <td><?=$record->email_id?></td>
                                                <?php
                                                    if($role == "general admin" || $role == "system admin"){
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }else{
                                                        echo "<td>".$record->post."</td>";
                                                    }
                                                ?> 
                                                <td><?php if($record->role_name != "super admin"){ echo $record->role_name; }?></td>
                                                <td><?=($record->status == 1 ? "Activate" : "Deactivated");?></td>    
                                                <td align="center">

                                                   <div class="btn-group">
                                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li> <a href="<?=base_url()?>user/editUser/<?=$encrypted_id?>">Edit</a></li>
                                                        <li class="divider"></li>

                                                        <?php
                                                            if($record->status == 1){
                                                        ?>
                                                        <li><a href="<?=base_url()?>user/changeStatus/<?=$encrypted_id?>/deactivate" 
                                                            onclick="return confirmDeactivate();">Deactivate</a></li>    
                                                        <?php
                                                            }else{
                                                        ?>
                                                            <li><a href="<?=base_url()?>user/changeStatus/<?=$encrypted_id?>/activate" 
                                                            onclick="return confirmActivate();">Activate</a></li> 
                                                        <?php
                                                            }
                                                        ?>                                    
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                                
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
```  <!--footer-->   
</body>
</html>