<?php $user=$this->session->userdata('username'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Human Resource Management</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?=base_url()?>admin_files/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?=base_url()?>admin_files/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?=base_url()?>admin_files/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?=base_url()?>admin_files/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" 
        type="text/css" />
        <!-- Theme style -->
        <link href="<?=base_url()?>admin_files/css/AdminLTE.css" rel="stylesheet" type="text/css" />
         <!-- DATA TABLES -->
        <link href="<?=base_url()?>admin_files/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/jQueryUI/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/js/bootstrap-timepicker-gh-pages/css/bootstrap-timepicker.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/jquery.chosen.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/nailthumb/jquery.nailthumb.1.1.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/jcrop/css/jquery.Jcrop.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/dynatable/jquery.dynatable.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/jquery-steps-master/css/jquery.steps.css">

        <script src="<?=base_url()?>admin_files/js/jquery.js" type="text/javascript"></script>
        <!-- jQuery UI 1.10.3 -->

    </head>
    
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
                <img src="<?=base_url()?>admin_files/img/florid-logo-012.png" style="padding-bottom:5px; padding-top:5px; height:119%" /> 
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       <!-- User Account: style can be found in dropdown.less -->
                       <?php
                     
                            $query=$this->db->order_by("id", "desc")->limit(10)->get('tbl_agent_notification')->result();
                            $total=count($query);
                       ?>
                           <li class="dropdown messages-menu">
                           <?php
                           if($total>0){
                            ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope"></i>
                                    <span class="label label-success"><?=$total?></span>
                                </a>
                                <?php } ?>
                                <ul class="dropdown-menu">
                                    <!-- <li class="header">You have 4 messages</li> -->
                                   
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <div class="slimScrollDiv" style="position: relative; overflow: scroll; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                          <?php
                                            foreach($query as $rowData){
                                            ?>  
                                            <li>
                                                <a href="<?=base_url()?>mobileController/mobileAgentNotification/<?=$rowData->id?>">
                                                    <i class="fa fa-angle-double-right"></i>

                                                    <h4>
                                                        Approval Request by <?=$rowData->sent_user?>
                                                    </h4>
                                                    <p><?=$rowData->remarks?></p>
                                                </a>
                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$rowData->id?>" 
                                                    id="seenId<?=$rowData->id?>" type="agent">
                                                          
                                                </span>
                                            </li>
                                            <?php } ?>
                                        </ul><div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; z-index: 99; right: 1px; height: 131.147540983607px; background: rgb(0, 0, 0);"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                                    </li>

                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>

                       



                       
                    </ul>
                </div>
            </nav>
        </header>

<script type="text/javascript">
    $(document).on('click','.seen-sts',function(e){
        var Id = $(this).attr('notification-id');
        var seenId = "#"+$(this).attr('id');
        var type = $(this).attr('type');
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>notification/changeStatus",
            data: {
                notification_id : Id,
                type : type
            },
            success: function (data) {

                if(data==1){
                    $(seenId).find('i').remove();
                    total=$("#notifyHeader").text();
                    $("#notifyHeader").text(total-1);
                }else{
                    alert('Seen Status could not be changed.');
                }
            }
        });
    });
</script>


<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Approval of Agents
                        <small>Notify yourself of small works</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Reminders</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                
                                <div class="box-body table-responsive">
                                    <table  class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="15%">Agent Name</th>
                                                <th>Contact No</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($agents as $record){
                                            $id = $record->agent_id;
                                            $encrypted_id = base64_encode($id);
                                            
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->agent_name?></td>
                                                <td><?=$record->phone?> , <?=$record->email?></td>
                                                <td>
                                                   <button class="btn btn-default agentApprove" id="abc<?=$counter?>" agentId="<?=$record->agent_id?>" status="approve"><i class="fa fa-check"></i></button> <button id="def<?=$counter?>" class="btn btn-default agentApprove" status="decline" agentId="<?=$record->agent_id?>"><i class="fa fa-times"></i></button>
                                                </td>                                               
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->

<!--footer-->   
</body>
</html>
<script>
$(function(){
    $(".agentApprove").click(function(){
        agentid=$(this).attr('agentId');
        status=$(this).attr('status');
        randomId=$(this).attr('id');
        $.post('<?=base_url()?>notification/approveAgent',{agentid:agentid,status:status}).done(function(d){
            // alert(d);
            if(d){
                $("#"+randomId).text(d);
                $("#"+randomId).removeClass("btn-default");
                $("#"+randomId).addClass("btn-success")
                $("#"+randomId).siblings().remove();   
            }

        });
    });

    // $(".navbar-nav a").click(function(){
    //     // alert("test");
    // })
});
</script>


<!-- footer -->
<style>

    .footer{
        width:100%;
        height:20px;
    }
</style>

<!--MODAL to set cash in hand for the first time-->
<div class="modal fade" id="set-cash" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Cash in hand</h4>
      </div>
      <form class="form-horizontal" role="form" action="<?=base_url()?>transaction/setCash" method="post">
      <div class="modal-body">        

        <div class="form-group">
            <label class="col-sm-4">Current Cash in Hand</label>
            <div class="col-sm-8">
            <input type="number" class="form-control" name="balance">
            <p class="help-block">Important! Once set cannot be changed.</p>
            </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form><!--END FORM-->

    </div>
  </div>
</div>
<!--END MODAL TO SET CASH IN HAND FOR THE FIRST TIME-->

<!--MODAL to view the elements-->
<div class="modal fade" id="view-ajax-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content view-ajax-content">
      

    </div>
  </div>
</div>
<!--END MODAL to view the elements-->


<!--MODAL to view the elements-->
<div class="modal fade" id="candidate-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Select Candidate List For Transfer</h4>
            </div>
            <div class="modal-body" id="view-candidate-list-transfer">
                
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--END MODAL to view the elements-->


<div class="footer">
    <div class="col-md-12">
        <span class="pull-right">design: <a href="http://thesunbi.com" target="_blank">SunBi</a></span>
    </div>
</div>
<script type="text/javascript" src="<?=base_url()?>plugin/dynatable/jquery.dynatable.js"></script>
<!--<script src="<?=base_url()?>admin_files/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script> -->

<script type="text/javascript" src="<?=base_url()?>admin_files/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>admin_files/js/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url()?>admin_files/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="<?=base_url()?>admin_files/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url()?>admin_files/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>admin_files/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>admin_files/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>admin_files/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!--CHAINED JQUERY JS-->
<script src="<?=base_url()?>admin_files/js/dropdown/jquery.chained.js" type="text/javascript"></script>
<!--BOOTSTRAP DATE TIME PICKER-->
<script type="text/javascript" 
src="<?=base_url()?>admin_files/js/bootstrap-timepicker-gh-pages/js/bootstrap-timepicker.js"></script>
<!--CHOSEN JQUERY -->
<script type="text/javascript" src="<?=base_url()?>admin_files/js/chosen.jquery.min.js"></script>
<!--NAIL THUMB -->
<script type="text/javascript" src="<?=base_url()?>plugin/nailthumb/jquery.nailthumb.1.1.js"></script>
<!--JCROP IMAGE -->
<script type="text/javascript" src="<?=base_url()?>plugin/jcrop/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?=base_url()?>plugin/jcrop/js/jquery.color.js"></script>

<!--JQUERY FLOT JS-->
<script type="text/javascript" src="<?=base_url()?>admin_files/js/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?=base_url()?>admin_files/js/plugins/flot/jquery.flot.categories.js"></script>

<!--END JQUERY FLOT JS-->

<script type="text/javascript">
    
    
    var t=$('#example1').dataTable();

     $(".chzn-select").chosen();

    $('#example2').dataTable();
    $('#example3').dataTable();

    function confirmDelete(){
        if(confirm("Are you sure you want to delete?")){
            return true;
        }
        else{
            return false;   
        }
    }

    /*Function to deactivate the record*/
    function confirmDeactivate(){
        if (confirm("Are you sure you want to deactivate the record?")){
            return true;
        }
        else{
            return false;   
        }
    }

    function confirmActivate(){
        if (confirm("Are you sure you want to activate the record?")){
            return true;
        }
        else{
            return false;   
        }
    }

    $(document).on('click','#view-element-content',function(e){
        e.preventDefault();
        var id = $(this).attr('content-id');
        var table = $(this).attr('tbl');
        $.ajax({
          type: "POST",
          url: "<?=base_url()?>general/getContent",
          data: { 
                id:id,
                table : table
            },
            success: function(data) {
               $('#view-ajax-modal .view-ajax-content').html(data);
               $('#view-ajax-modal').modal();
            }
        });
        $('#print-modal').modal();
    })


    $(document).ready(function(){
            
            $('.content').removeAttr('style');

                contentheight = $('.right-side .content').height() + 200;
                $('.right-side .content').height(contentheight);
    });
</script>

