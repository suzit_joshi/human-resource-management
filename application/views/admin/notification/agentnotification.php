
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Approval of Agents
                        <small>Notify yourself of small works</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Reminders</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="15%">Agent Name</th>
                                                <th>Contact No</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($agents as $record){
                                            $id = $record->agent_id;
                                            $encrypted_id = base64_encode($id);
                                            
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->agent_name?></td>
                                                <td><?=$record->phone?> , <?=$record->email?></td>
                                                <td>
                                                   <button class="btn btn-default agentApprove" id="abc<?=$counter?>" agentId="<?=$record->agent_id?>" status="approve">Approve</button> <button id="def<?=$counter?>" class="btn btn-default agentApprove" status="decline" agentId="<?=$record->agent_id?>">Decline</button>
                                                </td>                                               
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->

<!--footer-->   
</body>
</html>
<script>
$(function(){
    $(".agentApprove").click(function(){
        agentid=$(this).attr('agentId');
        status=$(this).attr('status');
        randomId=$(this).attr('id');
        $.post('<?=base_url()?>notification/approveAgent',{agentid:agentid,status:status}).done(function(d){
            // alert(d);
            if(d){
                $("#"+randomId).text(d);
                $("#"+randomId).siblings().remove();   
            }

        });
    });

    // $(".navbar-nav a").click(function(){
    //     // alert("test");
    // })
});
</script>