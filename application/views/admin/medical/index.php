<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Medical
                        <small>List of Medical Applicants and Result</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Medical</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                 <div class="box-footer">
                                    <?php
                                    $role = strtolower($this->session->userdata('role'));
                                    if( $role == "documentation" || $role == "general admin" || $role == "system admin"){
                                        ?>
                                        <button class="btn btn-primary" 
                                        onClick="javascript:location.replace('<?=base_url()?>medical/newMedical')">
                                        Add New</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Other Option <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li> <a href="<?=base_url()?>medical/getTransferList">Transfer medical passed candidates to Finance/General Admin</a> </li>
                                            </ul>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    
                                </div>


                                
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Date</th>
                                                <th>Candidate, Passport No</th>
                                                <th>Centre</th>
                                                <th>Type</th>
                                                <th>Result</th>
                                                <th>Expiry Date</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($medicals as $record){
                                            $id = $record->id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->medical_date?></td>
                                                <td><?=$record->candidate_name?> <br> <?=$record->passport_no?></td>
                                                <td><?=$record->medical_center?></td>
                                                <td><?=$record->type?></td>
                                                <td><?=($record->result == 1)? "PASS" : "FAIL" ?></td>
                                                <td><?php   
                                                            echo "Expire date:". $record->expire_date."<br>";
                                                            $today = date('Y-m-d');
                                                            $expire_date = $record->expire_date;
                                                            if(strtotime($today) >= strtotime($expire_date)){
                                                                echo "<span style='color:red'>Expired</span>";
                                                            }
                                                    ?></td>

                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li> <a href="#" id="view-medical" value="<?=base64_encode($id)?>">View </a> 
                                                            </li>
                                                            <?php
                                                            $role = strtolower($this->session->userdata('role'));
                                                            if( $role == "documentation" || $role == "general admin" || $role == "system admin"){
                                                                ?>
                                                            <li class="divider"></li>
                                                            <li>
                                                            <a href="<?=base_url()?>medical/editMedical/<?=$encrypted_id?>">Edit</a>
                                                            </li>  
                                                            <li class="divider"></li>
                                                            <li>
                                                            <a href="<?=base_url()?>medical/delete/<?=$encrypted_id?>" onclick="return confirmDelete();">Delete</a>
                                                            </li>
                                                            <?php
                                                                }
                                                            ?>                                                         
                                                        </ul>
                                                    </div>
                                                </td>                                               
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                               
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
<script>
    $(document).on('click','#view-medical',function(e){
        e.preventDefault();
        var Id = $(this).attr('value');
        $.post('<?=base_url()?>medical/getMedical',{id:Id}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })
</script>
