<style>
    .actions {
        display: block;
        font-size: 10px;
        opacity: 0;
    }
    
    tr:hover .actions {
        opacity: 1;
    }
    
    #success-alert,
    #warning-alert {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Income/Expense/Advance Headings
            <small>Manage your income/expenses/advance heading</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Income/Expense/Advance Headings</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#new-category">
                                Add New</button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="alert alert-success alert-dismissable" id="success-alert">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Success!</b> Task performed successfully.
                        </div>

                        <div class="alert alert-warning alert-dismissable" id="warning-alert">
                            <i class="fa fa-warning"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Error!</b> Data could not be entered.
                        </div>

                        <div class="row">
                            <div class="col-md-12 category-tab">
                                <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1" data-toggle="tab">Income</a>
                                        </li>
                                        <li><a href="#tab_2" data-toggle="tab">Expense</a>
                                        </li>
                                        <li><a href="#tab_3" data-toggle="tab">Advance</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Income Heading</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="bank-record">
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_income as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="income">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 

                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                      <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                  </button>
                                                                  <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li> <a href="#" class="edit-category">Edit</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                        onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                    </ul>
                                                                </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_2">
                                            <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Expense Category</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_expense as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="expense">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li> <a href="#" class="edit-category">Edit</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                    onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <!-- /.tab-pane 3-->
                                        <div class="tab-pane" id="tab_3">
                                            <table id="example3" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Advance Category</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_advance as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="advance">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li> <a href="#" class="edit-category">Edit</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                    onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- nav-tabs-custom -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
        </div>
        </div>
        <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>

</aside>


<!-- Modal -->
<div class="modal fade" id="new-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">New Income/Expense Category</h4>
            </div>
            <form class="form-horizontal" role="form" id="submit-category" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4">Select Type</label>
                        <div class="col-sm-8">
                            <select class="type form-control" required="required">
                                <option value="">Select One</option>
                                <option value="income">Income</option>
                                <option value="expense">Expense</option>
                                <option value="advance">Advance</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4">Select label</label>
                        <div class="col-sm-8">
                            <select id="category-heading" class="form-control" required="required">
                                <option value="Candidate">Candidate</option>
                                <option value="Other">Others</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4">Category Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="category" required="required">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            <!--END FORM-->

        </div>
    </div>
</div>

<!--MODAL TO EDIT-->
<div class="modal fade" id="edit-category-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Income/Expense Category</h4>
            </div>
            <form class="form-horizontal" role="form" id="edit-submit-category">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4">Select Type</label>
                        <input type="hidden" id="category_id">
                        <div class="col-sm-8">
                            <p class="form-control-static" id="category-type"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4">Select label</label>
                        <div class="col-sm-8">
                            <select id="edit-category-heading" class="form-control">
                                <option value="Candidate">Candidate</option>
                                <option value="Other">Others</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4">Category Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="edit-category">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Save changes</button>
                </div>
            </form>
            <!--END FORM-->

        </div>
    </div>
</div>


<script>

    /*AJAX REQUEST TO INSERT NEW BANK RECORD*/

    $(document).on('submit', '#submit-category', function (e) {
        e.preventDefault();
        var category_type = $('.type option:selected').val();
        var category_title = $('#category').val();
        var categoryHeading = $('#category-heading option:selected').val();
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>category/newCategory",
            data: {
                category_type: category_type,
                category_title: category_title,
                category_heading: categoryHeading
            },
            success: function (data) {
                $('.category-tab').html(data);
                $('#new-category').modal('hide');
                $('#success-alert').slideDown();
            }
        });
    });

    /*END AJAX REQUEST TO INSERT NEW RECORD*/
    var categoryId;
    var type;
    /*To generate an EDIT MODAL*/
    $(document).on('click', '.edit-category', function (e) {
        e.preventDefault();
        categoryId = $(this).parents('tr').find('.category-name').attr('data-id');
        categoryTitle = $(this).parents('tr').find('.category-name .name').text();
        categoryHeading = $(this).parents('tr').find('.category-name').attr('category-heading');
        type = $(this).parents('tr').find('.category-name').attr('type');
        $('#category-type').html(type);
        $('#edit-category').val(categoryTitle);
        $('#edit-category-heading').val(categoryHeading);
        $('#edit-category-modal').modal();
    });

    /*AJAX REQUEST TO EDIT THE INCOME AND EXPENSE CATEGORY*/
    $(document).on('submit', '#edit-submit-category', function (e) {
        e.preventDefault();
        var category_title = $('#edit-category').val();
        var categoryHeading = $('#edit-category-heading').val();
        
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>category/editCategory",
            data: {
                category_id: categoryId,
                category_type: type,
                category_title: category_title,
                category_heading: categoryHeading
            },
            success: function (data) {
                $('.category-tab').html(data);
                $('#edit-category-modal').modal('hide');
                $('#success-alert').slideDown();
            }
        });
        
    });
    /*END AJAX REQUEST TO INSERT NEW RECORD*/
</script>