 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
 	<?php $this->load->view('admin/include/left');?>
<!--END LEFT PANEL-->

<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Processing Jobs
                        <small>List of processing jobs</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Processing Jobs</li>
                    </ol>
                </section>
				
				<?php 
					$this->load->view('admin/include/notification');
				?>
				<!-- Main content-->
				<section class="content">
                    <div class='row'>
                        <div class='col-md-8 processing-record'>
							<div class="box">
								<div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
												<th>Processing Job</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										$counter = 1;
										foreach($result as $record){
											$id = $record->processing_work_id;
											$encrypted_id = base64_encode($id);
										?>
                                            <tr>
                                                <td><?=$counter?></td>
												<td><?=$record->processing_work_title?></td>
                                                <td align="center">
                                                	<a href="#" processing-id="<?=$encrypted_id?>" 
                                                    processing-name="<?=$record->processing_work_title?>" 
                                                    class="edit-processing"><i class="fa fa-edit" title="Edit"></i></a>
                                                	
                                                </td>
												
                                            </tr>
                                         <?php
										 	$counter+=1;
											}
										?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
							</div>
                            
                        </div><!-- /.col-->
                        <div class="col-md-4">
                        	<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">New Processing Work</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="new-processing" method="post">
                                   <div class="box-body">
                                   		<div class="form-group">
                                   			<input type="text" name="processing" placeholder="Processing Work Title" 
                                            class="form-control"
                                   			required id="name">
                                   		</div>
                                   </div>
	                                   <div class="box-footer">
	                                        <button id="processingSubmit" class="btn btn-primary" >Add New</button>
	                                   </div>
                                   </form>
                                <!--END FORM-->
                           	</div>
                            
                            <div class="box box-primary edit-processing-box">
                                <div class="box-header">
                                    <h3 class="box-title">Edit Processing Work</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="edit-processing" method="post">
                                   <div class="box-body">
                                        <div class="form-group">
                                            <input type="text" name="processing" class="form-control"required id="edit-name"
                                            processing-input-id="">
                                        </div>
                                   </div>
                                       <div class="box-footer">
                                            <button id="processingEdit" class="btn btn-primary" >Save Changes</button>
                                       </div>
                                   </form>
                                <!--END FORM-->
                            </div>
				        
                        </div>

                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>

<script>
$(document).ready(function(){
    var country_id;
    $('.edit-processing-box').hide();
    $('#processingSubmit').click(function(e){
        e.preventDefault();
        var name = $('#name').val();
        if(name == ""){
            alert("Processing Title cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>processing/newProcessing",
                data: { 
                        processing_title:name
                    },
                success: function(data) {
                        $('.processing-record').html(data);
                        $('#name').val('');
                    }
                });
        }
    }); 

    /*Displaying an edit box*/
    $(document).on('click','.edit-processing',function(e){
        e.preventDefault();
        var processing_title = $(this).attr('processing-name');
        processing_id = $(this).attr('processing-id');
        $('#edit-name').val(processing_title);
        $('.edit-processing-box').show();
    }); 

    /*EDIT THE COUNTRY DETAILS THROUGH AJAX*/
    $('#processingEdit').click(function(e){
        e.preventDefault();
        var name = $('#edit-name').val();
        if(name == ""){
            alert("Processing Work cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>processing/editProcessing",
                data: { 
                        processing_title:name,
                        id:processing_id
                    },
                success: function(data) {
                        $('.processing-record').html(data);
                        $('.edit-processing-box').hide();
                    }
                });
        }
    });   
});
</script>
         <!--footer-->   
</body>
</html>