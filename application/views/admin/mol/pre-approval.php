<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approval Date
            <small>Manage your Approval Date</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Moll Rate</li>
        </ol>
    </section>
    <?php 
    // $this->load->view('admin/include/notification');
    ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
              
                    <div class="box-footer">
                       <a href="<?=base_url()?>preApprovalForm"><button class="btn btn-primary">Add New</button></a>
                       <a href="<?=base_url()?>addApproval"><button class="btn btn-primary" data-toggle="modal" data-target="#preApprovalModal">Generate Pdf</button></a>
                   </div>
                  
                   <!-- Custom Tabs -->
                   
                        <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped molTable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Job</th>
                                             
                                                <th>Pre-Approval Date</th>
                                               
                                              
                                               
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        if($approvalList){
                                        foreach($approvalList as $record){
                                            $id = $record->approval_id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=@$record->job_title?></td>
                                               
                                                <td><?=@$record->pre_approval_date?></td>
                                                

                                                <td align="center">
                                                <button class="btn btn-primary">Edit</button>
                                                   
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }}
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                    <!-- end mol rate -->
                    
            </div><!-- /.col-->
                    </div>
                </div>
            </div>
  
    </section>
</aside>

<div class="modal fade" id="preApprovalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Generate Pdf</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?=base_url()?>mol/getJobPdf" target="_blank">
            <div class="form-group">
                <label>Select Job</label>
                <select class="form-control" name="selectJob">
                <?php
                if($jobs){
                    foreach($jobs as $jobData){
                        ?>
                        <option value="<?=$jobData->id?>"><?=$jobData->job_title?></option>
                        <?php } }?>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>