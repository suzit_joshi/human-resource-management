<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Moll Rate
			<small>Manage your Moll Rate</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Moll Rate</li>
		</ol>
	</section>
 <div class="alert alert-success alert-dismissable alertSubmit" style="display:none"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Inserted Successfully</div>
	<?php 
	// $this->load->view('admin/include/notification');
	?>
                <!-- Main content-->
	<section class="content">
		<div class='row'>
			<div class='col-md-12'>
				
					<div class='box'>
						<div class="box-header">
							<h3 class="box-title" style="display:block;">New Moll Rate</h3><br />
							<h7 class="pull-right"><i>Note: Field marked with * are compulsory.</i></h7>
						</div>
						<div class='box-body pad'>
							<?php echo form_open_multipart('mol/save',$arrayName = array('id' => 'submitMolRate')); ?>
							
							<div class="form-group">
								<label for="agent_name">Select Job *</label>
								<select class="form-control" name="job" id="jobTitle" required="required">
								<?php
								if($jobs){
									foreach ($jobs as $jobData) {
									?>
									<option value="<?=$jobData->id?>"><?=$jobData->job_title?></option>
									<?php
								} }
								?>
								</select>
							</div>

							<div class="form-group">
								<label for="email_id">Office Rate *</label>
								<input type="text" class="form-control" placeholder="Office Rate" name="officeRate" required="required">
							</div>

							<div class="form-group">
								<label for="email_id">Mol Rate *</label>
								<input type="text" class="form-control" placeholder="Moll Rate" name="mollRate" required="required">
							</div>

							

							<button class="btn btn-primary" type="submit" >Submit</button>
							<?php echo form_close(); ?>
						</div>   
					</div>
					<!-- list molRate -->
					<div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped molTable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Job</th>
                                                <th>Office Rate</th>
                                                <th>Mol Rate</th>
                                               
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        if($mol){
                                        foreach($mol as $record){
                                            $id = $record->mol_id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                            	<td><?=$counter?></td>
                                            	<td><?=$record->job_title?></td>
                                               
                                                <td><?=$record->office_rate?></td>
                                                <td><?=$record->mol_rate?></td>
                                                

                                                <td align="center">
                                                <button class="btn btn-primary">Edit</button>
                                                   
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }}
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
					<!-- end mol rate -->
					
			</div><!-- /.col-->
		</div><!-- ./row -->
	</section><!-- /.content -->
</aside>
<!--right_panel-->

<script>
	$(function(){
		$( "#datepicker" ).datepicker({
			yearRange: "-50:+0",
			dateFormat: "dd-M-yy",
			showAnim : "blind",
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<script>

</script>
<script type="text/javascript">
	$(function(){
		$("#submitMolRate").submit(function(e){
			e.preventDefault();
			  var formData=new FormData($(this)[0]);
		//console.log($(this)[0]);
		
		
		
		         $.ajax({
                url : "<?php echo base_url()?>mol/Save",
                type : 'POST',
                data : formData,
                processData: false,
                contentType: false,
                success:function(response)
                {
                   jsonData=JSON.parse(response);
                   job=$("#jobTitle option:selected").text();
                  
                  $(".alertSubmit").show();
                 
                   $("tbody").prepend('<tr><td><span class="badge bg-red">New</span></td><td>'+job+'</td><td>'+jsonData.officeRate+'</td><td>'+jsonData.mollRate+'</td><td><button class="btn btn-primary">Edit</button></td></tr>')
                }
            });
		})

	})
</script>
// <script>
// $(document).ready(function() {
//     var t = $('.molTable').DataTable( {
//         "columnDefs": [ {
//             "searchable": false,
//             "orderable": false,
//             "targets": 0
//         } ],
//         "order": [[ 1, 'asc' ]]
//     } );
 
//     t.on( 'order.dt search.dt', function () {
//         t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
//             cell.innerHTML = i+1;
//         } );
//     } ).draw();
// } );
// </script>