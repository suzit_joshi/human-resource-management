<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Final-Approval 
            <small>Manage your Approval</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Final-Approval</li>
        </ol>
    </section>
    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">Final-Approval</h3>
                    </div>
                    <div class='box-body pad'>
                        <form action="<?=base_url()?>mol/saveApproval" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" name="selectApprovalType" value="2">
                                
                                <label for="job">Select Candidate</label>
                                <select name="candidate" class="chzn-select form-control" id="job-select" required="required">
                                    <option value="">Select One</option>
                                    <?php 
                                    	if($candidate) 
                                    		foreach ($candidate as $candidateData) { 
                                    ?>
                                    	<option value="<?=$candidateData->id?>"><?=$candidateData->candidate_name?> - <?=$candidateData->passport_no?></option>
                                    	</option>
                                    <?php 
                                    	} 
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pre_date">Approval Date</label>
                                <input type="date" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="approvalDate" value="" required="required">
                            </div>

                            <div class="form-group">
                                <label for="pre_date">File</label>
                                <input type="file" multiple="multiple" name="userfile[]"  required="required">
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->

<script>
    $(function () {

        $(".approval").on('ifChecked', function (event) {
            var val = $(this).val();
            if (val == 1) {
                $("#hideQuota").hide();
            } else {
                $("#hideQuota").show();
            }
            $.post('<?=base_url()?>mol/getFinalApprovalJob', {
                option: val
            }).done(function (d) {
                if (d) {
                    $("#job-select").html(d);
                }
            });

        });

    });
</script>