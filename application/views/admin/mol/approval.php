<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approval Date
            <small>Manage your Approval Date</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Mol Rate</li>
        </ol>
    </section>
    <?php 
    // $this->load->view('admin/include/notification');
    ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
              
                    <div class="box-footer">
                       <a href="<?=base_url()?>addApproval"><button class="btn btn-default">Add New</button></a>

                   </div>
                  
                   <!-- Custom Tabs -->
                   
                        <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped molTable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Job</th>
                                             
                                                <th>Pre-Approval Date</th>
                                                <th>Final-Approval Date</th>
                                                <th>Quota Filled</th>
                                              
                                               
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        if($approvals){
                                        foreach($approvals as $record){
                                            $id = $record->mol_id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->job_title?></td>
                                               
                                                <td><?=$record->pre_approval_date?></td>
                                                <td><?=$record->final_approval_date?></td>
                                                 <td><?=$record->quota_filled?></td>

                                                <td align="center">
                                                <button class="btn btn-primary">Edit</button>
                                                   
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }}
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                    <!-- end mol rate -->
                    
            </div><!-- /.col-->
                    </div>
                </div>
            </div>
  
    </section>
</aside>