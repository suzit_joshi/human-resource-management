<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Final-Approval Date
            <small>Manage your Approval Date</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Final Approval</li>
        </ol>
    </section>
    <?php 
    // $this->load->view('admin/include/notification');
    ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <div class='box'>
              
                    <div class="box-footer">
                       <a href="<?=base_url()?>finalApprovalForm"><button class="btn btn-primary">Add New</button></a>
                       <a href="<?=base_url()?>addApproval"><button class="btn btn-primary" data-toggle="modal" data-target="#finalApprovalModal">Generate Pdf of Candidate</button></a>
                   </div>
                  
                   <!-- Custom Tabs -->
                   
                        <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped molTable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Candidate Name</th>
                                                <th>Final-Approval Date</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        if($finalApprovalList){
                                        foreach($finalApprovalList as $record){
                                            $id = $record->approval_id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=@$record->candidate_name?></td>
                                                <td><?=date('d-M-Y',strtotime($record->approval_date))?></td>
                                                <td align="center">
                                                <button class="btn btn-primary">Edit</button>
                                                </td>
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }}
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                    <!-- end mol rate -->
                    
            </div><!-- /.col-->
                    </div>
                </div>
            </div>
  
    </section>
</aside>

<div class="modal fade" id="finalApprovalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Generate Pdf</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?=base_url()?>mol/getCandidatePdf" target="_blank">
            <div class="form-group">
                <label>Select Candidate</label>
                <select class="form-control" name="candidate">
                <?php
                if($candidate){
                    foreach($candidate as $candidateData){
                        ?>
                        <option value="<?=$candidateData->id?>"><?=$candidateData->candidate_name?></option>
                        <?php } }?>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>