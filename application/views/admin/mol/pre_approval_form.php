
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Pre-Approval Date
                        <small>Manage your Approval</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Approval</li>
                    </ol>
                </section>
	<?php $this->load->view('admin/include/notification'); ?>
                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							
							<div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">Approval</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>mol/saveApproval" id="job-form" method="post"  enctype="multipart/form-data">

								
									
									

									<div class="form-group">
									<input type="hidden" name="selectApprovalType" value="1">
										<label for="job">Job</label>
										<select name="job" class="form-control" id="job-select" required="required">
											<option value="">Select One</option>
											<?php

											if($jobs)

												foreach ($jobs as $jobData) {
											?>
												<option value="<?=$jobData->id?>"><?=$jobData->job_title?></option>

												</option>
											<?php	
												}
											?>
										</select>
									</div>

										
									<div class="form-group">
										<label for="pre_date">Approval Date</label>
										<input type="text" class="form-control" id="datepicker" 
										name="approvalDate" value="" placeholder="Approval Date"
										required="required">
									</div>

									<div class="form-group">
										<label for="pre_date">Ref No</label>
										<input type="text" class="form-control" 
										name="ref_no" value="" placeholder="Reference Number"
										required="required">
									</div>

									<div class="form-group">
										<label for="pre_date">Power of attorney</label>
										<input type="text" class="form-control" id="datepicker1"
										name="power_of_attorney" value="" 
										required="required">
									</div>

									<div class="form-group">
										<label for="pre_date">kdn expiry date</label>
										<input type="text" class="form-control" 
										name="kdn_expiry_date" value="" id="datepicker2"
										required="required">
									</div>

									<div class="form-group">
										<label for="pre_date">Lt No</label>
										<input type="text" class="form-control" 
										name="lt_no" value="" 
										required="required">
									</div>



									<div class="form-group">
									<label for="pre_date">File</label>
									<input type="file" multiple="multiple" name="userfile[]" class="form-control" required="required">
									</div>
										

									
									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->
   
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
		dateFormat: "dd-M-yy",
		showAnim : "blind"
	});
	 $( "#datepicker1" ).datepicker({
		dateFormat: "dd-M-yy",
		showAnim : "blind"
	});

	$(".approval").on('ifChecked',function(event){
  	var val=$(this).val();
 	if(val==1){
 		$("#hideQuota").hide();
 	}
 	else{
 		$("#hideQuota").show();
 	}
  		$.post('<?=base_url()?>mol/getFinalApprovalJob',{option:val}).done(function(d){
  			if(d){
  			$("#job-select").html(d);
  		}
  		});
  	
  	});

  });


  	
</script>

