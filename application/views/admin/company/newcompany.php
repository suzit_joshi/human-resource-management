<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Company
                        <small>Add Company</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Company</li>
                    </ol>
                </section>
                <?php 
                    $this->load->view('admin/include/notification');
                ?>

                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<?php
								if(isset($result)){
									foreach($result as $record){
							?>
							<div class='box'>
								<div class="box-header">
                                	<h3 class="box-title">Edit Company</h3>
                                </div>
                                <div class='box-body pad'>
                                    <?php echo form_open_multipart('company/save');?>
									<form action="<?=base_url()?>company/save" id="company-form">
									<input type="hidden" value="<?=base64_encode($record->id)?>" name="company_id" />

									<div class="form-group">
										<label for="company_name">Company Name</label>
										<input type="text" class="form-control" id="company_name" 
										value="<?=$record->company_name?>" name="company_name" 
										required="required">
									</div>
									
									<div class="form-group">
										<label for="address">Address</label>
										<input type="text" class="form-control" id="address" 
										value="<?=$record->address?>" name="address" 
										required="required">
									</div>

									<div class="form-group">
										<label for="email">Email_id</label>
										<input type="email" class="form-control" id="email" 
										value="<?=$record->email_id?>" name="email" 
										required="required">
									</div>

									<div class="form-group">
										<label for="website">Website</label>
										<input type="url" class="form-control" id="website" 
										value="<?=$record->website?>" name="website" 
										>
									</div>

									<div class="form-group">
										<label for="country">Country</label>
										<select name="country" class="form-control" id="country-select">
											<option value="0">Select One</option>
											<?php
												$country_id = $record->country_id;
												foreach ($result_country as $record1) {
											?>
												<option value="<?=$record1->id?>"
												<?php if($country_id == $record1->id){echo "selected";}?>
												><?=$record1->country_name?></option>
											<?php	
												}
											?>
										</select>
									</div>

									<div class="form-group">
										<?php
											if($record->company_logo != ""){
										?>
										<img src="<?=base_url()?>uploads/company/<?=$record->company_logo?>" style="height:100px; width:auto;">
										<input type="hidden" value="<?=$record->company_logo?>" name="old_img">
										<br />
										<?php } ?>
										<label for="image-change">Upload Company Logo *</label>
										<input type="file" name="image"/>
										
									</div>

									<div class="form-group">
										<label for="category">General Information</label>
										<textarea class="textarea text-area-style" name="description"><?=$record->description?></textarea>
									</div>

									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
									}
								}else{
							?>
                            <div class='box'>
                            	<div class="box-header">
                               		<h3 class="box-title">New Company</h3>
                               	</div>
                                <div class='box-body pad'>
                                    <?php echo form_open_multipart('company/save');?>
									<form action="<?=base_url()?>company/save" id="company-form">
									<div class="form-group">
										<label for="company_name">Company Name</label>
										<input type="text" class="form-control" id="company_name" 
										placeholder="Company Name" name="company_name" value="<?php echo set_value('company_name'); ?>" 
										required="required">
									</div>
									
									<div class="form-group">
										<label for="address">Address</label>
										<input type="text" class="form-control" id="address" 
										placeholder="Company Address" name="address" value="<?php echo set_value('address'); ?>"
										required="required">
									</div>

									<div class="form-group">
										<label for="email">Email_id</label>
										<input type="email" class="form-control" id="email" 
										placeholder="Email address" name="email" value="<?php echo set_value('email'); ?>"
										required="required">
									</div>

									<div class="form-group">
										<label for="website">Website</label>
										<input type="url" class="form-control" id="website" 
										placeholder="http://www.abc.com (example)" name="website" value="<?php echo set_value('website'); ?>"
										>
									</div>

									<div class="form-group">
										<label for="country">Country</label>
										<select name="country" class="form-control" id="country-select">
											<option value="0">Select One</option>
											<?php
												foreach ($result_country as $record) {
											?>
												<option value="<?=$record->id?>"><?=$record->country_name?></option>
											<?php	
												}
											?>
										</select>
									</div>

									<div class="form-group">
										<label for="image-change">Upload Company Logo *</label>
										<input type="file" name="image"/>
									</div>

									<div class="form-group">
										<label for="category">General Information</label>

										<textarea class="textarea text-area-style" name="description" required="required"><?php echo set_value('description'); ?></textarea>
										
									</div>

									
									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
								}
							?>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->

