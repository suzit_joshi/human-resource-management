<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Company
                        <small>List of companies</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Company</li>
                    </ol>
                </section>
				
				<?php 
					$this->load->view('admin/include/notification');
				?>
				<!-- Main content-->
				<section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<div class="box">
                                <div class="box-footer">
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>company/createCompany')">
                                    Add New</button>
                                </div>
								<div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
												<th>Name</th>
                                                <th>Country</th>
                                                <th>Address</th>
                                                <th>Email-id</th>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        $flag = 0;
                                                        echo "<th>Last Updated</th>";
                                                    }else{
                                                        $flag = 1;
                                                        echo "<th>Website</th>";
                                                    }
                                                ?>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										$counter = 1;
										foreach($result_company as $record){
											$id = $record->id;
											$encrypted_id = base64_encode($id);
										?>
                                            <tr>
                                                <td><?=$counter?></td>
												<td><a href="#" id="view-element-content" content-id="<?=$id?>" tbl="tbl_company">
                                                    <?=$record->company_name?></a>
                                                </td>
                                                <td><?=$record->country_name?></td>
                                                <td><?=$record->address?></td>
                                                <td><?=$record->email_id?></td>
                                                <?php
                                                    if($flag == 1){
                                                        echo "<td>$record->website</td>";
                                                    }else{
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }
                                                ?>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li> <a href="#" id="view-element-content" content-id="<?=$id?>" tbl="tbl_company">View </a> 
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                            <a href="<?=base_url()?>company/getInformation/<?=$encrypted_id?>">Edit</a>
                                                            </li>
                                                            <!--<li class="divider"></li>
                                                            <li>
                                                            <a href="<?=base_url()?>company/deactivate/<?=$encrypted_id?>" 
                                                                   onclick="return confirmDeactivate();">Deactivate</a>
                                                            </li>-->    
                                                        </ul>
                                                    </div>
                                                </td>												
                                            </tr>
                                         <?php
										 	$counter+=1;
											}
										?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                                
							</div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
