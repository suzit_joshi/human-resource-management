 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
 	<?php $this->load->view('admin/include/left'); ?>
<!--END LEFT PANEL-->
            
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Reminders
                        <small>Notify yourself of small works</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Reminders</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<?php
								if(isset($result)){
									
							?>
							<div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">Edit reminder</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>reminder/save" method="post">
										<input type="hidden" name="id" value="<?=base64_encode($result->reminder_id)?>">
										<div class="form-group">
											<label>Reminder Date</label>
											<input type="text" class="form-control" id="edit-date" 
											value="<?=date('d-M-Y')?>" name="reminder_date" 
											required="required" value="<?=$result->reminder_date?>">
										</div>

										<div class="form-group">
											<label for="notification">Notification On</label>
											<select class="form-control" name="notification">
												<option value="1">Before 1 Day</option>
												<option value="2">Before 2 Days</option>
												<option value="3">Before 3 Days</option>
												<option value="7">Before 1 Week</option>
											</select>
										</div>

										<div class="form-group">
											<label for="reminder_desc">Description</label>
											<textarea class="textarea text-area-style" name="reminder_desc" required="required"><?=$result->description?></textarea>
										</div>
																		
										<div class="box-footer">
                                    	    <button type="submit" class="btn btn-primary">Save Changes</button>
                                    	</div>
								   </form>
                                </div>
                            </div>
							<?php
								}else{
							?>
                            <div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">Set new reminder</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>reminder/save" method="post">

										<div class="form-group">
											<label>Reminder Date</label>
											<input type="text" class="form-control" id="datepicker" 
											placeholder="Choose reminder Date" name="reminder_date" 
											required="required">
										</div>

										<div class="form-group">
											<label for="notification">Notification On</label>
											<select class="form-control" name="notification">
												<option value="1">Before 1 Day</option>
												<option value="2">Before 2 Days</option>
												<option value="3">Before 3 Days</option>
												<option value="7">Before 1 Week</option>
											</select>
										</div>

										<div class="form-group">
											<label for="reminder_desc">Description</label>
											<textarea class="textarea text-area-style" name="reminder_desc" required></textarea>
										</div>
																		
										<div class="box-footer">
                                    	    <button type="submit" class="btn btn-primary">Save Changes</button>
                                    	</div>
								   </form>
                                </div>
                            </div>
							<?php
								}
							?>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
         <!--footer-->  
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
		dateFormat: "dd-M-yy",
		showAnim : "blind"
	});
  });

  $('#edit-date').datepicker({
  		dateFormat: "dd-M-yy",
  		showAnim : "blind"
  });


</script>
</body>
</html>
