 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
    <?php $this->load->view('admin/include/left');?>
<!--END LEFT PANEL-->
<style>
.actions{
    display: block;
    font-size: 10px;
    opacity: 0;
}

tr:hover .actions{
    opacity: 1;
}
</style>
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Reminders
                        <small>Notify yourself of small works</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Reminders</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                <div class="box-header">
                                    <div class="box-title">
                                        <button class="btn btn-primary" 
                                        onClick="javascript:location.replace('<?=base_url()?>reminder/newReminder')">
                                        Add New</button>
                                    </div>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="15%">Reminder Date</th>
                                                <th>Reminder</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($result_reminder as $record){
                                            $id = $record->reminder_id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->reminder_date?></td>
                                                <td>
                                                    <?=$record->description?>
                                                    <span class="actions"><a href="<?=base_url()?>reminder/getReminder/<?=$encrypted_id?>">Edit</a> | 
                                                    <a href="<?=base_url()?>reminder/deleteReminder/<?=$encrypted_id?>" onclick="confirmDelete()"> Delete </a>
                                                    </span>
                                                </td>                                               
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
<!--footer-->   
</body>
</html>