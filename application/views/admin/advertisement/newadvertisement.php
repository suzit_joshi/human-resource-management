<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Advertisement
			<small>Manage your advertisements</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Agent</li>
		</ol>
	</section>

	<?php 
	$this->load->view('admin/include/notification');
	?>
                <!-- Main content-->
	<section class="content">
		<div class='row'>
			<div class='col-md-12'>
				
					<div class='box'>
						<div class="box-header">
							<h3 class="box-title" style="display:block;">New Agent</h3><br />
							<h7 class="pull-right"><i>Note: Field marked with * are compulsory.</i></h7>
						</div>
						<div class='box-body pad'>
							<?php echo form_open_multipart('advertise/save'); ?>
							
							<div class="form-group">
								<label for="agent_name">Select Job *</label>
								<select class="form-control" name="job" required="required">
								<?php
								if($jobs){
									foreach ($jobs as $jobData) {
									?>
									<option value="<?=$jobData->id?>"><?=$jobData->job_title?></option>
									<?php
								} }
								?>
								</select>
							</div>

							<div class="form-group">
								<label for="email_id">Release Date *</label>
								<input type="text" class="form-control" id="datepicker" placeholder="Release Date" name="releaseDate" required="required">
							</div>

							<div class="form-group">
								<label for="mobile">Paper *</label>
								<input type="text" class="form-control" placeholder="Enter Paper" name="paper" required="required">
							</div>

							<div class="form-group">
								<label for="phone">Size</label>
								<input type="text" class="form-control" id="size" 
								placeholder="Enter Size" name="size" 
								required="required">
							</div>

							<div class="form-group">
								<label for="perm_address">Page No *</label>
								<input type="number" class="form-control" 
								placeholder="Enter Page No" name="page_no" 
								required="required">
							</div>

							

							<div class="form-group">
								<label for="image-change">Upload Advertisement Picture *</label>
								<input type="file" name="image"/>
							</div>

							<button class="btn btn-primary" type="submit" >New Advertisement</button>
							<?php echo form_close(); ?>
						</div>   
					</div>
					
			</div><!-- /.col-->
		</div><!-- ./row -->
	</section><!-- /.content -->
</aside>
<!--right_panel-->

<script>
	$(function(){
		$( "#datepicker" ).datepicker({
			yearRange: "-50:+0",
			dateFormat: "dd-M-yy",
			showAnim : "blind",
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<script>
/*	$('#image-change').on('change', function(){ 
		$("#multiformImgUpld").submit();
	});

	$("#multiformImgUpld").submit(function(e)
	{
		var formObj = $(this);
		var formURL = formObj.attr("action");
	
		if(window.FormData !== undefined)  // for HTML5 browsers
		{

        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $('#img-display').html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $("#message_dealer").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
            }           
        });
        e.preventDefault();
        //e.unbind();
    }

    else  //for olden browsers
    {
        //generate a random id
        var  iframeId = 'unique' + (new Date().getTime());
        //create an empty iframe
        var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
        //hide it
        iframe.hide();
        //set form target to iframe
        formObj.attr('target',iframeId);
        //Add iframe to body
        iframe.appendTo('body');
        iframe.load(function(e)
        {
            var doc = getDoc(iframe[0]);
            var docRoot = doc.body ? doc.body : doc.documentElement;
            var data = docRoot.innerHTML;
            $("#multi-msg").html('<pre><code>'+data+'</code></pre>');
        });
    }
});*/

/*	$('#btn-agent-frm').on('click',function(){

		var agent = $('#agent_name').val();
		var email = $('#email_id').val();
		var mobile = $('#mobile').val();
		var phone = $('#phone').val();
		var perm = $('#perm_address').val();
		var temp = $('#temp_address').val();
		var dob = $('#datepicker').val();
		var image = $('#img-cropped').attr('title');

		if(agent== "" || email == "" || mobile == "" || perm == "" || image == ""){
			alert("Please fill in the form correctly!");
		}else{
			$.ajax({
                type: "POST",
                url: "<?=base_url()?>agent/save",
                data: { 
                        agent : agent,
                        email_id : email,
                        mobile_no : mobile,
                        phone_no : phone,
                        perm_address : perm,
                        temp_address : temp,
                        date_birth : dob,
                        img_name : image
                    },
                success: function(data) {
 						var msg = data;
 						if(msg == "success"){
 							window.location.href = '<?=base_url()?>agent/index/success';
 						}
                    }
                });
		}


	});*/

</script>


