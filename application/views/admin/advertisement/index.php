<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Advertisements
                        <small>Manage your ads</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Advertisements</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                <div class="box-footer">
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>advertise/newAd')">
                                    Add New</button>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Ad For</th>
                                                <th>Released date</th>
                                                <th>Paper</th>
                                                <th>Size</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        if($advertisement){
                                        foreach($advertisement as $record){
                                            $id = $record->advertisementId;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><?=$record->job_title?></td>
                                                <td><?=$record->released_date?></td>
                                                <td><?php echo $record->paper."(paper)<br>"
                                                        . $record->page_no."(page_no.)";
                                                    ?></td>
                                                <td><?=$record->size?></td>

                                                <td align="center">
                                                    <div class="btn-group">
                                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li> <a href="#"  id="view-element-content" content-id="<?=$id?>" tbl="tbl_advertisement">View </a> 
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>  <a href="#">Edit</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="<?=base_url()?>advertise/delete/<?=$encrypted_id?>" onclick="return confirmDelete();">Delete</a></li>
                                                    </ul>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }}
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                               
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
