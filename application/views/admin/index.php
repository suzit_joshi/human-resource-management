<style>
    .clear {
        clear: both;
    }
    
    .box-size {
        height: 110px;
    }

    .candidate-list span{
        float:left;
        
    }

    .candidate-list span:first-child, .candidate-list span:nth-child(2n+1){
        width:70%;
        font-weight: bold;
    }
</style>

<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Dashboard <small>Control panel</small> </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

                    <?php
                    $msg=$this->session->flashdata('sessionMessage');
                    if($msg!=""){
                       ?>
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Success!!</b> Your current cash in hand has been set.
                            <br /> The cash will be manipulated according to the transactions made.
                        </div>
                    </div>

                    <?php
                }
                ?>
           


            <div class="col-md-12">
                <p> Welcome, to the dashboard panel.</p>
                <p> Please navigate throught the navigation pane to your left. </p>
            </div>
            <?php 
                $usertype=strtolower($this->session->userdata('role'));

                $where = "lower(current_status) = 'new'";
                $this->db->where($where)->from('tbl_job_candidate');
                $new = $this->db->count_all_results();

                $where = "lower(current_status) = 'applied'";
                $this->db->where($where)->from('tbl_job_candidate');
                $applied = $this->db->count_all_results();

                $where = "lower(current_status) = 'applied'";
                $this->db->where($where)->from('tbl_job_candidate');
                $applied = $this->db->count_all_results();

                $where = "lower(current_status) = 'preinterview_passed'";
                $this->db->where($where)->from('tbl_job_candidate');
                $preinterview_passed = $this->db->count_all_results();

                $where = "lower(current_status) = 'finalinterview_passed'";
                $this->db->where($where)->from('tbl_job_candidate');
                $finalinterview_passed = $this->db->count_all_results();

                $where = "lower(current_status) = 'medical_passed'";
                $this->db->where($where)->from('tbl_job_candidate');
                $medical_passed = $this->db->count_all_results();


                $where = "lower(current_status) = 'orientation_appeared'";
                $this->db->where($where)->from('tbl_job_candidate');
                $orientation_appeared = $this->db->count_all_results();


                $where = "lower(current_status) = 'visa_processing'";
                $this->db->where($where)->from('tbl_job_candidate');
                $visa_processing = $this->db->count_all_results();


                $where = "lower(current_status) = 'visa_received'";
                $this->db->where($where)->from('tbl_job_candidate');
                $visa_received = $this->db->count_all_results();


                $where = "lower(current_status) = 'ticket_processing'";
                $this->db->where($where)->from('tbl_job_candidate');
                $ticket_processing = $this->db->count_all_results();


                $where = "lower(current_status) = 'ticket_received'";
                $this->db->where($where)->from('tbl_job_candidate');
                $ticket_received = $this->db->count_all_results();
                    
            ?>
            <div class="col-md-12">
            <!--Different number of candidates-->
                <div class="row">

                </div>
                <!--End of number of candidates-->

                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <?php 
                        if ($usertype == "system admin" || $usertype == "general admin" || $usertype == "finance") { 
                    ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <h3>Cash In Hand</h3>
                        <div class="small-box bg-aqua box-size">
                            <div class="inner">
                                <h3>
							Rs. <?=$cash?>
						</h3>
                            </div>
                            <div class="icon">
                                <i class="fa fa-money"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <?php
                        }
                    ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <h3>Candidates List</h3>
                        <div class="small-box bg-aqua box-size" style="height:225px !important;">
                        <style>
                           
                        </style>
                            <div class="inner candidate-list">
                                <span>New</span><span><?=$new?></span>
                                <span>Applied</span><span><?=$applied?></span>
                                <span>Pre Interview Passed</span><span><?=$preinterview_passed?></span>
                                <span>Final Interview Passed</span><span><?=$finalinterview_passed?></span>
                                <span>Medical Passed</span><span><?=$medical_passed?></span>
                                <span>Orientation Appeared</span><span><?=$orientation_appeared?></span>
                                <span>Visa Processing</span><span><?=$visa_processing?></span>
                                <span>Visa Received</span><span><?=$visa_received?></span>
                                <span>Ticket Processing</span><span><?=$ticket_processing?></span>
                                <span>Ticket Received</span><span><?=$ticket_received?></span>
                            </div>
                            <div class="icon">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->

                </div>
                <!--row-->
                <?php 
                    if ($usertype == "system admin" || $usertype == "general admin" || $usertype == "finance") { 
                ?>
                <div class="row">
                    <h3 style="margin-left:12px;">Bank Balance</h3>
                    <?php 
                        $box_color=array( 'bg-green', 'bg-yellow', 'bg-red'); 
                        if(isset($bank_result)){ 
                            foreach($bank_result as $bank){ 
                                $color_value=array_rand($box_color); ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box <?=$box_color[$color_value]?> box-size">
                            <div class="inner">
                                <h3>
									<?=$bank->balance?>
								</h3>
                                <p>
                                    <?=$bank->bank_name?>
                                </p>
                            </div>
                            <div class="icon"><i class="fa fa-rupee"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <?php } } ?>
                </div>
                <!-- /.row -->
                <?php } ?>
            </div>
            
        </div>
        <input type="hidden" id="notification_no" value="<?=$no_of_notification?>">
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->
</div>
<div class="clear"></div>

<!--MODAL FOR REMINDER NOTIFICATION-->
<div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">You have <?=$no_of_notification?> reminder </h4>
            </div>
            <form class="form-horizontal" role="form">
                <div class="modal-body">
                    <ul>
                        <?php if($notifications !=0 ){ foreach($notifications as $notification){ ?>
                        <li>
                            <p><strong> Reminder For: <?=$notification->reminder_date?> </strong> </p>
                            <p>
                                <?=$notification->description?></p>
                        </li>
                        <?php } } ?>
                    </ul>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="window.location.href = '<?=base_url()?>reminder/index'">Click for further action</button>
                </div>
            </form>
            <!--END FORM-->

        </div>
    </div>
</div>

<!--footer-->

<!--footer-->
<script>
    $(document).ready(function (e) {
        var notification = $('#notification_no').val();
        if (notification != "0") {
            $('#modal-notification').modal();
        }
    });
</script>


</body>

</html>