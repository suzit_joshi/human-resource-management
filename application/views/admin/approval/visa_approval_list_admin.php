
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Approval of Candidate
                        <small>Notify yourself of small works</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Reminders</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="15%">Candidate Name</th>
                                                <th>Company</th>
                                                <th>Passport No</th>
                                                <th>Remarks</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       <?php
                                        $counter = 1;
                                        foreach($candidate as $record){
                                            $id = $record->approval_id;
                                            $encrypted_id = base64_encode($id);
                                            
                                        ?>
                                            <tr>
                                               
                                                <td><?=$counter?></td>
                                                <td><?=$record->candidate_name?></td>
                                                <td><?=$record->company_name?></td>
                                                 <td><?=$record->passport_no?></td>
                                                 <td><?=$record->approval_remarks?></td>
                                                 <td>
                                                   <button class="btn btn-default agentApprove" id="abc<?=$counter?>" candidateId="<?=$record->candidate_id?>" status="approve">Approve</button> <button id="def<?=$counter?>" class="btn btn-default agentApprove" status="decline" candidateId="<?=$record->candidate_id?>">Decline</button>
                                                </td>  
                                                                                            
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->

<!--footer-->   
</body>
</html>
<script>
$(function(){
    $(".agentApprove").click(function(){
        candidateId=$(this).attr('candidateId');
        status=$(this).attr('status');
        randomId=$(this).attr('id');
        $.post('<?=base_url()?>approval/makeApprovalForVisa',{candidateId:candidateId,status:status}).done(function(d){
            // alert(d);
            if(d){
                $("#"+randomId).text(d);
                $("#"+randomId).siblings().remove();   
            }

        });
    });

    // $(".navbar-nav a").click(function(){
    //     // alert("test");
    // })
});
</script>