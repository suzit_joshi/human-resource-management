<form id="candidateListForm">
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
             Ticketing Approval
             <small>Send candidates for approval</small>
         </h1>

            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="active">Ticketing Approval</li>
            </ol>
        </section>

        <?php $this->load->view('admin/include/notification'); ?>
        <!-- Main content-->
        <section class="content">
            <div class='row'>
                <div class='col-md-12'>
                    <div class="box">

                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%">
                                            <input type="checkbox" id="allSelect">&nbsp;Select all</th>
                                        <th width="5%">S.No</th>
                                        <th>Candidate Name - Passport No</th>
                                        <th>Company</th>
                                        <th>Job</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $counter=1 ; 
                                        foreach($candidate as $record){ 
                                            $id=$record->candidate_id; 
                                            $encrypted_id = base64_encode($id); 
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="candidateId[]" class="agentCheckBox" value="<?=$record->candidate_id?>" required="required">
                                        </td>
                                        <td>
                                            <?=$counter?>
                                        </td>
                                        <td>
                                            <?=$record->candidate_name?> -
                                                <?=$record->passport_no?></td>
                                        <td>
                                            <?=$record->company_name?></td>
                                        <td>
                                            <?=$record->job_title?></td>
                                    </tr>
                                    <?php $counter+=1; } ?>
                                </tbody>
                            </table>
                            <div class="box-footer">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#visaApprovalModal">Forward for Approval</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
    </aside>
    <!--right_panel-->
    </div>


    <!--footer-->

    <!--footer-->
    </body>

    </html>

    <!-- for model -->
    <div class="modal fade" id="visaApprovalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Send Message</h4>
                </div>
                <div class="modal-body agentMsgBody">
                    <div class="form-group">
                        <label>Message</label>
                        <textarea name="msgForApproval" placeholder="Message" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary candidateListSubmit">Send for Approval</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="approval_type" value="ticket">
</form>
<script>
    $(function () {
        $(".candidateListSubmit").click(function () {
            // e.preventDefault();
            // alert('dead');

            var formData = new FormData($("#candidateListForm")[0]);
            $.ajax({
                url: "<?php echo base_url()?>approval/sendTicketApproval",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {

                    $(".agentMsgBody").prepend('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Successfuly Sent for approval.</div>')

                    $(".agentCheckBox").each(function (i) {
                        if ($('.agentCheckBox').eq(i).is(':checked')) {
                            $('.agentCheckBox').eq(i).parents('tr').hide();
                        }
                    });
                }
            });
        });

        // $("#allSelect").click(function(){
        //  $(".agentCheckBox").each(function(i){
        //      // $(".agentCheckBox").eq(i).attr("checked" ,"checked");
        //      $(".agentCheckBox").eq(i).checked=true;
        //  });
        // });

    });
    $(document).ready(function () {

        $('#allSelect').click(function (event) { //on click 
            if (this.checked) { // check select status
                $('.agentCheckBox').each(function () { //loop through each checkbox

                    this.checked = true; //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.agentCheckBox').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

    });
</script>