<style>
    #income-form,
    #expense-form,
    #advance-form,
    .voucher-no,
    .bank-list,
    #cheque-no,
    #exp-bank-list,
    .current-cash-badge {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                        Transaction
                        <small>Manage your income and expenses</small>
                    </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Transaction</li>
        </ol>
    </section>
    <?php 
    $this->load->view('admin/include/notification');
    ?>

    <!-- Main content -->
    <section class="content" style="height:auto !important;">
        <div class='row'>
            <div class='col-md-12'>

                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">New Transaction</h3>
                    </div>
                    <div class='box-body pad'>
                        <form action="<?=base_url()?>transaction/save" method="post">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label style="display:block;">Select Transaction Type</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="transaction_type" class="transaction-type" value="income" checked> Income
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="transaction_type" class="transaction-type" value="expense"> Expense
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="transaction_type" class="transaction-type" value="advance"> Advance
                                    </label>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="transaction_date">Transaction Date</label>
                                    <input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y')?>" name="transaction_date" required="required">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="amount">Amount &nbsp;&nbsp;&nbsp;
                                    <small class="badge pull-right bg-green current-cash-badge">Rs. <?=$current_cash?> (Cash)</i></small>
                                </label>

                                <input type="text" class="form-control" name="amount" required="required" id="amount">
                            </div>

                            <!--Income Form-->
                            <div id="income-form">

                                <div class="form-group">
                                    <label style="display:block;">Payment Type</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="payment-type" class="payment-type" value="Cash" checked> Cash
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="payment-type" class="payment-type" value="Cheque"> Cheque
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="payment-type" class="payment-type" value="Deposit"> Deposit
                                    </label>
                                </div>

                                <div class="form-group voucher-no">
                                    <div class="row">
                                        <label class="col-md-2">Cheque/Voucher No</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="voucher_no">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bank-list">
                                    <label>Bank</label>
                                    <select class="form-control" name="bank_id">
                                        <option value="0" selected="selected">Select One</option>
                                        <?php foreach($bank as $bank_record){ ?>
                                        <option value="<?=$bank_record->id?>">
                                            <?=$bank_record->bank_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                

                                <div class="form-group">
                                    <label>Income Category</label>
                                    <select class="form-control" name="category" id="income-category" required="requireds">
                                        <option value="0">Select One</option>
                                        <option value="candidate">Candidate</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>

                                <input type="hidden" id="income-category-type" name="income_category_type">

                                <div class="form-group">
                                    <label>Income From</label>
                                    <select class="form-control" name="income_candidate" id="income-candidate">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Income Source</label>
                                    <select class="form-control" name="source" id="income-source" required="requireds">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                            </div>
                            <!--END Income Form-->

                            <!--EXPENSE FORM-->
                            <div id="expense-form">
                                <input type="hidden" name="cash_id" value="<?=$cash_id?>">
                                <input type="hidden" name="current_cash" value="<?=$current_cash?>" id="current-cash">
                                <div class="form-group">
                                    <label style="display:block;">Payment Type</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="expense_payment_type" class="expense-payment-type" value="Cash" checked> Cash
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="expense_payment_type" class="expense-payment-type" value="Cheque"> Cheque
                                    </label>
                                </div>

                                <div class="form-group" id="cheque-no">
                                    <div class="row">
                                        <label class="col-md-2">Cheque No</label>
                                        <div class="col-md-4">
                                            <input type="text" name="cheque_no" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="exp-bank-list">
                                    <label>Bank</label>
                                    <select class="form-control" id="bank" name="expense_bank_id">
                                        <?php foreach($bank as $bank_record){ ?>
                                        <option value="<?=$bank_record->id?>" balance="<?=$bank_record->balance?>">
                                            <?=$bank_record->bank_name?> -
                                                <?=$bank_record->balance?>(Bal.)
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Expense Category</label>
                                    <select class="form-control" name="expense_category" id="expense-category">
                                        <option value="0">Select One</option>
                                        <option value="candidate">Candidate</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Candidate</label>
                                    <select class="form-control" name="expense_candidate" id="expense-candidate">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Expense For</label>
                                    <select class="form-control" name="expense_for" id="expense-for">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                            </div>
                            <!--End Expense Form-->

                            <!--Advance Form-->
                            <div id="advance-form">

                                <div class="form-group">
                                    <label style="display:block;">Payment Type</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="adv-payment-type" class="payment-type" value="Cash" checked> Cash
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="adv-payment-type" class="payment-type" value="Cheque"> Cheque
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="adv-payment-type" class="payment-type" value="Deposit"> Deposit
                                    </label>
                                </div>

                                <div class="form-group voucher-no">
                                    <div class="row">
                                        <label class="col-md-2">Cheque/Voucher No</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="adv_voucher_no">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bank-list">
                                    <label>Bank</label>
                                    <select class="form-control" name="adv_bank_id">
                                        <option value="0" selected="selected">Select One</option>
                                        <?php foreach($bank as $bank_record){ ?>
                                        <option value="<?=$bank_record->id?>">
                                            <?=$bank_record->bank_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                

                                <div class="form-group">
                                    <label>Advance Category</label>
                                    <select class="form-control" name="advance_category" id="advance-category" required="requireds">
                                        <option value="0">Select One</option>
                                        <option value="candidate">Candidate</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>

                                <input type="hidden" id="advance-category-type" name="advance_category_type">

                                <div class="form-group">
                                    <label>Advance From</label>
                                    <select class="form-control" name="advance_candidate" id="advance-candidate">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Advance Source</label>
                                    <select class="form-control" name="advance_source" id="advance-source" required="requireds">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>

                            </div>
                            <!--END Income Form-->

                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea class="textarea text-area-style" name="remarks"></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->

<script>
    var formType;
    $(document).ready(function (e) {
        formType = $('.transaction-type:checked').val();
        formDisplay(formType);
    });

    $(function () {
        $("#datepicker").datepicker({
            dateFormat: "dd-M-yy",
            showAnim: "blind"
        });
    });

    $('#amount').on('blur', function (e) {
        amount = $(this).val();
        var a = ".";
        if(amount.indexOf(a) == -1){
            newAmt = amount + ".00";
            $('#amount').val(newAmt);
        }
    });


    $('.transaction-type').on('ifChecked', function (event) {
        formType = $(this).val();
        formDisplay(formType);
    });

    function formDisplay(type) {
        if (type == 'income') {
            $('#advance-form').slideUp();
            $('#expense-form').slideUp(function () {
                $('#income-form').slideDown();
            });
            $('#form-submit').removeClass('expense').text('Save Changes');
            $('.current-cash-badge').hide();
        } else if(type == 'expense'){
             $('#advance-form').slideUp();
            $('#income-form').slideUp(function () {
                $('#expense-form').slideDown();
            });
            $('#form-submit').addClass('expense').text('Send For Approval');
            $('.current-cash-badge').show();

        } else{
            $('#expense-form').slideUp();
            $('#income-form').slideUp(function (){
               $('#advance-form').slideDown();
            });
            $('#form-submit').removeClass('expense').text('Save Changes');
            $('.current-cash-badge').hide();
        }
    }

    $('.payment-type').on('ifChecked', function (event) {
        paymentType = $(this).val();
        if (paymentType == 'Cheque') {
            $('.voucher-no').slideDown();
            $('.bank-list').slideUp();
        } else if (paymentType == 'Deposit') {
            $('.voucher-no').slideDown();
            $('.bank-list').slideDown();
        } else {
            $('.voucher-no').slideUp();
            $('.bank-list').hide();
        }
    });

    /*Displaying block according to the expensePayment TYpe*/
    var expensePaymentType;
    $('.expense-payment-type').on('ifChecked', function (e) {
        expensePaymentType = $(this).val();
        if (expensePaymentType == 'Cheque') {
            $('#cheque-no').slideDown();
            $('#exp-bank-list').slideDown();
        } else {
            $('#cheque-no').slideUp();
            $('#exp-bank-list').slideUp();
        }
    });
    /*END DISPLAYING BLOCK ACCORDING TO EXPENSE PAYMENT TYPE*/

    /*TO GET LIST OF CANDIDATES AND INCOME CATEGORY */
    $(document).on('change', '#income-category', function () {
        var categoryVal = $('#income-category option:selected').html().toLowerCase();
/*        $('#income-category-type').val(categoryVal);*/
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getData",
            data: {
                type: categoryVal,
                fetchData : 'candidate'
            },
            success: function(data){
                var abce = data.trim();
                if (abce == "a") {
                    $('#income-candidate').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#income-candidate').html(data);
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getSource",
            data: {
                type: categoryVal,
                fetchData : 'income'
            },
            success: function(adata){
                adata.trim();
                if (adata == "a") {
                    $('#income-source').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#income-source').html(adata);
                }
            }
        });
    });
    /*END LIST OF CANDIDATES AND INCOME CATEOGRY*/


 /*TO GET LIST OF CANDIDATES AND EXPENSE CATEGORY*/
    $(document).on('change', '#expense-category', function () {
        var categoryVal = $('#expense-category option:selected').html().toLowerCase();
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getData",
            data: {
                type: categoryVal,
                fetchData : 'candidate'
            },
            success: function(data){
                var abce = data.trim();
                if (abce == "a") {
                    $('#expense-candidate').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#expense-candidate').html(data);
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getSource",
            data: {
                type: categoryVal,
                fetchData : 'expense'
            },
            success: function(adata){
                adata.trim();
                if (adata == "a") {
                    $('#expense-for').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#expense-for').html(adata);
                }
            }
        });
    });
    /*END LIST OF CANDIDATES AND EXPENSE CATEGORY*/

    /*TO GET LIST OF CANDIDATES FOR ADVANCE AND CATEGORY*/
    $(document).on('change', '#advance-category', function () {
        var categoryVal = $('#advance-category option:selected').html().toLowerCase();
/*        $('#income-category-type').val(categoryVal);*/
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getData",
            data: {
                type: categoryVal,
                fetchData : 'candidate'
            },
            success: function(data){
                var abce = data.trim();
                if (abce == "a") {
                    $('#advance-candidate').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#advance-candidate').html(data);
                }
            }
        });

        $.ajax({
            type: "POST",
            url: "<?=base_url()?>transaction/getSource",
            data: {
                type: categoryVal,
                fetchData : 'advance'
            },
            success: function(adata){
                adata.trim();
                if (adata == "a") {
                    $('#advance-source').html("<option value='n/a'>Not Applicable</option>");
                } else {
                    $('#advance-source').html(adata);
                }
            }
        });
    });
    /*END LIST OF CANDIDATES*/


    /*Code to check whether the amount entered is sufficient according to the balance*/
    $(document).on('submit','.expense', function (e) {
        e.preventDefault();
        amount = parseInt($('#amount').val());
        if (amount == '') {
            alert("Enter expense amount");
            $('#amount').focus();
        } else {
            current_cash = parseInt($('#current-cash').val());
            flag = 0;
            if (expensePaymentType == "Cheque") {
                bankBalance = parseInt($('#bank option:selected').attr('balance'));
                if (amount < bankBalance) {
                    flag = 1;
                } else {
                    flag = 0;
                }
            } else {
                console.log('here');
                if (amount < current_cash) {
                    flag = 1;
                } else {
                    flag = 0;
                }
            }

            if (flag == 1) {
                $('#form-submit').removeClass('expense', function () {
                    $('#form-submit').trigger('click');
                });
            } else {
                alert("Insufficient Balance!!");
                $('#amount').focus();
            }
        }
    });
</script>