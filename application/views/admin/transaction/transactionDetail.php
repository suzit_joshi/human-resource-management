<style>
    #income-form,
    #expense-form,
    #advance-form,
    .voucher-no,
    .bank-list,
    #cheque-no,
    #exp-bank-list,
    .current-cash-badge {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                        Transaction
                        <small>Manage your income and expenses</small>
                    </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Transaction</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content" style="height:auto !important;">
        <div class='row'>
            <div class='col-md-12'>

                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">Transaction Approval Request</h3> 
                    </div>
                    <div class='box-body pad'>
                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-horizontal" method="post" action="<?=base_url()?>transaction/saveApproval" id="frm-approval" >
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Transaction Date</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"><?=date('d-M-Y',strtotime($row['transaction_date']))?></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Transaction Amount</label>
                                            <div class="col-sm-8">
                                            <p class="text-danger form-control-static">Rs. <?=number_format($row['amount'])?> On <?=$row['subtype']?></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Transaction Category</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"><?=$row['category_title']?></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Bank</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"><?=(is_null($row['bank_name'])?"N/A":$row['bank_name'])?></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Candidate</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"><?=(is_null($row['candidate_name'])?"N/A":$row['candidate_name'])?></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Transaction Remarks</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"><?=$row['remarks']?></p>
                                            </div>
                                        </div>  

                                        <?php
                                            $user_role = strtolower($this->session->userdata('role'));
                                            if($user_role == "general admin" || $user_role == "system admin"){
                                            
                                            if($approval_status){                                            
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Approval/Decline Remarks</label>
                                            <div class="col-sm-8">
                                            <textarea name="remarks" class="form-control" rows="5" readonly="readonly"><?=$approval_status['remarks']?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-2">
                                                <?php
                                                    if($approval_status['decision'] == "Approved"){
                                                ?>
                                                    <button class="btn btn-success res-btn" value="1" disabled="disabled">Approved</button>
                                                <?php
                                                    }
                                                    else{
                                                ?>
                                                    <button class="btn btn-danger res-btn" value="2" disabled="disabled">Declined</button>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div> 
                                        <?php
                                            }
                                            else{
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Approval/Decline Remarks</label>
                                            <div class="col-sm-8">
                                                <textarea name="remarks" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-2">
                                                <button class="btn btn-success res-btn" value="1" type="submit">Approve</button>
                                                <button class="btn btn-danger res-btn" value="2" type="submit">Decline</button>
                                            </div>
                                        </div> 

                                        <input type="hidden" value="<?=$row['id']?>" name="transaction_no">
                                        <input type="hidden" id="set-result" name="set_result">
                                        <?php
                                            }
                                        }
                                        
                                        ?>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="25%">Admins</th>
                                            <th width="20%">Date</th>
                                            <th width="15%">Decision</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <?php
                                        if($others_decision){
                                            foreach ($others_decision as $record) {
                                    ?>
                                    <tr>
                                        <td><?=$record->name?></td>
                                        <td><?=(is_null($record->date))?"N/A":$record->date?></td>
                                        <td><?=(is_null($record->decision))?"N/A":$record->decision?></td>
                                        <td><?=(is_null($record->remarks))?"N/A":$record->remarks?></td>
                                    </tr>
                                    <?php
                                            }
                                        }else{
                                    ?>
                                        <tr>
                                        <td colspan="4">No approval till date</td>
                                        </tr>
                                    <?php
                                        }
                                    ?>

                                </table>
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->
<script type="text/javascript">
    $(document).on('click','.res-btn',function(e){
        e.preventDefault();
        var a = $(this).val();
        $('#set-result').val(a);
        /*form submit*/
        $('#frm-approval').submit();
    });
</script>

