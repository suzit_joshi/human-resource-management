<style>
    .actions {
        display: block;
        font-size: 10px;
        opacity: 0;
    }
    
    tr:hover .actions {
        opacity: 1;
    }
    
    #success-alert,
    #warning-alert {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction Approval List
            <small>Approve Expense Transactions</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Transactions</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="example2" class="table table-bordered table-striped">
                                    <?php
                                        $role = strtolower($this->session->userdata('role'));
                                        if($role == "general admin" || $role == "system admin"){
                                    ?>
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Transaction Title</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Bank</th>
                                            <th>Candidate</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php 
                                            $counter=1 ; 
                                            
                                            foreach($transactions as $record){ 
                                                $id=$record->id; 
                                                $encrypt_id = base64_encode($id); ?>
                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$record->transaction_no?></td>
                                            <td><?=date('d-M-Y',strtotime($record->transaction_date))?></td>
                                            <td><?=$record->category_title?></td>
                                            <td><?=ucfirst($record->type)?></td>
                                            <td>Rs.<?=number_format($record->amount,2)?> On <?=$record->subtype?></td>
                                            <td><?php echo (is_null($record->bank_name) ? "N/A" : $record->bank_name );?></td>
                                            <td><?php echo (is_null($record->candidate_name) ? "N/A" : $record->candidate_name );?></td>
                                            <td>
                                                <?php                                                    
                                                    if($record->decision == "Approved" ){
                                                ?>  
                                                    <a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>">
                                                        <button class="btn btn-xs btn-primary">View</button>
                                                    </a>
                                                    <button class="btn btn-xs btn-success disabled">Approved</button>
                                                <?php
                                                    }
                                                    elseif($record->decision == "Declined" ){
                                                ?>  
                                                    <a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>">
                                                        <button class="btn btn-xs btn-primary">View</button>
                                                    </a>
                                                    <button class="btn btn-xs btn-danger disabled">Declined</button>
                                                       
                                                <?php
                                                    }else{
                                                ?>
                                                    <a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>">
                                                        <button class="btn btn-xs btn-primary">View</button>
                                                    </a>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                    <?php
                                        }elseif($role == "finance"){
                                    ?>
                                        <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Transaction Title</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Bank</th>
                                            <th>Candidate</th>
                                            <th>Status</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php 
                                            $counter=1 ; 
                                            
                                            foreach($finance_transactions as $record){ 
                                                $id=$record->id; 
                                                $encrypt_id = base64_encode($id); ?>
                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$record->transaction_no?></td>
                                            <td><?=date('d-M-Y',strtotime($record->transaction_date))?></td>
                                            <td><?=$record->category_title?></td>
                                            <td><?=ucfirst($record->type)?></td>
                                            <td>Rs.<?=number_format($record->amount,2)?> On <?=$record->subtype?></td>
                                            <td><?php echo (is_null($record->bank_name) ? "N/A" : $record->bank_name );?></td>
                                            <td><?php echo (is_null($record->candidate_name) ? "N/A" : $record->candidate_name );?></td>
                                            <td><?=(is_null($record->decision))?"Pending":$record->decision?></td>
                                            <td><a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>">
                                                        <button class="btn btn-xs btn-primary">View</button>
                                                    </a></td>
                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>

                                    <?php
                                        }else{
                                    ?>

                                    <?php
                                        }
                                    ?>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
        </div>
        </div>
        <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>

</aside>
<!--right_panel-->

<div class="modal fade" id="transactionModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Daily Report</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
        <?php
        echo form_open('transaction/viewPdf',array('target'=>"_blank"));
        ?>
            <div class="form-group">
                <label>Select date to generate report</label>
                <input class="form-control" type="text" name="transactionDate" id="datepicker" placeholder="select Date">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control" id="print-submit">Print</button>
            </div>
        <?php
        echo form_close();
        ?>
       </div>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).on('click','#view-transaction',function(e){
        e.preventDefault();
        var Id = $(this).attr('content-id');
        $.post('<?=base_url()?>transaction/loadTransaction',{id:Id}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })

    $(function () {
        $("#datepicker").datepicker({
            dateFormat: "dd-M-yy",
            showAnim: "blind"
        });
    });

    $(document).on('click','#print-submit',function(){
        $('#transactionModel').modal('hide');
    })

</script>