<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Sujan Khadgi">
<link rel="shortcut icon" href="<?=base_url()?>mobile_app/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?=base_url()?>mobile_app/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/bootstrap-3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/outdatedbrowser/outdatedbrowser.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/font-awesome-4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/css/style.css">
<script src="<?=base_url()?>mobile_app/assets/outdatedbrowser/outdatedbrowser.js"></script>
<script src="<?=base_url()?>mobile_app/js/jquery-1.10.2.js"></script>
<script src="<?=base_url()?>mobile_app/js/modernizr.custom.41672.js"></script>
<title>Florid</title>
<!--[if lt IE 9]>
	<script src="<?=base_url()?>mobile_app/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<noscript>
<div class="alert alert-danger no-js"><strong>Oh Snap! It seems that Javascript is disabled on your browser. Please enable it for a better browsing experience.</strong></div>
</noscript>
<!--end of no script-->
<div id="outdated">
	<h6>Your browser is out-of-date!</h6>
	<p>Update your browser to view this website correctly. <a id="btnUpdateBrowser" href="<?=base_url()?>mobile_app/http://outdatedbrowser.com/">Update my browser now </a></p>
	<p class="last"><a href="<?=base_url()?>mobile_app/#" id="btnCloseUpdateBrowser" title="Close">&times;</a></p>
</div>
<!--/.outdated browser-->

<nav class="navbar navbar-default app-header navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header"><a class="navbar-brand" href="<?=base_url()?>mobileController/index"><img alt="Brand" src="<?=base_url()?>mobile_app/florid-logo-01.png"></a> </div>
		<p class="user">Welcome, <?=$this->session->userdata('name')?></p>
	</div>
</nav>
<?php
	if($type == "finance"){
?>
<div class="app-body">
	<h3 class="app-page-title">Financial Transactions</h3>
	<div class="container-fluid">
		<?php
			$msg = $this->session->flashdata('msg');
			if($msg!=""){
		?>
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?=$msg?>
		</div>
		<?php
			}
		?>
		<div class="form-group">
			<label for="search">Search</label>
			<input type="text" class="form-control" id="search">
		</div>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				if($transactions!=0){
					foreach ($transactions as $row) {
			?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="panel-title"> 
						<a data-toggle="collapse" data-parent="#accordion" href="#<?=$row['transaction_no']?>" aria-expanded="true">
							<?=$row['category_title']?> - Rs.<?=number_format($row['amount'])?> on <?=$row['subtype']?>
							<?php
								if($row['approved_by']){
									$flag = 1;
							?>
								
								<?php foreach ($row['approved_by'] as $record) {
										if($record['decision'] == "Approved"){
								?>
										<p class="approved-by">Approved by <?=$record['name']?></p>
								<?php
										}else{
								?>
									<p class="declined-by">Declined by <?=$record['name']?></p>

							<?php
										}
									}
								}
							?>
							
						</a> 
					</h4>
					<p class="btn-wrapper"><a class="btn btn-default approve" href="#" role="button" transaction_no="<?=$row['id']?>">Approve</a> &nbsp; 
					<a class="btn btn-danger decline" href="#" role="button" transaction_no="<?=$row['id']?>">Decline</a></p>
				</div>
				<!--/.panel-heading-->
				<div id="<?=$row['transaction_no']?>" class="panel-collapse collapse" role="tabpanel">
					<div class="panel-body">
							Bank - <?=(is_null($row['bank_name'])?"N/A":$row['bank_name'])?><br>
							Candidate - <?=(is_null($row['candidate_name'])?"N/A":$row['candidate_name'])?><br>
							Remarks - <?php echo $row['remarks'];?><br>
							<?php
								if(isset($flag)){
									echo "Other decision:"."<br>";
									foreach ($row['approved_by'] as $record) {
										echo " - ". $record['name']." - ". $record['decision']. " - ".$record['remarks']."<br>";
									}
								}
							?>
					</div>
					<!--/.panel-body--> 
				</div>
				<!--/.panel-collapse--> 
			</div>
			<!--/.panel-->
			<?php
					}
				}else{
					echo "No approval request! ";
				}
			?>
		</div>
		<!--/.panel-group--> 
	</div>
</div>
<!--/.app-body--> 

<!-- Modal -->
<div class="modal fade" id="remarksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="<?=base_url()?>mobileController/setDecision" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="remarks">Remarks</label>
						<textarea class="form-control" rows="5" name="remarks"></textarea>
					</div>
				</div>
				<input type="hidden" id="transaction_no" name="transaction_no">

				<div class="modal-footer">
					<button type="submit" class="btn btn-default approve" value="Approved" name="btnvalue">Approve</button>
					<button type="submit" class="btn btn-danger decline" value="Declined" name="btnvalue">Decline</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php
	}
?>




<script>
$(document).ready(function(e) {
	outdatedBrowser({
		bgColor: '#f25648',
		color: '#ffffff',
		lowerThan: 'boxShadow',
		languagePath: ''
	})             
});           
</script> 
<script src="<?=base_url()?>mobile_app/js/jquery-easing.js"></script> 
<script src="<?=base_url()?>mobile_app/assets/bootstrap-3.3.4/js/bootstrap.min.js"></script> 
<script src="<?=base_url()?>mobile_app/js/custom.js"></script>
</body>
</html>
