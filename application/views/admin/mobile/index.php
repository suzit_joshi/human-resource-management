<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Sujan Khadgi">
<link rel="shortcut icon" href="<?=base_url()?>mobile_app/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?=base_url()?>mobile_app/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/bootstrap-3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/outdatedbrowser/outdatedbrowser.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/assets/font-awesome-4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>mobile_app/css/style.css">
<script src="<?=base_url()?>mobile_app/assets/outdatedbrowser/outdatedbrowser.js"></script>
<script src="<?=base_url()?>mobile_app/js/jquery-1.10.2.js"></script>
<script src="<?=base_url()?>mobile_app/js/modernizr.custom.41672.js"></script>
<title>Florid</title>
<!--[if lt IE 9]>
	<script src="<?=base_url()?>mobile_app/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<noscript>
<div class="alert alert-danger no-js"><strong>Oh Snap! It seems that Javascript is disabled on your browser. Please enable it for a better browsing experience.</strong></div>
</noscript>
<!--end of no script-->
<div id="outdated">
	<h6>Your browser is out-of-date!</h6>
	<p>Update your browser to view this website correctly. <a id="btnUpdateBrowser" href="<?=base_url()?>mobile_app/http://outdatedbrowser.com/">Update my browser now </a></p>
	<p class="last"><a href="<?=base_url()?>mobile_app/#" id="btnCloseUpdateBrowser" title="Close">&times;</a></p>
</div>
<!--/.outdated browser-->

<nav class="navbar navbar-default app-header navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header"><a class="navbar-brand" href="<?=base_url()?>mobileController/index"><img alt="Brand" src="<?=base_url()?>mobile_app/florid-logo-01.png"></a> </div>
		<p class="user">Welcome, <?=$username?></p>
	</div>
</nav>
<div class="app-body">
	<h3 class="app-page-title">Approval Request</h3>
	<div class="container-fluid">
		<ul class="request-list">
			<li class="yellow"><a href="<?=base_url()?>mobile_app/inside.html"><i class="fa fa-user"></i>Agent<span class="badge"><?=$agentNotification?></span></a></li>
			<li class="orange"><a href="<?=base_url()?>mobileController/getFinance"><i class="fa fa-money"></i>Finance<span class="badge"><?=$financeNotification?></span></a></li>
			<li class="red"><a href="<?=base_url()?>mobile_app/inside.html"><i class="fa fa-plane"></i>Visa<span class="badge">10</span></a></li>
			<li class="teal"><a href="<?=base_url()?>mobile_app/inside.html"><i class="fa fa-ticket"></i>Ticket<span class="badge">1</span></a></li>
		</ul>
	</div>
</div>
<!--/.app-body--> 
<script>
$(document).ready(function(e) {
	outdatedBrowser({
		bgColor: '#f25648',
		color: '#ffffff',
		lowerThan: 'boxShadow',
		languagePath: ''
	})             
});           
</script> 
<script src="<?=base_url()?>mobile_app/js/jquery-easing.js"></script> 
<script src="<?=base_url()?>mobile_app/assets/bootstrap-3.3.4/js/bootstrap.min.js"></script> 
<script src="<?=base_url()?>mobile_app/js/custom.js"></script>
</body>
</html>
