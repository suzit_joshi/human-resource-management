<ul class="sidebar-menu">
			<li> <a href="#">Navigation Pane </a> </li>
			
			<li <?php $x = ($url == "admin") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>admin"><i class="fa fa-home"></i> Dashboard </a> </li>


				<li class="treeview <?php $x = ($url == "report") ? "active": ""; echo $x;?>">
					<a href="#">
					<i class="fa fa-home"></i> <span>Office Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "branch") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>branch"><i class="fa fa-angle-double-right"></i><i class="fa fa-sitemap"></i>Branches</a></li>

						<li <?php $x = ($url == "roles") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>roles"><i class="fa fa-angle-double-right"></i><i class="fa fa-gears"></i>User Roles</a></li>

						<li <?php $x = ($url == "user") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>user"><i class="fa fa-angle-double-right"></i><i class="fa fa-user"></i>Create User</a></li>

						<li <?php $x = ($url == "agent") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>agent"><i class="fa fa-angle-double-right"></i><i class="fa fa-users"></i>Agent</a></li>	
					</ul>
				</li>

				<li class="treeview <?php if($url == "company" || $url == "country" || $url == "job" || $url == "advertise" ){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-suitcase"></i> <span>Job Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "country") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>country"><i class="fa fa-angle-double-right"></i><i class="fa fa-map-marker"></i>Country</a> </li>

						<li <?php $x = ($url == "company") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>company"><i class="fa fa-angle-double-right"></i><i class="fa fa-building-o"></i>Company</a> </li>

						<li <?php $x = ($url == "job") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>job"><i class="fa fa-angle-double-right"></i><i class="fa fa-suitcase"></i>Job</a></li>	

						<li <?php $x = ($url == "advertise") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>advertise"><i class="fa fa-angle-double-right"></i><i class="fa fa-bullhorn"></i>Advertisement</a></li>	
					</ul>

				</li>	
				

				</li>

                <li  <?php $x = ($url == "candidate") ? "active": ""; echo "class = ".$x;?>>
				<a href="<?=base_url()?>candidate"><i class="fa fa-users"></i>Candidate</a></li>

				<li class="treeview <?php if($url == "interview" || $url == "medical" || $url== "ticketing" || $url=="visa"){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-puzzle-piece"></i> <span>Processing Work</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "interview") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>interview"><i class="fa fa-angle-double-right"></i><i class="fa fa-comments-o"></i>Interview</a> </li>

						<li <?php $x = ($url == "medical") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>medical"><i class="fa fa-angle-double-right"></i><i class="fa fa-stethoscope"></i>Medical</a> </li>

						<li <?php $x = ($url == "visa") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>visa"><i class="fa fa-angle-double-right"></i><i class="fa fa-sign-in"></i>Visa</a> </li>

						<li <?php $x = ($url == "orientation") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>orientation"><i class="fa fa-angle-double-right"></i><i class="fa fa-refresh"></i>Orientation</a> </li>

						<li <?php $x = ($url == "ticketing") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>ticketing "><i class="fa fa-angle-double-right"></i><i class="fa fa-list-alt"></i>Ticketing</a> </li>
						
					</ul>
				</li>
        
                <li class="treeview <?php if($url == "bank" || $url == "category" || $url == "transaction"){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-credit-card"></i> <span>Finance Section</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "bank") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>bank"><i class="fa fa-angle-double-right"></i><i class="fa fa-rupee"></i>Bank</a></li>

						<li <?php $x = ($url == "category") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>category"><i class="fa fa-angle-double-right"></i><i class="fa fa-folder"></i>Category</a></li>

						<li <?php $x = ($url == "transaction") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>transaction"><i class="fa fa-angle-double-right"></i><i class="fa fa-dollar"></i>Transaction </a></li> </li>
					</ul>
				</li>

				<li class="treeview <?php $x = ($url == "report") ? "active": ""; echo $x;?>">
					<a href="#">
					<i class="fa fa-bar-chart-o"></i> <span>Reports</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?=base_url()?>report/getTransactionReport"><i class="fa fa-angle-double-right"></i>Transaction Report</a></li>
						<li><a href="<?=base_url()?>report/getCandidateReport"><i class="fa fa-angle-double-right"></i>Candidate Report</a></li>
					</ul>
				</li>


				<li>
				<a href="#" data-toggle="modal" data-target="#set-cash"><i class="fa fa-money"></i><span>Cash In Hand</span> <small class="badge pull-right bg-red"><i class="fa fa-exclamation"></i></small></a></li>
				
				

			<li <?php $x = ($url == "reminder") ? "active": ""; echo "class = ".$x;?>>
			<a href="<?=base_url()?>reminder"><i class="fa fa-bell"></i>Reminder</a></li>

			<li <?php $x = ($url == "password_mgmt") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>password_mgmt"> <i class="fa  fa-lock"></i>Change Password</a></li>
		</ul>