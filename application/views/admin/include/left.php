<?php 
	$data['url'] = $this->uri->segment(1);
?>

<aside class="left-side sidebar-offcanvas"> 
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar"> 
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<?php
			
			$userRole = strtolower($this->session->userdata('role'));
			$user_id=$this->session->userdata('user_id');
		 
			$query=$this->db->query("select t2.id as branch_id 
										from tbl_user t1 inner join tbl_company_branch t2 on t1.branch_id=t2.id where t1.id=$user_id")->row();
			if($query->branch_id==1){
			if($userRole == "general admin" || $userRole == "system admin"){
					
						$view=$this->load->view('admin/include/left_general_admin',$data,true);
						echo $view;
					 } elseif ($userRole == "mol admin") { 
						$view=$this->load->view('admin/include/left_mol',$data,true);
						echo $view;
					} elseif ($userRole == "finance") { 
						$view=$this->load->view('admin/include/left_finance',$data,true);
						echo $view;
					 } elseif ($userRole == "general user") { 
						$view=$this->load->view('admin/include/left_general_user',$data,true);
						echo $view;
					 }  elseif ($userRole == "admin") { 
						$view=$this->load->view('admin/include/left_admin',$data,true);
						echo $view;
					} elseif ($userRole == "recruitment" || $userRole == "visa processing" || $userRole == "ticketing") { 
						$view=$this->load->view('admin/include/left_processing',$data,true);
						echo $view;
					 }  elseif ($userRole == "documentation") { 
						$view=$this->load->view('admin/include/left_documentation',$data,true);
						echo $view;
				  } elseif($userRole=="mol_finance"){
				  		$view=$this->load->view('admin/include/left_mol_finance',$data,true);
						echo $view;
				  }

				   else { 
						echo "Sunbi Design Studio";
				 } 
				}else{
					$view=$this->load->view('admin/include/left_branch',$data,true);
						echo $view;
				} 
			 ?>		
		 	
		
	</section>
	<!-- /.sidebar --> 
</aside>


