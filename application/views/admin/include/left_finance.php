<ul class="sidebar-menu">
			<li> <a href="#">Navigation Pane </a> </li>
			
			<li <?php $x = ($url == "admin") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>admin"><i class="fa fa-home"></i> Dashboard </a> </li>

	
            <li  <?php $x = ($url == "candidate") ? "active": ""; echo "class = ".$x;?>>
			<a href="<?=base_url()?>candidate"><i class="fa fa-users"></i>Candidate</a></li>
    
            <li <?php $x = ($url == "mol-rate") ? "active": ""; echo "class = ".$x;?>>
                <a href="<?=base_url()?>mol-rate"><i class="fa fa-map-marker"></i>Mol Rate</a>
            </li>

			<li <?php $x = ($url == "bank") ? "active": ""; echo "class = ".$x;?>>
			<a href="<?=base_url()?>bank"><i class="fa fa-rupee"></i>Bank</a></li>

			<li <?php $x = ($url == "category") ? "active": ""; echo "class = ".$x;?>>
			<a href="<?=base_url()?>category"><i class="fa fa-folder"></i>Category</a></li>

			<li <?php $x = ($url == "transaction") ? "active": ""; echo "class = ".$x;?>>
			<a href="<?=base_url()?>transaction"><i class="fa fa-dollar"></i>Transaction </a></li> </li>


			<li class="treeview <?php if($url == "approval" ){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-envelope"></i> <span>Approval Request</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?=base_url()?>approval/approvalForVisa">
							<i class="fa fa-angle-double-right"></i><i class="fa fa-sign-in"></i>Visa Approval</a> 
						</li>

						<li><a href="<?=base_url()?>approval/approvalForTicket "><i class="fa fa-angle-double-right"></i>
						<i class="fa fa-list-alt"></i>Ticketing Approval</a> </li>
					</ul>
			</li>

			<li><a href="#" data-toggle="modal" data-target="#set-cash"><i class="fa fa-money"></i>
				<span>Cash In Hand</span> <small class="badge pull-right bg-red"><i class="fa fa-exclamation"></i></small></a>
			</li>
				

			<li class="treeview <?php if($url == "interview" || $url == "medical" || $url== "ticketing" || $url=="visa"){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-puzzle-piece"></i> <span>Processing Work</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "interview") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>interview"><i class="fa fa-angle-double-right"></i><i class="fa fa-comments-o"></i>Interview</a> </li>

						<li <?php $x = ($url == "medical") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>medical"><i class="fa fa-angle-double-right"></i><i class="fa fa-stethoscope"></i>Medical</a> </li>

						<li <?php $x = ($url == "visa") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>visa"><i class="fa fa-angle-double-right"></i><i class="fa fa-sign-in"></i>Visa</a> </li>

						<li <?php $x = ($url == "ticketing") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>ticketing "><i class="fa fa-angle-double-right"></i><i class="fa fa-list-alt"></i>Ticketing</a> </li>
						
					</ul>
				</li>
			
			<li <?php $x = ($url == "password_mgmt") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>password_mgmt"> <i class="fa  fa-lock"></i>Change Password</a></li>
</ul>