<style>

	.footer{
		width:100%;
		height:20px;
	}
</style>

<!--MODAL to set cash in hand for the first time-->
<div class="modal fade" id="set-cash" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Cash in hand</h4>
      </div>
      <form class="form-horizontal" role="form" action="<?=base_url()?>transaction/setCash" method="post">
      <div class="modal-body">        

        <div class="form-group">
            <label class="col-sm-4">Current Cash in Hand</label>
            <div class="col-sm-8">
            <input type="number" class="form-control" name="balance">
            <p class="help-block">Important! Once set cannot be changed.</p>
            </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form><!--END FORM-->

    </div>
  </div>
</div>
<!--END MODAL TO SET CASH IN HAND FOR THE FIRST TIME-->

<!--MODAL to view the elements-->
<div class="modal fade" id="view-ajax-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content view-ajax-content">
      

    </div>
  </div>
</div>
<!--END MODAL to view the elements-->


<!--MODAL to view the elements-->
<div class="modal fade" id="candidate-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Select Candidate List For Transfer</h4>
			</div>
			<div class="modal-body" id="view-candidate-list-transfer">
				
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!--END MODAL to view the elements-->


<div class="footer">
	<div class="col-md-12">
		<span class="pull-right">design: <a href="http://thesunbi.com" target="_blank">SunBi</a></span>
	</div>
</div>
<script type="text/javascript" src="<?=base_url()?>plugin/dynatable/jquery.dynatable.js"></script>
<!--<script src="<?=base_url()?>admin_files/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script> -->

<script type="text/javascript" src="<?=base_url()?>admin_files/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>admin_files/js/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url()?>admin_files/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="<?=base_url()?>admin_files/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url()?>admin_files/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>admin_files/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>admin_files/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>admin_files/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!--CHAINED JQUERY JS-->
<script src="<?=base_url()?>admin_files/js/dropdown/jquery.chained.js" type="text/javascript"></script>
<!--BOOTSTRAP DATE TIME PICKER-->
<script type="text/javascript" 
src="<?=base_url()?>admin_files/js/bootstrap-timepicker-gh-pages/js/bootstrap-timepicker.js"></script>
<!--CHOSEN JQUERY -->
<script type="text/javascript" src="<?=base_url()?>admin_files/js/chosen.jquery.min.js"></script>
<!--NAIL THUMB -->
<script type="text/javascript" src="<?=base_url()?>plugin/nailthumb/jquery.nailthumb.1.1.js"></script>
<!--JCROP IMAGE -->
<script type="text/javascript" src="<?=base_url()?>plugin/jcrop/js/jquery.Jcrop.js"></script>
<script type="text/javascript" src="<?=base_url()?>plugin/jcrop/js/jquery.color.js"></script>

<!--JQUERY FLOT JS-->
<script type="text/javascript" src="<?=base_url()?>admin_files/js/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?=base_url()?>admin_files/js/plugins/flot/jquery.flot.categories.js"></script>

<!--END JQUERY FLOT JS-->
<!-- InputMask -->
<script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
<!-- date-range-picker -->

<script type="text/javascript">
	
	
	var t=$('#example1').dataTable();

	 $(".chzn-select").chosen();

	$('#example2').dataTable();
	$('#example3').dataTable();

	$("#datemask").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
    $("[data-mask]").inputmask();

	function confirmDelete(){
		if(confirm("Are you sure you want to delete?")){
			return true;
		}
		else{
			return false;	
		}
	}

	/*Function to deactivate the record*/
	function confirmDeactivate(){
		if (confirm("Are you sure you want to deactivate the record?")){
			return true;
		}
		else{
			return false;	
		}
	}

	function confirmActivate(){
		if (confirm("Are you sure you want to activate the record?")){
			return true;
		}
		else{
			return false;	
		}
	}

	function confirmVisaCancel(){
		if (confirm("Are you sure you want to cancel the visa?")){
			return true;
		}
		else{
			return false;	
		}
	}
	
	$(document).on('click','#view-element-content',function(e){
		e.preventDefault();
		var id = $(this).attr('content-id');
		var table = $(this).attr('tbl');
		$.ajax({
          type: "POST",
          url: "<?=base_url()?>general/getContent",
          data: { 
	            id:id,
	            table : table
            },
            success: function(data) {
               $('#view-ajax-modal .view-ajax-content').html(data);
               $('#view-ajax-modal').modal();
            }
        });
        $('#print-modal').modal();
	})


	$(document).ready(function(){
		
		$('.content').removeAttr('style');

		contentheight = $('.right-side .content').height() + 200;
		$('.right-side .content').height(contentheight);
	});

	$(document).ready(function () {
        $('#candidate').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                        "url": "<?php echo base_url()?>candidate/serverProcessing",
                        "type": "POST",
                        "draw" : 100
                      }
        });

        $(document).ready(function() {
			$('#example').dataTable( {
				"ajax": "<?=base_url()?>test.txt"
			} );
		} );
    });

</script>

