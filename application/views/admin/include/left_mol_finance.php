<ul class="sidebar-menu">
			<li> <a href="#">Navigation Pane </a> </li>
			
			<li <?php $x = ($url == "admin") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>admin"><i class="fa fa-home"></i> Dashboard </a> </li>


				<li class="treeview <?php $x = ($url == "branch" || $url == "roles" || $url == "user" || $url == "agent") ? "active": ""; echo $x;?>">
					<a href="#">
					<i class="fa fa-home"></i> <span>Office Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "agent") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>agent"><i class="fa fa-angle-double-right"></i><i class="fa fa-users"></i>Agent</a></li>	
					</ul>
				</li>

				<li class="treeview <?php if($url == "company" || $url == "country" || $url == "job" || $url == "advertise" ){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-suitcase"></i> <span>Job Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "country") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>country"><i class="fa fa-angle-double-right"></i><i class="fa fa-map-marker"></i>Country</a> </li>

						<li <?php $x = ($url == "company") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>company"><i class="fa fa-angle-double-right"></i><i class="fa fa-building-o"></i>Company</a> </li>

						<li <?php $x = ($url == "job") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>job"><i class="fa fa-angle-double-right"></i><i class="fa fa-suitcase"></i>Job</a></li>	

						
					</ul>

				</li>	
				

                <li  <?php $x = ($url == "candidate") ? "active": ""; echo "class = ".$x;?>>
				<a href="<?=base_url()?>candidate"><i class="fa fa-users"></i>Candidate</a></li>

			<li class="treeview <?php if($url == "bank" || $url == "category" || $url == "transaction"){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-credit-card"></i> <span>Finance Section</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						

						<li <?php $x = ($url == "transaction") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>transaction"><i class="fa fa-angle-double-right"></i><i class="fa fa-dollar"></i>Transaction </a></li> </li>
					</ul>
				</li>
        
                
			<li <?php $x = ($url == "password_mgmt") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>password_mgmt"> <i class="fa  fa-lock"></i>Change Password</a></li>

		</ul>