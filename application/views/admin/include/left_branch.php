<ul class="sidebar-menu">
	<li> <a href="#">Navigation Pane </a> </li>
	
	<li <?php $x = ($url == "admin") ? "active": ""; echo "class = ".$x;?>> 
		<a href="<?=base_url()?>admin"><i class="fa fa-home"></i> Dashboard </a> </li>

	<li <?php $x = ($url == "job") ? "active": ""; echo "class = ".$x;?>>
	<a href="<?=base_url()?>job"><i class="fa fa-suitcase"></i>Job</a></li>	

	<li  <?php $x = ($url == "candidate") ? "active": ""; echo "class = ".$x;?>>
	<a href="<?=base_url()?>candidate"><i class="fa fa-users"></i>Candidate</a></li>

	<li <?php $x = ($url == "reminder") ? "active": ""; echo "class = ".$x;?>>
	<a href="<?=base_url()?>reminder"><i class="fa fa-bell"></i>Reminder</a></li>

	<li <?php $x = ($url == "password_mgmt") ? "active": ""; echo "class = ".$x;?>> 
	<a href="<?=base_url()?>password_mgmt"> <i class="fa  fa-lock"></i>Change Password</a></li>
</ul>