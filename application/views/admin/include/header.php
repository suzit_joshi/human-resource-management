<?php $user=$this->session->userdata('username'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	    <title>Human Resource Management</title>
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?=base_url()?>admin_files/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?=base_url()?>admin_files/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
        <link href="<?=base_url()?>admin_files/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?=base_url()?>admin_files/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" 
		type="text/css" />
		<!-- Theme style -->
        <link href="<?=base_url()?>admin_files/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		 <!-- DATA TABLES -->
        <link href="<?=base_url()?>admin_files/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/jQueryUI/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/js/bootstrap-timepicker-gh-pages/css/bootstrap-timepicker.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/jquery.chosen.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/custom.css">
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/nailthumb/jquery.nailthumb.1.1.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/jcrop/css/jquery.Jcrop.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/dynatable/jquery.dynatable.css">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugin/jquery-steps-master/css/jquery.steps.css">

        <script src="<?=base_url()?>admin_files/js/jquery.js" type="text/javascript"></script>
        <!-- jQuery UI 1.10.3 -->

    </head>
    <style type="text/css">
            tr td:nth-child(2) a{
  color:#333; 
  text-decoration:underline;
}
    </style>

	
	<body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
            	<img src="<?=base_url()?>admin_files/img/florid-logo-012.png" style="padding-bottom:5px; padding-top:5px; height:119%" /> 
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       <!-- User Account: style can be found in dropdown.less -->
                       <?php
                       $sessionRole=strtolower($this->session->userdata('role'));
                       $user_id = $this->session->userdata('user_id');
                       $branch_id = $this->session->userdata('branch');
                       if($branch_id == 1){
                       if($sessionRole=="general admin" || $sessionRole=="system admin") {
                            $query=$this->db->order_by("id", "desc")->limit(10)->get('tbl_agent_notification')->result();
                            $total=count($query);
                       ?>
                           <li class="dropdown messages-menu">
                           <?php
                           if($total>0){
                            ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user"></i>
                                    <span class="label label-success" id="agentNotification"><?=$total?></span>
                                </a>
                                <?php } ?>
                                <ul class="dropdown-menu">
                                    <!-- <li class="header">You have 4 messages</li> -->
                                   
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <div class="slimScrollDiv" style="position: relative; overflow: scroll; width: auto; height: 200px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                          <?php
                                            foreach($query as $rowData){
                                            ?>  
                                            <li>
                                                <a href="<?=base_url()?>notification/getAgentNotification/<?=$rowData->id?>">
                                                    <i class="fa fa-angle-double-right"></i>

                                                    <h4>Approval Request by <?=$rowData->sent_user?></h4>
                                                    <p><?=$rowData->remarks?></p>
                                                </a>
                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$rowData->id?>" 
                                                    id="seenId<?=$rowData->id?>" type="agent">
                                                          
                                                </span>
                                            </li>
                                            <?php } ?>
                                        </ul><div class="slimScrollBar" style="width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; z-index: 99; right: 1px; height: 131.147540983607px; background: rgb(0, 0, 0);"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                                    </li>

                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>



                        <?php }
                        if ($sessionRole == "finance" || $sessionRole == "recruitment" || $sessionRole == "documentation" 
                            || $sessionRole=="visa processing" || $sessionRole=="ticketing" || $sessionRole=="general admin" || $sessionRole=="system admin") {
                            $query = $this->db->query("SELECT * FROM tbl_general_notification WHERE receipent_role = '$sessionRole' AND status = 0");
                            $no_of_row = $query->num_rows();
                            if($no_of_row>0){ 
                        ?>
                            <!--NOTIFICATION SECTION FOR FINANCE USERS-->
                           <li class="dropdown tasks-menu">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-users"></i>
                                    <span class="label label-success" id="notifyHeader"><?=$no_of_row?></span>
                                </a>
    
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$no_of_row?> notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record) {
                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>notification/getCandidateList/<?=$record->heading?>" 
                                                    class="finance-notification" heading="<?=$record->heading?>">
                                                    <h3>
                                                        <?=@$record->sender?> , <?=@$record->sender_role?>
                                                    </h3>
                                                    <span style="color:black"><?=@$record->candidate_no?> candidate/s <br>
                                                    - <?=@$record->remarks?></span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="other">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li> 
                             <!--END NOTIFICATION SECTION FOR FINANCE USERS-->

                        <?php
                            }
                        } 
                        ?>

                        <?php 
                        if ($sessionRole=="general admin" || $sessionRole=="system admin") {
                            $query = $this->db->query("SELECT tbl_transaction.*,t1.category_title FROM tbl_transaction 
                                                            INNER JOIN tbl_category_transaction t1
                                                                ON tbl_transaction.category_id = t1.id                                                            
                                                        WHERE tbl_transaction.type='expense' AND sent_flag = '1'
                                                            AND tbl_transaction.id NOT IN(SELECT distinct(transaction_id) 
                                                                                            FROM tbl_finance_approval WHERE user_id = $user_id)");
                            $no_of_row = $query->num_rows();
                        ?>
                            <!--NOTIFICATION SECTION FOR FINANCE USERS-->
                           <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-money"></i>
                                    <span class="label label-warning" id="notifyHeaderFinance"><?=$no_of_row?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$no_of_row?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>" class="finance-notification" >
                                                    <h3>Expense for : Rs. <?=number_format($record->amount,2)?></h3>
                                                    <span style="color:black"><?=@$record->remarks?></span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="other">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>transaction/getTransaction">See All Expense Notifications</a></li>
                                </ul>
                            </li>  
                             <!--END NOTIFICATION SECTION FOR FINANCE TRANSACTIONS-->

                             <?php
                                $query = $this->db->query("SELECT tbl_visa_ticketing_approval.*, tbl_candidate.candidate_name, tbl_candidate.passport_no
                                                            FROM tbl_visa_ticketing_approval
                                                                INNER JOIN tbl_candidate 
                                                                    ON tbl_candidate.id = tbl_visa_ticketing_approval.candidate_id 
                                                            WHERE decision IS NULL AND type = 'visa'
                                                        ");
                                $count_no = $query->num_rows();
                            ?>

                             <!--NOTIFICATION SECTION FOR  VISA APPROVAL LIST-->
                           <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-qrcode"></i>
                                    <span class="label label-danger" id="notifyHeaderVisaAdmin"><?=$count_no?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$count_no?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$record->id?>" class="visa-notification" >
                                                    <h3>Visa Approval for : <?=$record->candidate_name?> , <?=$record->passport_no?></h3>
                                                    <span style="color:black"><?=substr($record->sender_remarks,0,50)?></span>
                                                </a>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>visa/getVisaApproval">See All Visa Notifications</a></li>
                                </ul>
                            </li>  
                            <!--END NOTIFICATION SECTION FOR FINANCE USERS-->
                            <?php
                                $query = $this->db->query("SELECT tbl_visa_ticketing_approval.*, tbl_candidate.candidate_name, tbl_candidate.passport_no
                                                            FROM tbl_visa_ticketing_approval
                                                                INNER JOIN tbl_candidate 
                                                                    ON tbl_candidate.id = tbl_visa_ticketing_approval.candidate_id 
                                                            WHERE decision IS NULL AND type = 'ticket'
                                                        ");
                                $count_no = $query->num_rows();
                            ?>
                            <!--NOTIFICATION FOR TICKET-->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-plane"></i>
                                    <span class="label label-danger" id="notifyHeaderTicketAdmin"><?=$count_no?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$count_no?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>ticketing/getticketApproval/<?=$record->id?>" class="visa-notification" >
                                                    <h3>Ticketing for : <?=$record->candidate_name?> , <?=$record->passport_no?></h3>
                                                    <span style="color:black"><?=substr($record->sender_remarks,0,40)?></span>
                                                </a>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>ticketing/getticketApproval">See All Ticketing Notifications</a></li>
                                </ul>
                            </li> 
                            
                             <!--END NOTIFICATION FOR TICKET-->
                        <?php
                            }
                        ?>


                        <?php 
                        if ($sessionRole=="finance") {
                            $query = $this->db->query("SELECT tbl_transaction.*,t1.category_title FROM tbl_transaction 
                                                            INNER JOIN tbl_category_transaction t1
                                                                ON tbl_transaction.category_id = t1.id                                                            
                                                        WHERE tbl_transaction.type='expense' AND sent_flag = '2'");
                            $no_of_row = $query->num_rows();
                        ?>
                            <!--NOTIFICATION SECTION FOR FINANCE USERS-->
                           <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-money"></i>
                                    <span class="label label-danger" id="notifyHeaderFinance"><?=$no_of_row?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$no_of_row?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>transaction/getTransaction/<?=$record->id?>" class="finance-notification" >
                                                    <h3>Expense for : Rs. <?=number_format($record->amount,2)?></h3>
                                                    <span style="color:black"><?=@$record->remarks?></span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="finance">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>transaction/getTransaction">See All Expense Notifications</a></li>
                                </ul>
                            </li>  
                             <!--END NOTIFICATION SECTION FOR FINANCE USERS-->
                             <?php
                                $query = $this->db->query("SELECT tbl_visa_ticketing_approval.*, tbl_candidate.candidate_name, tbl_candidate.passport_no
                                                            FROM tbl_visa_ticketing_approval
                                                                INNER JOIN tbl_candidate 
                                                                    ON tbl_candidate.id = tbl_visa_ticketing_approval.candidate_id 
                                                            WHERE decision IS NOT NULL AND type = 'visa' AND sent_flag = '1'");
                                $count_no = $query->num_rows();
                            ?>
                            <!--NOTIFICATION SECTION FOR FINANCE USERS FOR VISA-->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-qrcode"></i>
                                    <span class="label label-danger" id="notifyHeaderVisa"><?=$count_no?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$count_no?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>visa/getVisaApproval/<?=$record->id?>" class="visa-notification" >
                                                    <h3>Visa Request : <?=$record->decision?></h3>
                                                    <span style="color:black"><?=$record->candidate_name?> , <?=$record->passport_no?></span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="visa">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>visa/getVisaApproval">See All Visa Notifications</a></li>
                                </ul>
                            </li> 

                            <?php
                                $query = $this->db->query("SELECT tbl_visa_ticketing_approval.*, tbl_candidate.candidate_name, tbl_candidate.passport_no
                                                            FROM tbl_visa_ticketing_approval
                                                                INNER JOIN tbl_candidate 
                                                                    ON tbl_candidate.id = tbl_visa_ticketing_approval.candidate_id 
                                                            WHERE decision IS NOT NULL AND type = 'ticket' AND sent_flag = '1'");
                                $count_no = $query->num_rows();
                            ?>
                             <!--NOTIFICATION FOR TICKET-->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-plane"></i>
                                    <span class="label label-danger" id="notifyHeaderTicketFinance"><?=$count_no?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$count_no?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>ticketing/getticketApproval/<?=$record->id?>" class="ticket-notification" >
                                                    <h3>Ticketing Request : <?=$record->decision?></h3>
                                                    <span style="color:black"><?=$record->candidate_name?> , <?=$record->passport_no?></span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="ticket">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                    <li class="header text-center"><a href="<?=base_url()?>ticketing/getticketApproval">See All Ticketing Notifications</a></li>
                                </ul>
                            </li> 
                            <!--END NOTIFICATION FOR TICKET-->
                        <?php
                            }
                        ?>


                        <?php
                            if($sessionRole == "mol admin"){
                               $query = $this->db->query("SELECT * FROM tbl_general_notification WHERE receipent_role = '$sessionRole' AND status = 0");
                                $no_of_row = $query->num_rows(); 
                            
                        ?>
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-qrcode"></i>
                                    <span class="label label-danger" id="notifyHeaderMolAdmin"><?=$no_of_row?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?=$no_of_row?> Notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <!--FINANCE APPROVAL LIST-->
                                            <?php
                                                foreach($query->result() as $record){                                                    
                                            ?>
                                            <li style="position:relative;"><!-- Task item -->
                                                <a href="<?=base_url()?>notification/getCandidateList/<?=$record->heading?>" 
                                                class="mol-notification" >
                                                    <h3><?=$record->sender?> , <?=$record->sender_role?></h3>
                                                    <span style="color:black"><?=$record->candidate_no?> - <?=$record->remarks?> </span>
                                                </a>

                                                <span style="position:absolute;top:10px; right:10px;color:black;cursor:pointer;" 
                                                    class="seen-sts" notification-id="<?=$record->id?>" 
                                                    id="seenId<?=$record->id?>" type="mol">
                                                            <i class="fa fa-hand-o-up"></i>
                                                </span>
                                            </li><!-- end task item -->
                                            <?php
                                                }
                                            ?>

                                        </ul>
                                    </li>
                                </ul>
                            </li> 


                        <?php
                            }
                        }
                        ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span><?=$user?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?=base_url()?>admin/logout" class="btn btn-default btn-flat">Log out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

<script type="text/javascript">
    $(document).on('click','.seen-sts',function(e){
        var Id = $(this).attr('notification-id');
        // var seenId = "#"+$(this).attr('id');
        var seenId = $(this).find('i');
        var type = $(this).attr('type');
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>notification/changeStatus",
            data: {
                notification_id : Id,
                type : type
            },
            success: function (data) {

                if(data==1){
                    // $(seenId).find('i').remove();

                    $(seenId).remove();
                    if(type == "finance"){
                        total=$("#notifyHeaderFinance").text();
                        $("#notifyHeaderFinance").text(total-1);
                    }
                    else if(type == "visa"){
                        total=$("#notifyHeaderVisa").text();
                        $("#notifyHeaderVisa").text(total-1);
                    }
                    else if(type == "ticket"){
                        total=$("#notifyHeaderTicketFinance").text();
                        $("#notifyHeaderTicketFinance").text(total-1);
                    }
                    else if(type == "mol"){
                        total=$("#notifyHeaderMolAdmin").text();
                        $("#notifyHeaderMolAdmin").text(total-1);
                    }
                    else{
                        total=$("#notifyHeader").text();
                        $("#notifyHeader").text(total-1);
                    }
                }else{
                    alert('Seen Status could not be changed.');
                }
            }
        });
    });
</script>

