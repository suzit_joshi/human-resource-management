<?php
	@$flag = $this->uri->segment(3);
	if(@$flag == "editsuccess"){
?>
	<div class="alert alert-success alert-dismissable">
    	<i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> Changes have been successfully saved.
    </div>
<?php
	}elseif(@$flag == "success"){
?>
	<div class="alert alert-success alert-dismissable">
    	<i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> New record has been successfully added.
    </div>

<?php
    }elseif(@$flag == "trsuccess"){
?>
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> Transaction complete.
    </div>
<?php
	}elseif(@$flag == "delsuccess"){
?>
	<div class="alert alert-info alert-dismissable">
    	<i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> Record has been successfully deleted.
    </div>
<?php
	}elseif(@$flag == "imageedit"){
?>
	<div class="alert alert-success alert-dismissable">
    	<i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> New image has been added to album.
    </div>
<?php
	}elseif(@$flag == "deactivated"){
?>  
    <div class="alert alert-info alert-dismissable">
        <i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> The record has been deactivated.
    </div>
<?php
    }elseif(@$flag == "activated"){
?>  
    <div class="alert alert-info alert-dismissable">
        <i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!!</b> The record has been activated.
    </div>
<?php
    }elseif(@$flag == "unsucess"){
?>
<div class="alert alert-info alert-dismissable">
        <i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Error!!</b> Transaction could not be completed.
    </div>
<?php
}
?>

<?php
 $msg=$this->session->flashdata('sessionMessage');

 // die();
 if($msg!=""){
     ?>
     <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=$msg?>
    </div>
     
     <?php
     }
?>

<?php
 $notice_msg=$this->session->flashdata('noticeMessage');
 if($notice_msg!=""){
     ?>
     <div class="alert alert-info alert-dismissable">
        <i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>NOTICE!!</b> <?=$notice_msg?>
    </div>
     
     <?php
     }
?>

<?php
$val = validation_errors();

if(strlen($val) != 0){
?>

<div class="alert alert-info alert-dismissable">
    <i class="fa fa-info"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b>NOTICE!!</b> <?php echo validation_errors();?>
</div>
<?php } ?>