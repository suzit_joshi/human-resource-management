<ul class="sidebar-menu">
			<li> <a href="#">Navigation Pane </a> </li>
			
			<li <?php $x = ($url == "admin") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>admin"><i class="fa fa-home"></i> Dashboard </a> </li>


				<li class="treeview <?php $x = ($url == "branch" || $url == "roles" || $url == "user" || $url == "agent") ? "active": ""; echo $x;?>">
					<a href="#">
					<i class="fa fa-home"></i> <span>Office Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "branch") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>branch"><i class="fa fa-angle-double-right"></i><i class="fa fa-sitemap"></i>Branches</a></li>

						<li <?php $x = ($url == "user") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>user"><i class="fa fa-angle-double-right"></i><i class="fa fa-user"></i>Create User</a></li>

						<li <?php $x = ($url == "agent") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>agent"><i class="fa fa-angle-double-right"></i><i class="fa fa-users"></i>Agent</a></li>	
					</ul>
				</li>

				<li class="treeview <?php if($url == "company" || $url == "country" || $url == "job" || $url == "advertise" ){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-suitcase"></i> <span>Job Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "country") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>country"><i class="fa fa-angle-double-right"></i><i class="fa fa-map-marker"></i>Country</a> </li>

						<li <?php $x = ($url == "company") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>company"><i class="fa fa-angle-double-right"></i><i class="fa fa-building-o"></i>Company</a> </li>

						<li <?php $x = ($url == "job") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>job"><i class="fa fa-angle-double-right"></i><i class="fa fa-suitcase"></i>Job</a></li>	

						<li <?php $x = ($url == "advertise") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>advertise"><i class="fa fa-angle-double-right"></i><i class="fa fa-bullhorn"></i>Advertisement</a></li>	
					</ul>

				</li>	
				

                <li  <?php $x = ($url == "candidate") ? "active": ""; echo "class = ".$x;?>>
				<a href="<?=base_url()?>candidate"><i class="fa fa-users"></i>Candidate</a></li>

				<li class="treeview <?php if($url == "interview" || $url == "medical" || $url== "ticketing" || $url=="visa" || $url=="orientation"){ echo "active"; } ?>">
					<a href="#">
					<i class="fa fa-puzzle-piece"></i> <span>Processing Work</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?php $x = ($url == "interview") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>interview"><i class="fa fa-angle-double-right"></i><i class="fa fa-comments-o"></i>Interview</a> </li>

						<li <?php $x = ($url == "medical") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>medical"><i class="fa fa-angle-double-right"></i><i class="fa fa-stethoscope"></i>Medical</a> </li>

						<li <?php $x = ($url == "visa") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>visa"><i class="fa fa-angle-double-right"></i><i class="fa fa-sign-in"></i>Visa</a> </li>

						<li <?php $x = ($url == "orientation") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>orientation"><i class="fa fa-angle-double-right"></i><i class="fa fa-refresh"></i>Orientation</a> </li>

						<li <?php $x = ($url == "ticketing") ? "active": ""; echo "class = ".$x;?>>
						<a href="<?=base_url()?>ticketing "><i class="fa fa-angle-double-right"></i><i class="fa fa-list-alt"></i>Ticketing</a> </li>
						
					</ul>
				</li>
        
                
			<li <?php $x = ($url == "password_mgmt") ? "active": ""; echo "class = ".$x;?>> 
			<a href="<?=base_url()?>password_mgmt"> <i class="fa  fa-lock"></i>Change Password</a></li>
		</ul>