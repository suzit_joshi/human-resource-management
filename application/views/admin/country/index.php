
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Country
                        <small>List of countries</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Country</li>
                    </ol>
                </section>
				
				<?php 
					$this->load->view('admin/include/notification');
				?>
				<!-- Main content-->
				<section class="content">
                    <div class='row'>
                        <div class='col-md-8 country-record'>
							<div class="box">
								<div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
												<th>Country</th>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        echo "<th>Last Updated</th>";
                                                    }
                                                ?>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										$counter = 1;
										foreach($result_country as $record){
											$id = $record->id;
											$encrypted_id = base64_encode($id);
										?>
                                            <tr>
                                                <td><?=$counter?></td>
												<td><?=$record->country_name?> - <?=$record->company_count?></td>
                                                <?php
                                                    if($role == "general admin" || $role == "system admin"){
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }
                                                ?>
                                                <td align="center">
                                                	<a href="#" country-id="<?=$encrypted_id?>" 
                                                    country-name="<?=$record->country_name?>" class="edit-country"><i class="fa fa-edit" title="Edit"></i></a>
                                                </td>
												
                                            </tr>
                                         <?php
										 	$counter+=1;
											}
										?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
							</div>
                            
                        </div><!-- /.col-->
                        <div class="col-md-4">
                        	<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">New Country</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="new-country" method="post">
                                   <div class="box-body">
                                   		<div class="form-group">
                                   			<input type="text" name="country" placeholder="Country Name" class="form-control"
                                   			required id="name">
                                   		</div>
                                   </div>
	                                   <div class="box-footer">
	                                        <button id="countrySubmit" class="btn btn-primary" >Add New</button>
	                                   </div>
                                   </form>
                                <!--END FORM-->
                           	</div>
                            
                            <div class="box box-primary edit-country-box">
                                <div class="box-header">
                                    <h3 class="box-title">Edit Country</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="edit-country" method="post">
                                   <div class="box-body">
                                        <div class="form-group">
                                            <input type="text" name="country" class="form-control"required id="edit-name"
                                            country-input-id="">
                                        </div>
                                   </div>
                                       <div class="box-footer">
                                            <button id="countryEdit" class="btn btn-primary" >Save Changes</button>
                                       </div>
                                   </form>
                                <!--END FORM-->
                            </div>
				        
                        </div>

                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->


<script>
$(document).ready(function(){
    var country_id;
    $('.edit-country-box').hide();
    $('#countrySubmit').click(function(e){
        e.preventDefault();
        var name = $('#name').val();
        if(name == ""){
            alert("Country name cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>country/newCountry",
                data: { 
                        country_name:name
                    },
                success: function(data) {
                        $('.country-record').html(data);
                        $('#name').val('');
                    }
                });
        }
    }); 

    /*Displaying an edit box*/
    $(document).on('click','.edit-country',function(e){
        e.preventDefault();
        var country_name = $(this).attr('country-name');
        country_id = $(this).attr('country-id');
        $('#edit-name').val(country_name);

        $('.edit-country-box').show();
    }); 

    /*EDIT THE COUNTRY DETAILS THROUGH AJAX*/
    $('#countryEdit').click(function(e){
        e.preventDefault();
        var name = $('#edit-name').val();
        if(name == ""){
            alert("Country name cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>country/editCountry",
                data: { 
                        country_name:name,
                        id:country_id
                    },
                success: function(data) {
                        $('.country-record').html(data);
                        $('.edit-country-box').hide();
                    }
                });
        }
    });   
});
</script>
