<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Interview
            <small>List of interviews by candidate</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Interview</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    
    <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <?php
                                $role = strtolower($this->session->userdata('role'));
                                if( $role == "recruitment" || $role == "general admin" || $role == "system admin"){
                            ?>
                            <div class="box-title">
                                <button class="btn btn-primary" onclick="window.location='<?=base_url()?>interview/newInterview'">
                                    Add New</button>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Other Option <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                
                                <li> <a href="<?=base_url()?>interview/getTransferList/pre">Filter Pre-Interview Passed Candidates</a> </li>
                                <li> <a href="<?=base_url()?>interview/getTransferList/final">Transfer final-interview passed candidates to Documentation/General Admin</a> </li>
                                </ul>
                            </div>
                            <?php
                                }
                            ?>
                            </div>
                            
                        </div>
                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="col-md-12 category-tab">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">Pre-Interview Details</a>
                                            </li>
                                            <li><a href="#tab_2" data-toggle="tab">Final Interview Details</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Date</th>
                                                            <th>Candidate</th>
                                                            <th>Passport No</th>
                                                            <th>Interviewed By</th>
                                                            <th>Result</th>
                                                            <?php
                                                            $role = strtolower($this->session->userdata('role'));
                                                            if($role == "general admin" || $role == "system admin"){
                                                                echo "<th>Last Updated</th>";
                                                            }
                                                            ?>
                                                            <th width="5%">Option</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $counter = 1;
                                                            foreach($pre_interviews as $record){
                                                                $id = $record->id;
                                                                $encrypted_id = base64_encode($record->id);
                                                        ?>
                                                        <tr>
                                                            <td><?=$counter?></td>
                                                            <td><?=date('d-M-Y',strtotime($record->interview_date))?></td>
                                                            <td><?=$record->candidate_name?></td>
                                                            <td><?=$record->passport_no?></td>
                                                            <td><?=$record->taken_by?></td>
                                                            <td><?php echo (($record->result == 1) ? "PASS" : "FAIL" );?></td>
                                                            <?php
                                                            if($role == "general admin" || $role == "system admin"){
                                                                $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                                echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                            }
                                                            ?> 
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li value="<?=$id?>" type="Pre-interview"> <a href="#" id="view">View </a>
                                                                        </li>
                                                                         <?php
                                                                         $role = strtolower($this->session->userdata('role'));
                                                                         if( $role == "recruitment" || $role == "general admin" || $role == "system admin"){
                                                                            ?>
                                                                        <li class="divider"></li>
                                                                        <li> <a href="<?=base_url()?>interview/editInterview/<?=$encrypted_id?>">Edit</a>
                                                                        </li>
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $counter+=1; } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <table id="example2" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Date</th>
                                                            <th>Candidate</th>
                                                            <th>Passport No</th>
                                                            <th>Interviewed By</th>
                                                            <th>Result</th>
                                                            <?php
                                                            $role = strtolower($this->session->userdata('role'));
                                                            if($role == "general admin" || $role == "system admin"){
                                                                echo "<th>Last Updated</th>";
                                                            }
                                                            ?>
                                                            <th width="5%">Option</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $counter = 1;
                                                            
                                                            foreach($final_interviews as $record){
                                                               $encrypted_id = base64_encode($record->id); 
                                                        ?>
                                                        <tr>
                                                            <td><?=$counter?></td>
                                                            <td><?=date('d-M-Y',strtotime($record->interview_date))?></td>
                                                            <td><?=$record->candidate_name?></td>
                                                            <td><?=$record->passport_no?></td>
                                                            <td><?=$record->taken_by?></td>
                                                            <td><?php echo (($record->result == 1) ? "PASS" : "FAIL" );?></td>
                                                            <?php
                                                            if($role == "general admin" || $role == "system admin"){
                                                                $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                                echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                            }
                                                            ?> 
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li value="<?=$record->id?>" type="Final-interview"> <a href="#" id="view">View </a>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li> <a href="<?=base_url()?>interview/editInterview/<?=$encrypted_id?>">Edit</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $counter+=1; } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- nav-tabs-custom -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>

                </div>
            </div>
</div>
<!-- /.col-->
</div>
<!-- ./row -->
</section>
</aside>
<!--right_panel-->
<script>
    $(document).on('click','#view',function(e){
        e.preventDefault();
        var Id = $(this).parent().attr('value');
        var type = $(this).parent().attr('type');
        $.post('<?=base_url()?>interview/getInterview',{id:Id,type:type}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })
</script>