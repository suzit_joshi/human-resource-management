<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                        Interview
                        <small>List of interviews by candidate</small>
                    </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Interview</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <?php if(isset($interview)){ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">Edit Interview</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'interview/save');?> 
                            <input type="hidden" value="<?=base64_encode($interview['id'])?>" name="random">
                            <div class="form-group">
                               <label for="dob" class="control-label">Interview Type*</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="type" value="Pre-interview" <?php echo (($interview['type'] == "Pre-interview") ? "checked='checked'" : "disabled='disabled'" );?>> Pre-Interview
                                        </label>
                                        <label>
                                            <input type="radio" name="type" value="Final-interview" <?php echo (($interview['type'] == "Final-interview") ? "checked='checked'" : "disabled='disabled'" );?>> Final Interview
                                        </label>
                                    </div>                               
                            </div>
                            
                            <div class="form-group">
                                <label for="candidate">Candidate</label>
                                <select class="form-control" name="candidate">
                                    <?php foreach($candidates as $row){ 
                                            if($row->id == $interview['candidate_id']){
                                        ?>
                                        <option value="<?=$row->id?>"><?php echo $row->candidate_name." - ". $row->passport_no."(passport no)";?></option>
                                    <?php 
                                            }
                                        } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Interviewed By</label>
                                <input type="text" class="form-control" id="interviewee" value="<?=$interview['taken_by']?>" name="interviewee" required="required" readonly="readonly">
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Interviewed date*</label>
                                <input type="text" class="form-control" id="datepicker" 
                                value="<?=date('d-M-Y',strtotime($interview['interview_date']))?>" name="date" required="required">
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Result*</label>
                                <div class="radio">
                                        <label>
                                            <input type="radio" name="result" value="1" <?php echo (($interview['result'] == "1") ? "checked='checked'" : "" );?>> PASS
                                        </label>
                                        <label>
                                            <input type="radio" name="result" value="0" <?php echo (($interview['result'] == "0") ? "checked='checked'" : "" );?>> FAIL
                                        </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="description"><?=$interview['remarks']?></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="interview-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php }else{ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">New Interview</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'interview/save');?>                            
                            <div class="form-group">
                               <label for="dob" class="control-label">Interview Type*</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="type" value="Pre-interview" checked="checked" class="interview-type"> Pre-Interview
                                        </label>
                                        <label>
                                            <input type="radio" name="type" value="Final-interview" class="interview-type"> Final Interview
                                        </label>
                                    </div>                               
                            </div>
                            
                            <div class="form-group">
                                <label for="job">Select Job</label>
                                <select class="form-control" name="job" id="job-list" required>
                                    <option value="">Choose Job to sort candidate</option>
                                    <?php 
                                        if($jobs){
                                            foreach($jobs as $row){ ?>
                                                <option value="<?=$row->job_id?>" final_interviewer="<?=$row->final_interviewer?>">
                                        <?php echo $row->job_title?></option>
                                    <?php }
                                        }else{ echo "<option value=''>No Job Currently</option>";}
                                     ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="candidate">Candidate</label>
                                <select class="form-control" name="candidate" id="candidate-list" required>
                                    
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Interviewed By</label>
                                <input type="text" class="form-control" id="interviewee" placeholder="Interviewed By" name="interviewee" >
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Interviewed date*</label>
                                <select name="date" id="interview-date" class="form-control">
                                    <option value="">Select Date</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Result*</label>
                                <div class="radio">
                                        <label>
                                            <input type="radio" name="result" value="1" checked="checked"> PASS
                                        </label>
                                        <label>
                                            <input type="radio" name="result" value="0"> FAIL
                                        </label>
                                        <label style="display:none;" id="withheld">
                                            <input type="radio" name="result" value="2" > WITHHELD
                                        </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="description"></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="interview-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->
        
<script>

    
    var formType = "Pre-interview";
    var jobId;

    // function interviewType(){
    //     $('.interview-type').iCheck('check', function(){
    //       return $(this).val();
    //   });
    // }
    // formType = interviewType();


    $('.interview-type').on('ifChecked', function(event){
        formType    =   $(this).val();

        $('#job-list option:first').prop('selected',true);
        $('#interview-date').val('');
        $('#interviewee').val('').prop('readonly',false);
        $('#candidate-list').html('<option>Select Candidate</option>');

        if(formType == "Pre-interview"){
            $('#withheld').hide();
        }else{
            $('#withheld').show();
        }
       
    });

    $(document).on('change','#job-list',function(e){
        $('#candidate-list').html('<option>LOADING.......</option>');
        $('#interview-date').html('<option>LOADING.......</option>');

        jobId = $('#job-list option:selected').val();
        if(jobId != ""){
            //to get date range between multiple dates
            $.post('<?=base_url()?>interview/getInterviewDate',{jobId:jobId,type:formType}).done(function(d){
                $('#interview-date').html(d);
            });

            //To set final interviewer name
            if(formType == "Final-interview"){
                var interviewer = $('#job-list option:selected').attr('final_interviewer');
                $('#interviewee').val(interviewer).prop('readonly',true);
            }

            $.post('<?=base_url()?>interview/getCandidateList',{type:formType, job:jobId}).done(function(e){
                $('#candidate-list').html(e);
            });
        }
        else{
             $('#interview-date').val('');
             $('#candidate-list').html('<option>No Candidate for interview</option>');
        }
    });
    
/*  
candidateDisplay(formType);
   function candidateDisplay(type){
        $('#candidate-list').html('<option>LOADING.......</option>');
         
        $.post('<?=base_url()?>interview/getCandidateList',{type:type}).done(function(e){
            $('#candidate-list').html(e);
        });
     }*/
    
    function candidateDisplay(type){
        
        $('#candidate-list').html('<option>LOADING.......</option>');
         
        $.post('<?=base_url()?>interview/getCandidateList',{type:type}).done(function(e){
            $('#candidate-list').html(e);
        });
    }
    
</script>