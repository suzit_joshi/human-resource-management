<?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
 	<?php $this->load->view('admin/include/left');?>
<!--END LEFT PANEL-->

<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        User Role
                        <small>Manage your user roles</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Role</li>
                    </ol>
                </section>
				
				<?php 
					$this->load->view('admin/include/notification');
				?>
				<!-- Main content-->
				<section class="content">
                    <div class='row'>
                        <!--TABLE DISPLAY-->
                        <div class='col-md-8 role-record'>
							<div class="box">
								<div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
												<th>Role</th>
                                                <th>Status</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										$counter = 1;
										foreach($result_role as $record){
											$id = $record->role_id;
											$encrypted_id = base64_encode($id);
										?>
                                            <tr>
                                                <td><?=$counter?></td>
												<td><?=$record->role_name?></td>
                                                <td><?php if($record->status == '1'){ echo "Active";}else{ echo "Inactive";}?>
                                                </td>
                                                <td align="center">
                                                	<a href="#" role-id="<?=$encrypted_id?>" 
                                                    role-name="<?=$record->role_name?>" class="edit-role"><i class="fa fa-edit" title="Edit"></i></a>
                                                	<a href="<?=base_url()?>roles/deactivate/<?=$encrypted_id?>" 
                                                    role-id="<?=$encrypted_id?>" onclick="return confirmDeactivate();"><i class="fa fa-minus-square" title="Deactivate Role"></i></a>
                                                </td>
												
                                            </tr>
                                         <?php
										 	$counter+=1;
											}
										?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
							</div>
                        </div><!-- /.col-->
                        <!--END TABLE DISPLAY -->
                        <!--NEW ROLE AND EDIT ROLE FORM DIV-->
                        <div class="col-md-4">
                        	<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">New Role</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="new-role" method="post">
                                   <div class="box-body">
                                   		<div class="form-group">
                                   			<input type="text" name="role" placeholder="Role Name" class="form-control"
                                   			required id="name">
                                   		</div>
                                   </div>
	                                   <div class="box-footer">
	                                        <button id="roleSubmit" class="btn btn-primary" >Add New</button>
	                                   </div>
                                   </form>
                                <!--END FORM-->
                           	</div>
                            
                            <div class="box box-primary edit-role-box">
                                <div class="box-header">
                                    <h3 class="box-title">Edit Role</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                   <form id="edit-role" method="post">
                                   <div class="box-body">
                                        <div class="form-group">
                                            <input type="text" name="role" class="form-control"required id="edit-name"
                                            role-input-id="">
                                        </div>
                                   </div>
                                       <div class="box-footer">
                                            <button id="roleEdit" class="btn btn-primary" >Save Changes</button>
                                       </div>
                                   </form>
                                <!--END FORM-->
                            </div>
				        </div><!--END ROLE AND EDIT ROLE FORM DIV -->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>

<script>
$(document).ready(function(){
    var role_id;
    $('.edit-role-box').hide();
    $('#roleSubmit').click(function(e){
        e.preventDefault();
        var name = $('#name').val();
        if(name == ""){
            alert("Role name cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>roles/newRole",
                data: { 
                        role_name:name
                    },
                success: function(data) {
                        $('.role-record').html(data);
                        $('#name').val('');
                    }
                });
        }
    }); 

    /*Displaying an edit box*/
    $(document).on('click','.edit-role',function(e){
        e.preventDefault();
        var country_name = $(this).attr('role-name');
        role_id = $(this).attr('role-id');
        $('#edit-name').val(country_name);
        $('.edit-role-box').show();
    }); 

    /*EDIT THE COUNTRY DETAILS THROUGH AJAX*/
    $('#roleEdit').click(function(e){
        e.preventDefault();
        var name = $('#edit-name').val();
        if(name == ""){
            alert("Role name cannot be empty");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>roles/editRole",
                data: { 
                        role_name:name,
                        id:role_id
                    },
                success: function(data) {
                        $('.role-record').html(data);
                        $('.edit-role-box').hide();
                    }
                });
        }
    });   
});
</script>
         <!--footer-->   
</body>
</html>