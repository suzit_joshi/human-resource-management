<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Orientation
            <small>Manage your orientation</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Orientation</li>
        </ol>
    </section>
    <?php $this->load->view('admin/include/notification') ?>
    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <?php if(isset($medical)){ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">Edit Medical Details</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'medical/save');?>  
                            <input type="hidden" name="random" value="<?=base64_encode($medical['id'])?>">                          
                            <div class="form-group">
                               <label for="dob" class="control-label">Medical Type*</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="type" value="1st-Medical" checked="checked" class="medical-type"
                                            <?php echo (($medical['type'] == "1st-Medical") ? "checked='checked'" : "" );?>> 1st-Medical
                                        </label>
                                        <label>
                                            <input type="radio" name="type" value="Re-Medical" class="medical-type"
                                            <?php echo (($medical['type'] == "Re-Medical") ? "checked='checked'" : "" );?>> Re-Medical
                                        </label>
                                    </div>                               
                            </div>
                            
                            <div class="form-group">
                                <label for="candidate">Candidate</label>
                                <select class="form-control" name="candidate" id="candidate-list">
                                    <option value="<?=$medical['candidate_id']?>"><?=$medical['candidate_name']?></option>                                        
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Medical date*</label>
                                <input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y',strtotime($medical['medical_date']))?>" name="medical_date" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Medical Centre</label>
                                <input type="text" class="form-control" value="<?=$medical['medical_center']?>" name="medical_center" required="required">
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Result*</label>
                                <div class="radio">
                                        <label>
                                            <input type="radio" name="result" value="1" <?php echo (($medical['result'] == "1") ? "checked='checked'" : "" );?>> PASS
                                        </label>
                                        <label>
                                            <input type="radio" name="result" value="0" <?php echo (($medical['result'] == "0") ? "checked='checked'" : "" );?>> FAIL
                                        </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="description"><?=$medical['remarks']?></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="medical-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php }
                    else{ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">New Orientation Details</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'orientation/save');?>                            
                            
                            
                            <div class="form-group">
                                <label>Candidate, Passport No</label>
                                <select class="chzn-select form-control" name="candidate[]" required="required" multiple="multiple">
                                    <?php
                                        if($candidate){
                                            foreach ($candidate as $row) {
                                    ?>
                                        <option value="<?=$row->candidate_id?>"><?=$row->candidate_name?> , <?=$row->passport_no?></option>
                                    <?php
                                            }
                                        }
                                        else{
                                            echo "<option>No Candidate Till Now</option";
                                        }
                                    ?>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Orientation date*</label>
                                <input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y')?>" name="orientation_date" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Orientation Center</label>
                                <input type="text" class="form-control" placeholder="Enter Orientation Center" name="center" required="required">
                            </div>

                           <div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="remarks" placeholder="Enter Remarks if any"></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="orientation-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->
        
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });
    
    
   
</script>