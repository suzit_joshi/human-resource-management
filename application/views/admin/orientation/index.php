<style>
    .actions {
        display: block;
        font-size: 10px;
        opacity: 0;
    }
    
    tr:hover .actions {
        opacity: 1;
    }
    
    #success-alert,
    #warning-alert {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Orientation
            <small>Manage your orientation details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Orientation</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">
                            <?php
                                $role = strtolower($this->session->userdata('role'));
                                if($role == "documentation" || $role == "general admin" || $role == "system admin"){
                            ?>
                            <button class="btn btn-primary" onclick="window.location='<?=base_url()?>orientation/newOrientation'">
                                Add New
                            </button>
                            <div class="btn-group" style="margin-left:10px;">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    Options &nbsp;<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li> <a href="#" id="view-transaction">Other Options </a> 
                                    </li>
                                    <li class="divider"></li>
                                </ul>
                            </div>
                            <?php
                                }
                            ?>
                        </div>                        
                    </div>

                    <div class="box-body table-responsive">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>Center</th>
                                            <th>Date</th>
                                            <th>No of Candidates</th>
                                            <th>Remarks</th>
                                            <th width="5%">Option</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php 
                                            $counter=1 ; 
                                            foreach($orientation as $record){ 

                                                $id=$record->id; 
                                                $encrypt_id = base64_encode($id); ?>
                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$record->center?></td>
                                            <td><?=date('d-M-Y', strtotime($record->date))?></td>
                                            <td><?=$record->candidate_name?></td>
                                            <td><?=$record->remarks?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <?php
                                                        $role = strtolower($this->session->userdata('role'));
                                                        if($role == "documentation" || $role == "general admin" || $role == "system admin"){
                                                            ?>
                                                        <li> <a href="#">Edit</a> 
                                                        </li>
                                                        <li class="divider"></li>
                                                        
                                                         <?php
                                                            }else{
                                                                echo "<li><a href='#'>No option Available</a></li>";
                                                            }
                                                        ?>
                                                     </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
        </div>
        </div>
        <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>

</aside>
<!--right_panel-->
<script>
    $(document).on('click','#view-transaction',function(e){
        e.preventDefault();
        var Id = $(this).attr('content-id');
        $.post('<?=base_url()?>transaction/loadTransaction',{id:Id}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })
</script>