
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Job
                        <small>Manage your jobs</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Jobs</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<?php
								if(isset($job)){
							?>
							<div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">Edit Job</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>job/save" id="job-form" method="post"  enctype="multipart/form-data">

									<input type="hidden" value="<?=$job['id']?>" name="random">
									
									<div class="form-group">
										<label for="job_title">Job Title</label>
										<input type="text" class="form-control" id="job_title" 
										value="<?=$job['job_title']?>" name="job_title" 
										required="required">
									</div>

									<div class="form-group">
										<label for="company">Company</label>
										<select name="company" class="form-control" id="company-select">
											<option value="0">Select One</option>
											<?php
												foreach ($result_company as $record) {
											?>
												<option value="<?=$record->id?>" <?php if($record->id == $job['company_id']){ echo "selected"; } ?> >
													<?=$record->company_name?>, <?=$record->country_name?>

												</option>
											<?php	
												}
											?>
										</select>
									</div>

									<!-- <div class="form-group">
										<label for="demand_letter_img">Upload Demand Letter</label>
										<input type="file" name="userfile[]" multiple="multiple"/>
									</div> -->
									
									
									<div class="form-group">
										<label for="vacancy">No of vacancy</label>
										<input type="number" class="form-control" id="vacancy" 
										value="<?=$job['vacancy_no']?>" name="vacancy" 
										required="required">
									</div>

									<div class="form-group">
									<label for="dob" class="control-label">Demand Letter Expiry Date</label>
										<input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y',strtotime($job['demand_letter_expiry']))?>" name="demand_expiry_date" required="required">
									</div>

									<div class="form-group">
										<label for="salary">Salary</label>
										<input type="text" class="form-control" id="salary" 
										value="<?=$job['salary']?>" name="salary" 
										required="required">
									</div>

									<div class="form-group">
										<label for="period">Period</label>
										<input type="text" class="form-control" id="period" 
										value="<?=$job['period']?>" name="period" 
										required="required">
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												
													<label>Pre-Interview Date</label>
													<div class="row">
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="pre-to" 
																	value="<?=date("d-m-Y", strtotime($job['pre_interview_to_date']));?>" />
																</div>
														</div>
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="pre-for"
																	value="<?=date("d-m-Y", strtotime($job['pre_interview_for_date']));?>"/>
																</div>
														</div>
													
												</div>

											</div>
											<div class="col-md-6">
												
													<label>Final Interview Date</label>
													<div class="row">
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="final-to" 
																	value="<?=date("d-m-Y", strtotime($job['final_interview_to_date']));?>" />
																</div>
														</div>
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="final-for"
																	value="<?=date("d-m-Y", strtotime($job['final_interview_for_date']));?>"/>
																</div>
														</div>
													
												</div>

											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="final_interviewer">Final Interviewer Name</label>
										<input type="text" class="form-control" id="final_interviewer" 
										value="<?=$job['final_interviewer']?>" name="final_interviewer" 
										required="required">
									</div>

									<div class="row">
										<div class="form-group col-md-6">
											<label for="requirements">Requirements</label>
											<textarea class="textarea text-area-style" name="req_desc" required="required"><?=$job['requirement']?></textarea>
										</div>

										<div class="form-group col-md-6">
											<label for="category">Facilities</label>
											<textarea class="textarea text-area-style" name="facility_desc" required="required"><?=$job['facility']?></textarea>
										</div>
									</div>

									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
								}else{
							?>
                            <div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">New Job</h3>
                                </div>
                                <div class='box-body pad'>
									<form action="<?=base_url()?>job/save" id="job-form" method="post" enctype="multipart/form-data">

									<div class="form-group">
										<label for="job_title">Job Title</label>
										<input type="text" class="form-control" id="job_title" 
										placeholder="Job title" name="job_title" 
										required="required">
									</div>

									<div class="form-group">
										<label for="company">Company</label>
										<select name="company" class="form-control chzn-select" id="company-select">
											<option value="0">Select One</option>
											<?php
												foreach ($result_company as $record) {
											?>
												<option value="<?=$record->id?>"><?=$record->company_name?>, <?=$record->country_name?></option>
											<?php	
												}
											?>
										</select>
									</div>

									<div class="form-group">
										<label for="demand_letter_img">Upload Demand Letter</label>
										<input type="file" name="userfile[]" multiple="multiple" id="demand-letter"/>
									</div>
									
									
									<div class="form-group">
										<label for="vacancy">No of vacancy</label>
										<input type="number" class="form-control" id="vacancy" 
										placeholder="Number of Vacancy" name="vacancy" 
										required="required">
									</div>

									<div class="form-group">
									<label for="dob" class="control-label">Demand Letter Expiry Date</label>
										<input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y')?>" name="demand_expiry_date" required="required">
									</div>

									<div class="form-group">
										<label for="salary">Salary</label>
										<input type="text" class="form-control" id="salary" 
										placeholder="0000 currency" name="salary" 
										required="required">
									</div>

									<div class="form-group">
										<label for="period">Period</label>
										<input type="text" class="form-control" id="period" 
										placeholder="Number of years" name="period" 
										required="required">
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												
													<label>Pre-Interview Date</label>
													<div class="row">
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" name="pre-to"  class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask />
																</div>
														</div>
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="pre-for"/>
																</div>
														</div>
													
												</div>

											</div>
											<div class="col-md-6">
												
													<label>Final Interview Date</label>
													<div class="row">
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="final-to" />
																</div>
														</div>
														<div class="col-md-6">
															<div class="input-group">
																	<div class="input-group-addon">
																		<i class="fa fa-calendar"></i>
																	</div>
																	<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask name="final-for"/>
																</div>
														</div>
													
												</div>

											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="final_interviewer">Final Interviewer Name</label>
										<input type="text" class="form-control" id="final_interviewer" 
										placeholder="Enter final interviewer name" name="final_interviewer" 
										required="required">
									</div>

									<div class="row">
										<div class="form-group col-md-6">
											<label for="requirements">Requirements</label>
											<textarea class="textarea text-area-style" name="req_desc" required="required"></textarea>
										</div>

										<div class="form-group col-md-6">
											<label for="category">Facilities</label>
											<textarea class="textarea text-area-style" name="facility_desc" required="required"></textarea>
										</div>
									</div>

									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							<?php
								}
							?>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->
   

<script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
  <script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="<?=base_url()?>admin_files/js/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
 <script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});

  });
</script>
