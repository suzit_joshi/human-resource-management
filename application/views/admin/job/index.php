<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Job
                        <small>Manage your jobs</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Job</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                                <?php
                                    $role = strtolower($this->session->userdata('role'));
                                    if($role == "admin" || $role == "general admin" || $role == "system admin"){
                                ?>
                                <div class="box-footer">
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>job/newJob')">
                                    Add New</button>
                                </div>
                                <?php
                                    }
                                ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Title</th>
                                                <th>Company</th>
                                                <th>Country</th>
                                                <th>No of vacancy</th>
                                                <th>Salary</th>

                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        echo "<th>Demand Expiry</th><th>Last Updated</th>";
                                                        $flag = 1;
                                                    }else{
                                                        $flag = 0;
                                                    }
                                                ?>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($result_job as $record){
                                            $id = $record->id;
                                            $encrypted_id = base64_encode($id);

                                            $date1=date_create($record->demand_letter_expiry);
                                            $current_date=date_create(date('Y-m-d'));
                                            $diff=date_diff($date1,$current_date);
                                            $abc=(int)$diff->format("%R%a");
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><a href="#" id="view-job" job-id="<?=$id?>" ><?=$record->job_title?></a>- <?=$record->candidate_count?></td>
                                                <td><?=$record->company_name?></td>
                                                <td><?=$record->country_name?></td>
                                                <td><?=$record->vacancy_no?></td>
                                                <td><?=$record->salary?></td>
                                                <?php
                                                    if($flag == 1){
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                       
                                                        echo "<td>";
                                                        if($abc<0){
                                                            echo "<span style='color:red'>".$record->demand_letter_expiry."</span>";
                                                        }else{
                                                            echo $record->demand_letter_expiry."<br>".$abc."days";
                                                        }
                                                        echo "</td>";
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }
                                                ?>
                                                <td job-id="<?=$id?>"> 

                                                    <div class="btn-group">
                                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li> <a href="#" id="view-job" job-id="<?=$id?>" >View </a> 
                                                        </li>
                                                        <?php
                                                            if($role == "admin" || $role == "general admin" || $role == "system admin" ){
                                                        ?>
                                                        <li class="divider"></li>
                                                        <li> <a href="<?=base_url()?>job/getInformation/<?=$encrypted_id?>">Edit</a></li>
                                                        <!-- <li class="divider"></li>
                                                        <li><a href="<?=base_url()?>job/deactivate/<?=$encrypted_id?>" 
                                                                onclick="return confirmDeactivate();">Deactivate</a></li> -->
                                                        <li class="divider"></li>
                                                        <li><a href="<?=base_url()?>job/pdf/<?=$encrypted_id?>" target="_blank">Generate Demand Letter</a></li>
                                                        <?php
                                                            }
                                                        ?>
                                                    </ul>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                               
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->

<!--MODAL to view the elements-->
<div class="modal fade" id="view-job-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content job-modal">
      

    </div>
  </div>
</div>
<!--END MODAL to view the elements-->





<script>
$(document).on('click','#view-job',function(e){
    e.preventDefault();
    var jobId = $(this).attr('job-id');
     $.ajax({
            type: "POST",
            url: "<?=base_url()?>job/getPrintPreview",
            data: { 
                job_id : jobId
            },
            success: function(data) {
                $('.job-modal').html(data);
                $('#view-job-modal').modal();
            }
        });

});
</script>

<script>
    $(document).on('click','#print-preview',function(e){
        e.preventDefault();
        var jobId = $(this).parents('td').attr('job-id');
        $.ajax({
          type: "POST",
          url: "<?=base_url()?>job/getPrintPreview",
          data: { 
            job_id : jobId
            },
            success: function(data) {
                $('#print-modal .modal-body').html(data);
                alert(data);
                $('#print-modal').modal();
            }
        });
        $('#print-modal').modal();
    });


</script>
