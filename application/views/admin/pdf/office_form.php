<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$title = "Curriculum Vitae";
$obj_pdf->SetTitle($title);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
// $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();

?>



<html>

<!-- <body style="margin-top:0px;"> -->
   <!--  <p style="margin-top:0px; float:left; display:inline">

            <img src="<?=base_url()?>images/florid.png" style="height:50px;">
           
            <span style="vertical-align:middle; height:50px; display:inline-block; margin-top:-30px"> Florid Human Resource Pvt.Ltd</span>
           
    </p> -->
    <table>
        <tr>
            <td width="40%"  style="border-right:1px solid black"><img src="<?=base_url()?>images/florid5.png" style="height:55px; width:150px; height:50px;"><br><span>HUMAN RESOURCES PVT.LTD</span></td>
            <td width="2%">&nbsp;</td>
            <td width="58%" align="left"><span style="font-size:14px">New Baneshwor-10,Thapagaun</span><br>
            <span>(sideby Thapagaun Temple,Near Capital F.M)</span><br>
            <span>TOLL FREE NO:1660-011-2222</span><br>
            <span>Ph.No : +977-1-4492111,4494111</span>
             </td>
         </tr>   
    </table>
   
    <h2 align="center">"APPLICATION FOR EMPLOYMENT"</h2>
    <table>
        <tbody>
        <tr>
            <td width="70%">
                <table cellspacing="0" cellpadding="5px" border="1px">
                    <tr>
                        <td width="30%"><strong><i>Ref. No:</i></strong></td>
                        <td class="input" width="70%"></td>

                    </tr>
                    <tr>
                        <td><strong><i>Full Name(IN BLOCK):</i></strong></td>
                        <td class="input"><?=strtoupper(@$candidate['candidate_name'])?></td>
                    </tr>
                    <tr>
                        <td><strong><i>Post Applied For:</i></strong></td>
                        <td class="input"><?=$candidate['job_title']?></td>
                    </tr>
                    <tr>
                        <td><strong><i>Company & Country Name:</i></strong></td>
                        <td class="input"><?=@$candidate['company_name']?> , <?=@$candidate['country_name']?> </td>
                    </tr>

                </table>

            </td>
            <td width="5%">&nbsp;</td>
            <td width="25%">
                <img src="<?=base_url()?>uploads/candidate/profile_image/<?=$file['profile']?>" width="100px" height="100px">
            </td>
        </tr>

            
        </tbody>
       
    </table>
   
   
    <h2 align="center">PASSPORT & APPLICANT DETAILS</h2>
    <table cellspacing="0" cellpadding="5px" border="1px">
        <tbody>
            <tr>
                <td><strong><i>Passport No:</i></strong></td>
                <td class="input"><?=$candidate['passport_no']?></td>
                <td><strong><i>Nationality:</i></strong></td>
                <td class="input" colspan="3"><?=$candidate['nationality']?></td>
            </tr>
            <tr>
                <td><strong><i>Date of Issue:</i></strong></td>
                <td class="input"><?=date('d-M-Y',strtotime(@$candidate['issued_date']))?></td>
                <td><strong><i>Religion:</i></strong></td>
                <td class="input" colspan="3"><?=@$candidate['religion']?></td>
            </tr>
            <tr>
                <td><strong><i>Date of Expire:</i></strong></td>
                <td class="input"><?=date('d-M-Y',strtotime(@$candidate['expire_date']))?></td>
                <td><strong><i>Place of Birth:</i></strong></td>
                <td class="input" colspan="3"><?=@$candidate['birth_place']?></td>
            </tr>
            <tr>
                <td><strong><i>Place of Issue:</i></strong></td>
                <td class="input"><?=@$candidate['issued_from']?></td>
                <td><strong><i>Marital Status:</i></strong></td>
                <td class="input" colspan="3"><?=ucfirst(@$candidate['marital_status'])?></td>
            </tr>
            <tr>
                <td><strong><i>Date of Birth:</i></strong></td>
                <td class="input"><?=date('d-M-Y',strtotime(@$candidate['dob']))?></td>
                <td><strong><i>Height:</i></strong></td>
                <td class="input"><?=@$candidate['height']?> ft</td>
                <td><strong><i>Weight:</i></strong></td>
                <td class="input"><?=@$candidate['weight']?> Kg</td>
            </tr>
            <tr>
                <td><strong><i>Contact No:</i></strong></td>
                <td class="input" colspan="5"><?=@$candidate['mobile']?>(mob) , <?=@$candidate['phone']?> (phone)</td>
            </tr>
            <tr>
                <td><strong><i>Referred By & Contact No:</i></strong></td>
                <td class="input" colspan="5"><?=($candidate['agent_id'] == "0" ? "SELF" : $candidate['agent_name'] . " , ". $candidate['agent_mob']  )?></td>
            </tr>
        </tbody>
    </table>
     

   
    <h2>FAMILY DETAILS & ADDRESS</h2> 
    <table cellspacing="0" cellpadding="5px" border="1px">
        <tbody>
            <tr>
                <td><strong><i>Father's Name:</i></strong></td>
                <td class="input"><?=@$candidate['father_name']?></td>
            </tr>
            <tr>
                <td><strong><i>Mother's Name:</i></strong></td>
                <td class="input"><?=@$candidate['mother_name']?></td>
            </tr>
            <tr>
                <td><strong><i>Permanent Address:</i></strong></td>
                <td class="input"><?=@$candidate['perm_address']?></td>
            </tr>
            <tr>
                <td><strong><i>Temporary Address:</i></strong></td>
                <td class="input"><?=@$candidate['temp_address']?></td>
            </tr>
            <tr>
                <td><strong><i>Home Phone No:</i></strong></td>
                <td class="input"><?=@$candidate['phone']?></td>
            </tr>
        </tbody>
    </table>

    <h2>LANGUAGE KNOWN</h2>
    <P> <?php
                        $languages = unserialize($candidate['language']);
                        if($languages){

                            foreach ($languages as $key=> $value) { 
                                if($value==1){
                                    $a="Good";
                                }elseif($value==2){
                                    $a="better";
                                }elseif($value==3){
                                    $a="excellent";
                                }else{
                                    $a=2;
                                }

                                if($a!=2){
                                    echo $key." (".$a.")"." , ";
                                }
                            }  

                        }
                        else{
                            echo "N/A";
                        }                   
                    ?></P>

    <table cellspacing="0" cellpadding="5px" border="1px">
        <tr>
            <td><h2>WORKING EXPERIENCE</h2></td>
            <td><h2>TRAINING</h2></td>
        </tr>
        <tr>
            <td><P><?=@$candidate['experience']?></P></td>
            <td><P><?=@$candidate['training']?></P></td>
        </tr>
    </table>
    <br>
    <br>
    <table>
        <tr>
            <td align="center">..................................</td>
            <td align="center">..................................</td>
            
        </tr>
        <tr>
            <td align="center">Received By</td>
            <td align="center">Signature of Applicant</td>    
        </tr>
        
    </table>




<!-- </body> -->
</html>

<!--write yout content here-->
  <?php 
    $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output('output.pdf', 'I');
?>