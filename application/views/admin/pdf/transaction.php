<?php

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
//$title = "Project Report on Project '$project_name'";
//$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData('florid.JPG', '60,50', PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
?>
<html>
    <head>
<!--          -->
<style type="text/css">

</style>
    </head>

<body>
           
                <table border="1" cellpadding="4px">
                    <thead>
                        <tr>
                            <th colspan="2" align="center">
                                Income/Expense Statement
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>
                            <table border="1" cellpadding="4px">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Income</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sn=1; 
                                        $totalIncome=0; 
                                        if($income){ 
                                           
                                            foreach($income as $incomeData){ 
                                    ?>
                                    <tr>
                                        <td><?=$sn?></td>
                                        <td><?=$incomeData->category_title?></td>
                                        <td><?php echo number_format($incomeData->amount,2);?></td>
                                    </tr>
                                    <?php 
                                        $sn++; 
                                        $totalIncome+=$incomeData->amount;
                                            } 
                                        }
                                        else{
                                    ?>
                                        <tr><td colspan="3"> No income transaction</td></tr>
                                    <?php
                                        } 
                                    ?>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table border="1" cellpadding="4px">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Expense</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $sn=1; 
                                        $expenceTotal=0;
                                        if($expence){                                              
                                            foreach($expence as $incomeData){ 
                                    ?>
                                    <tr>
                                        <td><?=$sn?></td>
                                        <td><?=$incomeData->category_title?></td>
                                        <td><?php echo number_format($incomeData->amount,2);?></td>
                                    </tr>
                                    <?php 
                                        $sn++; 
                                        $expenceTotal+=$incomeData->amount;
                                            } 
                                        } 
                                        else{
                                    ?>
                                        <tr><td colspan="3"> No expense transaction</td></tr>
                                    <?php
                                        } 
                                    ?>
                                </tbody>

                            </table>
                        </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <span>Total Income:</span>
                                <span><?php echo number_format($totalIncome,2);?></span>
                            </td>
                            <td>
                                <span>Total Expense:</span>
                                <span><?php echo number_format($expenceTotal,2);?></span>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <br>
                <table border="1" cellpadding="4px">
                    <thead>
                        <tr>
                            <th colspan="4">Advance for the day</th>
                        </tr>
                        <tr>
                            <th>S.N</th>
                            <th>Advance Title</th>
                            <th>Candidate</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 1;
                            $total = 0;
                            if($advance){
                                foreach ($advance as $row) {
                        ?>
                            <tr>
                                <td><?=$count?></td>
                                <td><?=$row->category_title?></td>
                                <td><?=$row->candidate_name?></td>
                                <td><?=number_format($row->amount,2)?></td>
                            </tr>
                        <?php
                                $count+=1;
                                $total = $total+$row->amount;
                                }
                            }else{
                        ?>
                            <tr><td colspan="4">No advance record</td></tr>
                        <?php                         
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                Total : <?=number_format($total,2)?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            
</body>
</html>

<?php 
    $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output('output.pdf', 'I');
?>