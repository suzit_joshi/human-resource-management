<?php

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
//$title = "Project Report on Project '$project_name'";
//$obj_pdf->SetTitle($title);
$obj_pdf->SetFooterData('florid.JPG', '60,50', PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
?>
<html>
<style type="text/css">
  td{
    height: 25px;
    /*vertical-align: middle;*/
    line-height: 20px;
    text-align: left;

  }

</style>
<table border="1px" cellpadding="4px">
  <tr>
      <td>Job Title:</td>
      <td><?=@$candidateDetail['candidate_name']?></td>
  </tr>
  <tr>
      <td>Vacancy No:</td>
      <td><?=@$candidateDetail['dob']?></td>
  </tr>
  <tr>
      <td>Company Name:</td>
      <td><?=@$candidateDetail['gender']?></td>
  </tr>
  <tr>
      <td>Salary:</td>
      <td><?=@$candidateDetail['height']?></td>
  </tr>
  <tr>
      <td>Period:</td>
      <td><?=@$candidateDetail['weight']?></td>
  </tr>
  <tr>
      <td>Job Type:</td>
      <td><?=@$candidateDetail['father_name']?></td>
  </tr>
  <tr>
      <td>Requirement:</td>
      <td><?=@$candidateDetail['mother_name']?></td>
  </tr>
  <tr>
      <td>Facility:</td>
      <td><?=@$candidateDetail['email_id']?></td>
  </tr>
  <tr>
      <td>Phone No:</td>
      <td><?=@$candidateDetail['phone']?></td>
  </tr>
  <tr>
      <td>Mobile No:</td>
      <td><?=@$candidateDetail['mobile']?></td>
  </tr>
  <tr>
      <td>Temporary Adress:</td>
      <td><?=@$candidateDetail['temp_address']?></td>
  </tr>
  <tr>
      <td>Permanent Adress:</td>
      <td><?=@$candidateDetail['perm_addres']?></td>
  </tr>
  <tr>
      <td>Birth Place:</td>
      <td><?=@$candidateDetail['birth_place']?></td>
  </tr>
  <tr>
      <td>Marital Status:</td>
      <td><?=@$candidateDetail['marital_status']?></td>
  </tr>
  <tr>
      <td>Nationality:</td>
      <td><?=@$candidateDetail['nationality']?></td>
  </tr>
  <tr>
      <td>Passport No:</td>
      <td><?=@$candidateDetail['passport_no']?></td>
  </tr>
  <tr>
      <td>Issued Date:</td>
      <td><?=@$candidateDetail['issued_date']?></td>
  </tr>
  <tr>
      <td>Expire Date:</td>
      <td><?=@$candidateDetail['expire_date']?></td>
  </tr>
  <tr>
      <td>Issued From:</td>
      <td><?=@$candidateDetail['issued_from']?></td>
  </tr>
  <tr>
      <td>Qualification:</td>
      <td><?=@$candidateDetail['qualification']?></td>
  </tr>
  <tr>
      <td>Experience:</td>
      <td><?=@$candidateDetail['experience']?></td>
  </tr>
  <tr>
      <td>Training:</td>
      <td><?=@$candidateDetail['training']?></td>
  </tr>
  
</table>
</html>
<!-- <html>
<table>
<tr>
 <td>Income Expence Report</td>
</tr>
<tr>
<td>S.No</td><td>Income></td><td>Amount</td><td>S.No</td><td>Expence</td><td>Amount</td>
</tr>
</table>
</html> -->
<!--write yout content here-->
  <?php 
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>