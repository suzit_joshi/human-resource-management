<?php

tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$title = "Curriculum Vitae";
$obj_pdf->SetTitle($title);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
?>
<html>
<style type="text/css">
    td {
        height: 25px;
        /*vertical-align: middle;*/
        
        line-height: 20px;
        text-align: left;
    }
</style>
    <table cellpadding="2px">
        <thead>
            <tr>
                <th colspan="2" align="center"><h1>Curriculum Vitae</h1></th>
            </tr>
            
        </thead>

        <tbody>
            <tr>
                <td colspan="2"><h2>Personal Details</h2></td>
            </tr>

            <tr>
                <td>Name:</td>
                <td>
                    <?=@$candidateDetail[ 'candidate_name']?>
                </td>
            </tr>
            <tr>
                <td>Father Name:</td>
                <td>
                    <?=@$candidateDetail[ 'father_name']?>
                </td>
            </tr>
            <tr>
                <td>Mother Name:</td>
                <td>
                    <?=@$candidateDetail[ 'mother_name']?>
                </td>
            </tr>

            <tr>
                <td>Date of Birth:</td>
                <td>
                    <?=date('d-M-Y',strtotime(@$candidateDetail[ 'dob']))?>
                </td>
            </tr>
            <tr>
                <td>Permanent Adress:</td>
                <td>
                    <?=@$candidateDetail[ 'perm_address']?>
                </td>
            </tr>
            <tr>
                <td>Temporary Adress:</td>
                <td>
                    <?=@$candidateDetail[ 'temp_address']?>
                </td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td>
                    <?=@$candidateDetail[ 'gender']?>
                </td>
            </tr>
            <tr>
                <td>Nationality:</td>
                <td>
                    <?=@$candidateDetail[ 'nationality']?>
                </td>
            </tr>
            <tr>
                <td>Religion:</td>
                <td>
                    <?=@$candidateDetail[ 'religion']?>
                </td>
            </tr>
            <tr>
                <td>Language Known:</td>
                            <td>
                    <?php
                        $languages = unserialize($candidateDetail['language']);
                        if($languages){
                            foreach ($languages as $language_key => $value) { 
                                if($value != ""){
                                    echo ucfirst($language_key). " , ";
                                }  
                            }
                        }
                        else{
                            echo "N/A";
                        }                   
                    ?>

               </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>
                    <?=@$candidateDetail[ 'email_id']?>
                </td>
            </tr>
            <tr>
                <td>Phone No:</td>
                <td>
                    <?=@$candidateDetail[ 'phone']?>
                </td>
            </tr>
            <tr>
                <td>Mobile No:</td>
                <td>
                    <?=@$candidateDetail[ 'mobile']?>
                </td>
            </tr>
            <tr>
                <td>Birth Place:</td>
                <td>
                    <?=@$candidateDetail[ 'birth_place']?>
                </td>
            </tr>
            <tr>
                <td>Marital Status:</td>
                <td>
                    <?=@$candidateDetail[ 'marital_status']?>
                </td>
            </tr>
        </tbody>

    </table>
    <table>
        <thead>
            <tr>
                <th colspan="2">
                    <h2>Passport Details</h2>
                </th>
            </tr>
        </thead>
        <tbody>    
            <tr>
                <td>Passport No:</td>
                <td>
                    <?=@$candidateDetail[ 'passport_no']?>
                </td>
            </tr>
            <tr>
                <td>Issued Date:</td>
                <td>
                    <?=@$candidateDetail[ 'issued_date']?>
                </td>
            </tr>
            <tr>
                <td>Expire Date:</td>
                <td>
                    <?=@$candidateDetail[ 'expire_date']?>
                </td>
            </tr>
            <tr>
                <td>Issued From:</td>
                <td>
                    <?=@$candidateDetail[ 'issued_from']?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php 
    if($candidateDetail[ 'qualification']!=""){
        ?>
    <h2>Recent Academic Qualification</h2>
    <p><?=@$candidateDetail[ 'qualification']?></p>
        <?php } ?>   

    <?php
    if($candidateDetail[ 'training']!=""){
?>
    <h2>Training</h2>
    <p><?=@$candidateDetail[ 'training']?></p>
<?php    } ?>    

<?php
    if($candidateDetail[ 'experience']!=""){
?>
    <h2>Work Experience</h2>
    <p><?=@$candidateDetail[ 'experience']?></p>
    <?php } ?>
</html>



  <?php 
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>