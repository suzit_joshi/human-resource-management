<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$title = "Curriculum Vitae";
$obj_pdf->SetTitle($title);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
// $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();

?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/fonts/ChronicleDisplay-Semi/styles.css">
<style>
    .header{
        font-size: 30px;
        font-weight: bold;
        font-family: Times New Roman;
        text-align: center;
    }
    .body{
        font-size: 25px;
        font-weight: bold;
        text-align: left;
    }
</style>


<html>
<div border="1px solid black">
        <div class="header">
            Application for the post<br>Of<br><?=strtoupper($candidate->job_title)?>
        </div>
        <div class="body">
            For,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$candidate->company_name?>,<?=$candidate->country_name?>
        </div>
        <br>
        <br>
       &nbsp;&nbsp; <span style="font-size:15px; font-weight:bold">Candidate Name:</span>&nbsp;&nbsp;<span style="font-size:15px;"><?=strtoupper($candidate->candidate_name)?></span>
        <br>
       &nbsp;&nbsp; <span style="font-size:15px; font-weight:bold">Passport No:</span>&nbsp;&nbsp;<span style="font-size:15px;"><?=$candidate->passport_no?></span>
        <br>
        <br><br>

        <div align="center" style="margin-left:2px">
           &nbsp;&nbsp; <img src="<?=base_url()?>images/img.jpg" width="auto">
        </div>
  </div>      
</html>

<!--write yout content here-->
<?php 
$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>