<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Demand Letter - Florid Human Resources Private Limited";
$obj_pdf->SetTitle($title);
//$obj_pdf->SetHeaderData('florid.JPG', '100,50', PDF_HEADER_STRING);
//$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
define(PDF_MARGIN_TOP,'0px');
//$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->setPrintHeader(false);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
?>
<html>
    <body>
        <p style="margin-top:0px;" align="center">
            <img src="<?=base_url()?>images/florid.JPG" style="height:100px; width:auto;">
        </p>
        <h2>Florid Human Resources Private Limited</h2>
        <P>
            New Baneshwor - 10<br> 
            Thapagaun, Kathmandu,<br> 
            Nepal<br>
            GOVT. Lic No: 697/063/064
        </P>

        <table border="1px" cellpadding="5px">
            <thead>
                <tr>
                    <th>Position</th>
                    <th>Required No</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <?=$result[ 'job_title']?></td>
                    <td> <?=$result[ 'vacancy_no']?></td>
                    <td> <?=$result[ 'salary']?></td>
                </tr>
            </tbody>
        </table>

        <br><br><br>
        <h3>Terms & Conditions</h3>
        <table cellpadding="5px" width="100%">
            <tbody>
                <tr>
                    <td width="5%"> 1.</td>
                    <td> Period of Employment</td>
                    <td><?=$result[ 'period']?></td>
                </tr>
                <tr>
                    <td width="5%"> 2.</td>
                    <td> Place of Employment</td>
                    <td><?=$result[ 'period']?></td>
                </tr>
                <tr>
                    <td width="5%"> 3.</td>
                    <td> Place of Employment</td>
                    <td><?=$result[ 'address']?> , <?=$result['country_name']?></td>
                </tr>
                <tr>
                    <td width="5%"> 4.</td>
                    <td> Minimum Requirement</td>
                    <td><?=$result[ 'requirement']?></td>
                </tr>
                <tr>
                    <td width="5%"> 5.</td>
                    <td> Facilities</td>
                    <td> <?=$result[ 'facility']?></td>
                </tr>
                 <tr>
                    <td width="5%"> 6.</td>
                    <td> Other Terms and Condition</td>
                    <td>As per UAE Labour Law</td>
                </tr>
            </tbody>
        </table>
        <p>
        Thanking you, <br>
        For and on behalf of <?=$result['company_name']?>
        </p>

    </body>

</html>
<!--write yout content here-->
  <?php 
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>