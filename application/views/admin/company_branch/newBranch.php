           
<aside class="right-side">
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        Branch
                        <small>Manage your Branches</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Branch</li>
                    </ol>
                </section>
                <?php 
					$this->load->view('admin/include/notification');
					?>
                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							
							<div class='box'>
								<div class="box-header">
                                	<h3 class="box-title">Add  Branch</h3>
                                </div>
                                <div class='box-body pad'>
                                    <?php echo form_open('branch/save');?>
									<form action="<?=base_url()?>company/save" id="company-form">
									<?php if(isset($branchData)){ 
										?>
									<input type="hidden" value="<?=$branchData['id']?>" name="company_id" />
										<?php } ?>
									<div class="form-group">
										<label for="branch_name">Branch Name</label>
										<input type="text" class="form-control" id="branch_name" 
										value="<?=(isset($branchData))?$branchData['branch_name']:set_value('branchName')?>" name="branchName" placeholder="Branch Name" 
										required="required">
									</div>

									<div class="form-group">
										<label for="manager_name">Branch Manager Name</label>
										<input type="text" class="form-control" id="manager_name" 
										value="<?=(isset($branchData))?$branchData['manager_name']:set_value('managerName')?>" name="managerName" placeholder="Manager Name"
										required="required">
									</div>

									<div class="form-group">
										<label for="email_id">Email</label>
										<input type="email" class="form-control" id="email_id" 
										value="<?=(isset($branchData))?$branchData['email']:set_value('email')?>" name="email" placeholder="Email"
										required="required">
									</div>
									
									<div class="form-group">
										<label for="address">Address</label>
										<input type="text" class="form-control" id="address" 
										value="<?=(isset($branchData))?$branchData['address']:set_value('address')?>" name="address" placeholder="Address"
										required="required">
									</div>

									<div class="form-group">
										<label for="phone">Phone</label>
										<input type="text" class="form-control" id="phone" 
										value="<?=(isset($branchData))?$branchData['phone']:set_value('phone')?>" name="phone" placeholder="phone"
										required="required">
									</div>

									
									<div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                    </div>
								   <?php echo form_close(); ?>
                                </div>
                            </div>
							
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->

