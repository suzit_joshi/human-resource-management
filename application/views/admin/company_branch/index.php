
<aside class="right-side">
<!-- <button id="convert" class="btn btn-default">convert</button> -->
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Branch
                        <small>Manage your Branches</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Branch</li>
                    </ol>
                </section>
				
				<?php 
					$this->load->view('admin/include/notification');
				?>
				<!-- Main content-->
				<section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
							<div class="box">
                                <div class="box-footer">
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>branch/createBranch')">
                                    Add New</button>
                                </div>
								<div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
												<th>Name</th>
                                                <th>Address</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        echo "<th>Updated On</th>";
                                                    }
                                                ?>
                                                <th width="5%">Option</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										if($branch){
										$counter = 1;
										foreach($branch as $record){
											$id = $record->id;
											
										?>
                                            <tr>
                                                <td><?=$counter?></td>
												<td><?=$record->branch_name?></td>
                                                <td><?=$record->address?></td>
                                                <td><?=$record->email?></td>
                                                <td><?=$record->phone?></td>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role == "general admin" || $role == "system admin"){
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }
                                                ?>                                            
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                            <a href="<?=base_url()?>branch/editBranch/<?=$record->id?>">Edit</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                            <a href="<?=base_url()?>branch/deactivate/<?=$record->id?>" 
                                                                   onclick="return confirmDeactivate();">Deactivate</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>

                                              
                                            </tr>
                                         <?php
										 	$counter+=1;
											}
										}
										?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                                
							</div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->

<script>
$(document).ready(function() {
 
 $('#convert').click(function(){
 
 //Get all the values
 var amount = 1;
 var from = 'USD';
 var to = 'EURO';
 
 //Make data string
 var dataString = "amount=" + amount + "&from=" + from + "&to=" + to;
 
 $.ajax({
 type: "POST",
 url: '<?=base_url()?>branch/currencyConverter',
 data: dataString,
 
 success: function(data){
    alert(data);
 //Show results div
 // $('#results').show();
 
 //Put received response into result div
 // $('#results').html(data);
 }
 
 });
 });
});
</script>