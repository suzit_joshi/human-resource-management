<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Agent
			<small>Manage your agents</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Agent</li>
		</ol>
	</section>

	<?php 
	$this->load->view('admin/include/notification');
	?>
                <!-- Main content-->
	<section class="content">
		<div class='row'>
			<div class='col-md-12'>
				<?php
				if(isset($result)){
					foreach($result as $record){
						?>
						<div class='box'>
							<div class="box-header">
								<h3 class="box-title">Edit Agent</h3>
							</div>
							<div class='box-body pad'>
								<?php echo form_open_multipart('agent/editSave'); ?>
								<input type="hidden" value="<?=base64_encode($record->id)?>" name="id">

								<div class="form-group">
									<label for="agent_name">Agent Name *</label>
									<input type="text" class="form-control" id="agent_name" 
									value="<?=$record->agent_name?>" name="agent_name" 
									required="required">
								</div>

								<div class="form-group">
									<label for="email_id">Email Address *</label>
									<input type="email" class="form-control" id="email_id" 
									value="<?=$record->email?>" name="email_id" 
									required="required">
								</div>

								<div class="form-group">
									<label for="mobile">Mobile Number *</label>
									<input type="number" class="form-control" id="mobile" 
									value="<?=$record->mobile?>" name="mobile_no" 
									required="required">
								</div>

								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="number" class="form-control" id="phone" 
									value="<?=$record->phone?>" name="phone" 
									required="required">
								</div>

								<div class="form-group">
									<label for="perm_address">Permanent Address *</label>
									<input type="text" class="form-control" id="perm_address" 
									value="<?=$record->perm_address?>" name="perm_address" 
									required="required">
								</div>

								<div class="form-group">
									<label for="temp_address">Temporary Address</label>
									<input type="text" class="form-control" id="temp_address" 
									value="<?=$record->temp_address?>" name="temp_address" 
									required="required">
								</div>

								
								<div class="form-group">
									<label for="dob">Date Of Birth *</label>
									<input type="text" class="form-control" id="datepicker" 
									value="<?=date("d-M-Y",strtotime($record->dob))?>" name="dob" 
									required="required">
								</div>	

								<div class="form-group">
									<img src="<?=base_url()?>uploads/agent/<?=$record->image?>" style="height:100px; width:auto;">
									<br />
									<label for="image-change">Upload Profile Picture *</label>
									<input type="file" name="image"/>
									<input type="hidden" value="<?=$record->image?>" name="old_img">
								</div>								
								
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Save Changes</button>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
						<?php
					}
				}else{
					?>
					<div class='box'>
						<div class="box-header">
							<h3 class="box-title" style="display:block;">New Advertisement</h3><br />
							<h7 class="pull-right"><i>Note: Field marked with * are compulsory.</i></h7>
						</div>
						<div class='box-body pad'>
							<?php echo form_open_multipart('agent/save'); ?>
							
							<div class="form-group">
								<label for="ad_name">Advertisement For*</label>
								<select name="ad_name">
									<?php
										foreach ($jobs as $job) {
									?>
										<option value="<?=$job->id?>"><?=$job->job_title?></option>
									<?php
										}
									?>

								</select>
							</div>

							<div class="form-group">
								<label for="email_id">Email Address *</label>
								<input type="email" class="form-control" id="email_id" 
								placeholder="Example : abc@xyz.com" name="email_id" 
								required="required">
							</div>

							<div class="form-group">
								<label for="mobile">Mobile Number *</label>
								<input type="number" class="form-control" id="mobile" 
								placeholder="Enter Mobile Number" name="mobile" 
								required="required">
							</div>

							<div class="form-group">
								<label for="phone">Phone Number</label>
								<input type="number" class="form-control" id="phone" 
								placeholder="Enter phone Number" name="phone" 
								required="required">
							</div>

							<div class="form-group">
								<label for="perm_address">Permanent Address *</label>
								<input type="text" class="form-control" id="perm_address" 
								placeholder="Enter Permanent Address" name="perm_address" 
								required="required">
							</div>

							<div class="form-group">
								<label for="temp_address">Temporary Address</label>
								<input type="text" class="form-control" id="temp_address" 
								placeholder="Enter Temporary Address" name="temp_address" 
								required="required">
							</div>

							
							<div class="form-group">
								<label for="dob">Date Of Birth *</label>
								<input type="text" class="form-control" id="datepicker" 
								value="<?=date('d-M-Y')?>" name="dob" 
								required="required">
							</div>

							<div class="form-group">
								<label for="image-change">Upload Profile Picture *</label>
								<input type="file" name="image"/>
							</div>

							<button class="btn btn-primary" type="submit" >New Agent</button>
							<?php echo form_close(); ?>
						</div>   
					</div>
					<?php
				}
				?>
			</div><!-- /.col-->
		</div><!-- ./row -->
	</section><!-- /.content -->
</aside>
<!--right_panel-->

<script>
	$(function(){
		$( "#datepicker" ).datepicker({
			yearRange: "-50:+0",
			dateFormat: "dd-M-yy",
			showAnim : "blind",
			changeMonth: true,
			changeYear: true
		});
	});
</script>



