<form id="agentListForm">
<div class="box-body">
	<div class="row">
		<table class="table table-striped">
		<th width="10%"><input type="checkbox" id="allSelect">&nbsp;Select all</th>
		<th>S.No</th>
		<th>Name</th>
		<th>Phone</th>
		<?php
		if($agentList)
		{
			$sn=1;
			foreach($agentList as $listData){
			?>
		<tr><td><input type="checkbox" name="agentCheck[]" class="agentCheckBox" value="<?=$listData->id?>"></td><td><?=$sn?></td><td><?=$listData->agent_name?></td><td><?=$listData->mobile?></td></tr>
		<?php $sn++;
		} }?>
		</table>
	<div class="box-footer">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#agentModel">Forward for Approval</button>
                                    </div> 
	</div>

</div>


<!-- for model -->
<div class="modal fade" id="agentModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send Message</h4>
      </div>
      <div class="modal-body agentMsgBody">
       	<div class="form-group">
       		<label>Message</label>
       		<textarea name="msgForApproval" placeholder="Message" class="form-control"></textarea>
       	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button"  class="btn btn-primary agentListSubmit">Send for Approval</button>
      </div>
    </div>
  </div>
</div>

</form>

<!-- model end -->
<script>
$(function(){
$(".agentListSubmit").click(function(){
    // e.preventDefault();
    // alert('dead');

 var formData=new FormData($("#agentListForm")[0]);
              $.ajax({
                url : "<?php echo base_url()?>agent/agentApproval",
                type : 'POST',
                data : formData,
                processData: false,
                contentType: false,
                success:function(response)
                {
                 
                  $(".agentMsgBody").prepend('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Successfuly Sent for approval.</div>')
                
                  	$(".agentCheckBox").each(function(i){
						 if($('.agentCheckBox').eq(i).is(':checked')) { 
              		  			$('.agentCheckBox').eq(i).parents('tr').hide();
           				 } 		
					});
                }
            });
});

// $("#allSelect").click(function(){
// 	$(".agentCheckBox").each(function(i){
// 		// $(".agentCheckBox").eq(i).attr("checked" ,"checked");
// 		$(".agentCheckBox").eq(i).checked=true;
// 	});
// });

});
$(document).ready(function() {
    $('#allSelect').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.agentCheckBox').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.agentCheckBox').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>