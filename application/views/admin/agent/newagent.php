<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Agent
			<small>Manage your agents</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Agent</li>
		</ol>
	</section>

	<?php 
	$this->load->view('admin/include/notification');
	?>
                <!-- Main content-->
	<section class="content">
		<div class='row'>
			<div class='col-md-12'>
				<?php
				if(isset($result)){
					foreach($result as $record){
						?>
						<div class='box'>
							<div class="box-header">
								<h3 class="box-title">Edit Agent</h3>
							</div>
							<div class='box-body pad'>
								<?php echo form_open_multipart('agent/editSave'); ?>
								<input type="hidden" value="<?=base64_encode($record->id)?>" name="id">

								<div class="form-group">
									<label for="agent_name">Agent Name *</label>
									<input type="text" class="form-control" id="agent_name" 
									value="<?=$record->agent_name?>" name="agent_name" 
									required="required">
								</div>

								<div class="form-group">
									<label for="email_id">Email Address *</label>
									<input type="email" class="form-control" id="email_id" 
									value="<?=$record->email?>" name="email_id" 
									required="required">
								</div>

								<div class="form-group">
									<label for="mobile">Mobile Number *</label>
									<input type="text" class="form-control" id="mobile" 
									value="<?=$record->mobile?>" name="mobile_no" 
									required="required">
								</div>

								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="text" class="form-control" id="phone" 
									value="<?=$record->phone?>" name="phone">
								</div>

								<div class="form-group">
									<label for="perm_address">Address </label>
									<input type="text" class="form-control" id="address" 
									value="<?=$record->address?>" name="address" 
									required="required">
								</div>

								<div class="form-group">
									<img src="<?=base_url()?>uploads/agent/<?=$record->image?>" style="height:100px; width:auto;">
									<br />
									<label for="image-change">Upload Profile Picture *</label>
									<input type="file" name="image"/>
									<input type="hidden" value="<?=$record->image?>" name="old_img">
								</div>								
								
								<div class="form-group">
									<label for="category">Remarks</label>
									<textarea class="textarea text-area-style" name="remarks"><?=$record->remarks?></textarea>
								</div>

								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Save Changes</button>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
						<?php
					}
				}else{
					?>
					<div class='box'>
						<div class="box-header">
							<h3 class="box-title" style="display:block;">New Agent</h3><br />
							<h7 class="pull-right"><i>Note: Field marked with * are compulsory.</i></h7>
						</div>
						<div class='box-body pad'>
							<?php echo form_open_multipart('agent/save'); ?>
							
							<div class="form-group">
								<label for="agent_name">Agent Name *</label>
								<input type="text" class="form-control" id="agent_name" 
								placeholder="Enter Agent name" name="agent_name"  value="<?php echo set_value('agent_name'); ?>"
								required="required">
							</div>

							<div class="form-group">
								<label for="email_id">Email Address *</label>
								<input type="email" class="form-control" id="email_id" 
								placeholder="Example : abc@xyz.com" name="email_id" value="<?php echo set_value('email_id'); ?>"
								required="required">
							</div>

							<div class="form-group">
								<label for="mobile">Mobile Number *</label>
								<input type="text" class="form-control" id="mobile" 
								placeholder="Enter Mobile Number" name="mobile_no" value="<?php echo set_value('mobile_no'); ?>"
								required="required">
							</div>

							<div class="form-group">
								<label for="phone">Phone Number</label>
								<input type="text" class="form-control" id="phone" 
								placeholder="Enter phone Number" name="phone"value="<?php echo set_value('phone'); ?>" >
							</div>

							<div class="form-group">
								<label for="perm_address">Address</label>
								<input type="text" class="form-control" id="address" 
								placeholder="Enter Address" name="address" value="<?php echo set_value('address'); ?>">
							</div>
							
							<div class="form-group">
								<label for="image-change">Upload Profile Picture *</label>
								<input type="file" name="image"/>
							</div>

							<div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="remarks"><?php echo set_value('remarks'); ?></textarea>
                            </div>

							<button class="btn btn-primary" type="submit" >New Agent</button>
							<?php echo form_close(); ?>
						</div>   
					</div>
					<?php
				}
				?>
			</div><!-- /.col-->
		</div><!-- ./row -->
	</section><!-- /.content -->
</aside>
<!--right_panel-->

<script>
/*	$('#image-change').on('change', function(){ 
		$("#multiformImgUpld").submit();
	});

	$("#multiformImgUpld").submit(function(e)
	{
		var formObj = $(this);
		var formURL = formObj.attr("action");
	
		if(window.FormData !== undefined)  // for HTML5 browsers
		{

        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $('#img-display').html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $("#message_dealer").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
            }           
        });
        e.preventDefault();
        //e.unbind();
    }

    else  //for olden browsers
    {
        //generate a random id
        var  iframeId = 'unique' + (new Date().getTime());
        //create an empty iframe
        var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
        //hide it
        iframe.hide();
        //set form target to iframe
        formObj.attr('target',iframeId);
        //Add iframe to body
        iframe.appendTo('body');
        iframe.load(function(e)
        {
            var doc = getDoc(iframe[0]);
            var docRoot = doc.body ? doc.body : doc.documentElement;
            var data = docRoot.innerHTML;
            $("#multi-msg").html('<pre><code>'+data+'</code></pre>');
        });
    }
});*/

/*	$('#btn-agent-frm').on('click',function(){

		var agent = $('#agent_name').val();
		var email = $('#email_id').val();
		var mobile = $('#mobile').val();
		var phone = $('#phone').val();
		var perm = $('#perm_address').val();
		var temp = $('#temp_address').val();
		var dob = $('#datepicker').val();
		var image = $('#img-cropped').attr('title');

		if(agent== "" || email == "" || mobile == "" || perm == "" || image == ""){
			alert("Please fill in the form correctly!");
		}else{
			$.ajax({
                type: "POST",
                url: "<?=base_url()?>agent/save",
                data: { 
                        agent : agent,
                        email_id : email,
                        mobile_no : mobile,
                        phone_no : phone,
                        perm_address : perm,
                        temp_address : temp,
                        date_birth : dob,
                        img_name : image
                    },
                success: function(data) {
 						var msg = data;
 						if(msg == "success"){
 							window.location.href = '<?=base_url()?>agent/index/success';
 						}
                    }
                });
		}


	});*/

</script>


