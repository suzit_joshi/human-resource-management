<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Agent
                        <small>Manage your agents</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Agents</li>
                    </ol>
                </section>
                
                <?php 
                    $this->load->view('admin/include/notification');
                ?>
                <!-- Main content-->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="box">
                            <?php
                                 $role=strtolower($this->session->userdata('role'));
                                 if($role=="admin" || $role == "general admin" || $role == "system admin"){
                                    ?>
                                 <div class="box-footer">
                                 
                                    <button class="btn btn-primary" 
                                    onClick="javascript:location.replace('<?=base_url()?>agent/newAgent')">
                                    Add New</button>
                                   
                                     <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Other Option <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li> <a href="#" onclick="javascript:showAgentList()">Transfer for Approval</a> 
                                          </li>
                                          
                                          </ul>
                                        </div>
                                </div>
                                <?php } ?>
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Name</th>
                                                <?php
                                                    $role = strtolower($this->session->userdata('role'));
                                                    if($role != "general admin" && $role != "system admin"){
                                                        $flag = 1;
                                                        echo "<th>Email</th>";
                                                    }
                                                ?>                                                
                                                <th>Contact Details</th>
                                                <th>Address</th>
                                                <th>Image</th>
                                                <th>Status</th>
                                                <th>Approved By</th>
                                                <?php
                                                    if($role == "general admin" || $role == "system admin"){
                                                        echo "<th>Last Updated</th>";
                                                    }
                                                ?>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $counter = 1;
                                        foreach($result_agent as $record){
                                            $id = $record->id;
                                            $encrypted_id = base64_encode($id);
                                        ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                <td><a href="#" id="view-element-content" content-id="<?=$id?>" tbl="tbl_agent"><?=$record->agent_name?></a></td>
                                                <?php
                                                    if(isset($flag) && $flag == 1){
                                                        echo "<td>$record->email</td>";
                                                    }
                                                ?>                                                
                                                <td><?php echo $record->phone."(phone)<br>"
                                                        . $record->mobile."(mob.)";
                                                    ?></td>
                                                <td><?php echo $record->address ?></td>
                                                <td><img src='<?php echo base_url()."uploads/agent/".$record->image;?>' class="img-list-size"></td>
                                                 <td><?php if($record->status==0){ echo "New"; } elseif($record->status==1){ echo "sent for approval"; } else{ echo "Approved";}?></td>
                                                 <td><?=$record->approved_by?></td>   
                                                 <?php
                                                    if($role == "general admin" || $role == "system admin"){
                                                        $time = date('d-M-Y h:i:s',strtotime($record->updated_on));
                                                        echo "<td>".$record->updated_by." on <br >".$time."</td>";
                                                    }
                                                ?> 
                                                <td align="center">
                                                    <div class="btn-group">
                                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                    <?php
                                                     if($role!="mol admin"){
                                                        ?>
                                                        <li> <a href="#" id="view-element-content" content-id="<?=$id?>" tbl="tbl_agent">View </a> 
                                                        </li>
                                                        <?php } ?>
                                                        <?php
                                                         if($role=="admin"){
                                                     ?>
                                                        <li class="divider"></li>
                                                        <li>  <a href="<?=base_url()?>agent/getInformation/<?=$encrypted_id?>">Edit</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="<?=base_url()?>agent/deactivate/<?=$encrypted_id?>" 
                                                            onclick="return confirmDelete();">Deactivate</a></li>
                                                            <?php }?>
                                                    </ul>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                         <?php
                                            $counter+=1;
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                               
                            </div>
                            
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section>
            </aside>
            <!--right_panel-->
<script>
function showAgentList(){
    var url='<?=base_url()?>show-agent-list';
   $.get(url).done(function(d){
  $(".box-body").empty();
  $(".box-body").html(d);
   });
}
</script>

