<style>
.actions{
    display: block;
    font-size: 10px;
    opacity: 0;
}

tr:hover .actions{
    opacity: 1;
}
</style>
   <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bank
            <small>Manage your bank accounts</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Bank</li>
        </ol>
    </section>

    <?php 
    $this->load->view('admin/include/notification');
    ?>
    <!-- Main content-->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
             <div class="box">
             <div class="box-header">
                 <?php
                 $role = strtolower($this->session->userdata('role'));
                 if( $role == "finance" || $role == "general admin" || $role == "system admin"){
                  ?>
                <div class="box-title">
                <button class="btn btn-primary" data-toggle="modal" data-target="#new-bank">
                Add New</button>
                </div>
                <?php
                  }
                  ?>
             </div>
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="2%">S.No</th>
                                <th>Bank Name</th>
                                <th>Current Balance</th>
                                <th width="5%">Option</th>
                            </tr>
                        </thead>
                        <tbody id="bank-record">
                          <?php
                          $counter = 1;
                          foreach($result_bank as $record){
                             $id = $record->id;
                             $encrypted_id = base64_encode($id);
                             ?>
                             <tr>
                                <td><?=$counter?></td>
                                <td class="bank-name" data-id="<?=$id?>" balance="<?=$record->balance?>"><span class="name"><?=$record->bank_name?></span></td>
                                <td><?=$record->balance?></td>
                                <td align="center">
                                    <?php
                                       $role = strtolower($this->session->userdata('role'));
                                       if( $role == "finance" || $role == "general admin" || $role == "system admin"){
                                        ?>
                                      <div class="btn-group">
                                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu pull-right" role="menu">
                                        <li>  <a href="#" class="edit-bank">Edit</a></li>
                                      </ul>
                                      </div>
                                      <?php
                                    }else{
                                      echo "No option available";
                                    }
                                    ?>
                                  
                                </td>
                            </tr>
                            <?php
                            $counter+=1;
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div>

    </div><!-- /.col-->
</div><!-- ./row -->
</section>
</aside>

<!-- Modal -->
<div class="modal fade" id="new-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Bank</h4>
      </div>
      <form class="form-horizontal" role="form">
      <div class="modal-body">        
        <div class="form-group">
            <label class="col-sm-4">Bank Name</label>
            <div class="col-sm-8">
            <input type="text" class="form-control" id="bank">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4">Opening Balance</label>
            <div class="col-sm-8">
            <input type="number" class="form-control" id="balance">
            </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submit-bank">Save changes</button>
      </div>
      </form><!--END FORM-->

    </div>
  </div>
</div>

<!--MODAL TO EDIT-->
<div class="modal fade" id="edit-bank-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Bank</h4>
      </div>
      <form class="form-horizontal" role="form">
      <input type="hidden" id="bank-id">
      <div class="modal-body">        
        <div class="form-group">
            <label class="col-sm-4">Bank Name</label>
            <div class="col-sm-8">
            <input type="text" class="form-control" id="edit-bank-name">
            </div>
        </div>

         <div class="form-group">
            <label class="col-sm-4">Edit Opening Balance</label>
            <div class="col-sm-8">
            <input type="number" class="form-control" id="edit-balance" disabled="disabled">
            </div>
        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-submit-bank">Save changes</button>
      </div>
      </form><!--END FORM-->

    </div>
  </div>
</div>


<script>
/*AJAX REQUEST TO INSERT NEW BANK RECORD*/
 $('#submit-bank').click(function(e){
        e.preventDefault();
        var bank = $('#bank').val();
        var balance = $('#balance').val();
        if(bank == "" || balance == ""){
            alert("Both the fields are mandatory. Please fill up the form properly");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>bank/newBank",
                data: { 
                        bank:bank,
                        balance:balance
                    },
                success: function(data) {
                        $('#bank-record').html(data);
                        $('#new-bank').modal('hide');
                    }
                });
        }
    }); 
/*END AJAX REQUEST TO INSERT NEW RECORD*/
var bankId;
/*To generate an EDIT MODAL*/
$(document).on('click','.edit-bank',function(e){
    e.preventDefault();

    bankId = $(this).parents('tr').find('.bank-name').attr('data-id');
    bName = $(this).parents('tr').find('.bank-name .name').text();
    bankBal = $(this).parents('tr').find('.bank-name').attr('balance');
    
    $('#bank-id').val(bankId);
    $('#edit-bank-name').val(bName);
    $('#edit-balance').val(bankBal);
    $('#edit-bank-modal').modal();
});

/*AJAX REQUEST TO EDIT BANK RECORD*/
 $(document).on('click','#edit-submit-bank',function(e){
        e.preventDefault();
        var bank = $('#edit-bank-name').val();
        var newBal = $('#edit-balance').val();
        if(bank == "" || newBal == ""){
            alert("Please fill up the form properly");
        }
        else{
            $.ajax({
                type: "POST",
                url: "<?=base_url()?>bank/editBank",
                data: { 
                        bank:bank,
                        bank_id : bankId,
                        balance : newBal
                    },
                success: function(data) {
                        $('#bank-record').html(data);
                        $('#edit-bank-modal').modal('hide');
                    }
                });
        }
    }); 
/*END AJAX REQUEST TO INSERT NEW RECORD*/

</script>
