 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
   <!---left_panel-->
   <?php $this->load->view('admin/include/left');?>
   <!--END LEFT PANEL-->
<style>
.actions{
    display: block;
    font-size: 10px;
    opacity: 0;
}

tr:hover .actions{
    opacity: 1;
}

#success-alert, #warning-alert{
  display: none;
}
</style>
<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
        Money Transfer
        <small>Transfer to Cash or Bank</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Money Transfer</li>
      </ol>
  </section>


    <?php 
    $this->load->view('admin/include/notification');
    ?>
    <!-- Main content-->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
           <div class="box-header">
            <div class="box-title">
              <button class="btn btn-primary" onclick= "window.location='<?=base_url()?>transfer/newTransfer'">
                Add New</button>
              </div>
            </div>
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="2%">S.No</th>
                    <th>Transfer Date</th>
                    <th>Transfer Detail</th>
                    <th>Bank</th>
                    <th>Amount</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="bank-record">
                  <?php
                  $counter = 1;
                  foreach($result_transfer as $record){
                   $id = $record->transfer_id;
                   $encrypted_id = base64_encode($id);
                   ?>
                   <tr>
                    <td><?=$counter?></td>
                    <td><?=$record->transfer_date?></td>
                    <td><?=$record->transfer_type?></td>
                    <td><?=$record->bank_name?></td>
                    <td><?=$record->amount?></td>
                    <td><?=$record->description?></td>
                  </tr>
                  <?php
                  $counter+=1;
                }
                ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->

        </div>

      </div>
    </div>
  </div><!-- /.col-->
</div><!-- ./row -->
</section>

</aside>
<!--right_panel-->
</div>

<!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
<!--footer-->   
</body>
</html>