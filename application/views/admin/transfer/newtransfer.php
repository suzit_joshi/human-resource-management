 <?php $this->load->view('admin/include/header'); ?>
 <div class="wrapper row-offcanvas row-offcanvas-left">
 <!---left_panel-->
 	<?php $this->load->view('admin/include/left'); ?>
<!--END LEFT PANEL-->
<style>
	#undeposited-section{
		display: none;
	}

</style>          
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                      <h1>
                        Money Transfer
                        <small>Transfer to Cash or Bank</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Money Transfer</li>
                    </ol>
                 </section>

                <!-- Main content -->
                <section class="content">
                    <div class='row'>
                        <div class='col-md-12'>

                            <div class='box'>
                            	<div class="box-header">
                                	<h3 class="box-title">New Transfer</h3>
                                </div>
                                <div class='box-body pad'>
                                	<form action="<?=base_url()?>transfer/save" method="post" id="transfer-frm">

                                        <div class="row">
                                    		<div class="form-group col-md-4">
                                    			<label style="display:block;">Transfer Type</label>
                                    			<select class="form-control" id="transfer-type" name="transfer_type">
                                                    <option value="1">Cash to Bank A/C</option>
                                                    <option value="2">Bank A/C to Cash</option>
                                                    <option value="3">Undeposited Cheques to Bank A/C</option>
                                                </select>
                                    		</div>

                                            <div class="form-group col-md-4">
                                                <label>Transfer Date</label>
                                                <input type="text" class="form-control" id="datepicker" 
                                                value="<?=date('d-M-Y')?>" name="transfer_date" required="required">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label>Amount &nbsp;&nbsp;&nbsp;
                                                    <small class="badge pull-right bg-green current-cash-badge">Rs. <?=$current_cash?> (Cash)</i></small>
                                                </label>

                                                <input type="number" class="form-control" name="amount" id="amount" required="required">  

                                                <input type="hidden" value="<?=$current_cash?>" name="current_cash">
                                                <input type="hidden" value="<?=$cash_id?>" name="cash_id">          
                                            </div>
                                        </div>

                                        <div class="form-group" id="undeposited-section">
                                            <label>Cheques</label>
                                            <select class="form-control" name="undeposited_cheque" id="undeposited-cheque">
                                                <option value="0">Select One</option>
                                                <?php
                                                    foreach($undeposited_result as $cheque){
                                                ?>
                                                <option value="<?=$cheque->income_id?>" amt="<?=$cheque->amount?>"><?=$cheque->transaction_date?>(Date), <?=$cheque->amount?> (Amount)</option>

                                                <?php
                                                    }
                                                ?> 
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Bank A/C</label>
                                            <select name="bank" class="form-control" id="bank">
                                                <?php
                                                    foreach($bank_result as $bank){
                                                ?>
                                                <option value="<?=$bank->bank_id?>" balance="<?=$bank->balance?>">
                                                <?=$bank->bank_name?> - (<?=$bank->balance?>)</option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                		                                		
                                		<div class="form-group">
                                            <label>Cheque/Voucher No</label>
                                            <input type="text" class="form-control" name="voucher_no" required="required">
                                        </div>
                                		                         			

                                		<div class="form-group">
                                			<label for="category">Remarks</label>
                                			<textarea class="textarea text-area-style" name="remarks"></textarea>
                                		</div>
                                		
                                		<div class="box-footer">
                                			<button type="submit" class="btn btn-primary" id="form-submit">Save Changes</button>
                                		</div>
                                	</form>								   
                                </div>
                            </div>
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
            </aside>
            <!--right_panel-->
 </div>
        
        
        <!--footer-->
<?php $this->load->view('admin/include/footer'); ?>
         <!--footer-->  
<script>
/*Code to intialize date picker*/
$(function(){
    $( "#datepicker" ).datepicker({
        dateFormat: "dd-M-yy",
        showAnim : "blind"
    });
});
/*END code to initialize date picker*/

/* CODE TO DISPLAY THE BLOCK SECTION FOR UNDEPOSITED CHEQUES*/
$(document).on('change','#transfer-type',function(e){
    var transferType = $('#transfer-type option:selected').val();
    if(transferType == "3"){
        $('.current-cash-badge').hide();
        $('#undeposited-section').slideDown();
        assignAmt();
        $('#amount').attr('readonly','readonly',true);
    }else{
        $('.current-cash-badge').show();
        $('#undeposited-section').slideUp();
        $('#amount').removeAttr('readonly',false);
    }
});

$(document).on('change','#undeposited-cheque',function(e){
  assignAmt();  
})

function assignAmt(){
    var chequeAmt = $('#undeposited-cheque option:selected').attr('amt');
    $('#amount').val(chequeAmt);
}

/*END CODE TO DISPLAY THE BLOCK SECTION FOR UNDEPOSITED CHEQUES*/

/*CODE TO CHECK WHETHER THE CASH OR BANK BALANCE ARE ENOUGH FOR TRANSFERS*/
$(document).on('click','#form-submit',function(e){
    e.preventDefault();
    var transferType = $('#transfer-type option:selected').val();
    var cash = parseInt("<?=$current_cash?>");
    var bankBal = parseInt($('#bank option:selected').attr('balance'));
    var transferAmt = parseInt($('#amount').val());
    var flag;
    //condition to check cash to bank
    if(transferType == 1){
        if(cash < transferAmt){
            flag = 1;
        }else{
            flag = 2;
        }

    }
    //condition to check bank to cash
    else if(transferType == 2){
        if(bankBal < transferAmt){
            flag = 1;
        }else{
            flag = 2;
        }
    }else{
        flag = 2;
    }

    if(flag == 1){
        alert("Error! The transfer amount exceeds the balance");
        $('#amount').focus();
    }else if(flag == 2){
        $('#transfer-frm').submit();
    }

})


/*END CODE TO CHECK WHETHER THE CASH OR BANK BALANCE ARE ENOUGH FOR TRANSFERS*/

</script>
</body>
</html>
