<style>
    .actions {
        display: block;
        font-size: 10px;
        opacity: 0;
    }
    
    tr:hover .actions {
        opacity: 1;
    }
    
    #success-alert,
    #warning-alert {
        display: none;
    }
</style>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Ticketing
            <small>Manage your candidates ticketing</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Ticketing</li>
        </ol>
    </section>

    <?php $this->load->view('admin/include/notification'); ?>
    <!-- Main content-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                         <?php
                            $role = strtolower($this->session->userdata('role'));
                            if( $role == "ticketing" || $role == "general admin" || $role == "system admin"){
                              ?>
                        <div class="box-title">
                            <button class="btn btn-primary" onclick="window.location='<?=base_url()?>ticketing/newTicketing'">
                                Add New
                            </button>
                            <div class="btn-group" style="margin-left:10px;">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    Ticketing Options &nbsp;<span class="caret"></span>
                                </button>
                            </div>
                        </div> 
                        <?php
                            }
                            ?>

                        <?php
                            $role = strtolower($this->session->userdata('role'));
                            if( $role == "finance"){
                        ?>
                            <div class="box-title">
                            <button class="btn btn-primary" onclick="window.location='<?=base_url()?>ticketing/getTransferList/final_approval_received'">
                                Transfer for ticket processing    
                            </button>
                            </div>
                        <?php
                            }
                        ?>                       
                    </div>

                    <div class="box-body table-responsive">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Custom Tabs -->
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%">S.No</th>
                                            <th>Candidate</th>
                                            <th>Ticket_no</th>
                                            <th>Flight No</th>
                                            <th>Departure</th>
                                            <th>Arrival</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php 
                                            $counter=1 ; 
                                            foreach($ticketing as $record){ 

                                                $id=$record->id; 
                                                $encrypt_id = base64_encode($id); ?>
                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$record->candidate_name?><br><?=$record->passport_no?></td>
                                            <td><?=$record->ticket_no?></td>
                                            <td><?=$record->flight_no?></td>
                                            <td><?=$record->departure?></td>
                                            <td><?=$record->arrival?></td>
                                            <td><?=$record->origin?></td>
                                            <td><?=$record->destination?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i> &nbsp;<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li> <a href="#" content-id="<?=$id?>" id="view-ticket">View </a> 
                                                        </li>
                                                        <?php
                                                        $role = strtolower($this->session->userdata('role'));
                                                        if( $role == "ticketing" || $role == "general admin" || $role == "system admin"){
                                                          ?>
                                                        <li class="divider"></li>
                                                        <li> <a href="<?=base_url()?>ticketing/editTicketing/<?=$encrypt_id?>">Edit</a> 
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="#" 
                                                             onclick="return confirmDelete();">Delete</a>
                                                         </li>
                                                         <?php
                                                            }
                                                            ?>s
                                                     </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $counter+=1; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>
        </div>
        </div>
        <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>

</aside>
<!--right_panel-->
<script>
    $(document).on('click','#view-transaction',function(e){
        e.preventDefault();
        var Id = $(this).attr('content-id');
        $.post('<?=base_url()?>transaction/loadTransaction',{id:Id}).done(function(e){
            $('.view-ajax-content').html(e);
            $('#view-ajax-modal').modal();
        });
    })
</script>