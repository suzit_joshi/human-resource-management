<link rel="stylesheet" type="text/css" href="<?=base_url()?>admin_files/css/jquery.datetimepicker.css"/ >
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Ticketing
            <small>Manage your candidates ticketing</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>admin/"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="active">Ticketing</li>
        </ol>
    </section>
    <?php $this->load->view('admin/include/notification') ?>
    <!-- Main content -->
    <section class="content">
        <div class='row'>
            <div class='col-md-12'>
                <?php if(isset($medical)){ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">Edit Medical Details</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'medical/save');?>  
                            <input type="hidden" name="random" value="<?=base64_encode($medical['id'])?>">                          
                            <div class="form-group">
                               <label for="dob" class="control-label">Medical Type*</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="type" value="1st-Medical" checked="checked" class="medical-type"
                                            <?php echo (($medical['type'] == "1st-Medical") ? "checked='checked'" : "" );?>> 1st-Medical
                                        </label>
                                        <label>
                                            <input type="radio" name="type" value="Re-Medical" class="medical-type"
                                            <?php echo (($medical['type'] == "Re-Medical") ? "checked='checked'" : "" );?>> Re-Medical
                                        </label>
                                    </div>                               
                            </div>
                            
                            <div class="form-group">
                                <label for="candidate">Candidate</label>
                                <select class="form-control" name="candidate" id="candidate-list">
                                    <option value="<?=$medical['candidate_id']?>"><?=$medical['candidate_name']?></option>                                        
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Medical date*</label>
                                <input type="text" class="form-control" id="datepicker" value="<?=date('d-M-Y',strtotime($medical['medical_date']))?>" name="medical_date" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Medical Centre</label>
                                <input type="text" class="form-control" value="<?=$medical['medical_center']?>" name="medical_center" required="required">
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Result*</label>
                                <div class="radio">
                                        <label>
                                            <input type="radio" name="result" value="1" <?php echo (($medical['result'] == "1") ? "checked='checked'" : "" );?>> PASS
                                        </label>
                                        <label>
                                            <input type="radio" name="result" value="0" <?php echo (($medical['result'] == "0") ? "checked='checked'" : "" );?>> FAIL
                                        </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category">Remarks</label>
                                <textarea class="textarea text-area-style" name="description"><?=$medical['remarks']?></textarea>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="medical-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php }
                    else{ ?>
                <div class='box'>
                    <div class="box-header">
                        <h3 class="box-title">New Ticketing Details</h3>
                    </div>
                    <div class='box-body pad'>
                        <?php echo form_open( 'ticketing/save');?>                            
                            
                            
                            <div class="form-group">
                                <label>Candidate, Passport No</label>
                                <select name="candidate" class="chzn-select form-control" 
                                     tabindex="4" id="candidate" required>
                                    <option value="">Enter candidate name/passport no</option>
                                    <?php
                                        if($candidate){
                                            foreach ($candidate as $row) {
                                    ?>
                                        <option value="<?=$row->candidate_id?>"><?=$row->candidate_name?> , <?=$row->passport_no?></option>
                                    <?php
                                            }
                                        }
                                        else{
                                            echo "<option>No Candidate Till Now</option";
                                        }
                                    ?>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="interviewee">Ticket Number</label>
                                <input type="text" class="form-control" placeholder="Enter ticket no" name="ticket_no" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Airlines</label>
                                <input type="text" class="form-control" placeholder="Enter airlines company" name="airlines" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Flight Number</label>
                                <input type="text" class="form-control" placeholder="Enter ticket no" name="flight_no" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Baggage</label>
                                <input type="text" class="form-control" placeholder="Enter baggage" name="baggage" required="required">
                            </div>

                            <div class="form-group">
                                <label for="dob" class="control-label">Departure date*</label>
                                <input type="text" class="form-control" id="datetimepicker" value="<?=date('d-M-Y')?>" name="departure_date" required="required">
                            </div>

                             <div class="form-group">
                                <label for="dob" class="control-label">Arrival date*</label>
                                <input type="text" class="form-control" id="datetimepicker1" value="<?=date('d-M-Y')?>" name="departure_date" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Origin</label>
                                <input type="text" class="form-control" placeholder="Enter Origin" name="origin" required="required">
                            </div>

                            <div class="form-group">
                                <label for="interviewee">Destination</label>
                                <input type="text" class="form-control" placeholder="Enter Destination" name="destination" required="required">
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" name="ticketing-submit" value="1">Save Changes</button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
</aside>
<!--right_panel-->
        
<script>
  $(function(){
    $( "#datepicker" ).datepicker({
    	yearRange: "-50:+0",
		dateFormat: "dd-M-yy",
		showAnim : "blind",
		changeMonth: true,
		changeYear: true
	});
  });
    
    
     function candidateDisplay(type){
        $('#candidate-list').html('<option>LOADING.......</option>');
         
        $.post('<?=base_url()?>medical/getCandidateList',{type:type}).done(function(e){
            $('#candidate-list').html(e);
        });
     }
    
    
</script>
<script src="<?=base_url()?>admin_files/js/jquery.datetimepicker.js"></script>
<script>

jQuery('#datetimepicker').datetimepicker();
jQuery('#datetimepicker1').datetimepicker();
</script>