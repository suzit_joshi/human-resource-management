<?php
	$counter = 1;
	foreach($result as $record){
		$id = $record->id;
        $encrypted_id = base64_encode($id);
?>
<tr>
	<td><?=$counter?></td>
	<td class="bank-name" data-id="<?=$id?>" balance="<?=$record->balance?>"><span class="name"><?=$record->bank_name?></span></td>
	<td><?=$record->balance?></td>
	<td align="center">
		<div class="btn-group">
			<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<i class="fa fa-cog"></i> <span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>  <a href="#" class="edit-bank">Edit</a></li>
			</ul>
		</div>
	</td>
</tr>

<?php
	$counter+=1;
}
?>