<?php
	if(isset($job)){
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Job Details</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                
                <div class="form-group">
                    <label>Position</label>
                    <input type="text" class="form-control" value="<?=$job['job_title']?>" readonly>
                </div>

                <div class="form-group">
                    <label>Demand:</label>
                    <input type="text" class="form-control" value="<?=$job['vacancy_no']?>" readonly="readonly">
                </div>

                 <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" value="<?=$job['salary']?>" readonly>
                </div>

                <div class="form-group">
                    <label>Company Name:</label>
                    <input type="text" class="form-control" value="<?=$job['company_name']?>" readonly="readonly">
                </div>

                <div class="form-group">
                    <label>Company Address:</label>
                    <input type="text" class="form-control" value="<?=$job['address']?> , <?=$job['country_name']?>" readonly="readonly">
                </div>

                <div class="form-group">
                    <label>Requirements</label>
                    <textarea class="form-control" readonly="readonly"><?=$job['requirement']?></textarea>
                </div>

                 <div class="form-group">
                    <label>Facilities</label>
                    <textarea class="form-control" readonly="readonly"><?=$job['facility']?></textarea>
                </div>
            </form>

        </div>
    </div>
</div>

	
<?php
}
else{
	echo "CONTENT NOT FOUND";
}
?>