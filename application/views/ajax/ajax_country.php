<?php
  if($type == "new"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Success!!</b>New country added.
                                </div>
<?php
  }elseif($type == "edit"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b>Success!!</b>Record has been changed successfully.
  </div>
<?php
}
?>
  <div class="box">
                <div class="box-body table-responsive">
                                    <table id="country-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Country</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                    <?php
                    $counter = 1;
                    foreach($result_country as $record){
                      $id = $record->id;
                      $encrypted_id = base64_encode($id);
                    ?>
                                            <tr>
                                                <td><?=$counter?></td>
                        <td><?=$record->country_name?></td>
                                                <td align="center">
                                                  <a href="#" country-id="<?=$encrypted_id?>" 
                                                    country-name="<?=$record->country_name?>" class="edit-country"><i class="fa fa-edit" title="Edit"></i></a>
                                                  <a href="<?=base_url()?>country/deactivate/<?=$encrypted_id?>" 
                                                    country-id="<?=$encrypted_id?>" onclick="return confirmDeactivate();"><i class="fa fa-minus-square" title="Deactivate Country"></i></a>
                                                </td>
                        
                                            </tr>
                                         <?php
                      $counter+=1;
                      }
                    ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
  </div>
<script>
  $('#country-table').dataTable();
</script>
                            

