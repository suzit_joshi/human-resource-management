<?php
  if($type == "new"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Success!!</b>New Country has been added.
                                </div>
<?php
  }elseif($type == "edit"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b>Success!!</b>Record has been changed successfully.
  </div>
<?php
}
?>
              <div class="box">
                <div class="box-body table-responsive">
                                    <table id="role-table" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th>Role</th>
                                                <th>Status</th>
                                                <th width="5%">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                    <?php
                    $counter = 1;
                    foreach($result_role as $record){
                      $id = $record->role_id;
                      $encrypted_id = base64_encode($id);
                    ?>
                                            <tr>
                                                <td><?=$counter?></td>
                                                 <td><?=$record->role_name?></td>
                                                 <td><?php if($record->status == '1'){ echo "Active";}else{ echo "Inactive";}?>
                                                  </td>
                                                <td align="center">
                                                  <a href="#" role-id="<?=$encrypted_id?>" 
                                                    role-name="<?=$record->role_name?>" class="edit-role"><i class="fa fa-edit" title="Edit"></i></a>
                                                  <a href="<?=base_url()?>role/deactivate/<?=$encrypted_id?>" 
                                                    role-id="<?=$encrypted_id?>" onclick="return confirmDeactivate();"><i class="fa fa-minus-square" title="Deactivate Role"></i></a>
                                                </td>
                        
                                            </tr>
                                         <?php
                      $counter+=1;
                      }
                    ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
              </div>                      

<script>
  $('#role-table').dataTable();
</script>