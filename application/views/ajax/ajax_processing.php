<?php
  if($type == "new"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Success!!</b>New Processing Work has been added.
                                </div>
<?php
  }elseif($type == "edit"){
?>
  <div class="alert alert-success alert-dismissable success-msg">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b>Success!!</b>Record has been changed successfully.
  </div>
<?php
}
?>
<div class="box">
  <div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="5%">S.No</th>
          <th>Processing Job</th>
          <th width="5%">Option</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $counter = 1;
        foreach($result as $record){
          $id = $record->processing_work_id;
          $encrypted_id = base64_encode($id);
          ?>
          <tr>
            <td><?=$counter?></td>
            <td><?=$record->processing_work_title?></td>
            <td align="center">
              <a href="#" processing-id="<?=$encrypted_id?>" 
              processing-name="<?=$record->processing_work_title?>" class="edit-processing"><i class="fa fa-edit" title="Edit"></i></a>
                </td>

              </tr>
              <?php
              $counter+=1;
            }
            ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div>
<script>
  $('#example1').dataTable();
</script>
                            

