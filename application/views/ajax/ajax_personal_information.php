<p class="heading-candidate"> <?=$candidate->candidate_name?></p>
                                    <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                    	<?php echo $candidate->perm_address."(per.)  ".$candidate->temp_address."(tem.)"?>
                                    </p>
                                    <p><i class="fa fa-phone"></i>&nbsp;&nbsp;  
                                    	<?php echo $candidate->phone."(Res.)   ".$candidate->mobile."(Mob.)"?></p>
                                    <p><i class="fa fa-info-circle"></i> &nbsp;&nbsp;
                                    	 <?php echo $candidate->email_id?></p>
                                    <p><i class="fa fa-users"></i> &nbsp;&nbsp;
                                    <?=$candidate->father_name?>(father), <?=$candidate->mother_name?>(mother)</p>
                                    <p>
                                      <i class="fa fa-file"></i> &nbsp;&nbsp;
                                      <a href="#"  data-toggle="modal" data-target="#documents-modal" style="font-size:10px;"> Upload Documents</a>
                                    </p>
                                    <p><i class="fa fa-pencil"></i> &nbsp;&nbsp;
                                    	<a href="#"  data-toggle="modal" data-target="#info-modal" style="font-size:10px;"> Edit personal information</a>
                                    </p>

                                    <p><i class="fa fa-refresh"></i> &nbsp;&nbsp;
                                      <a href="#"  data-toggle="modal" data-target="#status-modal" style="font-size:10px;"> Update Status</a>
                                    </p>

                                    <p><i class="fa fa-trash-o"></i> &nbsp;&nbsp;
                                      <a href="<?=base_url()?>candidate/deleteCandidate/<?=base64_encode($candidate->candidate_id)?>" 
                                      onclick="return confirmDelete();" style="font-size:10px;"> Delete Candidate</a>
                                    </p>

                                    </div>
                                    <div class="col-sm-4" style="text-align:right;">
                                    	<img src="<?php echo base_url()."uploads/candidate/".$candidate->folder_name."/".$candidate->candidate_image;?>" style="height:100px; width:auto;"/>
                                    </div>
                                    <div style="clear:both;"></div>