<?php
	$role = strtolower($this->session->userdata('role'));
	if(isset($type))//if1
	{
		if($type == "interview")//if 2
		{
			if($candidate_result)//if 3
			{
				foreach ($candidate_result as $candidate) //foreach 1
				{
?>
				<option value="<?=$candidate->candidate_id?>"><?=$candidate->candidate_name?> , (<?=$candidate->passport_no?>)</option>
		<?php
				}// endforeach 1
			}// end if 3
			else//else 3
			{
				echo "<option value=''>No Candidate for interview</option>";
			}// end else 3
		}// end if 2
		elseif($type == "filtered_list")// elseif 1
		{
            $counter = 1;
            foreach($candidate_result as $candidate)// foreach 2
            {
                $id = $candidate->candidate_id;
				$encrypted_id = base64_encode($id);
	    ?>
	    <tr>
	    	<td><?=$counter?></td>
	    	<td><?=$candidate->candidate_name?></td>
	    	<td><?=$candidate->job_title?><br><?=$candidate->company_name?></td>
	    	<td><?=$candidate->passport_no?></td>
	    	<td><?=$candidate->mobile. ", ".$candidate->phone?></td>
	    	<td><?php if($candidate->agent_id == "0"){
                                  echo "Self";
                                }
                                  elseif (is_null($candidate->agent_id)) {
                                    echo "NO Name";
                                  }else{
                                  echo $candidate->agent_name." , ". $candidate->agent_mob;
                                  }?>
            </td>
	    	<td>
	    		<?php 
                                $b = $candidate->current_status;
                                $pos = strpos($b, "_");
                                if ($pos === false) {
                                  echo ucfirst($b);
                                }else{
                                  $b = explode('_',$b);
                                  echo ucfirst($b[0]).' '.$b[1];
                                }
                              ?>
	    	</td>
	    	<td job-id="<?=$id?>"> 

	    		<div class="btn-group">
	    			<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	    				<i class="fa fa-cog"></i> <span class="caret"></span>
	    			</button>
	    			<ul class="dropdown-menu pull-right" role="menu">
	    				<li> <a href="<?=base_url()?>candidate/getCandidateDetails/<?=$encrypted_id?>" onclick="javascript:getCandidate('<?=$encrypted_id?>')">View</a> 
	    				</li>
	    				<?php

	    				if($role == "documentation" || $role == "admin" || $role == "system admin" || $role == "general admin"){
	    					?>
	    					<li class="divider"></li>
	    					<li> <a href="<?=base_url()?>candidate/editCandidateDetails/<?=$encrypted_id?>">Edit</a></li>
	    					<li class="divider"></li>
	    					<li><a href="#" onclick="return confirmDeactivate();">Deactivate</a></li>
	    					<li class="divider"></li>
	    					<li> <a href="#" class="cv-select-option" candidate-id="<?=$id?>">Generate CV</a> 
	    					</li>
	    					<?php
	    				}
	    				?>
	    			</ul>
	    		</div>
	    	</td>
	    </tr>
        <?php
            $counter+=1;
        	}//endforeach 2
		}//end elseif 1
	}//end if 1
	else
	{
		foreach ($candidate_result as $candidate) {
	?>
		<option value="<?=$candidate->id?>"><?=$candidate->candidate_name?> , (<?=$candidate->passport_no?>)</option>
	<?php
		}
	}	
?>
