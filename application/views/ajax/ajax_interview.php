<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Interview Details</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                
                <div class="form-group">
                    <label>Interview Result</label>
                    <input type="text" class="form-control" value="<?php echo (($interview['result'] == 1) ? " PASS " : "FAIL " );?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Interview Date</label>
                    <input type="text" class="form-control" value="<?=date('d-M-Y',strtotime($interview['interview_date']))?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Candidate Name, Passport No</label>
                    <input type="text" class="form-control" value="<?=$interview['candidate_name']?>, <?=$interview['passport_no']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Interviewed By</label>
                    <input type="text" class="form-control" value="<?=$interview['taken_by']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" disabled="disabled">
                        <?=$interview[ 'remarks']?>
                    </textarea>
                </div>
            </form>

        </div>
    </div>
</div>