<div class='col-md-12'>
                            <!-- Personal Information -->
                            <?php
                              @$error_no = $this->uri->segment(4);
                              if(@$error_no != ""){
                              if(@$error_no == 0){
                            ?>
                            <div class="alert alert-info alert-dismissable">
                              <i class="fa fa-info"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <b>Success!!</b> All files uploaded successfully.
                            </div>
                            <?php
                              }else{
                            ?>
                            <div class="alert alert-info alert-dismissable">
                              <i class="fa fa-info"></i>
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <b>Notice!</b> <?=@$error_no?> files could not be uploaded.
                            </div>

                            <?php
                              }
                            }
                            ?>

                            <div class='box box-info'>
                                <div class='box-header'>
                                    <h3 class='box-title'>Personal Information</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-info btn-sm" data-widget='collapse' 
                                        data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class='box-body pad'>
                                  <div class="col-sm-8">
                                    <p class="heading-candidate"> <?=$candidate->candidate_name?></p>
                                    <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                      <?php echo $candidate->perm_address."(per.)  ".$candidate->temp_address."(tem.)"?>
                                    </p>
                                    <p><i class="fa fa-phone"></i>&nbsp;&nbsp;  
                                      <?php echo $candidate->phone."(Res.)   ".$candidate->mobile."(Mob.)"?></p>
                                    <p><i class="fa fa-info-circle"></i> &nbsp;&nbsp;
                                       <?php echo $candidate->email_id?></p>
                                    <p><i class="fa fa-users"></i> &nbsp;&nbsp;
                                    <?=$candidate->father_name?>(father), <?=$candidate->mother_name?>(mother)</p>
                                    <p title="Current Processing Status"><i class="fa fa-tags"></i>&nbsp;&nbsp;
                                    <?=$candidate->candidate_status?></p>
                                    
                                    
                                    </div>
                                    <div class="col-sm-4" style="text-align:right;">
                                      <img src="<?php echo base_url()."uploads/candidate/".$candidate->folder_name."/".$candidate->candidate_image;?>" style="height:100px; width:auto;"/>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div><!-- /.Personal Information -->

                            <!--Passport and JOB Information-->
                            <div class='box collapsed-box' >
                                <div class='box-header'>
                                    <h3 class='box-title'>Passport and Job Information</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-default btn-sm" data-widget='collapse' 
                                        data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        <button class="btn btn-default btn-sm" data-widget='remove' 
                                        data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class='box-body pad' style="display:none;">
                                  <table class="table" id="passport-table">
                                    <tr><td><b>Applied Job</b></td><td ><?=$candidate->job_title?></td></tr>
                                    <tr><td><b>Agent</b></td><td><?=$candidate->agent_name?></td></tr>
                                    <tr><td><b>Passport No</b></td><td><?=$candidate->passport_no?></td></tr>
                                    <tr><td><b>Issued From</b></td><td><?=$candidate->issued_from?></td></tr>
                                    <tr><td><b>Issued Date</b></td><td><?=$candidate->issued_date?></td></tr>
                                  </table>
                                </div>
                            </div><!--END Passport and job INFORMATION-->

                            <!--Work Processing Information-->
                            <div class='box collapsed-box' >
                                <div class='box-header'>
                                    <h3 class='box-title'>Processing Work</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-default btn-sm" data-widget='collapse' 
                                        data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        <button class="btn btn-default btn-sm" data-widget='remove' 
                                        data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class='box-body pad' style="display:none;">
                                  <table class="table" id="processing-table">
                                    <thead>
                                      <tr>
                                        <th>S.NO.</th>
                                        <th>Processing Work</th>
                                        <th>Date</th>
                                        <th>Descriptions</th>
                                      </tr>
                                    </thead>
                                    <tbody id="processing-content">
                                      
                                      <?php
                                      $counter = 1;
                                      if($count_result_set == 0){
                                      ?>
                                      <tr>
                                      <td colspan="5"> No processing work carried out till now. </td>
                                      </tr>
                                      <?php  
                                      }
                                      else{
                                        foreach($result_set as $row){
                                          ?>
                                          <tr>
                                            <td><p style="display:none;" class="work-id"><?=$row->work_id?></p><?=$counter?></td>
                                            <td class="work-title"><?=$row->processing_work_title?></td>
                                            <td class="work-date"><?=$row->date?></td>
                                            <td class="work-desc"><?=$row->description?></td>
                                          </tr>

                                          <?php
                                          $counter++;
                                        }
                                      }
                                        ?>
                                    </tbody>
                                  
                                  </table>
                                </div>
                            </div><!--Working Processing INFORMATION-->

                            <!--Uploaded Documents Information-->
                            <div class='box collapsed-box' >
                                <div class='box-header'>
                                    <h3 class='box-title'>Client Documents</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-default btn-sm" data-widget='collapse' 
                                        data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>

                                        <button class="btn btn-default btn-sm" data-widget='remove' 
                                        data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class='box-body pad' style="display:none;">
                                  <table class="table" id="document-table">
                                    <thead>
                                      <tr>
                                        <th>S.NO.</th>
                                        <th>Document Title</th>
                                        <th>View</th>
                                      </tr>
                                    </thead>
                                    <tbody id="processing-content">
                                      
                                      <?php
                                      $counter = 1;

                                      if($document_count == 0){
                                      ?>
                                      <tr>
                                      <td colspan="5"> No documents uploaded till now. </td>
                                      </tr>
                                      <?php  
                                      }
                                      else{
                                        $folder = $candidate->folder_name;
                                        foreach($document as $record){
                                          ?>
                                          <tr>
                                            <td><p style="display:none;" class="document-id"><?=$record->upload_id?></p><?=$counter?></td>
                                            <td class="document-title"><?=$record->file_name?></td>
                                            <td><a target="_blank" href="<?=base_url()?>uploads/candidate/<?=$folder?>/<?=$record->file_name?>">
                                                <i class="fa fa-folder-open" title="View"></i>
                                            </a>
                                            </td>
                                          </tr>

                                          <?php
                                          $counter++;
                                        }
                                      }
                                        ?>
                                    </tbody>
                                  
                                  </table>
                                </div>
                            </div><!--END UPLOADED DOCUMENTS INFORMATION-->
                        </div><!-- /.col-->

<div class="col-md-12 category-tab">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Income Transactions</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Expense Transactions</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                          <table id="income-table" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>S.No</th>
                                <th>Date</th>
                                <th>Payment Type</th>
                                <th>Amount</th>
                                <th>Bank</th>                                                      
                                <th>Remarks</th>
                              </tr>
                            </thead>
                            <tbody id="bank-record">
                              <?php
                              $counter = 1;
                              foreach($incomes as $record){
                               $income_id = $record->income_id;
                               $encrypt_id = base64_encode($income_id);
                               ?>
                               <tr>
                                <td><?=$counter?></td>
                                <td><?=$record->transaction_date?></td>
                                <td><?=$record->type?></td>
                                <td><?=$record->amount?></td>
                                <td><?php echo (is_null($record->bank_name) ? "N/A" : $record->bank_name );?></td>
                                <td><?=$record->description?></td>
                              </tr>
                              <?php
                              $counter+=1;
                            }
                            ?>
                          </tbody>
                        </table>                
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <table id="expense-table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S.No</th>
                              <th>Date</th>
                              <th>Payment Type</th>
                              <th>Amount</th>
                              <th>Bank</th>                                                               
                              <th>Remarks</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $counter = 1;
                            foreach($expenses as $record){
                             $id = $record->expense_id;
                             $encrypt_id = base64_encode($id);
                             ?>
                             <tr>
                              <td><?=$counter?></td>
                              <td><?=$record->transaction_date?></td>
                              <td><?=$record->type?></td>
                              <td><?=$record->amount?></td>
                              <td><?php echo (is_null($record->bank_name) ? "N/A" : $record->bank_name );?></td>
                              <td><?=$record->description?></td>
                            </tr>
                            <?php
                            $counter+=1;
                          }
                          ?>
                        </tbody>
                      </table>
                      </div><!-- /.tab-pane -->
                      </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                  </div><!-- /.col -->
<script type="text/javascript">

  $('#income-table').dataTable();
  $('#expense-table').dataTable();

   $("[data-widget='collapse']").click(function() {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!box.hasClass("collapsed-box")) {
            box.addClass("collapsed-box");
            //Convert minus into plus
            $(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            bf.slideUp();
        } else {
            box.removeClass("collapsed-box");
            //Convert plus into minus
            $(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            bf.slideDown();
        }
    });
</script>