<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?=ucfirst($title)?></h4>
</div>
<?php
	if($title == "agent"){
?>
<div class="modal-body">  
	<div class="row">  

		<div class="form-group">
			<img src="<?=base_url()?>uploads/agent/<?=$record->image?>" style="height:200px; width:auto;">
		</div>

		<div class="form-group">
			<label for="agent_name" class="col-md-4">Agent Name *</label>
			<input type="text" class="form-control col-md-8" value="<?=$record->agent_name?>" readonly="readonly">
		</div>

		<div class="form-group">
			<label for="email_id" class="col-md-4">Email Address *</label>
			<input type="email" class="form-control col-md-8" value="<?=$record->email?>" readonly="readonly">
		</div>

		<div class="form-group">
			<label for="mobile" class="col-md-4">Mobile Number *</label>
			<input type="number" class="form-control col-md-8" value="<?=$record->mobile?>" readonly="readonly">
		</div>

		<div class="form-group">
			<label for="phone" class="col-md-4">Phone Number</label>
			<input type="number" class="form-control col-md-8" value="<?=$record->phone?>" readonly="readonly">
		</div>

		<div class="form-group">
			<label for="temp_address" class="col-md-4">Address</label>
			<input type="text" class="form-control col-md-8" value="<?=$record->address?>" readonly="readonly">
		</div>
	</div>
</div>
<?php
	}elseif ($title == "company") {
?>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<img src="<?=base_url()?>uploads/company/<?=$record->company_logo?>" style="height:200px; width:auto;">
			</div>

			<div class="form-group">
				<label for="company_name">Company Name</label>
				<input type="text" class="form-control" value="<?=$record->company_name?>" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="address">Address</label>
				<input type="text" class="form-control" readonly="readonly" value="<?=$record->address?>">
			</div>

			<div class="form-group">
				<label for="email">Email_id</label>
				<input type="email" class="form-control" value="<?=$record->email_id?>" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="website">Website</label>
				<input type="url" class="form-control" value="<?=$record->website?>" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="country">Country</label>
				<?php
					$country = $this->db->get_where('tbl_country',array('id' => $record->country_id))->row();
				?>
				<input type="text" class="form-control" readonly="readonly" value="<?=$country->country_name?>">
			</div>

			<div class="form-group">
				<label for="category">General Information</label>
				<textarea class="textarea text-area-style" readonly="readonly"><?=$record->description?></textarea>
			</div>
		</div>
	</div>
</div>
<?php
	}elseif ($title == "advertisement") {
?>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<img src="<?=base_url()?>uploads/advertisement/<?=$record->image?>" style="height:200px; width:auto;">
			</div>

			<div class="form-group">
				<label for="company_name">Advertised Date</label>
				<input type="text" class="form-control" value="<?=$record->released_date?>" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="address">Advertisement On</label>
				<input type="text" class="form-control" readonly="readonly" value="<?=$record->paper?>">
			</div>

			<div class="form-group">
				<label for="email">Size</label>
				<input type="email" class="form-control" value="<?=$record->size?>" readonly="readonly">
			</div>

			<div class="form-group">
				<label for="website">Page_no</label>
				<input type="url" class="form-control" value="<?=$record->page_no?>" readonly="readonly">
			</div>

		</div>
	</div>
</div>


<?php
		# code...
	}
	else{
		echo "No content Found";
	}
?>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
