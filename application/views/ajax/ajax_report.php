<div id="placeholder" class="demo-placeholder"></div>
<?php
 	//$amount = new array();
 	foreach($cash_result as $cash){
 		$month = $cash->month;
 		$amount[$month] = $cash->amount; 
 	}

 	for($i = 1; $i<=12; $i++)
 	{
 		if(!array_key_exists($i, $amount))
 		{
 			$amount[$i] = 0;
 		}
 	}

 	$month = array("","January","February","March","April","May","June","July",
 					"August","September","October","November","December");

 	for($i=1;$i<=12;$i++){
 		$report_value[] = "[\"".$month[$i]."\",".$amount[$i]."]";
 	}

 	$final_report_string = implode(",",$report_value);
?>



<script type="text/javascript">

	$(function() {

		var data = [<?=$final_report_string?>];

		$.plot("#placeholder", [ data ], {
			series: {
				bars: {
					show: true,
					barWidth: 0.6,
					align: "center"
				}
			},
			xaxis: {
				mode: "categories",
				tickLength: 0
			}
		});
	});
</script>