
<div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li <?php if($flag == 1){ echo "class='active'"; } ?>><a href="#tab_1" data-toggle="tab">Income</a>
                                        </li>
                                        <li <?php if($flag == 2){ echo "class='active'"; } ?>><a href="#tab_2" data-toggle="tab">Expense</a>
                                        </li>
                                        <li <?php if($flag == 3){ echo "class='active'"; } ?>><a href="#tab_3" data-toggle="tab">Advance</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane  <?php if($flag == 1){ echo "active"; } ?>" id="tab_1">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Income Heading</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="bank-record">
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_income as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="income">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 

                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                      <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                  </button>
                                                                  <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li> <a href="#" class="edit-category">Edit</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                        onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                    </ul>
                                                                </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane <?php if($flag == 2){ echo "active"; } ?>" id="tab_2">
                                            <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Expense Category</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_expense as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="expense">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li> <a href="#" class="edit-category">Edit</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                    onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                        <!-- /.tab-pane 3-->
                                        <div class="tab-pane <?php if($flag == 3){ echo "active"; } ?>" id="tab_3">
                                            <table id="example3" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="width:2% !important;">S.No</th>
                                                        <th>Advance Category</th>
                                                        <th>Type</th>
                                                        <th>Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $counter=1 ; 
                                                        foreach($result_advance as $record){ 
                                                            $id=$record->id; 
                                                            $encrypt_id = base64_encode($id); 
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?=$counter?>
                                                        </td>
                                                        <td class="category-name" data-id="<?=$id?>" category-heading="<?=$record->subtype?>" type="expense">
                                                            <span class="name"><?=$record->category_title?></span>
                                                        </td>
                                                        <td><?=$record->subtype?></td>
                                                        <td> 
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li> <a href="#" class="edit-category">Edit</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="<?=base_url()?>category/delCategory/<?=$encrypt_id?>" 
                                                                    onclick="confirmDeactivate()"class="delete-category">Delete</a></li>                                                        
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter+=1; } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- nav-tabs-custom -->
<script>
    $('#example1').dataTable();
    $('#example2').dataTable();
    $('#example3').dataTable();
</script>