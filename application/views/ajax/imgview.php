<?php
	$name=$upload_data['file_name'];
?>
<img src="<?=base_url()?>tmp/<?=$name?>" id="cropbox" />
		<!-- This is the form that our event handler fills -->

		<form action="<?=base_url()?>crop/imgcrop" method="post" id="crop-frm"onsubmit="return checkCoords();">
			<input type="hidden" value="<?=$name?>" name="image_name" />
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<button id="crop-btn" class="btn btn-large btn-inverse">Crop Image</button>
		</form>
		<script>
		
		
 $(function(){

    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords,
      bgColor:  'black',
      bgOpacity:   .4,
      boxWidth: 400
    });

  });

  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  };

</script>

<script>
  $('#crop-btn').on('click', function(){ 
    $("#crop-frm").submit();
  });

  $("#crop-frm").submit(function(e)
  {
    var formObj = $(this);
    var formURL = formObj.attr("action");
  
    if(window.FormData !== undefined)  // for HTML5 browsers
    {

        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                $('#img-display').html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                $("#message_dealer").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
            }           
        });
        e.preventDefault();
        //e.unbind();
    }

    else  //for olden browsers
    {
        //generate a random id
        var  iframeId = 'unique' + (new Date().getTime());
        //create an empty iframe
        var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
        //hide it
        iframe.hide();
        //set form target to iframe
        formObj.attr('target',iframeId);
        //Add iframe to body
        iframe.appendTo('body');
        iframe.load(function(e)
        {
            var doc = getDoc(iframe[0]);
            var docRoot = doc.body ? doc.body : doc.documentElement;
            var data = docRoot.innerHTML;
            $("#multi-msg").html('<pre><code>'+data+'</code></pre>');
        });
    }
});

  </script>