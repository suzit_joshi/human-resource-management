<?php
	if($type == "Pre-interview"){
		$strDateFrom = $jobs['pre_interview_to_date'];
		$strDateTo = $jobs['pre_interview_for_date'];
	}
	else{
		$strDateFrom = $jobs['final_interview_to_date'];
		$strDateTo = $jobs['final_interview_for_date'];
	}

	$aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    foreach ($aryRange as $record) {
?>
	<option value="<?=$record?>"><?=date('d-M-Y',strtotime($record))?></option>
<?php
	}
?>
	

