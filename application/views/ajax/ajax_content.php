<?php
    if($contentType == "interview"){
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Interview Details</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                
                <div class="form-group">
                    <label>Interview Result</label>
                    <input type="text" class="form-control" value="<?php echo (($interview['result'] == 1) ? " PASS " : "FAIL " );?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Interview Date</label>
                    <input type="text" class="form-control" value="<?=date('d-M-Y',strtotime($interview['interview_date']))?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Candidate Name, Passport No</label>
                    <input type="text" class="form-control" value="<?=$interview['candidate_name']?>, <?=$interview['passport_no']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Interviewed By</label>
                    <input type="text" class="form-control" value="<?=$interview['taken_by']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" disabled="disabled">
                        <?=$interview[ 'remarks']?>
                    </textarea>
                </div>
            </form>

        </div>
    </div>
</div>
<?php
    }
    elseif($contentType == "transaction"){
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Transaction Details</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                
                <div class="form-group">
                    <label>Transaction Date</label>
                    <input type="text" class="form-control" value="<?=$transaction['transaction_date']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Transaction Title</label>
                    <input type="text" class="form-control" value="<?=$transaction['category_title']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Transaction Type</label>
                    <input type="text" class="form-control" value="<?=ucfirst($transaction['type'])?> ON <?=$transaction['subtype']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Amount</label>
                    <input type="text" class="form-control" value="Rs.<?=$transaction['amount']?>.00" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Candidate Name</label>
                    <input type="text" class="form-control" value="<?php echo (is_null($transaction['candidate_name']) ? "N/A" : $transaction['candidate_name'] );?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Bank Name</label>
                    <input type="text" class="form-control" value="<?php echo (is_null($transaction['bank_name']) ? "N/A" : $transaction['bank_name'] );?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Cheque No</label>
                    <input type="text" class="form-control" value="<?php 
                        if($transaction['type'] == "income" && $transaction['subtype'] == "Cheque"){
                                echo $transaction['income_cheque_no'];
                            }
                            else{
                                echo ($transaction['cheque_no'] == "" ? "N/A" : $transaction['cheque_no'] );
                        }
                    ?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Voucher No</label>
                    <input type="text" class="form-control" value="<?php echo ($transaction['voucher_no'] == "" ? "N/A" : $transaction['voucher_no'] );?>" disabled="disabled">
                </div>
              
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" disabled="disabled">
                        <?=$transaction['remarks']?>
                    </textarea>
                </div>
            </form>

        </div>
    </div>
</div>  
<?php
    }
    elseif($contentType == "medical"){
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Medical Details</h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                
                <div class="form-group">
                    <label>Medical Result</label>
                    <input type="text" class="form-control" value="<?php echo (($medical['result'] == 1) ? " PASS " : "FAIL " );?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Medical Date</label>
                    <input type="text" class="form-control" value="<?=date('d-M-Y',strtotime($medical['medical_date']))?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Candidate Name, Passport No</label>
                    <input type="text" class="form-control" value="<?=$medical['candidate_name']?>, <?=$medical['passport_no']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Medical Center</label>
                    <input type="text" class="form-control" value="<?=$medical['medical_center']?>" disabled="disabled">
                </div>

                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" disabled="disabled">
                        <?=$medical['remarks']?>
                    </textarea>
                </div>
            </form>

        </div>
    </div>
</div>
<?php
    }
    else{
        echo "No Content Found!";
    }
?>