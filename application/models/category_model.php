<?php
class Category_model extends MY_Model{
    
    public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_category_transaction';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get category transaction
	public function getCategory($type=NULL,$id=NULL){
		$query_statement = "SELECT $this->_table.* 
                            FROM $this->_table
                                WHERE status='0'
                            ";
        
        if(is_NULL($type) && !is_NULL($id)){
            $condition = "AND $this->_table.id = '$id'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->row();
        }
        elseif(!is_NULL($type) && !is_NULL($id)){
            $condition = "AND $this->_table.type = '$type' AND $this->_table.id = '$id'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->row();
        }
        elseif(!is_NULL($type) && is_NULL($id)){
            $condition = "AND $this->_table.type = '$type'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->result();
        }	
	}

    //@author suzit
    //@params none
    //@returns none
    //@get category transaction
    public function getFilteredCategory($type,$subtype){
        $query_statement = "SELECT $this->_table.* 
                            FROM $this->_table
                                WHERE status='0'
                                AND type = '$type'
                                AND subtype = '$subtype'
                            ";
        return $this->db->query($query_statement)->result();
    }
}
