<?php
class Orientation_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_orientation';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params id of orientation
    //@returns none
	//@get orientation list
	public function listOrientation($id=NULL){
		$query_statement = "SELECT $this->_table.*, candidate_name
								FROM $this->_table
								LEFT JOIN tbl_candidate
									ON $this->_table.candidate_id = tbl_candidate.id
								";
		if(!is_NULL($id)){
			$condition = "WHERE $this->_table.$this->primary_key = $id";
			$query_statement = $query_statement.$condition;
			return $query_statement->row();
		}else
		{
			return $this->db->query($query_statement)->result();
		}
	}
	
}
	
?>