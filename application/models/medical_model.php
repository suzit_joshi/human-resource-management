<?php
class Medical_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_medical';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get company list
	public function listMedical($id=NULL){
		$query_statement = "SELECT $this->_table.*, DATE_ADD(medical_date, INTERVAL 6 MONTH) AS expire_date, tc.candidate_name, 
    							tc.passport_no
    						FROM $this->_table
    							INNER JOIN tbl_candidate tc 
    							ON tc.id = $this->_table.candidate_id
								";
		if(!is_null($id)){
			$query_statement = $query_statement."WHERE $this->_table.$this->primary_key=$id";
			return $this->db->query($query_statement)->row();
		}
		else{
			return $this->db->query($query_statement)->result();
		}
	}

	//@author suzit
    //@params none
    //@returns none
	//@get company list
	public function getMedicalList(){
		$query = "SELECT tbl_candidate.id AS candidate_id, candidate_name, passport_no 
                   FROM tbl_candidate 
                       INNER JOIN tbl_job_candidate ON tbl_job_candidate.candidate_id = tbl_candidate.id 
                   WHERE (
                   			(tbl_job_candidate.current_status ='finalinterview_passed' AND tbl_job_candidate.data_entry = 1)
                   		 OR (tbl_job_candidate.current_status = 'medical_passed' AND tbl_job_candidate.data_entry = 0)
                   		 )
                   ";
       return $this->db->query($query)->result(); 
	}


	
}
	
?>