<?php
	class Upload_model extends MY_Model{




			//@author Sujit
	//@params none
	//@returns none
	//Image Upload Function + THUMBNAIL CREATION	
		public function do_upload()
		{
			$config['upload_path'] = 'image/subpage/';
			$config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|jpeg';
			$config['max_size']	= '1000';

			$this->load->library('upload');
			$this->upload->initialize($config);

			if (!$this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				$return_array = array('status' => FALSE, 'message' => $error);
				return $return_array;
			}
			else
			{
			$data = $this->upload->data(); // Data upload
			$name = $data['file_name'];
			$config['image_library'] = 'gd2';
			$config['source_image']	= "image/subpage/$name";
			$config['new_image'] = "image/subpage/thumb/$name";
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']	= 100;
			$config['height']	= 100;
			
			$this->load->library('image_lib'); 
			$this->image_lib->initialize($config);
			$error = $this->image_lib->resize();
			$return_array = array('status' => TRUE, 'message' => $name);
			return $return_array;
		}
	}

	}

