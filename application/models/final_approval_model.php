<?php
class Final_approval_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_final_approval';
		$this->primary_key = 'id';
	}

	public function generateFinalApprovalCandidateList(){
		$query=$this->db->query("SELECT t1.id, t1.candidate_name, t1.passport_no 
							     FROM tbl_candidate t1
							     	INNER JOIN tbl_job_candidate t2
							     		ON t2.candidate_id = t1.id
							     WHERE t2.current_status = 'visa_received'")->result();
		if($query){
			return $query;
		}
		else{
			return false;
		}
	}

	public function finalApprovalList(){
		$query=$this->db->query("select t1.id as approval_id,t1.approval_date,t2.candidate_name from tbl_final_approval t1
			 					INNER JOIN tbl_candidate t2 on t1.candidate_id=t2.id");
		return $query->result();
	}

	
	}
?>