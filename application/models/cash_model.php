<?php
class Cash_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_cash';
		$this->primary_key = 'id';

	}

	//@author Sujit
	//@params None
	//@returns lastest cash
	public function getCash(){

		$cash_query = $this->db->query("SELECT id,amount,year,month FROM tbl_cash 
										WHERE updated_on = (SELECT MAX(updated_on) from $this->_table)");
		return $cash_query->row();
	}
}
?>