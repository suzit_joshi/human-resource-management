<?php
class Interview_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_interview';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get Interview and their candidate
	public function getInterview($type=NULL,$id=NULL){
		$query_statement = "SELECT $this->_table.* ,tc.candidate_name, tc.passport_no
                            FROM $this->_table
                                INNER JOIN tbl_candidate tc
                                    ON $this->_table.candidate_id = tc.id
                            ";
        
        if(is_NULL($type) && !is_NULL($id)){
            $condition = "WHERE $this->_table.id = '$id'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->row();
        }
        elseif(!is_NULL($type) && !is_NULL($id)){
            $condition = "WHERE $this->_table.type = '$type' AND $this->_table.id = '$id'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->row();
        }
        elseif(!is_NULL($type) && is_NULL($id)){
            $condition = "WHERE $this->_table.type = '$type'";
            $query_statement = $query_statement.$condition;
            return $this->db->query($query_statement)->result();
        }	
	}

  public function getPreinterviewList($job_id)
    {
     $query = "SELECT tbl_candidate.id AS candidate_id, candidate_name, passport_no 
                   FROM tbl_candidate 
                       INNER JOIN tbl_job_candidate ON tbl_job_candidate.candidate_id = tbl_candidate.id 
                   WHERE tbl_job_candidate.job_id = $job_id
                   AND tbl_job_candidate.data_entry = 0 
                   AND (tbl_job_candidate.current_status ='preinterview_passed' OR tbl_job_candidate.current_status = 'applied')";
       return $this->db->query($query)->result();    
   }


  public function getFinalInterviewList($job_id)
    {
     $query = "SELECT tbl_candidate.id AS candidate_id, candidate_name, passport_no 
                FROM tbl_candidate 
                    INNER JOIN tbl_job_candidate 
                        ON tbl_job_candidate.candidate_id = tbl_candidate.id 
                WHERE tbl_job_candidate.job_id = $job_id 
                AND (
                        (tbl_job_candidate.data_entry = 0 AND tbl_job_candidate.current_status ='finalinterview_passed') 
                     OR (tbl_job_candidate.data_entry = 1 AND tbl_job_candidate.current_status ='preinterview_passed')
                )";
       return $this->db->query($query)->result();    
   }


}
	
?>