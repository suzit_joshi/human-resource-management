<?php
class Visa_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_visa';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get company list
	public function getVisaDetails($id=NULL){
		$query_statement = "SELECT $this->_table.*, tc.candidate_name, tc.passport_no
								FROM $this->_table
									INNER JOIN tbl_candidate tc
										ON tc.id = $this->_table.candidate_id
								";
		if(!is_NULL($id)){
			$condition = "WHERE $this->_table.$this->primary_key = $id";
			$query_statement = $query_statement.$condition;
			return $query_statement->row();
		}else
		{
			$condition = "ORDER BY $this->_table.received_date DESC";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->result();
		}
	}
	
}
	
?>