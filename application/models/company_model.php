<?php
class Company_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_company';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get company list
	public function getCompany(){
		$query_statement = "SELECT tc.id, tc.address,tc.company_name,tc.email_id,tc1.id AS country_id, tc1.country_name
								FROM tbl_company tc 
								INNER JOIN tbl_country tc1
								ON tc.country_id = tc1.id
								WHERE tc.status = 1
								";
		return $this->db->query($query_statement)->result();
	}

	
	
}
	
?>