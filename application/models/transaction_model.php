<?php
class Transaction_model extends MY_Model{
    
    public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_transaction';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get all transactions and their respective data
	public function listTransaction($id = NULL){
		$query_statement = "SELECT $this->_table.*, tb.id as 'bank_id', tb.bank_name, tb.balance, 
									tc.candidate_name, tcl.category_title, tcl.subtype as 'category_sub'
							FROM $this->_table
								INNER JOIN tbl_category_transaction AS tcl
									ON tcl.id = $this->_table.category_id
								LEFT JOIN tbl_bank AS tb
									ON tb.id = $this->_table.bank_id
								LEFT JOIN tbl_candidate AS tc
									ON tc.id = $this->_table.candidate_id							
							";
		if(is_NULL($id)){
			$condition = "ORDER BY transaction_date DESC";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->result();
		}
		else{
			$condition = "WHERE $this->_table.$this->primary_key = $id";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->row();
		}
		
	}


	public function selectByDate($date,$type){
		$id=NULL;
		$query_statement = "SELECT $this->_table.*, tb.id as 'bank_id', tb.bank_name, tb.balance, tc.candidate_name, tcl.category_title, tcl.subtype as 'category_sub'
							FROM $this->_table
								INNER JOIN tbl_category_transaction AS tcl
									ON tcl.id = $this->_table.category_id
								LEFT JOIN tbl_bank AS tb
									ON tb.id = $this->_table.bank_id
								LEFT JOIN tbl_candidate AS tc
									ON tc.id = $this->_table.candidate_id
							
							";
		if(is_NULL($id)){

			$condition = "WHERE $this->_table.transaction_date = '$date' AND $this->_table.type = '$type' ";
			$query_statement = $query_statement.$condition;
			// echo $query_statement;
			// die();
			return $this->db->query($query_statement)->result();
		}
		else{
			$condition = "WHERE $this->_table.$this->primary_key = $id";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->row();
		}
		
	}


	//@author Sujit
	//@used for generating unique transaction_id

	public function genId($transaction_type)
	{
		$query = $this->db->query("SELECT transaction_no FROM $this->_table WHERE type='$transaction_type'");

		//$query = $this->db->query("SELECT $key as id FROM $tbl_name");
		if($transaction_type == "income"){
			$initials = "inc_";
		}elseif($transaction_type == "expense"){
			$initials = "exp_";
		}else{
			$initials = "adv_";
		}

		$queryCount = $query->num_rows();
		$sn = 0;
		foreach ($query->result() as $rec)
		{
			$oldid = $rec->transaction_no;
			$value = substr($oldid,4);
			
			if($sn<$queryCount){
				$result[$sn] = $value;
				$sn++;
			}
		}
		
		if(!isset($oldid))
		{
			$nwid= $initials."1";
		}	
		else
		{
			$max = max($result);
			$nwid=$max+1;
			$nwid = $initials.$nwid;
		}	
		return $nwid;
	}


	//@author suzit
    //@params none
    //@returns none
	//@get all transactions and their respective data
	public function listApprovalTransactions($id = NULL){
		$user_id = $this->session->userdata('user_id');
		$query_statement = "SELECT $this->_table.*, tb.id as 'bank_id', tb.bank_name, tb.balance, tc.candidate_name, 
									tcl.category_title, tcl.subtype as 'category_sub',
									ta.user_id, ta.decision
							FROM $this->_table
								INNER JOIN tbl_category_transaction AS tcl
									ON tcl.id = $this->_table.category_id
								LEFT JOIN tbl_bank AS tb
									ON tb.id = $this->_table.bank_id
								LEFT JOIN tbl_candidate AS tc
									ON tc.id = $this->_table.candidate_id
								LEFT JOIN tbl_finance_approval AS ta
									ON ta.transaction_id = $this->_table.id	
										AND ta.user_id = $user_id				
							";
		if(is_NULL($id)){
			$condition = "WHERE $this->_table.type = 'expense' ORDER BY transaction_date DESC";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->result(); 
		}
		else{
			$condition = "WHERE $this->_table.$this->primary_key = $id";
			$query_statement = $query_statement.$condition;
			return $this->db->query($query_statement)->row();
		}
	}

	//@author suzit
    //@params none
    //@returns none
	//@get transactions of specific candidate
	public function getFinancialInfo($candidate_id)
	{
		$query_statement = $this->db->query("SELECT $this->_table.*, tb.id as 'bank_id', tb.bank_name, tb.balance, 
									tc.candidate_name, tcl.category_title, tcl.subtype as 'category_sub'
							FROM $this->_table
								INNER JOIN tbl_category_transaction AS tcl
									ON tcl.id = $this->_table.category_id
								LEFT JOIN tbl_bank AS tb
									ON tb.id = $this->_table.bank_id
								LEFT JOIN tbl_candidate AS tc
									ON tc.id = $this->_table.candidate_id
								WHERE $this->_table.candidate_id = $candidate_id				
							");
		return $query_statement->result();
	}
}