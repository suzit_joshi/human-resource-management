<?php
class Advertise_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_advertisement';
		$this->primary_key = 'id';
	}

	public function listAdvertisement(){
		$advertise=$this->db->query("SELECT 
									t1.id as advertisementId, t1.released_date, t1.paper, t1.size, t1.page_no, t2.job_title 
									from tbl_advertisement t1 
										inner join tbl_job t2 
										on t1.job_id=t2.id")->result();
		
		return $advertise;
	}
}
	
?>