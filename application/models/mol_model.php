<?php
class Mol_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_job_rate';
		$this->primary_key = 'id';
	}

	public function selectedJob(){
		$query=$this->db->query("SELECT id,job_title FROM tbl_job WHERE id NOT IN (SELECT distinct job_id FROM tbl_job_rate) ")->result();
		if($query){
			return $query;
		}
		else{
			return false;
		}
	}

	// get a list of job for pre-approval date
	public function preApprovalJobList(){
		$query=$this->db->query("SELECT id,job_title FROM tbl_job WHERE id  IN (SELECT distinct job_id FROM tbl_job_rate where approval_flag='1') ")->result();
		if($query){
			return $query;
		}
		else{
			return false;
		}
	}

	public function finalApprovalJobList(){
		$query=$this->db->query("SELECT id,job_title FROM tbl_job WHERE id  IN (SELECT distinct job_id FROM tbl_job_rate where approval_flag='2') ")->result();
		if($query){
			return $query;
		}
		else{
			return false;
		}
	}


	public function getPreApprovalList(){
		 $query=$this->db->query('select t1.job_title,t2.id as approval_id,t2.pre_approval_date from tbl_job t1 inner join tbl_job_rate t2 on t1.id=t2.job_id')->result();
		return $query;
	}

	public function check_if_job_exist($id){
		// $check=$this->count_by()
	}

	//@author suzit
    //@params none
    //@returns none
	//@check the username and email already exists
	
}
	
?>