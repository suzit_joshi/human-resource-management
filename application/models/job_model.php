<?php
class Job_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_job';
		$this->primary_key = 'id';
	}

	//@author suzit
    //@params none
    //@returns none
	//@get job list
	public function getJob($id = NULL){
		$query_statement = " SELECT tj.*,tc.id AS company_id, tc.address,tc.company_name,tc.email_id,tc1.id AS country_id, tc1.country_name,
							(SELECT count(*) FROM tbl_job_candidate WHERE tj.id = tbl_job_candidate.job_id) AS candidate_count
								FROM tbl_job tj
									INNER JOIN tbl_company tc 
										ON tj.company_id = tc.id
									INNER JOIN tbl_country tc1
										ON tc.country_id = tc1.id

								";
		if(is_null($id)){
			$condition = "WHERE tj.status = 0";
			
		}
		else{
			$condition = "WHERE tj.id = $id";
		}

		$query_statement = $query_statement.$condition;
		

		if(is_null($id)){
			return $this->db->query($query_statement)->result();
		}
		else{
			return $this->db->query($query_statement)->row();
		}
	}

	//@author suzit
    //@params none
    //@returns none
	//@get job list
	public function getJobInfo($select){
		$query_statement = " SELECT $select
								FROM tbl_job 
									INNER JOIN tbl_company  
										ON tbl_job.company_id = tbl_company.id
									INNER JOIN tbl_country 
										ON tbl_company.country_id = tbl_country.id
							WHERE tbl_job.status = 0
								";
		$query_statement = $query_statement;
		return $this->db->query($query_statement)->result();
	}
	
}
	
?>