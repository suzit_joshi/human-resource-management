<?php
class Mobile_model extends MY_Model{

	public function getTransactions($user_id){
		$transactions=$this->db->query("SELECT tbl_transaction.*,t1.category_title , t2.bank_name, t3.candidate_name
										FROM tbl_transaction 
										INNER JOIN tbl_category_transaction t1
											ON tbl_transaction.category_id = t1.id    
										LEFT JOIN tbl_bank t2
											ON tbl_transaction.bank_id = t2.id
										LEFT JOIN tbl_candidate t3
											ON tbl_transaction.candidate_id = t3.id                                                        
										WHERE tbl_transaction.type='expense' AND sent_flag = '1'
										AND tbl_transaction.id NOT IN(SELECT distinct(transaction_id) 
											FROM tbl_finance_approval WHERE user_id = $user_id)")->result();
		
		return $transactions;
	}
}
	
?>