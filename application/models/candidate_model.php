<?php
class Candidate_model extends MY_Model{
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'tbl_candidate';
		$this->primary_key = 'id';
	}

	//candidate_status flag
	// 1:NEW

	public function getGeneralCandidateInfo($id=NULL,$select,$branch_id){

        $query = "SELECT $select
					FROM tbl_candidate tc
						LEFT JOIN tbl_agent ta ON ta.id = tc.agent_id
						INNER JOIN tbl_job_candidate tjb
							ON tjb.candidate_id = tc.id
							INNER JOIN tbl_job tj
								ON tj.id = tjb.job_id
									INNER JOIN tbl_company tc1
										ON tc1.id = tj.company_id
                                            INNER JOIN tbl_country tc2
                                                ON tc1.country_id = tc2.id
				  ";
		if(!is_null($id)){
			$condition = "WHERE tc.id = $id";
			$query = $query.$condition;
		}else{
            if($branch_id != 1){
                $condition = "WHERE tc.branch_id = $branch_id";
                $query = $query.$condition; 
            }
        }

		if(is_null($id)){
			return $this->db->query($query)->result();
		}
		else{
			return $this->db->query($query)->row();
		}			
	}
    
    //@author Sujit
	//@params None
	//@returns None
    //@View to load new interview form
    public function getCandidate($subquery=NULL){
        $query = "SELECT id,candidate_name,passport_no, phone, mobile   
                    FROM tbl_candidate "; 
        if(!is_NULL($subquery)){
            $query = $query.$subquery;
        }
        
        return $this->db->query($query)->result();
    }


    //@author Sujit
	//@params None
	//@returns None
    //@Model to exchange list of documents of Candidate
    public function getDocuments($candidate_id){
    	$query = "SELECT * FROM tbl_candidate_image
    				WHERE candidate_id=$candidate_id";
    	return $this->db->query($query)->row();
    }


    //@author Sujit
	//@params candidate_id , data
	//@returns None
    //@Model to update list of documents of Candidate
    public function updateDocuments($candidate_id,$data){
    	return $this->db->where('candidate_id', $candidate_id)
                        ->set($data)
                        ->update('tbl_candidate_image');
    }


    //@author Sujit
	//@params None
	//@returns None
    //@View to load new interview form
    public function getCandidateList($status){
    	$current_status = strtolower($status);
        $query = "SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                    job_title, company_name, t1.current_status, $this->_table.agent_id
                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                    FROM $this->_table
                    	INNER JOIN tbl_job_candidate t1
                    		ON  t1.candidate_id = $this->_table.id
                        INNER JOIN tbl_job t2
                            ON t2.id = t1.job_id
                            INNER JOIN tbl_company t3
                                ON t3.id = t2.company_id
                                LEFT JOIN tbl_agent t4
                                    ON t4.id = $this->_table.agent_id
                    WHERE t1.current_status = '$status'"; 
        return $this->db->query($query)->result();
    }


     //@author Sujit
	//@params None
	//@returns None
    //@Model to generate candidate list and their respective interviews
    public function getCandidateJob($subquery=NULL){
    	$query = "SELECT $this->_table.id AS candidate_id, candidate_name, passport_no
    				FROM $this->_table
    					INNER JOIN tbl_job_candidate
    						ON tbl_job_candidate.candidate_id = $this->_table.id
    			";
    	 if(!is_NULL($subquery)){
            $query = $query.$subquery;
        }
        
    	return $this->db->query($query)->result();
    }


    //@author Sujit
    //@params None
    //@returns None
    //@Model to generate candidate list and their respective processing Works
    public function generateCandidateWork($candidate_id, $select){
        $query = "SELECT $select
                        FROM tbl_candidate
                        LEFT JOIN tbl_interview
                            WHERE ";


    }

    public function interviewResult($id,$type){
        $query=$this->db->query("select i.interview_date,i.result from tbl_interview i inner join tbl_candidate c on i.candidate_id=c.id where i.type='$type' AND c.id=$id")->row();
        return $query;
    }

    public function medicalResult($id){
        $query=$this->db->query("select  medical_date,result as medical_result from tbl_medical i
                                    inner join tbl_candidate c on i.candidate_id=c.id where c.id=$id")->row();
        return $query;
    }

    public function visaResult($id){
        $query=$this->db->query("select  i.* from tbl_visa i
                                    inner join tbl_candidate c on i.candidate_id=c.id where c.id=$id")->row();
        return $query;
    }

    public function ticketingResult($id){
        $query=$this->db->query("select  i.* from tbl_ticket i
                                    inner join tbl_candidate c on i.candidate_id=c.id where c.id=$id")->row();
        return $query;
    }

    public function otherProcessing($id){
        $query=$this->db->query("select m.medical_date,m.result as medical_result,v.received_date as visa_received_date,t.ticket_no as ticket_number from tbl_candidate c 
                                LEFT JOIN tbl_medical m on c.id=m.candidate_id 
                                LEFT JOIN  tbl_visa v on m.candidate_id =v.candidate_id
                                LEFT JOIN tbl_ticket t on v.candidate_id=t.candidate_id where c.id=$id");
       return $query->row();
    }

    public function approvalList($type){
        $query=$this->db->query("SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                                    job_title, company_name, t1.current_status, $this->_table.agent_id, t5.id as approval_id, t5.decision
                                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                                    FROM $this->_table
                                        INNER JOIN tbl_job_candidate t1
                                            ON  t1.candidate_id = $this->_table.id
                                        INNER JOIN tbl_job t2
                                            ON t2.id = t1.job_id
                                        INNER JOIN tbl_company t3
                                            ON t3.id = t2.company_id
                                        LEFT JOIN tbl_agent t4
                                            ON t4.id = $this->_table.agent_id
                                        INNER JOIN tbl_visa_ticketing_approval t5
                                            ON t5.candidate_id = $this->_table.id
                                    WHERE t5.decision IS NULL AND t5.type = '$type'");
        return $query->result();
    }


    public function approvedList($type){
        $query=$this->db->query("SELECT $this->_table.id AS candidate_id, candidate_name, passport_no, $this->_table.phone, $this->_table.mobile, 
                                    job_title, company_name, t1.current_status, $this->_table.agent_id, t5.id as approval_id, t5.decision
                                    ,t4.agent_name, t4.phone As agent_phone, t4.mobile AS agent_mob 
                                    FROM $this->_table
                                        INNER JOIN tbl_job_candidate t1
                                            ON  t1.candidate_id = $this->_table.id
                                        INNER JOIN tbl_job t2
                                            ON t2.id = t1.job_id
                                        INNER JOIN tbl_company t3
                                            ON t3.id = t2.company_id
                                        LEFT JOIN tbl_agent t4
                                            ON t4.id = $this->_table.agent_id
                                        INNER JOIN tbl_visa_ticketing_approval t5
                                            ON t5.candidate_id = $this->_table.id
                                    WHERE t5.decision IS NOT NULL AND t5.type = '$type'");
        return $query->result();
    }

}
/*END OF CLASS candidate_model.php*/
