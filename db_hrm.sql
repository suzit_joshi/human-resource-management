-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 19, 2014 at 01:30 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_hrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent`
--

CREATE TABLE IF NOT EXISTS `tbl_agent` (
  `agent_id` varchar(255) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `perm_address` varchar(255) NOT NULL,
  `temp_address` varchar(255) NOT NULL,
  `regd_date` date NOT NULL,
  `dob` date NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agent`
--

INSERT INTO `tbl_agent` (`agent_id`, `agent_name`, `email`, `mobile`, `phone`, `image`, `perm_address`, `temp_address`, `regd_date`, `dob`, `status`) VALUES
('age_1', 'Ram Shrestha', 'ram@hotmail.com', '1231231321', '123124124', 'abc.jpg', 'Dhalku', 'Dhalku', '2014-09-16', '0000-00-00', 0),
('age_2', 'Hari', 'hari@hotmail.com', '123123', '12313', '2010_mercedes_benz_e_class_coupe-wide.jpg', 'asfasdf', 'asfdasfd', '2014-09-17', '0000-00-00', 1),
('age_3', 'Shyam', 'shyam@hotmail.com', '3232434', '28932823', '4.jpg', 'kahtm', 'aslkdfj', '2014-10-15', '0000-00-00', 1),
('age_4', 'Test name', 'tes@gmail.com', '333393939', '9399393', '17.jpg', 'nepal', 'nasdnf', '2014-12-09', '2014-12-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applicant_processing_works`
--

CREATE TABLE IF NOT EXISTS `tbl_applicant_processing_works` (
  `work_id` varchar(255) NOT NULL,
  `applicant_id` varchar(255) NOT NULL,
  `processing_work_id` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_applicant_processing_works`
--

INSERT INTO `tbl_applicant_processing_works` (`work_id`, `applicant_id`, `processing_work_id`, `description`, `date`) VALUES
('app_1', 'can_2', 'pro_1', 'Interview with Mr.ABCD', '2014-10-01'),
('app_2', 'can_2', 'pro_2', 'Interview with Mr.ABCD eeeeeeeeeee', '2014-10-01'),
('app_3', 'can_1', 'pro_1', 'asfasdf', '2014-10-15'),
('app_4', 'can_1', 'pro_2', 'asaaa', '2014-10-15'),
('app_5', 'can_3', 'pro_2', 'Checked at Norvic', '2014-10-14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bank`
--

CREATE TABLE IF NOT EXISTS `tbl_bank` (
  `bank_id` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `balance` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bank`
--

INSERT INTO `tbl_bank` (`bank_id`, `bank_name`, `balance`, `created_date`) VALUES
('ban_1', 'Nabil Bank', '40000', '2014-11-07 09:45:22'),
('ban_2', 'Everest Bank', '33000', '2014-11-07 09:46:06'),
('ban_3', 'Nepal Investment Bank', '110000', '2014-11-07 09:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate`
--

CREATE TABLE IF NOT EXISTS `tbl_candidate` (
  `candidate_id` varchar(255) NOT NULL,
  `candidate_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` int(1) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `temp_address` varchar(255) NOT NULL,
  `perm_address` varchar(255) NOT NULL,
  `candidate_image` varchar(255) NOT NULL,
  `passport_no` varchar(255) NOT NULL,
  `issued_date` date NOT NULL,
  `issued_from` varchar(255) NOT NULL,
  `passport_image` varchar(255) NOT NULL,
  `agent_id` varchar(255) NOT NULL,
  `job_id` varchar(255) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `applied_date` date NOT NULL,
  `candidate_status` varchar(10) NOT NULL,
  `folder_name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `language` varchar(255) NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `qualification` mediumtext NOT NULL,
  `other_info` mediumtext NOT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_candidate`
--

INSERT INTO `tbl_candidate` (`candidate_id`, `candidate_name`, `date_of_birth`, `gender`, `father_name`, `mother_name`, `email_id`, `mobile`, `phone`, `temp_address`, `perm_address`, `candidate_image`, `passport_no`, `issued_date`, `issued_from`, `passport_image`, `agent_id`, `job_id`, `barcode`, `applied_date`, `candidate_status`, `folder_name`, `status`, `language`, `birth_place`, `height`, `weight`, `marital_status`, `religion`, `nationality`, `qualification`, `other_info`) VALUES
('can_1', 'Narayan Tripathi', '2000-10-08', 1, 'Tri narayan tripathi', 'Laxmi Tripathi', 'narayan@hotmail.com', '99999999', '09287328', 'Kathmandu', 'Pokhara', '2Kfd2gHh1.jpg', '2710455', '2014-10-02', 'Kathmandu', 'U0NaSRKL2.jpg', 'age_2', 'job_1', '', '2014-10-16', 'Processing', '99999999529808', 1, '', '', '', '', '', '', '', '', ''),
('can_2', 'Rajesh Hamal', '1995-10-11', 1, 'Sajesh Hamal', 'Mother Hamal', 'rajesh@gmail.com', '9746321', '2345678', 'Pokhara', 'KTM', '03ChsWm013.jpg', '555555', '2014-02-02', 'Dharan', 'wjRUnJ5o14.jpg', 'age_1', 'job_2', '', '2014-10-16', 'Hold', '9746321946288', 1, '', '', '', '', '', '', '', '', ''),
('can_3', 'Test Name2', '2014-10-23', 0, 'Test Father Name2', 'Test Mother Name2', 'test@gmail.com2', '2348238232222', '123412342222', 'test temporary address2', 'Test permanent address2', 'A4cVWYn518.jpg', '2223333', '2014-10-30', 'test issued place', 'xkOgvG6C17.jpg', 'age_3', 'job_4', '', '2014-10-28', 'Hold', '234823823823880', 1, '', '', '', '', '', '', '', '', ''),
('can_4', 'Shree Krishna Shrestha', '1996-12-03', 1, 'Krishna Shrestha', 'Krisha Shrestha', 'shri.krishna@gmail.com', '9999999', '9999999', 'ksjd', 'sadf', '0c9ZS4BgScreenshot_from_2014-09-17_11:49:14.png', '343333233', '2014-12-10', 'Kathmandu', 'hvuQhec01.jpg', '0', 'job_1', '', '2014-12-08', 'Hold', '9999999832772', 1, '', '', '', '', '', '', '', '', ''),
('can_5', 'Sudeep Pandeyy', '1999-12-08', 1, 'Sudeep Pandey Father6', 'Suddep Pandey Mother6', 'suddep@hotmail.com6', '89889896', '23444466', 'Balaju6', 'Balaju6', 'n7S7HZ9710805620_10205734925233446_8854340950818617584_n.jpg', '2324234', '2014-12-17', 'Kathmandu', 'nxHmsIvW10805620_10205734925233446_8854340950818617584_n.jpg', 'age_3', 'job_3', '', '2014-12-16', 'Hold', '8988989700550', 1, 'b:0;', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cash`
--

CREATE TABLE IF NOT EXISTS `tbl_cash` (
  `cash_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(2) NOT NULL,
  PRIMARY KEY (`cash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cash`
--

INSERT INTO `tbl_cash` (`cash_id`, `amount`, `updated_on`, `status`, `year`, `month`) VALUES
('cas_1', '10000', '2014-10-22 05:27:21', 1, 2014, 10),
('cas_2', '10000', '2014-11-19 10:10:02', 1, 2014, 11),
('cas_3', '266000', '2014-12-03 04:15:27', 1, 2014, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_expense`
--

CREATE TABLE IF NOT EXISTS `tbl_category_expense` (
  `category_id` varchar(255) NOT NULL,
  `expense_title` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category_expense`
--

INSERT INTO `tbl_category_expense` (`category_id`, `expense_title`, `created_date`, `status`, `type`) VALUES
('cat_1', 'Medical Expense', '2014-12-11 06:29:40', 1, 'Candidate'),
('cat_2', 'avsdv', '2014-12-11 06:32:09', 1, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_income`
--

CREATE TABLE IF NOT EXISTS `tbl_category_income` (
  `category_id` varchar(255) NOT NULL,
  `income_title` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category_income`
--

INSERT INTO `tbl_category_income` (`category_id`, `income_title`, `created_date`, `status`, `type`) VALUES
('cat_1', 'Medical Expenses', '2014-12-10 07:37:11', 1, 'Candidate'),
('cat_2', 'Job Percentage', '2014-12-11 06:12:15', 1, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE IF NOT EXISTS `tbl_company` (
  `company_id` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`company_id`, `company_name`, `address`, `email_id`, `website`, `description`, `country_id`, `status`) VALUES
('com_1', 'Moustha', 'Doha, Qatar', 'moasudf@hotmail.com', 'http://www.abc.com', '0asfsafd', 'cou_3', 1),
('com_2', 'Qatar Overseas company', 'Toha', 'qatar.overseas@hotmail.com', 'http://www.qatar.co', 'asdfsafasdfasdf', 'cou_3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE IF NOT EXISTS `tbl_country` (
  `country_id` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`country_id`, `country_name`, `status`) VALUES
('cou_1', 'Nepal', 1),
('cou_2', 'Saudi Arabia', 0),
('cou_3', 'Qatar', 1),
('cou_4', 'Malaysia', 1),
('cou_5', 'Dubai', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_expense_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_expense_transaction` (
  `expense_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `transaction_date` date NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` mediumtext NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `candidate_id` varchar(255) NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_expense_transaction`
--

INSERT INTO `tbl_expense_transaction` (`expense_id`, `amount`, `type`, `bank_id`, `transaction_date`, `created_date`, `description`, `cheque_no`, `category_id`, `candidate_id`) VALUES
('exp_2', '4000', 'Cheque', '', '2014-12-25', '2014-12-12 06:21:23', 'asD', '', 'cat_1', 'can_3'),
('exp_3', '7322', 'Cheque', 'ban_1', '2014-12-17', '2014-12-12 06:34:26', 'Payment Done<br>', '12346', 'cat_2', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_income_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_income_transaction` (
  `income_id` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `candidate_id` varchar(255) NOT NULL,
  `transaction_date` date NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bank_id` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `voucher_no` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`income_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

CREATE TABLE IF NOT EXISTS `tbl_job` (
  `job_id` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `vacancy_no` int(5) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `requirement` mediumtext NOT NULL,
  `facility` mediumtext NOT NULL,
  `job_type` varchar(100) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `published_date` date NOT NULL,
  `last_application_date` date NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`job_id`, `job_title`, `vacancy_no`, `company_id`, `requirement`, `facility`, `job_type`, `salary`, `published_date`, `last_application_date`, `status`) VALUES
('job_1', 'HR Manager', 2, 'com_2', 'sdfghujytgvb', 'xcfsxcb gfdcvbgfr<br>', 'full_time', '10000 Real', '2014-09-12', '2014-09-30', 1),
('job_2', 'asdfsadf', 22, 'com_1', 'asdf', 'asdf', 'full_time', '22222 real', '2014-09-12', '2014-09-25', 0),
('job_3', 'Receptionist', 1, 'com_1', 'Good Speaking english<br>', 'Free Fooding and lodging<br>', 'full_time', '10000', '2014-09-12', '2014-09-26', 1),
('job_4', 'Senior Manager', 3, 'com_1', 'reqRequirements<br>', 'Facilities<br>', 'part_time', '20000 Real', '2014-09-12', '2014-09-23', 1),
('job_5', 'Junior Manager', 3, 'com_1', 'asfdas23', 'asdf234r243<br>', 'part_time', '21213123', '2014-09-12', '2014-09-30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_labor_verification`
--

CREATE TABLE IF NOT EXISTS `tbl_labor_verification` (
  `labor_id` varchar(255) NOT NULL,
  `sticker_no` varchar(255) NOT NULL,
  `appliant_id` varchar(255) NOT NULL,
  `approved_date` date NOT NULL,
  `description` mediumtext NOT NULL,
  `validity_year` varchar(4) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`labor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_processing_work`
--

CREATE TABLE IF NOT EXISTS `tbl_processing_work` (
  `processing_work_id` varchar(255) NOT NULL,
  `processing_work_title` varchar(255) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`processing_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_processing_work`
--

INSERT INTO `tbl_processing_work` (`processing_work_id`, `processing_work_title`, `last_updated`) VALUES
('pro_1', 'Interview', '2014-10-21 05:20:43'),
('pro_2', 'Health Checkup', '2014-10-21 05:22:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reminder`
--

CREATE TABLE IF NOT EXISTS `tbl_reminder` (
  `reminder_id` varchar(255) NOT NULL,
  `notify_on` date NOT NULL,
  `reminder_date` date NOT NULL,
  `description` mediumtext NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`reminder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reminder`
--

INSERT INTO `tbl_reminder` (`reminder_id`, `notify_on`, `reminder_date`, `description`, `status`, `user_id`) VALUES
('rem_1', '2014-11-17', '2014-11-18', 'Reminder222222222222222222<br>', '1', 'use_2'),
('rem_2', '2014-11-17', '2014-11-19', 'Reminder', '1', 'use_3'),
('rem_3', '2014-11-30', '2014-12-01', 'abcd', '1', 'use_4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` varchar(255) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `role_name`, `status`, `last_updated`) VALUES
('rol_1', 'super admin', 1, '0000-00-00 00:00:00'),
('rol_2', 'Admin', 1, '0000-00-00 00:00:00'),
('rol_3', 'Finance', 1, '0000-00-00 00:00:00'),
('rol_4', 'General user', 1, '2014-11-21 05:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transfer_amount`
--

CREATE TABLE IF NOT EXISTS `tbl_transfer_amount` (
  `transfer_id` varchar(255) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `transfer_date` date NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transfer_type` varchar(255) NOT NULL,
  `income_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transfer_amount`
--

INSERT INTO `tbl_transfer_amount` (`transfer_id`, `bank_id`, `amount`, `description`, `transfer_date`, `created_date`, `transfer_type`, `income_id`) VALUES
('tra_1', 'ban_1', '100', 'Transfer to Nabil Bank<br>', '2014-11-18', '2014-11-19 05:06:38', 'Cash to Bank A/C', 'inc_12'),
('tra_2', 'ban_1', '100', 'Transfer to cash<br>', '2014-11-20', '2014-11-19 05:07:52', 'Bank A/C to Cash', 'inc_12'),
('tra_3', 'ban_1', '20000', 'Undeposited cheque - 2014-11-11 of amount 20,000<br>', '2014-11-20', '2014-11-19 06:37:51', 'Undeposited Cheques to Bank A/C', 'inc_12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_uploads_candidate`
--

CREATE TABLE IF NOT EXISTS `tbl_uploads_candidate` (
  `upload_id` varchar(255) NOT NULL,
  `candidate_id` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_uploads_candidate`
--

INSERT INTO `tbl_uploads_candidate` (`upload_id`, `candidate_id`, `file_name`) VALUES
('upl_1', 'can_2', 'AlbumArtSmall.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `email_id`, `password`, `role_id`, `status`) VALUES
('use_1', 'sunbi@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 'rol_1', 1),
('use_2', 'florid_admin@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 'rol_2', 1),
('use_3', 'florid_finance@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 'rol_3', 1),
('use_4', 'florid_general_user@gmail.com', '3fc2e17f35c212cd7c312be6f2ca8d14', 'rol_4', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
